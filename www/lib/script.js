(function(Z, Q, r) {
    "use strict";
    function F(b) {
        return function() {
            var a = arguments[0], c, a = "[" + (b ? b + ":" : "") + a + "] http://errors.angularjs.org/1.2.8/" + (b ? b + "/" : "") + a;
            for (c = 1; c < arguments.length; c++) a = a + (1 == c ? "?" : "&") + "p" + (c - 1) + "=" + encodeURIComponent("function" == typeof arguments[c] ? arguments[c].toString().replace(/ \{[\s\S]*$/, "") : "undefined" == typeof arguments[c] ? "undefined" : "string" != typeof arguments[c] ? JSON.stringify(arguments[c]) : arguments[c]);
            return Error(a);
        };
    }
    function rb(b) {
        if (null == b || Aa(b)) return !1;
        var a = b.length;
        return 1 === b.nodeType && a ? !0 : D(b) || K(b) || 0 === a || "number" === typeof a && 0 < a && a - 1 in b;
    }
    function q(b, a, c) {
        var d;
        if (b) if (L(b)) for (d in b) "prototype" == d || ("length" == d || "name" == d || b.hasOwnProperty && !b.hasOwnProperty(d)) || a.call(c, b[d], d); else if (b.forEach && b.forEach !== q) b.forEach(a, c); else if (rb(b)) for (d = 0; d < b.length; d++) a.call(c, b[d], d); else for (d in b) b.hasOwnProperty(d) && a.call(c, b[d], d);
        return b;
    }
    function Pb(b) {
        var a = [], c;
        for (c in b) b.hasOwnProperty(c) && a.push(c);
        return a.sort();
    }
    function Pc(b, a, c) {
        for (var d = Pb(b), e = 0; e < d.length; e++) a.call(c, b[d[e]], d[e]);
        return d;
    }
    function Qb(b) {
        return function(a, c) {
            b(c, a);
        };
    }
    function Za() {
        for (var b = ka.length, a; b; ) {
            b--;
            a = ka[b].charCodeAt(0);
            if (57 == a) return ka[b] = "A", ka.join("");
            if (90 == a) ka[b] = "0"; else return ka[b] = String.fromCharCode(a + 1), ka.join("");
        }
        ka.unshift("0");
        return ka.join("");
    }
    function Rb(b, a) {
        a ? b.$$hashKey = a : delete b.$$hashKey;
    }
    function t(b) {
        var a = b.$$hashKey;
        q(arguments, function(a) {
            a !== b && q(a, function(a, c) {
                b[c] = a;
            });
        });
        Rb(b, a);
        return b;
    }
    function S(b) {
        return parseInt(b, 10);
    }
    function Sb(b, a) {
        return t(new (t(function() {}, {
            prototype: b
        }))(), a);
    }
    function w() {}
    function Ba(b) {
        return b;
    }
    function $(b) {
        return function() {
            return b;
        };
    }
    function z(b) {
        return "undefined" === typeof b;
    }
    function B(b) {
        return "undefined" !== typeof b;
    }
    function X(b) {
        return null != b && "object" === typeof b;
    }
    function D(b) {
        return "string" === typeof b;
    }
    function sb(b) {
        return "number" === typeof b;
    }
    function La(b) {
        return "[object Date]" === $a.call(b);
    }
    function K(b) {
        return "[object Array]" === $a.call(b);
    }
    function L(b) {
        return "function" === typeof b;
    }
    function ab(b) {
        return "[object RegExp]" === $a.call(b);
    }
    function Aa(b) {
        return b && b.document && b.location && b.alert && b.setInterval;
    }
    function Qc(b) {
        return !(!b || !(b.nodeName || b.on && b.find));
    }
    function Rc(b, a, c) {
        var d = [];
        q(b, function(b, g, f) {
            d.push(a.call(c, b, g, f));
        });
        return d;
    }
    function bb(b, a) {
        if (b.indexOf) return b.indexOf(a);
        for (var c = 0; c < b.length; c++) if (a === b[c]) return c;
        return -1;
    }
    function Ma(b, a) {
        var c = bb(b, a);
        0 <= c && b.splice(c, 1);
        return a;
    }
    function fa(b, a) {
        if (Aa(b) || b && b.$evalAsync && b.$watch) throw Na("cpws");
        if (a) {
            if (b === a) throw Na("cpi");
            if (K(b)) for (var c = a.length = 0; c < b.length; c++) a.push(fa(b[c])); else {
                c = a.$$hashKey;
                q(a, function(b, c) {
                    delete a[c];
                });
                for (var d in b) a[d] = fa(b[d]);
                Rb(a, c);
            }
        } else (a = b) && (K(b) ? a = fa(b, []) : La(b) ? a = new Date(b.getTime()) : ab(b) ? a = RegExp(b.source) : X(b) && (a = fa(b, {})));
        return a;
    }
    function Tb(b, a) {
        a = a || {};
        for (var c in b) b.hasOwnProperty(c) && ("$" !== c.charAt(0) && "$" !== c.charAt(1)) && (a[c] = b[c]);
        return a;
    }
    function ua(b, a) {
        if (b === a) return !0;
        if (null === b || null === a) return !1;
        if (b !== b && a !== a) return !0;
        var c = typeof b, d;
        if (c == typeof a && "object" == c) if (K(b)) {
            if (!K(a)) return !1;
            if ((c = b.length) == a.length) {
                for (d = 0; d < c; d++) if (!ua(b[d], a[d])) return !1;
                return !0;
            }
        } else {
            if (La(b)) return La(a) && b.getTime() == a.getTime();
            if (ab(b) && ab(a)) return b.toString() == a.toString();
            if (b && b.$evalAsync && b.$watch || a && a.$evalAsync && a.$watch || Aa(b) || Aa(a) || K(a)) return !1;
            c = {};
            for (d in b) if ("$" !== d.charAt(0) && !L(b[d])) {
                if (!ua(b[d], a[d])) return !1;
                c[d] = !0;
            }
            for (d in a) if (!c.hasOwnProperty(d) && "$" !== d.charAt(0) && a[d] !== r && !L(a[d])) return !1;
            return !0;
        }
        return !1;
    }
    function Ub() {
        return Q.securityPolicy && Q.securityPolicy.isActive || Q.querySelector && !(!Q.querySelector("[ng-csp]") && !Q.querySelector("[data-ng-csp]"));
    }
    function cb(b, a) {
        var c = 2 < arguments.length ? va.call(arguments, 2) : [];
        return !L(a) || a instanceof RegExp ? a : c.length ? function() {
            return arguments.length ? a.apply(b, c.concat(va.call(arguments, 0))) : a.apply(b, c);
        } : function() {
            return arguments.length ? a.apply(b, arguments) : a.call(b);
        };
    }
    function Sc(b, a) {
        var c = a;
        "string" === typeof b && "$" === b.charAt(0) ? c = r : Aa(a) ? c = "$WINDOW" : a && Q === a ? c = "$DOCUMENT" : a && (a.$evalAsync && a.$watch) && (c = "$SCOPE");
        return c;
    }
    function qa(b, a) {
        return "undefined" === typeof b ? r : JSON.stringify(b, Sc, a ? "  " : null);
    }
    function Vb(b) {
        return D(b) ? JSON.parse(b) : b;
    }
    function Oa(b) {
        "function" === typeof b ? b = !0 : b && 0 !== b.length ? (b = x("" + b), b = !("f" == b || "0" == b || "false" == b || "no" == b || "n" == b || "[]" == b)) : b = !1;
        return b;
    }
    function ga(b) {
        b = A(b).clone();
        try {
            b.empty();
        } catch (a) {}
        var c = A("<div>").append(b).html();
        try {
            return 3 === b[0].nodeType ? x(c) : c.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/, function(a, b) {
                return "<" + x(b);
            });
        } catch (d) {
            return x(c);
        }
    }
    function Wb(b) {
        try {
            return decodeURIComponent(b);
        } catch (a) {}
    }
    function Xb(b) {
        var a = {}, c, d;
        q((b || "").split("&"), function(b) {
            b && (c = b.split("="), d = Wb(c[0]), B(d) && (b = B(c[1]) ? Wb(c[1]) : !0, a[d] ? K(a[d]) ? a[d].push(b) : a[d] = [ a[d], b ] : a[d] = b));
        });
        return a;
    }
    function Yb(b) {
        var a = [];
        q(b, function(b, d) {
            K(b) ? q(b, function(b) {
                a.push(wa(d, !0) + (!0 === b ? "" : "=" + wa(b, !0)));
            }) : a.push(wa(d, !0) + (!0 === b ? "" : "=" + wa(b, !0)));
        });
        return a.length ? a.join("&") : "";
    }
    function tb(b) {
        return wa(b, !0).replace(/%26/gi, "&").replace(/%3D/gi, "=").replace(/%2B/gi, "+");
    }
    function wa(b, a) {
        return encodeURIComponent(b).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, a ? "%20" : "+");
    }
    function Tc(b, a) {
        function c(a) {
            a && d.push(a);
        }
        var d = [ b ], e, g, f = [ "ng:app", "ng-app", "x-ng-app", "data-ng-app" ], h = /\sng[:\-]app(:\s*([\w\d_]+);?)?\s/;
        q(f, function(a) {
            f[a] = !0;
            c(Q.getElementById(a));
            a = a.replace(":", "\\:");
            b.querySelectorAll && (q(b.querySelectorAll("." + a), c), q(b.querySelectorAll("." + a + "\\:"), c), 
            q(b.querySelectorAll("[" + a + "]"), c));
        });
        q(d, function(a) {
            if (!e) {
                var b = h.exec(" " + a.className + " ");
                b ? (e = a, g = (b[2] || "").replace(/\s+/g, ",")) : q(a.attributes, function(b) {
                    !e && f[b.name] && (e = a, g = b.value);
                });
            }
        });
        e && a(e, g ? [ g ] : []);
    }
    function Zb(b, a) {
        var c = function() {
            b = A(b);
            if (b.injector()) {
                var c = b[0] === Q ? "document" : ga(b);
                throw Na("btstrpd", c);
            }
            a = a || [];
            a.unshift([ "$provide", function(a) {
                a.value("$rootElement", b);
            } ]);
            a.unshift("ng");
            c = $b(a);
            c.invoke([ "$rootScope", "$rootElement", "$compile", "$injector", "$animate", function(a, b, c, d, e) {
                a.$apply(function() {
                    b.data("$injector", d);
                    c(b)(a);
                });
            } ]);
            return c;
        }, d = /^NG_DEFER_BOOTSTRAP!/;
        if (Z && !d.test(Z.name)) return c();
        Z.name = Z.name.replace(d, "");
        Ca.resumeBootstrap = function(b) {
            q(b, function(b) {
                a.push(b);
            });
            c();
        };
    }
    function db(b, a) {
        a = a || "_";
        return b.replace(Uc, function(b, d) {
            return (d ? a : "") + b.toLowerCase();
        });
    }
    function ub(b, a, c) {
        if (!b) throw Na("areq", a || "?", c || "required");
        return b;
    }
    function Pa(b, a, c) {
        c && K(b) && (b = b[b.length - 1]);
        ub(L(b), a, "not a function, got " + (b && "object" == typeof b ? b.constructor.name || "Object" : typeof b));
        return b;
    }
    function xa(b, a) {
        if ("hasOwnProperty" === b) throw Na("badname", a);
    }
    function vb(b, a, c) {
        if (!a) return b;
        a = a.split(".");
        for (var d, e = b, g = a.length, f = 0; f < g; f++) d = a[f], b && (b = (e = b)[d]);
        return !c && L(b) ? cb(e, b) : b;
    }
    function wb(b) {
        var a = b[0];
        b = b[b.length - 1];
        if (a === b) return A(a);
        var c = [ a ];
        do {
            a = a.nextSibling;
            if (!a) break;
            c.push(a);
        } while (a !== b);
        return A(c);
    }
    function Vc(b) {
        var a = F("$injector"), c = F("ng");
        b = b.angular || (b.angular = {});
        b.$$minErr = b.$$minErr || F;
        return b.module || (b.module = function() {
            var b = {};
            return function(e, g, f) {
                if ("hasOwnProperty" === e) throw c("badname", "module");
                g && b.hasOwnProperty(e) && (b[e] = null);
                return b[e] || (b[e] = function() {
                    function b(a, d, e) {
                        return function() {
                            c[e || "push"]([ a, d, arguments ]);
                            return n;
                        };
                    }
                    if (!g) throw a("nomod", e);
                    var c = [], d = [], l = b("$injector", "invoke"), n = {
                        _invokeQueue: c,
                        _runBlocks: d,
                        requires: g,
                        name: e,
                        provider: b("$provide", "provider"),
                        factory: b("$provide", "factory"),
                        service: b("$provide", "service"),
                        value: b("$provide", "value"),
                        constant: b("$provide", "constant", "unshift"),
                        animation: b("$animateProvider", "register"),
                        filter: b("$filterProvider", "register"),
                        controller: b("$controllerProvider", "register"),
                        directive: b("$compileProvider", "directive"),
                        config: l,
                        run: function(a) {
                            d.push(a);
                            return this;
                        }
                    };
                    f && l(f);
                    return n;
                }());
            };
        }());
    }
    function Qa(b) {
        return b.replace(Wc, function(a, b, d, e) {
            return e ? d.toUpperCase() : d;
        }).replace(Xc, "Moz$1");
    }
    function xb(b, a, c, d) {
        function e(b) {
            var e = c && b ? [ this.filter(b) ] : [ this ], m = a, k, l, n, p, s, C;
            if (!d || null != b) for (;e.length; ) for (k = e.shift(), l = 0, n = k.length; l < n; l++) for (p = A(k[l]), 
            m ? p.triggerHandler("$destroy") : m = !m, s = 0, p = (C = p.children()).length; s < p; s++) e.push(Da(C[s]));
            return g.apply(this, arguments);
        }
        var g = Da.fn[b], g = g.$original || g;
        e.$original = g;
        Da.fn[b] = e;
    }
    function O(b) {
        if (b instanceof O) return b;
        if (!(this instanceof O)) {
            if (D(b) && "<" != b.charAt(0)) throw yb("nosel");
            return new O(b);
        }
        if (D(b)) {
            var a = Q.createElement("div");
            a.innerHTML = "<div>&#160;</div>" + b;
            a.removeChild(a.firstChild);
            zb(this, a.childNodes);
            A(Q.createDocumentFragment()).append(this);
        } else zb(this, b);
    }
    function Ab(b) {
        return b.cloneNode(!0);
    }
    function Ea(b) {
        ac(b);
        var a = 0;
        for (b = b.childNodes || []; a < b.length; a++) Ea(b[a]);
    }
    function bc(b, a, c, d) {
        if (B(d)) throw yb("offargs");
        var e = la(b, "events");
        la(b, "handle") && (z(a) ? q(e, function(a, c) {
            Bb(b, c, a);
            delete e[c];
        }) : q(a.split(" "), function(a) {
            z(c) ? (Bb(b, a, e[a]), delete e[a]) : Ma(e[a] || [], c);
        }));
    }
    function ac(b, a) {
        var c = b[eb], d = Ra[c];
        d && (a ? delete Ra[c].data[a] : (d.handle && (d.events.$destroy && d.handle({}, "$destroy"), 
        bc(b)), delete Ra[c], b[eb] = r));
    }
    function la(b, a, c) {
        var d = b[eb], d = Ra[d || -1];
        if (B(c)) d || (b[eb] = d = ++Yc, d = Ra[d] = {}), d[a] = c; else return d && d[a];
    }
    function cc(b, a, c) {
        var d = la(b, "data"), e = B(c), g = !e && B(a), f = g && !X(a);
        d || f || la(b, "data", d = {});
        if (e) d[a] = c; else if (g) {
            if (f) return d && d[a];
            t(d, a);
        } else return d;
    }
    function Cb(b, a) {
        return b.getAttribute ? -1 < (" " + (b.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ").indexOf(" " + a + " ") : !1;
    }
    function Db(b, a) {
        a && b.setAttribute && q(a.split(" "), function(a) {
            b.setAttribute("class", aa((" " + (b.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ").replace(" " + aa(a) + " ", " ")));
        });
    }
    function Eb(b, a) {
        if (a && b.setAttribute) {
            var c = (" " + (b.getAttribute("class") || "") + " ").replace(/[\n\t]/g, " ");
            q(a.split(" "), function(a) {
                a = aa(a);
                -1 === c.indexOf(" " + a + " ") && (c += a + " ");
            });
            b.setAttribute("class", aa(c));
        }
    }
    function zb(b, a) {
        if (a) {
            a = a.nodeName || !B(a.length) || Aa(a) ? [ a ] : a;
            for (var c = 0; c < a.length; c++) b.push(a[c]);
        }
    }
    function dc(b, a) {
        return fb(b, "$" + (a || "ngController") + "Controller");
    }
    function fb(b, a, c) {
        b = A(b);
        9 == b[0].nodeType && (b = b.find("html"));
        for (a = K(a) ? a : [ a ]; b.length; ) {
            for (var d = 0, e = a.length; d < e; d++) if ((c = b.data(a[d])) !== r) return c;
            b = b.parent();
        }
    }
    function ec(b) {
        for (var a = 0, c = b.childNodes; a < c.length; a++) Ea(c[a]);
        for (;b.firstChild; ) b.removeChild(b.firstChild);
    }
    function fc(b, a) {
        var c = gb[a.toLowerCase()];
        return c && gc[b.nodeName] && c;
    }
    function Zc(b, a) {
        var c = function(c, e) {
            c.preventDefault || (c.preventDefault = function() {
                c.returnValue = !1;
            });
            c.stopPropagation || (c.stopPropagation = function() {
                c.cancelBubble = !0;
            });
            c.target || (c.target = c.srcElement || Q);
            if (z(c.defaultPrevented)) {
                var g = c.preventDefault;
                c.preventDefault = function() {
                    c.defaultPrevented = !0;
                    g.call(c);
                };
                c.defaultPrevented = !1;
            }
            c.isDefaultPrevented = function() {
                return c.defaultPrevented || !1 === c.returnValue;
            };
            var f = Tb(a[e || c.type] || []);
            q(f, function(a) {
                a.call(b, c);
            });
            8 >= M ? (c.preventDefault = null, c.stopPropagation = null, c.isDefaultPrevented = null) : (delete c.preventDefault, 
            delete c.stopPropagation, delete c.isDefaultPrevented);
        };
        c.elem = b;
        return c;
    }
    function Fa(b) {
        var a = typeof b, c;
        "object" == a && null !== b ? "function" == typeof (c = b.$$hashKey) ? c = b.$$hashKey() : c === r && (c = b.$$hashKey = Za()) : c = b;
        return a + ":" + c;
    }
    function Sa(b) {
        q(b, this.put, this);
    }
    function hc(b) {
        var a, c;
        "function" == typeof b ? (a = b.$inject) || (a = [], b.length && (c = b.toString().replace($c, ""), 
        c = c.match(ad), q(c[1].split(bd), function(b) {
            b.replace(cd, function(b, c, d) {
                a.push(d);
            });
        })), b.$inject = a) : K(b) ? (c = b.length - 1, Pa(b[c], "fn"), a = b.slice(0, c)) : Pa(b, "fn", !0);
        return a;
    }
    function $b(b) {
        function a(a) {
            return function(b, c) {
                if (X(b)) q(b, Qb(a)); else return a(b, c);
            };
        }
        function c(a, b) {
            xa(a, "service");
            if (L(b) || K(b)) b = n.instantiate(b);
            if (!b.$get) throw Ta("pget", a);
            return l[a + h] = b;
        }
        function d(a, b) {
            return c(a, {
                $get: b
            });
        }
        function e(a) {
            var b = [], c, d, g, h;
            q(a, function(a) {
                if (!k.get(a)) {
                    k.put(a, !0);
                    try {
                        if (D(a)) for (c = Ua(a), b = b.concat(e(c.requires)).concat(c._runBlocks), d = c._invokeQueue, 
                        g = 0, h = d.length; g < h; g++) {
                            var f = d[g], m = n.get(f[0]);
                            m[f[1]].apply(m, f[2]);
                        } else L(a) ? b.push(n.invoke(a)) : K(a) ? b.push(n.invoke(a)) : Pa(a, "module");
                    } catch (s) {
                        throw K(a) && (a = a[a.length - 1]), s.message && (s.stack && -1 == s.stack.indexOf(s.message)) && (s = s.message + "\n" + s.stack), 
                        Ta("modulerr", a, s.stack || s.message || s);
                    }
                }
            });
            return b;
        }
        function g(a, b) {
            function c(d) {
                if (a.hasOwnProperty(d)) {
                    if (a[d] === f) throw Ta("cdep", m.join(" <- "));
                    return a[d];
                }
                try {
                    return m.unshift(d), a[d] = f, a[d] = b(d);
                } catch (e) {
                    throw a[d] === f && delete a[d], e;
                } finally {
                    m.shift();
                }
            }
            function d(a, b, e) {
                var g = [], h = hc(a), f, k, m;
                k = 0;
                for (f = h.length; k < f; k++) {
                    m = h[k];
                    if ("string" !== typeof m) throw Ta("itkn", m);
                    g.push(e && e.hasOwnProperty(m) ? e[m] : c(m));
                }
                a.$inject || (a = a[f]);
                return a.apply(b, g);
            }
            return {
                invoke: d,
                instantiate: function(a, b) {
                    var c = function() {}, e;
                    c.prototype = (K(a) ? a[a.length - 1] : a).prototype;
                    c = new c();
                    e = d(a, c, b);
                    return X(e) || L(e) ? e : c;
                },
                get: c,
                annotate: hc,
                has: function(b) {
                    return l.hasOwnProperty(b + h) || a.hasOwnProperty(b);
                }
            };
        }
        var f = {}, h = "Provider", m = [], k = new Sa(), l = {
            $provide: {
                provider: a(c),
                factory: a(d),
                service: a(function(a, b) {
                    return d(a, [ "$injector", function(a) {
                        return a.instantiate(b);
                    } ]);
                }),
                value: a(function(a, b) {
                    return d(a, $(b));
                }),
                constant: a(function(a, b) {
                    xa(a, "constant");
                    l[a] = b;
                    p[a] = b;
                }),
                decorator: function(a, b) {
                    var c = n.get(a + h), d = c.$get;
                    c.$get = function() {
                        var a = s.invoke(d, c);
                        return s.invoke(b, null, {
                            $delegate: a
                        });
                    };
                }
            }
        }, n = l.$injector = g(l, function() {
            throw Ta("unpr", m.join(" <- "));
        }), p = {}, s = p.$injector = g(p, function(a) {
            a = n.get(a + h);
            return s.invoke(a.$get, a);
        });
        q(e(b), function(a) {
            s.invoke(a || w);
        });
        return s;
    }
    function dd() {
        var b = !0;
        this.disableAutoScrolling = function() {
            b = !1;
        };
        this.$get = [ "$window", "$location", "$rootScope", function(a, c, d) {
            function e(a) {
                var b = null;
                q(a, function(a) {
                    b || "a" !== x(a.nodeName) || (b = a);
                });
                return b;
            }
            function g() {
                var b = c.hash(), d;
                b ? (d = f.getElementById(b)) ? d.scrollIntoView() : (d = e(f.getElementsByName(b))) ? d.scrollIntoView() : "top" === b && a.scrollTo(0, 0) : a.scrollTo(0, 0);
            }
            var f = a.document;
            b && d.$watch(function() {
                return c.hash();
            }, function() {
                d.$evalAsync(g);
            });
            return g;
        } ];
    }
    function ed(b, a, c, d) {
        function e(a) {
            try {
                a.apply(null, va.call(arguments, 1));
            } finally {
                if (C--, 0 === C) for (;y.length; ) try {
                    y.pop()();
                } catch (b) {
                    c.error(b);
                }
            }
        }
        function g(a, b) {
            (function T() {
                q(E, function(a) {
                    a();
                });
                u = b(T, a);
            })();
        }
        function f() {
            v = null;
            R != h.url() && (R = h.url(), q(ha, function(a) {
                a(h.url());
            }));
        }
        var h = this, m = a[0], k = b.location, l = b.history, n = b.setTimeout, p = b.clearTimeout, s = {};
        h.isMock = !1;
        var C = 0, y = [];
        h.$$completeOutstandingRequest = e;
        h.$$incOutstandingRequestCount = function() {
            C++;
        };
        h.notifyWhenNoOutstandingRequests = function(a) {
            q(E, function(a) {
                a();
            });
            0 === C ? a() : y.push(a);
        };
        var E = [], u;
        h.addPollFn = function(a) {
            z(u) && g(100, n);
            E.push(a);
            return a;
        };
        var R = k.href, H = a.find("base"), v = null;
        h.url = function(a, c) {
            k !== b.location && (k = b.location);
            l !== b.history && (l = b.history);
            if (a) {
                if (R != a) return R = a, d.history ? c ? l.replaceState(null, "", a) : (l.pushState(null, "", a), 
                H.attr("href", H.attr("href"))) : (v = a, c ? k.replace(a) : k.href = a), h;
            } else return v || k.href.replace(/%27/g, "'");
        };
        var ha = [], N = !1;
        h.onUrlChange = function(a) {
            if (!N) {
                if (d.history) A(b).on("popstate", f);
                if (d.hashchange) A(b).on("hashchange", f); else h.addPollFn(f);
                N = !0;
            }
            ha.push(a);
            return a;
        };
        h.baseHref = function() {
            var a = H.attr("href");
            return a ? a.replace(/^(https?\:)?\/\/[^\/]*/, "") : "";
        };
        var V = {}, J = "", ba = h.baseHref();
        h.cookies = function(a, b) {
            var d, e, g, h;
            if (a) b === r ? m.cookie = escape(a) + "=;path=" + ba + ";expires=Thu, 01 Jan 1970 00:00:00 GMT" : D(b) && (d = (m.cookie = escape(a) + "=" + escape(b) + ";path=" + ba).length + 1, 
            4096 < d && c.warn("Cookie '" + a + "' possibly not set or overflowed because it was too large (" + d + " > 4096 bytes)!")); else {
                if (m.cookie !== J) for (J = m.cookie, d = J.split("; "), V = {}, g = 0; g < d.length; g++) e = d[g], 
                h = e.indexOf("="), 0 < h && (a = unescape(e.substring(0, h)), V[a] === r && (V[a] = unescape(e.substring(h + 1))));
                return V;
            }
        };
        h.defer = function(a, b) {
            var c;
            C++;
            c = n(function() {
                delete s[c];
                e(a);
            }, b || 0);
            s[c] = !0;
            return c;
        };
        h.defer.cancel = function(a) {
            return s[a] ? (delete s[a], p(a), e(w), !0) : !1;
        };
    }
    function fd() {
        this.$get = [ "$window", "$log", "$sniffer", "$document", function(b, a, c, d) {
            return new ed(b, d, a, c);
        } ];
    }
    function gd() {
        this.$get = function() {
            function b(b, d) {
                function e(a) {
                    a != n && (p ? p == a && (p = a.n) : p = a, g(a.n, a.p), g(a, n), n = a, n.n = null);
                }
                function g(a, b) {
                    a != b && (a && (a.p = b), b && (b.n = a));
                }
                if (b in a) throw F("$cacheFactory")("iid", b);
                var f = 0, h = t({}, d, {
                    id: b
                }), m = {}, k = d && d.capacity || Number.MAX_VALUE, l = {}, n = null, p = null;
                return a[b] = {
                    put: function(a, b) {
                        var c = l[a] || (l[a] = {
                            key: a
                        });
                        e(c);
                        if (!z(b)) return a in m || f++, m[a] = b, f > k && this.remove(p.key), b;
                    },
                    get: function(a) {
                        var b = l[a];
                        if (b) return e(b), m[a];
                    },
                    remove: function(a) {
                        var b = l[a];
                        b && (b == n && (n = b.p), b == p && (p = b.n), g(b.n, b.p), delete l[a], delete m[a], 
                        f--);
                    },
                    removeAll: function() {
                        m = {};
                        f = 0;
                        l = {};
                        n = p = null;
                    },
                    destroy: function() {
                        l = h = m = null;
                        delete a[b];
                    },
                    info: function() {
                        return t({}, h, {
                            size: f
                        });
                    }
                };
            }
            var a = {};
            b.info = function() {
                var b = {};
                q(a, function(a, e) {
                    b[e] = a.info();
                });
                return b;
            };
            b.get = function(b) {
                return a[b];
            };
            return b;
        };
    }
    function hd() {
        this.$get = [ "$cacheFactory", function(b) {
            return b("templates");
        } ];
    }
    function jc(b, a) {
        var c = {}, d = "Directive", e = /^\s*directive\:\s*([\d\w\-_]+)\s+(.*)$/, g = /(([\d\w\-_]+)(?:\:([^;]+))?;?)/, f = /^(on[a-z]+|formaction)$/;
        this.directive = function m(a, e) {
            xa(a, "directive");
            D(a) ? (ub(e, "directiveFactory"), c.hasOwnProperty(a) || (c[a] = [], b.factory(a + d, [ "$injector", "$exceptionHandler", function(b, d) {
                var e = [];
                q(c[a], function(c, g) {
                    try {
                        var f = b.invoke(c);
                        L(f) ? f = {
                            compile: $(f)
                        } : !f.compile && f.link && (f.compile = $(f.link));
                        f.priority = f.priority || 0;
                        f.index = g;
                        f.name = f.name || a;
                        f.require = f.require || f.controller && f.name;
                        f.restrict = f.restrict || "A";
                        e.push(f);
                    } catch (m) {
                        d(m);
                    }
                });
                return e;
            } ])), c[a].push(e)) : q(a, Qb(m));
            return this;
        };
        this.aHrefSanitizationWhitelist = function(b) {
            return B(b) ? (a.aHrefSanitizationWhitelist(b), this) : a.aHrefSanitizationWhitelist();
        };
        this.imgSrcSanitizationWhitelist = function(b) {
            return B(b) ? (a.imgSrcSanitizationWhitelist(b), this) : a.imgSrcSanitizationWhitelist();
        };
        this.$get = [ "$injector", "$interpolate", "$exceptionHandler", "$http", "$templateCache", "$parse", "$controller", "$rootScope", "$document", "$sce", "$animate", "$$sanitizeUri", function(a, b, l, n, p, s, C, y, E, u, R, H) {
            function v(a, b, c, d, e) {
                a instanceof A || (a = A(a));
                q(a, function(b, c) {
                    3 == b.nodeType && b.nodeValue.match(/\S+/) && (a[c] = A(b).wrap("<span></span>").parent()[0]);
                });
                var g = N(a, b, a, c, d, e);
                ha(a, "ng-scope");
                return function(b, c, d) {
                    ub(b, "scope");
                    var e = c ? Ga.clone.call(a) : a;
                    q(d, function(a, b) {
                        e.data("$" + b + "Controller", a);
                    });
                    d = 0;
                    for (var f = e.length; d < f; d++) {
                        var m = e[d].nodeType;
                        1 !== m && 9 !== m || e.eq(d).data("$scope", b);
                    }
                    c && c(e, b);
                    g && g(b, e, e);
                    return e;
                };
            }
            function ha(a, b) {
                try {
                    a.addClass(b);
                } catch (c) {}
            }
            function N(a, b, c, d, e, g) {
                function f(a, c, d, e) {
                    var g, k, s, l, n, p, I;
                    g = c.length;
                    var C = Array(g);
                    for (n = 0; n < g; n++) C[n] = c[n];
                    I = n = 0;
                    for (p = m.length; n < p; I++) k = C[I], c = m[n++], g = m[n++], s = A(k), c ? (c.scope ? (l = a.$new(), 
                    s.data("$scope", l)) : l = a, (s = c.transclude) || !e && b ? c(g, l, k, d, V(a, s || b)) : c(g, l, k, d, e)) : g && g(a, k.childNodes, r, e);
                }
                for (var m = [], k, s, l, n, p = 0; p < a.length; p++) k = new Fb(), s = J(a[p], [], k, 0 === p ? d : r, e), 
                (g = s.length ? ia(s, a[p], k, b, c, null, [], [], g) : null) && g.scope && ha(A(a[p]), "ng-scope"), 
                k = g && g.terminal || !(l = a[p].childNodes) || !l.length ? null : N(l, g ? g.transclude : b), 
                m.push(g, k), n = n || g || k, g = null;
                return n ? f : null;
            }
            function V(a, b) {
                return function(c, d, e) {
                    var g = !1;
                    c || (c = a.$new(), g = c.$$transcluded = !0);
                    d = b(c, d, e);
                    if (g) d.on("$destroy", cb(c, c.$destroy));
                    return d;
                };
            }
            function J(a, b, c, d, f) {
                var k = c.$attr, m;
                switch (a.nodeType) {
                  case 1:
                    T(b, ma(Ha(a).toLowerCase()), "E", d, f);
                    var s, l, n;
                    m = a.attributes;
                    for (var p = 0, C = m && m.length; p < C; p++) {
                        var y = !1, R = !1;
                        s = m[p];
                        if (!M || 8 <= M || s.specified) {
                            l = s.name;
                            n = ma(l);
                            W.test(n) && (l = db(n.substr(6), "-"));
                            var v = n.replace(/(Start|End)$/, "");
                            n === v + "Start" && (y = l, R = l.substr(0, l.length - 5) + "end", l = l.substr(0, l.length - 6));
                            n = ma(l.toLowerCase());
                            k[n] = l;
                            c[n] = s = aa(s.value);
                            fc(a, n) && (c[n] = !0);
                            S(a, b, s, n);
                            T(b, n, "A", d, f, y, R);
                        }
                    }
                    a = a.className;
                    if (D(a) && "" !== a) for (;m = g.exec(a); ) n = ma(m[2]), T(b, n, "C", d, f) && (c[n] = aa(m[3])), 
                    a = a.substr(m.index + m[0].length);
                    break;

                  case 3:
                    F(b, a.nodeValue);
                    break;

                  case 8:
                    try {
                        if (m = e.exec(a.nodeValue)) n = ma(m[1]), T(b, n, "M", d, f) && (c[n] = aa(m[2]));
                    } catch (E) {}
                }
                b.sort(z);
                return b;
            }
            function ba(a, b, c) {
                var d = [], e = 0;
                if (b && a.hasAttribute && a.hasAttribute(b)) {
                    do {
                        if (!a) throw ja("uterdir", b, c);
                        1 == a.nodeType && (a.hasAttribute(b) && e++, a.hasAttribute(c) && e--);
                        d.push(a);
                        a = a.nextSibling;
                    } while (0 < e);
                } else d.push(a);
                return A(d);
            }
            function P(a, b, c) {
                return function(d, e, g, f, m) {
                    e = ba(e[0], b, c);
                    return a(d, e, g, f, m);
                };
            }
            function ia(a, c, d, e, g, f, m, n, p) {
                function y(a, b, c, d) {
                    if (a) {
                        c && (a = P(a, c, d));
                        a.require = G.require;
                        if (H === G || G.$$isolateScope) a = kc(a, {
                            isolateScope: !0
                        });
                        m.push(a);
                    }
                    if (b) {
                        c && (b = P(b, c, d));
                        b.require = G.require;
                        if (H === G || G.$$isolateScope) b = kc(b, {
                            isolateScope: !0
                        });
                        n.push(b);
                    }
                }
                function R(a, b, c) {
                    var d, e = "data", g = !1;
                    if (D(a)) {
                        for (;"^" == (d = a.charAt(0)) || "?" == d; ) a = a.substr(1), "^" == d && (e = "inheritedData"), 
                        g = g || "?" == d;
                        d = null;
                        c && "data" === e && (d = c[a]);
                        d = d || b[e]("$" + a + "Controller");
                        if (!d && !g) throw ja("ctreq", a, ca);
                    } else K(a) && (d = [], q(a, function(a) {
                        d.push(R(a, b, c));
                    }));
                    return d;
                }
                function E(a, e, g, f, p) {
                    function y(a, b) {
                        var c;
                        2 > arguments.length && (b = a, a = r);
                        z && (c = ba);
                        return p(a, b, c);
                    }
                    var I, v, N, u, P, J, ba = {}, hb;
                    I = c === g ? d : Tb(d, new Fb(A(g), d.$attr));
                    v = I.$$element;
                    if (H) {
                        var T = /^\s*([@=&])(\??)\s*(\w*)\s*$/;
                        f = A(g);
                        J = e.$new(!0);
                        ia && ia === H.$$originalDirective ? f.data("$isolateScope", J) : f.data("$isolateScopeNoTemplate", J);
                        ha(f, "ng-isolate-scope");
                        q(H.scope, function(a, c) {
                            var d = a.match(T) || [], g = d[3] || c, f = "?" == d[2], d = d[1], m, l, n, p;
                            J.$$isolateBindings[c] = d + g;
                            switch (d) {
                              case "@":
                                I.$observe(g, function(a) {
                                    J[c] = a;
                                });
                                I.$$observers[g].$$scope = e;
                                I[g] && (J[c] = b(I[g])(e));
                                break;

                              case "=":
                                if (f && !I[g]) break;
                                l = s(I[g]);
                                p = l.literal ? ua : function(a, b) {
                                    return a === b;
                                };
                                n = l.assign || function() {
                                    m = J[c] = l(e);
                                    throw ja("nonassign", I[g], H.name);
                                };
                                m = J[c] = l(e);
                                J.$watch(function() {
                                    var a = l(e);
                                    p(a, J[c]) || (p(a, m) ? n(e, a = J[c]) : J[c] = a);
                                    return m = a;
                                }, null, l.literal);
                                break;

                              case "&":
                                l = s(I[g]);
                                J[c] = function(a) {
                                    return l(e, a);
                                };
                                break;

                              default:
                                throw ja("iscp", H.name, c, a);
                            }
                        });
                    }
                    hb = p && y;
                    V && q(V, function(a) {
                        var b = {
                            $scope: a === H || a.$$isolateScope ? J : e,
                            $element: v,
                            $attrs: I,
                            $transclude: hb
                        }, c;
                        P = a.controller;
                        "@" == P && (P = I[a.name]);
                        c = C(P, b);
                        ba[a.name] = c;
                        z || v.data("$" + a.name + "Controller", c);
                        a.controllerAs && (b.$scope[a.controllerAs] = c);
                    });
                    f = 0;
                    for (N = m.length; f < N; f++) try {
                        u = m[f], u(u.isolateScope ? J : e, v, I, u.require && R(u.require, v, ba), hb);
                    } catch (G) {
                        l(G, ga(v));
                    }
                    f = e;
                    H && (H.template || null === H.templateUrl) && (f = J);
                    a && a(f, g.childNodes, r, p);
                    for (f = n.length - 1; 0 <= f; f--) try {
                        u = n[f], u(u.isolateScope ? J : e, v, I, u.require && R(u.require, v, ba), hb);
                    } catch (B) {
                        l(B, ga(v));
                    }
                }
                p = p || {};
                var N = -Number.MAX_VALUE, u, V = p.controllerDirectives, H = p.newIsolateScopeDirective, ia = p.templateDirective;
                p = p.nonTlbTranscludeDirective;
                for (var T = !1, z = !1, t = d.$$element = A(c), G, ca, U, F = e, O, M = 0, na = a.length; M < na; M++) {
                    G = a[M];
                    var Va = G.$$start, S = G.$$end;
                    Va && (t = ba(c, Va, S));
                    U = r;
                    if (N > G.priority) break;
                    if (U = G.scope) u = u || G, G.templateUrl || (x("new/isolated scope", H, G, t), 
                    X(U) && (H = G));
                    ca = G.name;
                    !G.templateUrl && G.controller && (U = G.controller, V = V || {}, x("'" + ca + "' controller", V[ca], G, t), 
                    V[ca] = G);
                    if (U = G.transclude) T = !0, G.$$tlb || (x("transclusion", p, G, t), p = G), "element" == U ? (z = !0, 
                    N = G.priority, U = ba(c, Va, S), t = d.$$element = A(Q.createComment(" " + ca + ": " + d[ca] + " ")), 
                    c = t[0], ib(g, A(va.call(U, 0)), c), F = v(U, e, N, f && f.name, {
                        nonTlbTranscludeDirective: p
                    })) : (U = A(Ab(c)).contents(), t.empty(), F = v(U, e));
                    if (G.template) if (x("template", ia, G, t), ia = G, U = L(G.template) ? G.template(t, d) : G.template, 
                    U = Y(U), G.replace) {
                        f = G;
                        U = A("<div>" + aa(U) + "</div>").contents();
                        c = U[0];
                        if (1 != U.length || 1 !== c.nodeType) throw ja("tplrt", ca, "");
                        ib(g, t, c);
                        na = {
                            $attr: {}
                        };
                        U = J(c, [], na);
                        var W = a.splice(M + 1, a.length - (M + 1));
                        H && ic(U);
                        a = a.concat(U).concat(W);
                        B(d, na);
                        na = a.length;
                    } else t.html(U);
                    if (G.templateUrl) x("template", ia, G, t), ia = G, G.replace && (f = G), E = w(a.splice(M, a.length - M), t, d, g, F, m, n, {
                        controllerDirectives: V,
                        newIsolateScopeDirective: H,
                        templateDirective: ia,
                        nonTlbTranscludeDirective: p
                    }), na = a.length; else if (G.compile) try {
                        O = G.compile(t, d, F), L(O) ? y(null, O, Va, S) : O && y(O.pre, O.post, Va, S);
                    } catch (Z) {
                        l(Z, ga(t));
                    }
                    G.terminal && (E.terminal = !0, N = Math.max(N, G.priority));
                }
                E.scope = u && !0 === u.scope;
                E.transclude = T && F;
                return E;
            }
            function ic(a) {
                for (var b = 0, c = a.length; b < c; b++) a[b] = Sb(a[b], {
                    $$isolateScope: !0
                });
            }
            function T(b, e, g, f, k, s, n) {
                if (e === k) return null;
                k = null;
                if (c.hasOwnProperty(e)) {
                    var p;
                    e = a.get(e + d);
                    for (var C = 0, y = e.length; C < y; C++) try {
                        p = e[C], (f === r || f > p.priority) && -1 != p.restrict.indexOf(g) && (s && (p = Sb(p, {
                            $$start: s,
                            $$end: n
                        })), b.push(p), k = p);
                    } catch (v) {
                        l(v);
                    }
                }
                return k;
            }
            function B(a, b) {
                var c = b.$attr, d = a.$attr, e = a.$$element;
                q(a, function(d, e) {
                    "$" != e.charAt(0) && (b[e] && (d += ("style" === e ? ";" : " ") + b[e]), a.$set(e, d, !0, c[e]));
                });
                q(b, function(b, g) {
                    "class" == g ? (ha(e, b), a["class"] = (a["class"] ? a["class"] + " " : "") + b) : "style" == g ? (e.attr("style", e.attr("style") + ";" + b), 
                    a.style = (a.style ? a.style + ";" : "") + b) : "$" == g.charAt(0) || a.hasOwnProperty(g) || (a[g] = b, 
                    d[g] = c[g]);
                });
            }
            function w(a, b, c, d, e, g, f, m) {
                var k = [], s, l, C = b[0], y = a.shift(), v = t({}, y, {
                    templateUrl: null,
                    transclude: null,
                    replace: null,
                    $$originalDirective: y
                }), R = L(y.templateUrl) ? y.templateUrl(b, c) : y.templateUrl;
                b.empty();
                n.get(u.getTrustedResourceUrl(R), {
                    cache: p
                }).success(function(n) {
                    var p, E;
                    n = Y(n);
                    if (y.replace) {
                        n = A("<div>" + aa(n) + "</div>").contents();
                        p = n[0];
                        if (1 != n.length || 1 !== p.nodeType) throw ja("tplrt", y.name, R);
                        n = {
                            $attr: {}
                        };
                        ib(d, b, p);
                        var u = J(p, [], n);
                        X(y.scope) && ic(u);
                        a = u.concat(a);
                        B(c, n);
                    } else p = C, b.html(n);
                    a.unshift(v);
                    s = ia(a, p, c, e, b, y, g, f, m);
                    q(d, function(a, c) {
                        a == p && (d[c] = b[0]);
                    });
                    for (l = N(b[0].childNodes, e); k.length; ) {
                        n = k.shift();
                        E = k.shift();
                        var H = k.shift(), ha = k.shift(), u = b[0];
                        E !== C && (u = Ab(p), ib(H, A(E), u));
                        E = s.transclude ? V(n, s.transclude) : ha;
                        s(l, n, u, d, E);
                    }
                    k = null;
                }).error(function(a, b, c, d) {
                    throw ja("tpload", d.url);
                });
                return function(a, b, c, d, e) {
                    k ? (k.push(b), k.push(c), k.push(d), k.push(e)) : s(l, b, c, d, e);
                };
            }
            function z(a, b) {
                var c = b.priority - a.priority;
                return 0 !== c ? c : a.name !== b.name ? a.name < b.name ? -1 : 1 : a.index - b.index;
            }
            function x(a, b, c, d) {
                if (b) throw ja("multidir", b.name, c.name, a, ga(d));
            }
            function F(a, c) {
                var d = b(c, !0);
                d && a.push({
                    priority: 0,
                    compile: $(function(a, b) {
                        var c = b.parent(), e = c.data("$binding") || [];
                        e.push(d);
                        ha(c.data("$binding", e), "ng-binding");
                        a.$watch(d, function(a) {
                            b[0].nodeValue = a;
                        });
                    })
                });
            }
            function O(a, b) {
                if ("srcdoc" == b) return u.HTML;
                var c = Ha(a);
                if ("xlinkHref" == b || "FORM" == c && "action" == b || "IMG" != c && ("src" == b || "ngSrc" == b)) return u.RESOURCE_URL;
            }
            function S(a, c, d, e) {
                var g = b(d, !0);
                if (g) {
                    if ("multiple" === e && "SELECT" === Ha(a)) throw ja("selmulti", ga(a));
                    c.push({
                        priority: 100,
                        compile: function() {
                            return {
                                pre: function(c, d, m) {
                                    d = m.$$observers || (m.$$observers = {});
                                    if (f.test(e)) throw ja("nodomevents");
                                    if (g = b(m[e], !0, O(a, e))) m[e] = g(c), (d[e] || (d[e] = [])).$$inter = !0, (m.$$observers && m.$$observers[e].$$scope || c).$watch(g, function(a, b) {
                                        "class" === e && a != b ? m.$updateClass(a, b) : m.$set(e, a);
                                    });
                                }
                            };
                        }
                    });
                }
            }
            function ib(a, b, c) {
                var d = b[0], e = b.length, g = d.parentNode, f, m;
                if (a) for (f = 0, m = a.length; f < m; f++) if (a[f] == d) {
                    a[f++] = c;
                    m = f + e - 1;
                    for (var k = a.length; f < k; f++, m++) m < k ? a[f] = a[m] : delete a[f];
                    a.length -= e - 1;
                    break;
                }
                g && g.replaceChild(c, d);
                a = Q.createDocumentFragment();
                a.appendChild(d);
                c[A.expando] = d[A.expando];
                d = 1;
                for (e = b.length; d < e; d++) g = b[d], A(g).remove(), a.appendChild(g), delete b[d];
                b[0] = c;
                b.length = 1;
            }
            function kc(a, b) {
                return t(function() {
                    return a.apply(null, arguments);
                }, a, b);
            }
            var Fb = function(a, b) {
                this.$$element = a;
                this.$attr = b || {};
            };
            Fb.prototype = {
                $normalize: ma,
                $addClass: function(a) {
                    a && 0 < a.length && R.addClass(this.$$element, a);
                },
                $removeClass: function(a) {
                    a && 0 < a.length && R.removeClass(this.$$element, a);
                },
                $updateClass: function(a, b) {
                    this.$removeClass(lc(b, a));
                    this.$addClass(lc(a, b));
                },
                $set: function(a, b, c, d) {
                    var e = fc(this.$$element[0], a);
                    e && (this.$$element.prop(a, b), d = e);
                    this[a] = b;
                    d ? this.$attr[a] = d : (d = this.$attr[a]) || (this.$attr[a] = d = db(a, "-"));
                    e = Ha(this.$$element);
                    if ("A" === e && "href" === a || "IMG" === e && "src" === a) this[a] = b = H(b, "src" === a);
                    !1 !== c && (null === b || b === r ? this.$$element.removeAttr(d) : this.$$element.attr(d, b));
                    (c = this.$$observers) && q(c[a], function(a) {
                        try {
                            a(b);
                        } catch (c) {
                            l(c);
                        }
                    });
                },
                $observe: function(a, b) {
                    var c = this, d = c.$$observers || (c.$$observers = {}), e = d[a] || (d[a] = []);
                    e.push(b);
                    y.$evalAsync(function() {
                        e.$$inter || b(c[a]);
                    });
                    return b;
                }
            };
            var ca = b.startSymbol(), na = b.endSymbol(), Y = "{{" == ca || "}}" == na ? Ba : function(a) {
                return a.replace(/\{\{/g, ca).replace(/}}/g, na);
            }, W = /^ngAttr[A-Z]/;
            return v;
        } ];
    }
    function ma(b) {
        return Qa(b.replace(id, ""));
    }
    function lc(b, a) {
        var c = "", d = b.split(/\s+/), e = a.split(/\s+/), g = 0;
        a: for (;g < d.length; g++) {
            for (var f = d[g], h = 0; h < e.length; h++) if (f == e[h]) continue a;
            c += (0 < c.length ? " " : "") + f;
        }
        return c;
    }
    function jd() {
        var b = {}, a = /^(\S+)(\s+as\s+(\w+))?$/;
        this.register = function(a, d) {
            xa(a, "controller");
            X(a) ? t(b, a) : b[a] = d;
        };
        this.$get = [ "$injector", "$window", function(c, d) {
            return function(e, g) {
                var f, h, m;
                D(e) && (f = e.match(a), h = f[1], m = f[3], e = b.hasOwnProperty(h) ? b[h] : vb(g.$scope, h, !0) || vb(d, h, !0), 
                Pa(e, h, !0));
                f = c.instantiate(e, g);
                if (m) {
                    if (!g || "object" != typeof g.$scope) throw F("$controller")("noscp", h || e.name, m);
                    g.$scope[m] = f;
                }
                return f;
            };
        } ];
    }
    function kd() {
        this.$get = [ "$window", function(b) {
            return A(b.document);
        } ];
    }
    function ld() {
        this.$get = [ "$log", function(b) {
            return function(a, c) {
                b.error.apply(b, arguments);
            };
        } ];
    }
    function mc(b) {
        var a = {}, c, d, e;
        if (!b) return a;
        q(b.split("\n"), function(b) {
            e = b.indexOf(":");
            c = x(aa(b.substr(0, e)));
            d = aa(b.substr(e + 1));
            c && (a[c] = a[c] ? a[c] + (", " + d) : d);
        });
        return a;
    }
    function nc(b) {
        var a = X(b) ? b : r;
        return function(c) {
            a || (a = mc(b));
            return c ? a[x(c)] || null : a;
        };
    }
    function oc(b, a, c) {
        if (L(c)) return c(b, a);
        q(c, function(c) {
            b = c(b, a);
        });
        return b;
    }
    function md() {
        var b = /^\s*(\[|\{[^\{])/, a = /[\}\]]\s*$/, c = /^\)\]\}',?\n/, d = {
            "Content-Type": "application/json;charset=utf-8"
        }, e = this.defaults = {
            transformResponse: [ function(d) {
                D(d) && (d = d.replace(c, ""), b.test(d) && a.test(d) && (d = Vb(d)));
                return d;
            } ],
            transformRequest: [ function(a) {
                return X(a) && "[object File]" !== $a.call(a) ? qa(a) : a;
            } ],
            headers: {
                common: {
                    Accept: "application/json, text/plain, */*"
                },
                post: d,
                put: d,
                patch: d
            },
            xsrfCookieName: "XSRF-TOKEN",
            xsrfHeaderName: "X-XSRF-TOKEN"
        }, g = this.interceptors = [], f = this.responseInterceptors = [];
        this.$get = [ "$httpBackend", "$browser", "$cacheFactory", "$rootScope", "$q", "$injector", function(a, b, c, d, n, p) {
            function s(a) {
                function c(a) {
                    var b = t({}, a, {
                        data: oc(a.data, a.headers, d.transformResponse)
                    });
                    return 200 <= a.status && 300 > a.status ? b : n.reject(b);
                }
                var d = {
                    transformRequest: e.transformRequest,
                    transformResponse: e.transformResponse
                }, g = function(a) {
                    function b(a) {
                        var c;
                        q(a, function(b, d) {
                            L(b) && (c = b(), null != c ? a[d] = c : delete a[d]);
                        });
                    }
                    var c = e.headers, d = t({}, a.headers), g, f, c = t({}, c.common, c[x(a.method)]);
                    b(c);
                    b(d);
                    a: for (g in c) {
                        a = x(g);
                        for (f in d) if (x(f) === a) continue a;
                        d[g] = c[g];
                    }
                    return d;
                }(a);
                t(d, a);
                d.headers = g;
                d.method = Ia(d.method);
                (a = Gb(d.url) ? b.cookies()[d.xsrfCookieName || e.xsrfCookieName] : r) && (g[d.xsrfHeaderName || e.xsrfHeaderName] = a);
                var f = [ function(a) {
                    g = a.headers;
                    var b = oc(a.data, nc(g), a.transformRequest);
                    z(a.data) && q(g, function(a, b) {
                        "content-type" === x(b) && delete g[b];
                    });
                    z(a.withCredentials) && !z(e.withCredentials) && (a.withCredentials = e.withCredentials);
                    return C(a, b, g).then(c, c);
                }, r ], h = n.when(d);
                for (q(u, function(a) {
                    (a.request || a.requestError) && f.unshift(a.request, a.requestError);
                    (a.response || a.responseError) && f.push(a.response, a.responseError);
                }); f.length; ) {
                    a = f.shift();
                    var k = f.shift(), h = h.then(a, k);
                }
                h.success = function(a) {
                    h.then(function(b) {
                        a(b.data, b.status, b.headers, d);
                    });
                    return h;
                };
                h.error = function(a) {
                    h.then(null, function(b) {
                        a(b.data, b.status, b.headers, d);
                    });
                    return h;
                };
                return h;
            }
            function C(b, c, g) {
                function f(a, b, c) {
                    u && (200 <= a && 300 > a ? u.put(r, [ a, b, mc(c) ]) : u.remove(r));
                    m(b, a, c);
                    d.$$phase || d.$apply();
                }
                function m(a, c, d) {
                    c = Math.max(c, 0);
                    (200 <= c && 300 > c ? p.resolve : p.reject)({
                        data: a,
                        status: c,
                        headers: nc(d),
                        config: b
                    });
                }
                function k() {
                    var a = bb(s.pendingRequests, b);
                    -1 !== a && s.pendingRequests.splice(a, 1);
                }
                var p = n.defer(), C = p.promise, u, q, r = y(b.url, b.params);
                s.pendingRequests.push(b);
                C.then(k, k);
                (b.cache || e.cache) && (!1 !== b.cache && "GET" == b.method) && (u = X(b.cache) ? b.cache : X(e.cache) ? e.cache : E);
                if (u) if (q = u.get(r), B(q)) {
                    if (q.then) return q.then(k, k), q;
                    K(q) ? m(q[1], q[0], fa(q[2])) : m(q, 200, {});
                } else u.put(r, C);
                z(q) && a(b.method, r, c, f, g, b.timeout, b.withCredentials, b.responseType);
                return C;
            }
            function y(a, b) {
                if (!b) return a;
                var c = [];
                Pc(b, function(a, b) {
                    null === a || z(a) || (K(a) || (a = [ a ]), q(a, function(a) {
                        X(a) && (a = qa(a));
                        c.push(wa(b) + "=" + wa(a));
                    }));
                });
                return a + (-1 == a.indexOf("?") ? "?" : "&") + c.join("&");
            }
            var E = c("$http"), u = [];
            q(g, function(a) {
                u.unshift(D(a) ? p.get(a) : p.invoke(a));
            });
            q(f, function(a, b) {
                var c = D(a) ? p.get(a) : p.invoke(a);
                u.splice(b, 0, {
                    response: function(a) {
                        return c(n.when(a));
                    },
                    responseError: function(a) {
                        return c(n.reject(a));
                    }
                });
            });
            s.pendingRequests = [];
            (function(a) {
                q(arguments, function(a) {
                    s[a] = function(b, c) {
                        return s(t(c || {}, {
                            method: a,
                            url: b
                        }));
                    };
                });
            })("get", "delete", "head", "jsonp");
            (function(a) {
                q(arguments, function(a) {
                    s[a] = function(b, c, d) {
                        return s(t(d || {}, {
                            method: a,
                            url: b,
                            data: c
                        }));
                    };
                });
            })("post", "put");
            s.defaults = e;
            return s;
        } ];
    }
    function nd(b) {
        return 8 >= M && "patch" === x(b) ? new ActiveXObject("Microsoft.XMLHTTP") : new Z.XMLHttpRequest();
    }
    function od() {
        this.$get = [ "$browser", "$window", "$document", function(b, a, c) {
            return pd(b, nd, b.defer, a.angular.callbacks, c[0]);
        } ];
    }
    function pd(b, a, c, d, e) {
        function g(a, b) {
            var c = e.createElement("script"), d = function() {
                c.onreadystatechange = c.onload = c.onerror = null;
                e.body.removeChild(c);
                b && b();
            };
            c.type = "text/javascript";
            c.src = a;
            M && 8 >= M ? c.onreadystatechange = function() {
                /loaded|complete/.test(c.readyState) && d();
            } : c.onload = c.onerror = function() {
                d();
            };
            e.body.appendChild(c);
            return d;
        }
        var f = -1;
        return function(e, m, k, l, n, p, s, C) {
            function y() {
                u = f;
                H && H();
                v && v.abort();
            }
            function E(a, d, e, g) {
                r && c.cancel(r);
                H = v = null;
                d = 0 === d ? e ? 200 : 404 : d;
                a(1223 == d ? 204 : d, e, g);
                b.$$completeOutstandingRequest(w);
            }
            var u;
            b.$$incOutstandingRequestCount();
            m = m || b.url();
            if ("jsonp" == x(e)) {
                var R = "_" + (d.counter++).toString(36);
                d[R] = function(a) {
                    d[R].data = a;
                };
                var H = g(m.replace("JSON_CALLBACK", "angular.callbacks." + R), function() {
                    d[R].data ? E(l, 200, d[R].data) : E(l, u || -2);
                    d[R] = Ca.noop;
                });
            } else {
                var v = a(e);
                v.open(e, m, !0);
                q(n, function(a, b) {
                    B(a) && v.setRequestHeader(b, a);
                });
                v.onreadystatechange = function() {
                    if (v && 4 == v.readyState) {
                        var a = null, b = null;
                        u !== f && (a = v.getAllResponseHeaders(), b = "response" in v ? v.response : v.responseText);
                        E(l, u || v.status, b, a);
                    }
                };
                s && (v.withCredentials = !0);
                C && (v.responseType = C);
                v.send(k || null);
            }
            if (0 < p) var r = c(y, p); else p && p.then && p.then(y);
        };
    }
    function qd() {
        var b = "{{", a = "}}";
        this.startSymbol = function(a) {
            return a ? (b = a, this) : b;
        };
        this.endSymbol = function(b) {
            return b ? (a = b, this) : a;
        };
        this.$get = [ "$parse", "$exceptionHandler", "$sce", function(c, d, e) {
            function g(g, k, l) {
                for (var n, p, s = 0, C = [], y = g.length, E = !1, u = []; s < y; ) -1 != (n = g.indexOf(b, s)) && -1 != (p = g.indexOf(a, n + f)) ? (s != n && C.push(g.substring(s, n)), 
                C.push(s = c(E = g.substring(n + f, p))), s.exp = E, s = p + h, E = !0) : (s != y && C.push(g.substring(s)), 
                s = y);
                (y = C.length) || (C.push(""), y = 1);
                if (l && 1 < C.length) throw pc("noconcat", g);
                if (!k || E) return u.length = y, s = function(a) {
                    try {
                        for (var b = 0, c = y, f; b < c; b++) "function" == typeof (f = C[b]) && (f = f(a), 
                        f = l ? e.getTrusted(l, f) : e.valueOf(f), null === f || z(f) ? f = "" : "string" != typeof f && (f = qa(f))), 
                        u[b] = f;
                        return u.join("");
                    } catch (h) {
                        a = pc("interr", g, h.toString()), d(a);
                    }
                }, s.exp = g, s.parts = C, s;
            }
            var f = b.length, h = a.length;
            g.startSymbol = function() {
                return b;
            };
            g.endSymbol = function() {
                return a;
            };
            return g;
        } ];
    }
    function rd() {
        this.$get = [ "$rootScope", "$window", "$q", function(b, a, c) {
            function d(d, f, h, m) {
                var k = a.setInterval, l = a.clearInterval, n = c.defer(), p = n.promise, s = 0, C = B(m) && !m;
                h = B(h) ? h : 0;
                p.then(null, null, d);
                p.$$intervalId = k(function() {
                    n.notify(s++);
                    0 < h && s >= h && (n.resolve(s), l(p.$$intervalId), delete e[p.$$intervalId]);
                    C || b.$apply();
                }, f);
                e[p.$$intervalId] = n;
                return p;
            }
            var e = {};
            d.cancel = function(a) {
                return a && a.$$intervalId in e ? (e[a.$$intervalId].reject("canceled"), clearInterval(a.$$intervalId), 
                delete e[a.$$intervalId], !0) : !1;
            };
            return d;
        } ];
    }
    function sd() {
        this.$get = function() {
            return {
                id: "en-us",
                NUMBER_FORMATS: {
                    DECIMAL_SEP: ".",
                    GROUP_SEP: ",",
                    PATTERNS: [ {
                        minInt: 1,
                        minFrac: 0,
                        maxFrac: 3,
                        posPre: "",
                        posSuf: "",
                        negPre: "-",
                        negSuf: "",
                        gSize: 3,
                        lgSize: 3
                    }, {
                        minInt: 1,
                        minFrac: 2,
                        maxFrac: 2,
                        posPre: "¤",
                        posSuf: "",
                        negPre: "(¤",
                        negSuf: ")",
                        gSize: 3,
                        lgSize: 3
                    } ],
                    CURRENCY_SYM: "$"
                },
                DATETIME_FORMATS: {
                    MONTH: "January February March April May June July August September October November December".split(" "),
                    SHORTMONTH: "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
                    DAY: "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
                    SHORTDAY: "Sun Mon Tue Wed Thu Fri Sat".split(" "),
                    AMPMS: [ "AM", "PM" ],
                    medium: "MMM d, y h:mm:ss a",
                    "short": "M/d/yy h:mm a",
                    fullDate: "EEEE, MMMM d, y",
                    longDate: "MMMM d, y",
                    mediumDate: "MMM d, y",
                    shortDate: "M/d/yy",
                    mediumTime: "h:mm:ss a",
                    shortTime: "h:mm a"
                },
                pluralCat: function(b) {
                    return 1 === b ? "one" : "other";
                }
            };
        };
    }
    function qc(b) {
        b = b.split("/");
        for (var a = b.length; a--; ) b[a] = tb(b[a]);
        return b.join("/");
    }
    function rc(b, a, c) {
        b = ya(b, c);
        a.$$protocol = b.protocol;
        a.$$host = b.hostname;
        a.$$port = S(b.port) || td[b.protocol] || null;
    }
    function sc(b, a, c) {
        var d = "/" !== b.charAt(0);
        d && (b = "/" + b);
        b = ya(b, c);
        a.$$path = decodeURIComponent(d && "/" === b.pathname.charAt(0) ? b.pathname.substring(1) : b.pathname);
        a.$$search = Xb(b.search);
        a.$$hash = decodeURIComponent(b.hash);
        a.$$path && "/" != a.$$path.charAt(0) && (a.$$path = "/" + a.$$path);
    }
    function oa(b, a) {
        if (0 === a.indexOf(b)) return a.substr(b.length);
    }
    function Wa(b) {
        var a = b.indexOf("#");
        return -1 == a ? b : b.substr(0, a);
    }
    function Hb(b) {
        return b.substr(0, Wa(b).lastIndexOf("/") + 1);
    }
    function tc(b, a) {
        this.$$html5 = !0;
        a = a || "";
        var c = Hb(b);
        rc(b, this, b);
        this.$$parse = function(a) {
            var e = oa(c, a);
            if (!D(e)) throw Ib("ipthprfx", a, c);
            sc(e, this, b);
            this.$$path || (this.$$path = "/");
            this.$$compose();
        };
        this.$$compose = function() {
            var a = Yb(this.$$search), b = this.$$hash ? "#" + tb(this.$$hash) : "";
            this.$$url = qc(this.$$path) + (a ? "?" + a : "") + b;
            this.$$absUrl = c + this.$$url.substr(1);
        };
        this.$$rewrite = function(d) {
            var e;
            if ((e = oa(b, d)) !== r) return d = e, (e = oa(a, e)) !== r ? c + (oa("/", e) || e) : b + d;
            if ((e = oa(c, d)) !== r) return c + e;
            if (c == d + "/") return c;
        };
    }
    function Jb(b, a) {
        var c = Hb(b);
        rc(b, this, b);
        this.$$parse = function(d) {
            var e = oa(b, d) || oa(c, d), e = "#" == e.charAt(0) ? oa(a, e) : this.$$html5 ? e : "";
            if (!D(e)) throw Ib("ihshprfx", d, a);
            sc(e, this, b);
            d = this.$$path;
            var g = /^\/?.*?:(\/.*)/;
            0 === e.indexOf(b) && (e = e.replace(b, ""));
            g.exec(e) || (d = (e = g.exec(d)) ? e[1] : d);
            this.$$path = d;
            this.$$compose();
        };
        this.$$compose = function() {
            var c = Yb(this.$$search), e = this.$$hash ? "#" + tb(this.$$hash) : "";
            this.$$url = qc(this.$$path) + (c ? "?" + c : "") + e;
            this.$$absUrl = b + (this.$$url ? a + this.$$url : "");
        };
        this.$$rewrite = function(a) {
            if (Wa(b) == Wa(a)) return a;
        };
    }
    function uc(b, a) {
        this.$$html5 = !0;
        Jb.apply(this, arguments);
        var c = Hb(b);
        this.$$rewrite = function(d) {
            var e;
            if (b == Wa(d)) return d;
            if (e = oa(c, d)) return b + a + e;
            if (c === d + "/") return c;
        };
    }
    function jb(b) {
        return function() {
            return this[b];
        };
    }
    function vc(b, a) {
        return function(c) {
            if (z(c)) return this[b];
            this[b] = a(c);
            this.$$compose();
            return this;
        };
    }
    function ud() {
        var b = "", a = !1;
        this.hashPrefix = function(a) {
            return B(a) ? (b = a, this) : b;
        };
        this.html5Mode = function(b) {
            return B(b) ? (a = b, this) : a;
        };
        this.$get = [ "$rootScope", "$browser", "$sniffer", "$rootElement", function(c, d, e, g) {
            function f(a) {
                c.$broadcast("$locationChangeSuccess", h.absUrl(), a);
            }
            var h, m = d.baseHref(), k = d.url();
            a ? (m = k.substring(0, k.indexOf("/", k.indexOf("//") + 2)) + (m || "/"), e = e.history ? tc : uc) : (m = Wa(k), 
            e = Jb);
            h = new e(m, "#" + b);
            h.$$parse(h.$$rewrite(k));
            g.on("click", function(a) {
                if (!a.ctrlKey && !a.metaKey && 2 != a.which) {
                    for (var b = A(a.target); "a" !== x(b[0].nodeName); ) if (b[0] === g[0] || !(b = b.parent())[0]) return;
                    var e = b.prop("href");
                    X(e) && "[object SVGAnimatedString]" === e.toString() && (e = ya(e.animVal).href);
                    var f = h.$$rewrite(e);
                    e && (!b.attr("target") && f && !a.isDefaultPrevented()) && (a.preventDefault(), 
                    f != d.url() && (h.$$parse(f), c.$apply(), Z.angular["ff-684208-preventDefault"] = !0));
                }
            });
            h.absUrl() != k && d.url(h.absUrl(), !0);
            d.onUrlChange(function(a) {
                h.absUrl() != a && (c.$evalAsync(function() {
                    var b = h.absUrl();
                    h.$$parse(a);
                    c.$broadcast("$locationChangeStart", a, b).defaultPrevented ? (h.$$parse(b), d.url(b)) : f(b);
                }), c.$$phase || c.$digest());
            });
            var l = 0;
            c.$watch(function() {
                var a = d.url(), b = h.$$replace;
                l && a == h.absUrl() || (l++, c.$evalAsync(function() {
                    c.$broadcast("$locationChangeStart", h.absUrl(), a).defaultPrevented ? h.$$parse(a) : (d.url(h.absUrl(), b), 
                    f(a));
                }));
                h.$$replace = !1;
                return l;
            });
            return h;
        } ];
    }
    function vd() {
        var b = !0, a = this;
        this.debugEnabled = function(a) {
            return B(a) ? (b = a, this) : b;
        };
        this.$get = [ "$window", function(c) {
            function d(a) {
                a instanceof Error && (a.stack ? a = a.message && -1 === a.stack.indexOf(a.message) ? "Error: " + a.message + "\n" + a.stack : a.stack : a.sourceURL && (a = a.message + "\n" + a.sourceURL + ":" + a.line));
                return a;
            }
            function e(a) {
                var b = c.console || {}, e = b[a] || b.log || w;
                a = !1;
                try {
                    a = !!e.apply;
                } catch (m) {}
                return a ? function() {
                    var a = [];
                    q(arguments, function(b) {
                        a.push(d(b));
                    });
                    return e.apply(b, a);
                } : function(a, b) {
                    e(a, null == b ? "" : b);
                };
            }
            return {
                log: e("log"),
                info: e("info"),
                warn: e("warn"),
                error: e("error"),
                debug: function() {
                    var c = e("debug");
                    return function() {
                        b && c.apply(a, arguments);
                    };
                }()
            };
        } ];
    }
    function da(b, a) {
        if ("constructor" === b) throw za("isecfld", a);
        return b;
    }
    function Xa(b, a) {
        if (b) {
            if (b.constructor === b) throw za("isecfn", a);
            if (b.document && b.location && b.alert && b.setInterval) throw za("isecwindow", a);
            if (b.children && (b.nodeName || b.on && b.find)) throw za("isecdom", a);
        }
        return b;
    }
    function kb(b, a, c, d, e) {
        e = e || {};
        a = a.split(".");
        for (var g, f = 0; 1 < a.length; f++) {
            g = da(a.shift(), d);
            var h = b[g];
            h || (h = {}, b[g] = h);
            b = h;
            b.then && e.unwrapPromises && (ra(d), "$$v" in b || function(a) {
                a.then(function(b) {
                    a.$$v = b;
                });
            }(b), b.$$v === r && (b.$$v = {}), b = b.$$v);
        }
        g = da(a.shift(), d);
        return b[g] = c;
    }
    function wc(b, a, c, d, e, g, f) {
        da(b, g);
        da(a, g);
        da(c, g);
        da(d, g);
        da(e, g);
        return f.unwrapPromises ? function(f, m) {
            var k = m && m.hasOwnProperty(b) ? m : f, l;
            if (null == k) return k;
            (k = k[b]) && k.then && (ra(g), "$$v" in k || (l = k, l.$$v = r, l.then(function(a) {
                l.$$v = a;
            })), k = k.$$v);
            if (!a) return k;
            if (null == k) return r;
            (k = k[a]) && k.then && (ra(g), "$$v" in k || (l = k, l.$$v = r, l.then(function(a) {
                l.$$v = a;
            })), k = k.$$v);
            if (!c) return k;
            if (null == k) return r;
            (k = k[c]) && k.then && (ra(g), "$$v" in k || (l = k, l.$$v = r, l.then(function(a) {
                l.$$v = a;
            })), k = k.$$v);
            if (!d) return k;
            if (null == k) return r;
            (k = k[d]) && k.then && (ra(g), "$$v" in k || (l = k, l.$$v = r, l.then(function(a) {
                l.$$v = a;
            })), k = k.$$v);
            if (!e) return k;
            if (null == k) return r;
            (k = k[e]) && k.then && (ra(g), "$$v" in k || (l = k, l.$$v = r, l.then(function(a) {
                l.$$v = a;
            })), k = k.$$v);
            return k;
        } : function(g, f) {
            var k = f && f.hasOwnProperty(b) ? f : g;
            if (null == k) return k;
            k = k[b];
            if (!a) return k;
            if (null == k) return r;
            k = k[a];
            if (!c) return k;
            if (null == k) return r;
            k = k[c];
            if (!d) return k;
            if (null == k) return r;
            k = k[d];
            return e ? null == k ? r : k = k[e] : k;
        };
    }
    function wd(b, a) {
        da(b, a);
        return function(a, d) {
            return null == a ? r : (d && d.hasOwnProperty(b) ? d : a)[b];
        };
    }
    function xd(b, a, c) {
        da(b, c);
        da(a, c);
        return function(c, e) {
            if (null == c) return r;
            c = (e && e.hasOwnProperty(b) ? e : c)[b];
            return null == c ? r : c[a];
        };
    }
    function xc(b, a, c) {
        if (Kb.hasOwnProperty(b)) return Kb[b];
        var d = b.split("."), e = d.length, g;
        if (a.unwrapPromises || 1 !== e) if (a.unwrapPromises || 2 !== e) if (a.csp) g = 6 > e ? wc(d[0], d[1], d[2], d[3], d[4], c, a) : function(b, g) {
            var f = 0, h;
            do h = wc(d[f++], d[f++], d[f++], d[f++], d[f++], c, a)(b, g), g = r, b = h; while (f < e);
            return h;
        }; else {
            var f = "var p;\n";
            q(d, function(b, d) {
                da(b, c);
                f += "if(s == null) return undefined;\ns=" + (d ? "s" : '((k&&k.hasOwnProperty("' + b + '"))?k:s)') + '["' + b + '"];\n' + (a.unwrapPromises ? 'if (s && s.then) {\n pw("' + c.replace(/(["\r\n])/g, "\\$1") + '");\n if (!("$$v" in s)) {\n p=s;\n p.$$v = undefined;\n p.then(function(v) {p.$$v=v;});\n}\n s=s.$$v\n}\n' : "");
            });
            var f = f + "return s;", h = new Function("s", "k", "pw", f);
            h.toString = $(f);
            g = a.unwrapPromises ? function(a, b) {
                return h(a, b, ra);
            } : h;
        } else g = xd(d[0], d[1], c); else g = wd(d[0], c);
        "hasOwnProperty" !== b && (Kb[b] = g);
        return g;
    }
    function yd() {
        var b = {}, a = {
            csp: !1,
            unwrapPromises: !1,
            logPromiseWarnings: !0
        };
        this.unwrapPromises = function(b) {
            return B(b) ? (a.unwrapPromises = !!b, this) : a.unwrapPromises;
        };
        this.logPromiseWarnings = function(b) {
            return B(b) ? (a.logPromiseWarnings = b, this) : a.logPromiseWarnings;
        };
        this.$get = [ "$filter", "$sniffer", "$log", function(c, d, e) {
            a.csp = d.csp;
            ra = function(b) {
                a.logPromiseWarnings && !yc.hasOwnProperty(b) && (yc[b] = !0, e.warn("[$parse] Promise found in the expression `" + b + "`. Automatic unwrapping of promises in Angular expressions is deprecated."));
            };
            return function(d) {
                var e;
                switch (typeof d) {
                  case "string":
                    if (b.hasOwnProperty(d)) return b[d];
                    e = new Lb(a);
                    e = new Ya(e, c, a).parse(d, !1);
                    "hasOwnProperty" !== d && (b[d] = e);
                    return e;

                  case "function":
                    return d;

                  default:
                    return w;
                }
            };
        } ];
    }
    function zd() {
        this.$get = [ "$rootScope", "$exceptionHandler", function(b, a) {
            return Ad(function(a) {
                b.$evalAsync(a);
            }, a);
        } ];
    }
    function Ad(b, a) {
        function c(a) {
            return a;
        }
        function d(a) {
            return f(a);
        }
        var e = function() {
            var h = [], m, k;
            return k = {
                resolve: function(a) {
                    if (h) {
                        var c = h;
                        h = r;
                        m = g(a);
                        c.length && b(function() {
                            for (var a, b = 0, d = c.length; b < d; b++) a = c[b], m.then(a[0], a[1], a[2]);
                        });
                    }
                },
                reject: function(a) {
                    k.resolve(f(a));
                },
                notify: function(a) {
                    if (h) {
                        var c = h;
                        h.length && b(function() {
                            for (var b, d = 0, e = c.length; d < e; d++) b = c[d], b[2](a);
                        });
                    }
                },
                promise: {
                    then: function(b, g, f) {
                        var k = e(), C = function(d) {
                            try {
                                k.resolve((L(b) ? b : c)(d));
                            } catch (e) {
                                k.reject(e), a(e);
                            }
                        }, y = function(b) {
                            try {
                                k.resolve((L(g) ? g : d)(b));
                            } catch (c) {
                                k.reject(c), a(c);
                            }
                        }, E = function(b) {
                            try {
                                k.notify((L(f) ? f : c)(b));
                            } catch (d) {
                                a(d);
                            }
                        };
                        h ? h.push([ C, y, E ]) : m.then(C, y, E);
                        return k.promise;
                    },
                    "catch": function(a) {
                        return this.then(null, a);
                    },
                    "finally": function(a) {
                        function b(a, c) {
                            var d = e();
                            c ? d.resolve(a) : d.reject(a);
                            return d.promise;
                        }
                        function d(e, g) {
                            var f = null;
                            try {
                                f = (a || c)();
                            } catch (h) {
                                return b(h, !1);
                            }
                            return f && L(f.then) ? f.then(function() {
                                return b(e, g);
                            }, function(a) {
                                return b(a, !1);
                            }) : b(e, g);
                        }
                        return this.then(function(a) {
                            return d(a, !0);
                        }, function(a) {
                            return d(a, !1);
                        });
                    }
                }
            };
        }, g = function(a) {
            return a && L(a.then) ? a : {
                then: function(c) {
                    var d = e();
                    b(function() {
                        d.resolve(c(a));
                    });
                    return d.promise;
                }
            };
        }, f = function(c) {
            return {
                then: function(g, f) {
                    var l = e();
                    b(function() {
                        try {
                            l.resolve((L(f) ? f : d)(c));
                        } catch (b) {
                            l.reject(b), a(b);
                        }
                    });
                    return l.promise;
                }
            };
        };
        return {
            defer: e,
            reject: f,
            when: function(h, m, k, l) {
                var n = e(), p, s = function(b) {
                    try {
                        return (L(m) ? m : c)(b);
                    } catch (d) {
                        return a(d), f(d);
                    }
                }, C = function(b) {
                    try {
                        return (L(k) ? k : d)(b);
                    } catch (c) {
                        return a(c), f(c);
                    }
                }, y = function(b) {
                    try {
                        return (L(l) ? l : c)(b);
                    } catch (d) {
                        a(d);
                    }
                };
                b(function() {
                    g(h).then(function(a) {
                        p || (p = !0, n.resolve(g(a).then(s, C, y)));
                    }, function(a) {
                        p || (p = !0, n.resolve(C(a)));
                    }, function(a) {
                        p || n.notify(y(a));
                    });
                });
                return n.promise;
            },
            all: function(a) {
                var b = e(), c = 0, d = K(a) ? [] : {};
                q(a, function(a, e) {
                    c++;
                    g(a).then(function(a) {
                        d.hasOwnProperty(e) || (d[e] = a, --c || b.resolve(d));
                    }, function(a) {
                        d.hasOwnProperty(e) || b.reject(a);
                    });
                });
                0 === c && b.resolve(d);
                return b.promise;
            }
        };
    }
    function Bd() {
        var b = 10, a = F("$rootScope"), c = null;
        this.digestTtl = function(a) {
            arguments.length && (b = a);
            return b;
        };
        this.$get = [ "$injector", "$exceptionHandler", "$parse", "$browser", function(d, e, g, f) {
            function h() {
                this.$id = Za();
                this.$$phase = this.$parent = this.$$watchers = this.$$nextSibling = this.$$prevSibling = this.$$childHead = this.$$childTail = null;
                this["this"] = this.$root = this;
                this.$$destroyed = !1;
                this.$$asyncQueue = [];
                this.$$postDigestQueue = [];
                this.$$listeners = {};
                this.$$listenerCount = {};
                this.$$isolateBindings = {};
            }
            function m(b) {
                if (p.$$phase) throw a("inprog", p.$$phase);
                p.$$phase = b;
            }
            function k(a, b) {
                var c = g(a);
                Pa(c, b);
                return c;
            }
            function l(a, b, c) {
                do a.$$listenerCount[c] -= b, 0 === a.$$listenerCount[c] && delete a.$$listenerCount[c]; while (a = a.$parent);
            }
            function n() {}
            h.prototype = {
                constructor: h,
                $new: function(a) {
                    a ? (a = new h(), a.$root = this.$root, a.$$asyncQueue = this.$$asyncQueue, a.$$postDigestQueue = this.$$postDigestQueue) : (a = function() {}, 
                    a.prototype = this, a = new a(), a.$id = Za());
                    a["this"] = a;
                    a.$$listeners = {};
                    a.$$listenerCount = {};
                    a.$parent = this;
                    a.$$watchers = a.$$nextSibling = a.$$childHead = a.$$childTail = null;
                    a.$$prevSibling = this.$$childTail;
                    this.$$childHead ? this.$$childTail = this.$$childTail.$$nextSibling = a : this.$$childHead = this.$$childTail = a;
                    return a;
                },
                $watch: function(a, b, d) {
                    var e = k(a, "watch"), g = this.$$watchers, f = {
                        fn: b,
                        last: n,
                        get: e,
                        exp: a,
                        eq: !!d
                    };
                    c = null;
                    if (!L(b)) {
                        var h = k(b || w, "listener");
                        f.fn = function(a, b, c) {
                            h(c);
                        };
                    }
                    if ("string" == typeof a && e.constant) {
                        var m = f.fn;
                        f.fn = function(a, b, c) {
                            m.call(this, a, b, c);
                            Ma(g, f);
                        };
                    }
                    g || (g = this.$$watchers = []);
                    g.unshift(f);
                    return function() {
                        Ma(g, f);
                        c = null;
                    };
                },
                $watchCollection: function(a, b) {
                    var c = this, d, e, f = 0, h = g(a), m = [], k = {}, l = 0;
                    return this.$watch(function() {
                        e = h(c);
                        var a, b;
                        if (X(e)) if (rb(e)) for (d !== m && (d = m, l = d.length = 0, f++), a = e.length, 
                        l !== a && (f++, d.length = l = a), b = 0; b < a; b++) d[b] !== e[b] && (f++, d[b] = e[b]); else {
                            d !== k && (d = k = {}, l = 0, f++);
                            a = 0;
                            for (b in e) e.hasOwnProperty(b) && (a++, d.hasOwnProperty(b) ? d[b] !== e[b] && (f++, 
                            d[b] = e[b]) : (l++, d[b] = e[b], f++));
                            if (l > a) for (b in f++, d) d.hasOwnProperty(b) && !e.hasOwnProperty(b) && (l--, 
                            delete d[b]);
                        } else d !== e && (d = e, f++);
                        return f;
                    }, function() {
                        b(e, d, c);
                    });
                },
                $digest: function() {
                    var d, f, g, h, k = this.$$asyncQueue, l = this.$$postDigestQueue, q, v, r = b, N, V = [], J, A, P;
                    m("$digest");
                    c = null;
                    do {
                        v = !1;
                        for (N = this; k.length; ) {
                            try {
                                P = k.shift(), P.scope.$eval(P.expression);
                            } catch (B) {
                                p.$$phase = null, e(B);
                            }
                            c = null;
                        }
                        a: do {
                            if (h = N.$$watchers) for (q = h.length; q--; ) try {
                                if (d = h[q]) if ((f = d.get(N)) !== (g = d.last) && !(d.eq ? ua(f, g) : "number" == typeof f && "number" == typeof g && isNaN(f) && isNaN(g))) v = !0, 
                                c = d, d.last = d.eq ? fa(f) : f, d.fn(f, g === n ? f : g, N), 5 > r && (J = 4 - r, 
                                V[J] || (V[J] = []), A = L(d.exp) ? "fn: " + (d.exp.name || d.exp.toString()) : d.exp, 
                                A += "; newVal: " + qa(f) + "; oldVal: " + qa(g), V[J].push(A)); else if (d === c) {
                                    v = !1;
                                    break a;
                                }
                            } catch (t) {
                                p.$$phase = null, e(t);
                            }
                            if (!(h = N.$$childHead || N !== this && N.$$nextSibling)) for (;N !== this && !(h = N.$$nextSibling); ) N = N.$parent;
                        } while (N = h);
                        if (v && !r--) throw p.$$phase = null, a("infdig", b, qa(V));
                    } while (v || k.length);
                    for (p.$$phase = null; l.length; ) try {
                        l.shift()();
                    } catch (z) {
                        e(z);
                    }
                },
                $destroy: function() {
                    if (!this.$$destroyed) {
                        var a = this.$parent;
                        this.$broadcast("$destroy");
                        this.$$destroyed = !0;
                        this !== p && (q(this.$$listenerCount, cb(null, l, this)), a.$$childHead == this && (a.$$childHead = this.$$nextSibling), 
                        a.$$childTail == this && (a.$$childTail = this.$$prevSibling), this.$$prevSibling && (this.$$prevSibling.$$nextSibling = this.$$nextSibling), 
                        this.$$nextSibling && (this.$$nextSibling.$$prevSibling = this.$$prevSibling), this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead = this.$$childTail = null);
                    }
                },
                $eval: function(a, b) {
                    return g(a)(this, b);
                },
                $evalAsync: function(a) {
                    p.$$phase || p.$$asyncQueue.length || f.defer(function() {
                        p.$$asyncQueue.length && p.$digest();
                    });
                    this.$$asyncQueue.push({
                        scope: this,
                        expression: a
                    });
                },
                $$postDigest: function(a) {
                    this.$$postDigestQueue.push(a);
                },
                $apply: function(a) {
                    try {
                        return m("$apply"), this.$eval(a);
                    } catch (b) {
                        e(b);
                    } finally {
                        p.$$phase = null;
                        try {
                            p.$digest();
                        } catch (c) {
                            throw e(c), c;
                        }
                    }
                },
                $on: function(a, b) {
                    var c = this.$$listeners[a];
                    c || (this.$$listeners[a] = c = []);
                    c.push(b);
                    var d = this;
                    do d.$$listenerCount[a] || (d.$$listenerCount[a] = 0), d.$$listenerCount[a]++; while (d = d.$parent);
                    var e = this;
                    return function() {
                        c[bb(c, b)] = null;
                        l(e, 1, a);
                    };
                },
                $emit: function(a, b) {
                    var c = [], d, f = this, g = !1, h = {
                        name: a,
                        targetScope: f,
                        stopPropagation: function() {
                            g = !0;
                        },
                        preventDefault: function() {
                            h.defaultPrevented = !0;
                        },
                        defaultPrevented: !1
                    }, m = [ h ].concat(va.call(arguments, 1)), k, l;
                    do {
                        d = f.$$listeners[a] || c;
                        h.currentScope = f;
                        k = 0;
                        for (l = d.length; k < l; k++) if (d[k]) try {
                            d[k].apply(null, m);
                        } catch (p) {
                            e(p);
                        } else d.splice(k, 1), k--, l--;
                        if (g) break;
                        f = f.$parent;
                    } while (f);
                    return h;
                },
                $broadcast: function(a, b) {
                    for (var c = this, d = this, f = {
                        name: a,
                        targetScope: this,
                        preventDefault: function() {
                            f.defaultPrevented = !0;
                        },
                        defaultPrevented: !1
                    }, g = [ f ].concat(va.call(arguments, 1)), h, k; c = d; ) {
                        f.currentScope = c;
                        d = c.$$listeners[a] || [];
                        h = 0;
                        for (k = d.length; h < k; h++) if (d[h]) try {
                            d[h].apply(null, g);
                        } catch (m) {
                            e(m);
                        } else d.splice(h, 1), h--, k--;
                        if (!(d = c.$$listenerCount[a] && c.$$childHead || c !== this && c.$$nextSibling)) for (;c !== this && !(d = c.$$nextSibling); ) c = c.$parent;
                    }
                    return f;
                }
            };
            var p = new h();
            return p;
        } ];
    }
    function Cd() {
        var b = /^\s*(https?|ftp|mailto|tel|file):/, a = /^\s*(https?|ftp|file):|data:image\//;
        this.aHrefSanitizationWhitelist = function(a) {
            return B(a) ? (b = a, this) : b;
        };
        this.imgSrcSanitizationWhitelist = function(b) {
            return B(b) ? (a = b, this) : a;
        };
        this.$get = function() {
            return function(c, d) {
                var e = d ? a : b, g;
                if (!M || 8 <= M) if (g = ya(c).href, "" !== g && !g.match(e)) return "unsafe:" + g;
                return c;
            };
        };
    }
    function Dd(b) {
        if ("self" === b) return b;
        if (D(b)) {
            if (-1 < b.indexOf("***")) throw sa("iwcard", b);
            b = b.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08").replace("\\*\\*", ".*").replace("\\*", "[^:/.?&;]*");
            return RegExp("^" + b + "$");
        }
        if (ab(b)) return RegExp("^" + b.source + "$");
        throw sa("imatcher");
    }
    function zc(b) {
        var a = [];
        B(b) && q(b, function(b) {
            a.push(Dd(b));
        });
        return a;
    }
    function Ed() {
        this.SCE_CONTEXTS = ea;
        var b = [ "self" ], a = [];
        this.resourceUrlWhitelist = function(a) {
            arguments.length && (b = zc(a));
            return b;
        };
        this.resourceUrlBlacklist = function(b) {
            arguments.length && (a = zc(b));
            return a;
        };
        this.$get = [ "$injector", function(c) {
            function d(a) {
                var b = function(a) {
                    this.$$unwrapTrustedValue = function() {
                        return a;
                    };
                };
                a && (b.prototype = new a());
                b.prototype.valueOf = function() {
                    return this.$$unwrapTrustedValue();
                };
                b.prototype.toString = function() {
                    return this.$$unwrapTrustedValue().toString();
                };
                return b;
            }
            var e = function(a) {
                throw sa("unsafe");
            };
            c.has("$sanitize") && (e = c.get("$sanitize"));
            var g = d(), f = {};
            f[ea.HTML] = d(g);
            f[ea.CSS] = d(g);
            f[ea.URL] = d(g);
            f[ea.JS] = d(g);
            f[ea.RESOURCE_URL] = d(f[ea.URL]);
            return {
                trustAs: function(a, b) {
                    var c = f.hasOwnProperty(a) ? f[a] : null;
                    if (!c) throw sa("icontext", a, b);
                    if (null === b || b === r || "" === b) return b;
                    if ("string" !== typeof b) throw sa("itype", a);
                    return new c(b);
                },
                getTrusted: function(c, d) {
                    if (null === d || d === r || "" === d) return d;
                    var g = f.hasOwnProperty(c) ? f[c] : null;
                    if (g && d instanceof g) return d.$$unwrapTrustedValue();
                    if (c === ea.RESOURCE_URL) {
                        var g = ya(d.toString()), l, n, p = !1;
                        l = 0;
                        for (n = b.length; l < n; l++) if ("self" === b[l] ? Gb(g) : b[l].exec(g.href)) {
                            p = !0;
                            break;
                        }
                        if (p) for (l = 0, n = a.length; l < n; l++) if ("self" === a[l] ? Gb(g) : a[l].exec(g.href)) {
                            p = !1;
                            break;
                        }
                        if (p) return d;
                        throw sa("insecurl", d.toString());
                    }
                    if (c === ea.HTML) return e(d);
                    throw sa("unsafe");
                },
                valueOf: function(a) {
                    return a instanceof g ? a.$$unwrapTrustedValue() : a;
                }
            };
        } ];
    }
    function Fd() {
        var b = !0;
        this.enabled = function(a) {
            arguments.length && (b = !!a);
            return b;
        };
        this.$get = [ "$parse", "$sniffer", "$sceDelegate", function(a, c, d) {
            if (b && c.msie && 8 > c.msieDocumentMode) throw sa("iequirks");
            var e = fa(ea);
            e.isEnabled = function() {
                return b;
            };
            e.trustAs = d.trustAs;
            e.getTrusted = d.getTrusted;
            e.valueOf = d.valueOf;
            b || (e.trustAs = e.getTrusted = function(a, b) {
                return b;
            }, e.valueOf = Ba);
            e.parseAs = function(b, c) {
                var d = a(c);
                return d.literal && d.constant ? d : function(a, c) {
                    return e.getTrusted(b, d(a, c));
                };
            };
            var g = e.parseAs, f = e.getTrusted, h = e.trustAs;
            q(ea, function(a, b) {
                var c = x(b);
                e[Qa("parse_as_" + c)] = function(b) {
                    return g(a, b);
                };
                e[Qa("get_trusted_" + c)] = function(b) {
                    return f(a, b);
                };
                e[Qa("trust_as_" + c)] = function(b) {
                    return h(a, b);
                };
            });
            return e;
        } ];
    }
    function Gd() {
        this.$get = [ "$window", "$document", function(b, a) {
            var c = {}, d = S((/android (\d+)/.exec(x((b.navigator || {}).userAgent)) || [])[1]), e = /Boxee/i.test((b.navigator || {}).userAgent), g = a[0] || {}, f = g.documentMode, h, m = /^(Moz|webkit|O|ms)(?=[A-Z])/, k = g.body && g.body.style, l = !1, n = !1;
            if (k) {
                for (var p in k) if (l = m.exec(p)) {
                    h = l[0];
                    h = h.substr(0, 1).toUpperCase() + h.substr(1);
                    break;
                }
                h || (h = "WebkitOpacity" in k && "webkit");
                l = !!("transition" in k || h + "Transition" in k);
                n = !!("animation" in k || h + "Animation" in k);
                !d || l && n || (l = D(g.body.style.webkitTransition), n = D(g.body.style.webkitAnimation));
            }
            return {
                history: !(!b.history || !b.history.pushState || 4 > d || e),
                hashchange: "onhashchange" in b && (!f || 7 < f),
                hasEvent: function(a) {
                    if ("input" == a && 9 == M) return !1;
                    if (z(c[a])) {
                        var b = g.createElement("div");
                        c[a] = "on" + a in b;
                    }
                    return c[a];
                },
                csp: Ub(),
                vendorPrefix: h,
                transitions: l,
                animations: n,
                android: d,
                msie: M,
                msieDocumentMode: f
            };
        } ];
    }
    function Hd() {
        this.$get = [ "$rootScope", "$browser", "$q", "$exceptionHandler", function(b, a, c, d) {
            function e(e, h, m) {
                var k = c.defer(), l = k.promise, n = B(m) && !m;
                h = a.defer(function() {
                    try {
                        k.resolve(e());
                    } catch (a) {
                        k.reject(a), d(a);
                    } finally {
                        delete g[l.$$timeoutId];
                    }
                    n || b.$apply();
                }, h);
                l.$$timeoutId = h;
                g[h] = k;
                return l;
            }
            var g = {};
            e.cancel = function(b) {
                return b && b.$$timeoutId in g ? (g[b.$$timeoutId].reject("canceled"), delete g[b.$$timeoutId], 
                a.defer.cancel(b.$$timeoutId)) : !1;
            };
            return e;
        } ];
    }
    function ya(b, a) {
        var c = b;
        M && (Y.setAttribute("href", c), c = Y.href);
        Y.setAttribute("href", c);
        return {
            href: Y.href,
            protocol: Y.protocol ? Y.protocol.replace(/:$/, "") : "",
            host: Y.host,
            search: Y.search ? Y.search.replace(/^\?/, "") : "",
            hash: Y.hash ? Y.hash.replace(/^#/, "") : "",
            hostname: Y.hostname,
            port: Y.port,
            pathname: "/" === Y.pathname.charAt(0) ? Y.pathname : "/" + Y.pathname
        };
    }
    function Gb(b) {
        b = D(b) ? ya(b) : b;
        return b.protocol === Ac.protocol && b.host === Ac.host;
    }
    function Id() {
        this.$get = $(Z);
    }
    function Bc(b) {
        function a(d, e) {
            if (X(d)) {
                var g = {};
                q(d, function(b, c) {
                    g[c] = a(c, b);
                });
                return g;
            }
            return b.factory(d + c, e);
        }
        var c = "Filter";
        this.register = a;
        this.$get = [ "$injector", function(a) {
            return function(b) {
                return a.get(b + c);
            };
        } ];
        a("currency", Cc);
        a("date", Dc);
        a("filter", Jd);
        a("json", Kd);
        a("limitTo", Ld);
        a("lowercase", Md);
        a("number", Ec);
        a("orderBy", Fc);
        a("uppercase", Nd);
    }
    function Jd() {
        return function(b, a, c) {
            if (!K(b)) return b;
            var d = typeof c, e = [];
            e.check = function(a) {
                for (var b = 0; b < e.length; b++) if (!e[b](a)) return !1;
                return !0;
            };
            "function" !== d && (c = "boolean" === d && c ? function(a, b) {
                return Ca.equals(a, b);
            } : function(a, b) {
                b = ("" + b).toLowerCase();
                return -1 < ("" + a).toLowerCase().indexOf(b);
            });
            var g = function(a, b) {
                if ("string" == typeof b && "!" === b.charAt(0)) return !g(a, b.substr(1));
                switch (typeof a) {
                  case "boolean":
                  case "number":
                  case "string":
                    return c(a, b);

                  case "object":
                    switch (typeof b) {
                      case "object":
                        return c(a, b);

                      default:
                        for (var d in a) if ("$" !== d.charAt(0) && g(a[d], b)) return !0;
                    }
                    return !1;

                  case "array":
                    for (d = 0; d < a.length; d++) if (g(a[d], b)) return !0;
                    return !1;

                  default:
                    return !1;
                }
            };
            switch (typeof a) {
              case "boolean":
              case "number":
              case "string":
                a = {
                    $: a
                };

              case "object":
                for (var f in a) (function(b) {
                    "undefined" != typeof a[b] && e.push(function(c) {
                        return g("$" == b ? c : vb(c, b), a[b]);
                    });
                })(f);
                break;

              case "function":
                e.push(a);
                break;

              default:
                return b;
            }
            d = [];
            for (f = 0; f < b.length; f++) {
                var h = b[f];
                e.check(h) && d.push(h);
            }
            return d;
        };
    }
    function Cc(b) {
        var a = b.NUMBER_FORMATS;
        return function(b, d) {
            z(d) && (d = a.CURRENCY_SYM);
            return Gc(b, a.PATTERNS[1], a.GROUP_SEP, a.DECIMAL_SEP, 2).replace(/\u00A4/g, d);
        };
    }
    function Ec(b) {
        var a = b.NUMBER_FORMATS;
        return function(b, d) {
            return Gc(b, a.PATTERNS[0], a.GROUP_SEP, a.DECIMAL_SEP, d);
        };
    }
    function Gc(b, a, c, d, e) {
        if (isNaN(b) || !isFinite(b)) return "";
        var g = 0 > b;
        b = Math.abs(b);
        var f = b + "", h = "", m = [], k = !1;
        if (-1 !== f.indexOf("e")) {
            var l = f.match(/([\d\.]+)e(-?)(\d+)/);
            l && "-" == l[2] && l[3] > e + 1 ? f = "0" : (h = f, k = !0);
        }
        if (k) 0 < e && (-1 < b && 1 > b) && (h = b.toFixed(e)); else {
            f = (f.split(Hc)[1] || "").length;
            z(e) && (e = Math.min(Math.max(a.minFrac, f), a.maxFrac));
            f = Math.pow(10, e);
            b = Math.round(b * f) / f;
            b = ("" + b).split(Hc);
            f = b[0];
            b = b[1] || "";
            var l = 0, n = a.lgSize, p = a.gSize;
            if (f.length >= n + p) for (l = f.length - n, k = 0; k < l; k++) 0 === (l - k) % p && 0 !== k && (h += c), 
            h += f.charAt(k);
            for (k = l; k < f.length; k++) 0 === (f.length - k) % n && 0 !== k && (h += c), 
            h += f.charAt(k);
            for (;b.length < e; ) b += "0";
            e && "0" !== e && (h += d + b.substr(0, e));
        }
        m.push(g ? a.negPre : a.posPre);
        m.push(h);
        m.push(g ? a.negSuf : a.posSuf);
        return m.join("");
    }
    function Mb(b, a, c) {
        var d = "";
        0 > b && (d = "-", b = -b);
        for (b = "" + b; b.length < a; ) b = "0" + b;
        c && (b = b.substr(b.length - a));
        return d + b;
    }
    function W(b, a, c, d) {
        c = c || 0;
        return function(e) {
            e = e["get" + b]();
            if (0 < c || e > -c) e += c;
            0 === e && -12 == c && (e = 12);
            return Mb(e, a, d);
        };
    }
    function lb(b, a) {
        return function(c, d) {
            var e = c["get" + b](), g = Ia(a ? "SHORT" + b : b);
            return d[g][e];
        };
    }
    function Dc(b) {
        function a(a) {
            var b;
            if (b = a.match(c)) {
                a = new Date(0);
                var g = 0, f = 0, h = b[8] ? a.setUTCFullYear : a.setFullYear, m = b[8] ? a.setUTCHours : a.setHours;
                b[9] && (g = S(b[9] + b[10]), f = S(b[9] + b[11]));
                h.call(a, S(b[1]), S(b[2]) - 1, S(b[3]));
                g = S(b[4] || 0) - g;
                f = S(b[5] || 0) - f;
                h = S(b[6] || 0);
                b = Math.round(1e3 * parseFloat("0." + (b[7] || 0)));
                m.call(a, g, f, h, b);
            }
            return a;
        }
        var c = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;
        return function(c, e) {
            var g = "", f = [], h, m;
            e = e || "mediumDate";
            e = b.DATETIME_FORMATS[e] || e;
            D(c) && (c = Od.test(c) ? S(c) : a(c));
            sb(c) && (c = new Date(c));
            if (!La(c)) return c;
            for (;e; ) (m = Pd.exec(e)) ? (f = f.concat(va.call(m, 1)), e = f.pop()) : (f.push(e), 
            e = null);
            q(f, function(a) {
                h = Qd[a];
                g += h ? h(c, b.DATETIME_FORMATS) : a.replace(/(^'|'$)/g, "").replace(/''/g, "'");
            });
            return g;
        };
    }
    function Kd() {
        return function(b) {
            return qa(b, !0);
        };
    }
    function Ld() {
        return function(b, a) {
            if (!K(b) && !D(b)) return b;
            a = S(a);
            if (D(b)) return a ? 0 <= a ? b.slice(0, a) : b.slice(a, b.length) : "";
            var c = [], d, e;
            a > b.length ? a = b.length : a < -b.length && (a = -b.length);
            0 < a ? (d = 0, e = a) : (d = b.length + a, e = b.length);
            for (;d < e; d++) c.push(b[d]);
            return c;
        };
    }
    function Fc(b) {
        return function(a, c, d) {
            function e(a, b) {
                return Oa(b) ? function(b, c) {
                    return a(c, b);
                } : a;
            }
            if (!K(a) || !c) return a;
            c = K(c) ? c : [ c ];
            c = Rc(c, function(a) {
                var c = !1, d = a || Ba;
                if (D(a)) {
                    if ("+" == a.charAt(0) || "-" == a.charAt(0)) c = "-" == a.charAt(0), a = a.substring(1);
                    d = b(a);
                }
                return e(function(a, b) {
                    var c;
                    c = d(a);
                    var e = d(b), g = typeof c, f = typeof e;
                    g == f ? ("string" == g && (c = c.toLowerCase(), e = e.toLowerCase()), c = c === e ? 0 : c < e ? -1 : 1) : c = g < f ? -1 : 1;
                    return c;
                }, c);
            });
            for (var g = [], f = 0; f < a.length; f++) g.push(a[f]);
            return g.sort(e(function(a, b) {
                for (var d = 0; d < c.length; d++) {
                    var e = c[d](a, b);
                    if (0 !== e) return e;
                }
                return 0;
            }, d));
        };
    }
    function ta(b) {
        L(b) && (b = {
            link: b
        });
        b.restrict = b.restrict || "AC";
        return $(b);
    }
    function Ic(b, a) {
        function c(a, c) {
            c = c ? "-" + db(c, "-") : "";
            b.removeClass((a ? mb : nb) + c).addClass((a ? nb : mb) + c);
        }
        var d = this, e = b.parent().controller("form") || ob, g = 0, f = d.$error = {}, h = [];
        d.$name = a.name || a.ngForm;
        d.$dirty = !1;
        d.$pristine = !0;
        d.$valid = !0;
        d.$invalid = !1;
        e.$addControl(d);
        b.addClass(Ja);
        c(!0);
        d.$addControl = function(a) {
            xa(a.$name, "input");
            h.push(a);
            a.$name && (d[a.$name] = a);
        };
        d.$removeControl = function(a) {
            a.$name && d[a.$name] === a && delete d[a.$name];
            q(f, function(b, c) {
                d.$setValidity(c, !0, a);
            });
            Ma(h, a);
        };
        d.$setValidity = function(a, b, h) {
            var n = f[a];
            if (b) n && (Ma(n, h), n.length || (g--, g || (c(b), d.$valid = !0, d.$invalid = !1), 
            f[a] = !1, c(!0, a), e.$setValidity(a, !0, d))); else {
                g || c(b);
                if (n) {
                    if (-1 != bb(n, h)) return;
                } else f[a] = n = [], g++, c(!1, a), e.$setValidity(a, !1, d);
                n.push(h);
                d.$valid = !1;
                d.$invalid = !0;
            }
        };
        d.$setDirty = function() {
            b.removeClass(Ja).addClass(pb);
            d.$dirty = !0;
            d.$pristine = !1;
            e.$setDirty();
        };
        d.$setPristine = function() {
            b.removeClass(pb).addClass(Ja);
            d.$dirty = !1;
            d.$pristine = !0;
            q(h, function(a) {
                a.$setPristine();
            });
        };
    }
    function pa(b, a, c, d) {
        b.$setValidity(a, c);
        return c ? d : r;
    }
    function qb(b, a, c, d, e, g) {
        if (!e.android) {
            var f = !1;
            a.on("compositionstart", function(a) {
                f = !0;
            });
            a.on("compositionend", function() {
                f = !1;
            });
        }
        var h = function() {
            if (!f) {
                var e = a.val();
                Oa(c.ngTrim || "T") && (e = aa(e));
                d.$viewValue !== e && (b.$$phase ? d.$setViewValue(e) : b.$apply(function() {
                    d.$setViewValue(e);
                }));
            }
        };
        if (e.hasEvent("input")) a.on("input", h); else {
            var m, k = function() {
                m || (m = g.defer(function() {
                    h();
                    m = null;
                }));
            };
            a.on("keydown", function(a) {
                a = a.keyCode;
                91 === a || (15 < a && 19 > a || 37 <= a && 40 >= a) || k();
            });
            if (e.hasEvent("paste")) a.on("paste cut", k);
        }
        a.on("change", h);
        d.$render = function() {
            a.val(d.$isEmpty(d.$viewValue) ? "" : d.$viewValue);
        };
        var l = c.ngPattern;
        l && ((e = l.match(/^\/(.*)\/([gim]*)$/)) ? (l = RegExp(e[1], e[2]), e = function(a) {
            return pa(d, "pattern", d.$isEmpty(a) || l.test(a), a);
        }) : e = function(c) {
            var e = b.$eval(l);
            if (!e || !e.test) throw F("ngPattern")("noregexp", l, e, ga(a));
            return pa(d, "pattern", d.$isEmpty(c) || e.test(c), c);
        }, d.$formatters.push(e), d.$parsers.push(e));
        if (c.ngMinlength) {
            var n = S(c.ngMinlength);
            e = function(a) {
                return pa(d, "minlength", d.$isEmpty(a) || a.length >= n, a);
            };
            d.$parsers.push(e);
            d.$formatters.push(e);
        }
        if (c.ngMaxlength) {
            var p = S(c.ngMaxlength);
            e = function(a) {
                return pa(d, "maxlength", d.$isEmpty(a) || a.length <= p, a);
            };
            d.$parsers.push(e);
            d.$formatters.push(e);
        }
    }
    function Nb(b, a) {
        b = "ngClass" + b;
        return function() {
            return {
                restrict: "AC",
                link: function(c, d, e) {
                    function g(b) {
                        if (!0 === a || c.$index % 2 === a) {
                            var d = f(b || "");
                            h ? ua(b, h) || e.$updateClass(d, f(h)) : e.$addClass(d);
                        }
                        h = fa(b);
                    }
                    function f(a) {
                        if (K(a)) return a.join(" ");
                        if (X(a)) {
                            var b = [];
                            q(a, function(a, c) {
                                a && b.push(c);
                            });
                            return b.join(" ");
                        }
                        return a;
                    }
                    var h;
                    c.$watch(e[b], g, !0);
                    e.$observe("class", function(a) {
                        g(c.$eval(e[b]));
                    });
                    "ngClass" !== b && c.$watch("$index", function(d, g) {
                        var h = d & 1;
                        if (h !== g & 1) {
                            var n = f(c.$eval(e[b]));
                            h === a ? e.$addClass(n) : e.$removeClass(n);
                        }
                    });
                }
            };
        };
    }
    var x = function(b) {
        return D(b) ? b.toLowerCase() : b;
    }, Ia = function(b) {
        return D(b) ? b.toUpperCase() : b;
    }, M, A, Da, va = [].slice, Rd = [].push, $a = Object.prototype.toString, Na = F("ng"), Ca = Z.angular || (Z.angular = {}), Ua, Ha, ka = [ "0", "0", "0" ];
    M = S((/msie (\d+)/.exec(x(navigator.userAgent)) || [])[1]);
    isNaN(M) && (M = S((/trident\/.*; rv:(\d+)/.exec(x(navigator.userAgent)) || [])[1]));
    w.$inject = [];
    Ba.$inject = [];
    var aa = function() {
        return String.prototype.trim ? function(b) {
            return D(b) ? b.trim() : b;
        } : function(b) {
            return D(b) ? b.replace(/^\s\s*/, "").replace(/\s\s*$/, "") : b;
        };
    }();
    Ha = 9 > M ? function(b) {
        b = b.nodeName ? b : b[0];
        return b.scopeName && "HTML" != b.scopeName ? Ia(b.scopeName + ":" + b.nodeName) : b.nodeName;
    } : function(b) {
        return b.nodeName ? b.nodeName : b[0].nodeName;
    };
    var Uc = /[A-Z]/g, Sd = {
        full: "1.2.8",
        major: 1,
        minor: 2,
        dot: 8,
        codeName: "interdimensional-cartography"
    }, Ra = O.cache = {}, eb = O.expando = "ng-" + new Date().getTime(), Yc = 1, Jc = Z.document.addEventListener ? function(b, a, c) {
        b.addEventListener(a, c, !1);
    } : function(b, a, c) {
        b.attachEvent("on" + a, c);
    }, Bb = Z.document.removeEventListener ? function(b, a, c) {
        b.removeEventListener(a, c, !1);
    } : function(b, a, c) {
        b.detachEvent("on" + a, c);
    }, Wc = /([\:\-\_]+(.))/g, Xc = /^moz([A-Z])/, yb = F("jqLite"), Ga = O.prototype = {
        ready: function(b) {
            function a() {
                c || (c = !0, b());
            }
            var c = !1;
            "complete" === Q.readyState ? setTimeout(a) : (this.on("DOMContentLoaded", a), O(Z).on("load", a));
        },
        toString: function() {
            var b = [];
            q(this, function(a) {
                b.push("" + a);
            });
            return "[" + b.join(", ") + "]";
        },
        eq: function(b) {
            return 0 <= b ? A(this[b]) : A(this[this.length + b]);
        },
        length: 0,
        push: Rd,
        sort: [].sort,
        splice: [].splice
    }, gb = {};
    q("multiple selected checked disabled readOnly required open".split(" "), function(b) {
        gb[x(b)] = b;
    });
    var gc = {};
    q("input select option textarea button form details".split(" "), function(b) {
        gc[Ia(b)] = !0;
    });
    q({
        data: cc,
        inheritedData: fb,
        scope: function(b) {
            return A(b).data("$scope") || fb(b.parentNode || b, [ "$isolateScope", "$scope" ]);
        },
        isolateScope: function(b) {
            return A(b).data("$isolateScope") || A(b).data("$isolateScopeNoTemplate");
        },
        controller: dc,
        injector: function(b) {
            return fb(b, "$injector");
        },
        removeAttr: function(b, a) {
            b.removeAttribute(a);
        },
        hasClass: Cb,
        css: function(b, a, c) {
            a = Qa(a);
            if (B(c)) b.style[a] = c; else {
                var d;
                8 >= M && (d = b.currentStyle && b.currentStyle[a], "" === d && (d = "auto"));
                d = d || b.style[a];
                8 >= M && (d = "" === d ? r : d);
                return d;
            }
        },
        attr: function(b, a, c) {
            var d = x(a);
            if (gb[d]) if (B(c)) c ? (b[a] = !0, b.setAttribute(a, d)) : (b[a] = !1, b.removeAttribute(d)); else return b[a] || (b.attributes.getNamedItem(a) || w).specified ? d : r; else if (B(c)) b.setAttribute(a, c); else if (b.getAttribute) return b = b.getAttribute(a, 2), 
            null === b ? r : b;
        },
        prop: function(b, a, c) {
            if (B(c)) b[a] = c; else return b[a];
        },
        text: function() {
            function b(b, d) {
                var e = a[b.nodeType];
                if (z(d)) return e ? b[e] : "";
                b[e] = d;
            }
            var a = [];
            9 > M ? (a[1] = "innerText", a[3] = "nodeValue") : a[1] = a[3] = "textContent";
            b.$dv = "";
            return b;
        }(),
        val: function(b, a) {
            if (z(a)) {
                if ("SELECT" === Ha(b) && b.multiple) {
                    var c = [];
                    q(b.options, function(a) {
                        a.selected && c.push(a.value || a.text);
                    });
                    return 0 === c.length ? null : c;
                }
                return b.value;
            }
            b.value = a;
        },
        html: function(b, a) {
            if (z(a)) return b.innerHTML;
            for (var c = 0, d = b.childNodes; c < d.length; c++) Ea(d[c]);
            b.innerHTML = a;
        },
        empty: ec
    }, function(b, a) {
        O.prototype[a] = function(a, d) {
            var e, g;
            if (b !== ec && (2 == b.length && b !== Cb && b !== dc ? a : d) === r) {
                if (X(a)) {
                    for (e = 0; e < this.length; e++) if (b === cc) b(this[e], a); else for (g in a) b(this[e], g, a[g]);
                    return this;
                }
                e = b.$dv;
                g = e === r ? Math.min(this.length, 1) : this.length;
                for (var f = 0; f < g; f++) {
                    var h = b(this[f], a, d);
                    e = e ? e + h : h;
                }
                return e;
            }
            for (e = 0; e < this.length; e++) b(this[e], a, d);
            return this;
        };
    });
    q({
        removeData: ac,
        dealoc: Ea,
        on: function a(c, d, e, g) {
            if (B(g)) throw yb("onargs");
            var f = la(c, "events"), h = la(c, "handle");
            f || la(c, "events", f = {});
            h || la(c, "handle", h = Zc(c, f));
            q(d.split(" "), function(d) {
                var g = f[d];
                if (!g) {
                    if ("mouseenter" == d || "mouseleave" == d) {
                        var l = Q.body.contains || Q.body.compareDocumentPosition ? function(a, c) {
                            var d = 9 === a.nodeType ? a.documentElement : a, e = c && c.parentNode;
                            return a === e || !!(e && 1 === e.nodeType && (d.contains ? d.contains(e) : a.compareDocumentPosition && a.compareDocumentPosition(e) & 16));
                        } : function(a, c) {
                            if (c) for (;c = c.parentNode; ) if (c === a) return !0;
                            return !1;
                        };
                        f[d] = [];
                        a(c, {
                            mouseleave: "mouseout",
                            mouseenter: "mouseover"
                        }[d], function(a) {
                            var c = a.relatedTarget;
                            c && (c === this || l(this, c)) || h(a, d);
                        });
                    } else Jc(c, d, h), f[d] = [];
                    g = f[d];
                }
                g.push(e);
            });
        },
        off: bc,
        one: function(a, c, d) {
            a = A(a);
            a.on(c, function g() {
                a.off(c, d);
                a.off(c, g);
            });
            a.on(c, d);
        },
        replaceWith: function(a, c) {
            var d, e = a.parentNode;
            Ea(a);
            q(new O(c), function(c) {
                d ? e.insertBefore(c, d.nextSibling) : e.replaceChild(c, a);
                d = c;
            });
        },
        children: function(a) {
            var c = [];
            q(a.childNodes, function(a) {
                1 === a.nodeType && c.push(a);
            });
            return c;
        },
        contents: function(a) {
            return a.childNodes || [];
        },
        append: function(a, c) {
            q(new O(c), function(c) {
                1 !== a.nodeType && 11 !== a.nodeType || a.appendChild(c);
            });
        },
        prepend: function(a, c) {
            if (1 === a.nodeType) {
                var d = a.firstChild;
                q(new O(c), function(c) {
                    a.insertBefore(c, d);
                });
            }
        },
        wrap: function(a, c) {
            c = A(c)[0];
            var d = a.parentNode;
            d && d.replaceChild(c, a);
            c.appendChild(a);
        },
        remove: function(a) {
            Ea(a);
            var c = a.parentNode;
            c && c.removeChild(a);
        },
        after: function(a, c) {
            var d = a, e = a.parentNode;
            q(new O(c), function(a) {
                e.insertBefore(a, d.nextSibling);
                d = a;
            });
        },
        addClass: Eb,
        removeClass: Db,
        toggleClass: function(a, c, d) {
            z(d) && (d = !Cb(a, c));
            (d ? Eb : Db)(a, c);
        },
        parent: function(a) {
            return (a = a.parentNode) && 11 !== a.nodeType ? a : null;
        },
        next: function(a) {
            if (a.nextElementSibling) return a.nextElementSibling;
            for (a = a.nextSibling; null != a && 1 !== a.nodeType; ) a = a.nextSibling;
            return a;
        },
        find: function(a, c) {
            return a.getElementsByTagName ? a.getElementsByTagName(c) : [];
        },
        clone: Ab,
        triggerHandler: function(a, c, d) {
            c = (la(a, "events") || {})[c];
            d = d || [];
            var e = [ {
                preventDefault: w,
                stopPropagation: w
            } ];
            q(c, function(c) {
                c.apply(a, e.concat(d));
            });
        }
    }, function(a, c) {
        O.prototype[c] = function(c, e, g) {
            for (var f, h = 0; h < this.length; h++) z(f) ? (f = a(this[h], c, e, g), B(f) && (f = A(f))) : zb(f, a(this[h], c, e, g));
            return B(f) ? f : this;
        };
        O.prototype.bind = O.prototype.on;
        O.prototype.unbind = O.prototype.off;
    });
    Sa.prototype = {
        put: function(a, c) {
            this[Fa(a)] = c;
        },
        get: function(a) {
            return this[Fa(a)];
        },
        remove: function(a) {
            var c = this[a = Fa(a)];
            delete this[a];
            return c;
        }
    };
    var ad = /^function\s*[^\(]*\(\s*([^\)]*)\)/m, bd = /,/, cd = /^\s*(_?)(\S+?)\1\s*$/, $c = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm, Ta = F("$injector"), Td = F("$animate"), Ud = [ "$provide", function(a) {
        this.$$selectors = {};
        this.register = function(c, d) {
            var e = c + "-animation";
            if (c && "." != c.charAt(0)) throw Td("notcsel", c);
            this.$$selectors[c.substr(1)] = e;
            a.factory(e, d);
        };
        this.classNameFilter = function(a) {
            1 === arguments.length && (this.$$classNameFilter = a instanceof RegExp ? a : null);
            return this.$$classNameFilter;
        };
        this.$get = [ "$timeout", function(a) {
            return {
                enter: function(d, e, g, f) {
                    g ? g.after(d) : (e && e[0] || (e = g.parent()), e.append(d));
                    f && a(f, 0, !1);
                },
                leave: function(d, e) {
                    d.remove();
                    e && a(e, 0, !1);
                },
                move: function(a, c, g, f) {
                    this.enter(a, c, g, f);
                },
                addClass: function(d, e, g) {
                    e = D(e) ? e : K(e) ? e.join(" ") : "";
                    q(d, function(a) {
                        Eb(a, e);
                    });
                    g && a(g, 0, !1);
                },
                removeClass: function(d, e, g) {
                    e = D(e) ? e : K(e) ? e.join(" ") : "";
                    q(d, function(a) {
                        Db(a, e);
                    });
                    g && a(g, 0, !1);
                },
                enabled: w
            };
        } ];
    } ], ja = F("$compile");
    jc.$inject = [ "$provide", "$$sanitizeUriProvider" ];
    var id = /^(x[\:\-_]|data[\:\-_])/i, pc = F("$interpolate"), Vd = /^([^\?#]*)(\?([^#]*))?(#(.*))?$/, td = {
        http: 80,
        https: 443,
        ftp: 21
    }, Ib = F("$location");
    uc.prototype = Jb.prototype = tc.prototype = {
        $$html5: !1,
        $$replace: !1,
        absUrl: jb("$$absUrl"),
        url: function(a, c) {
            if (z(a)) return this.$$url;
            var d = Vd.exec(a);
            d[1] && this.path(decodeURIComponent(d[1]));
            (d[2] || d[1]) && this.search(d[3] || "");
            this.hash(d[5] || "", c);
            return this;
        },
        protocol: jb("$$protocol"),
        host: jb("$$host"),
        port: jb("$$port"),
        path: vc("$$path", function(a) {
            return "/" == a.charAt(0) ? a : "/" + a;
        }),
        search: function(a, c) {
            switch (arguments.length) {
              case 0:
                return this.$$search;

              case 1:
                if (D(a)) this.$$search = Xb(a); else if (X(a)) this.$$search = a; else throw Ib("isrcharg");
                break;

              default:
                z(c) || null === c ? delete this.$$search[a] : this.$$search[a] = c;
            }
            this.$$compose();
            return this;
        },
        hash: vc("$$hash", Ba),
        replace: function() {
            this.$$replace = !0;
            return this;
        }
    };
    var za = F("$parse"), yc = {}, ra, Ka = {
        "null": function() {
            return null;
        },
        "true": function() {
            return !0;
        },
        "false": function() {
            return !1;
        },
        undefined: w,
        "+": function(a, c, d, e) {
            d = d(a, c);
            e = e(a, c);
            return B(d) ? B(e) ? d + e : d : B(e) ? e : r;
        },
        "-": function(a, c, d, e) {
            d = d(a, c);
            e = e(a, c);
            return (B(d) ? d : 0) - (B(e) ? e : 0);
        },
        "*": function(a, c, d, e) {
            return d(a, c) * e(a, c);
        },
        "/": function(a, c, d, e) {
            return d(a, c) / e(a, c);
        },
        "%": function(a, c, d, e) {
            return d(a, c) % e(a, c);
        },
        "^": function(a, c, d, e) {
            return d(a, c) ^ e(a, c);
        },
        "=": w,
        "===": function(a, c, d, e) {
            return d(a, c) === e(a, c);
        },
        "!==": function(a, c, d, e) {
            return d(a, c) !== e(a, c);
        },
        "==": function(a, c, d, e) {
            return d(a, c) == e(a, c);
        },
        "!=": function(a, c, d, e) {
            return d(a, c) != e(a, c);
        },
        "<": function(a, c, d, e) {
            return d(a, c) < e(a, c);
        },
        ">": function(a, c, d, e) {
            return d(a, c) > e(a, c);
        },
        "<=": function(a, c, d, e) {
            return d(a, c) <= e(a, c);
        },
        ">=": function(a, c, d, e) {
            return d(a, c) >= e(a, c);
        },
        "&&": function(a, c, d, e) {
            return d(a, c) && e(a, c);
        },
        "||": function(a, c, d, e) {
            return d(a, c) || e(a, c);
        },
        "&": function(a, c, d, e) {
            return d(a, c) & e(a, c);
        },
        "|": function(a, c, d, e) {
            return e(a, c)(a, c, d(a, c));
        },
        "!": function(a, c, d) {
            return !d(a, c);
        }
    }, Wd = {
        n: "\n",
        f: "\f",
        r: "\r",
        t: "	",
        v: "",
        "'": "'",
        '"': '"'
    }, Lb = function(a) {
        this.options = a;
    };
    Lb.prototype = {
        constructor: Lb,
        lex: function(a) {
            this.text = a;
            this.index = 0;
            this.ch = r;
            this.lastCh = ":";
            this.tokens = [];
            var c;
            for (a = []; this.index < this.text.length; ) {
                this.ch = this.text.charAt(this.index);
                if (this.is("\"'")) this.readString(this.ch); else if (this.isNumber(this.ch) || this.is(".") && this.isNumber(this.peek())) this.readNumber(); else if (this.isIdent(this.ch)) this.readIdent(), 
                this.was("{,") && ("{" === a[0] && (c = this.tokens[this.tokens.length - 1])) && (c.json = -1 === c.text.indexOf(".")); else if (this.is("(){}[].,;:?")) this.tokens.push({
                    index: this.index,
                    text: this.ch,
                    json: this.was(":[,") && this.is("{[") || this.is("}]:,")
                }), this.is("{[") && a.unshift(this.ch), this.is("}]") && a.shift(), this.index++; else if (this.isWhitespace(this.ch)) {
                    this.index++;
                    continue;
                } else {
                    var d = this.ch + this.peek(), e = d + this.peek(2), g = Ka[this.ch], f = Ka[d], h = Ka[e];
                    h ? (this.tokens.push({
                        index: this.index,
                        text: e,
                        fn: h
                    }), this.index += 3) : f ? (this.tokens.push({
                        index: this.index,
                        text: d,
                        fn: f
                    }), this.index += 2) : g ? (this.tokens.push({
                        index: this.index,
                        text: this.ch,
                        fn: g,
                        json: this.was("[,:") && this.is("+-")
                    }), this.index += 1) : this.throwError("Unexpected next character ", this.index, this.index + 1);
                }
                this.lastCh = this.ch;
            }
            return this.tokens;
        },
        is: function(a) {
            return -1 !== a.indexOf(this.ch);
        },
        was: function(a) {
            return -1 !== a.indexOf(this.lastCh);
        },
        peek: function(a) {
            a = a || 1;
            return this.index + a < this.text.length ? this.text.charAt(this.index + a) : !1;
        },
        isNumber: function(a) {
            return "0" <= a && "9" >= a;
        },
        isWhitespace: function(a) {
            return " " === a || "\r" === a || "	" === a || "\n" === a || "" === a || " " === a;
        },
        isIdent: function(a) {
            return "a" <= a && "z" >= a || "A" <= a && "Z" >= a || "_" === a || "$" === a;
        },
        isExpOperator: function(a) {
            return "-" === a || "+" === a || this.isNumber(a);
        },
        throwError: function(a, c, d) {
            d = d || this.index;
            c = B(c) ? "s " + c + "-" + this.index + " [" + this.text.substring(c, d) + "]" : " " + d;
            throw za("lexerr", a, c, this.text);
        },
        readNumber: function() {
            for (var a = "", c = this.index; this.index < this.text.length; ) {
                var d = x(this.text.charAt(this.index));
                if ("." == d || this.isNumber(d)) a += d; else {
                    var e = this.peek();
                    if ("e" == d && this.isExpOperator(e)) a += d; else if (this.isExpOperator(d) && e && this.isNumber(e) && "e" == a.charAt(a.length - 1)) a += d; else if (!this.isExpOperator(d) || e && this.isNumber(e) || "e" != a.charAt(a.length - 1)) break; else this.throwError("Invalid exponent");
                }
                this.index++;
            }
            a *= 1;
            this.tokens.push({
                index: c,
                text: a,
                json: !0,
                fn: function() {
                    return a;
                }
            });
        },
        readIdent: function() {
            for (var a = this, c = "", d = this.index, e, g, f, h; this.index < this.text.length; ) {
                h = this.text.charAt(this.index);
                if ("." === h || this.isIdent(h) || this.isNumber(h)) "." === h && (e = this.index), 
                c += h; else break;
                this.index++;
            }
            if (e) for (g = this.index; g < this.text.length; ) {
                h = this.text.charAt(g);
                if ("(" === h) {
                    f = c.substr(e - d + 1);
                    c = c.substr(0, e - d);
                    this.index = g;
                    break;
                }
                if (this.isWhitespace(h)) g++; else break;
            }
            d = {
                index: d,
                text: c
            };
            if (Ka.hasOwnProperty(c)) d.fn = Ka[c], d.json = Ka[c]; else {
                var m = xc(c, this.options, this.text);
                d.fn = t(function(a, c) {
                    return m(a, c);
                }, {
                    assign: function(d, e) {
                        return kb(d, c, e, a.text, a.options);
                    }
                });
            }
            this.tokens.push(d);
            f && (this.tokens.push({
                index: e,
                text: ".",
                json: !1
            }), this.tokens.push({
                index: e + 1,
                text: f,
                json: !1
            }));
        },
        readString: function(a) {
            var c = this.index;
            this.index++;
            for (var d = "", e = a, g = !1; this.index < this.text.length; ) {
                var f = this.text.charAt(this.index), e = e + f;
                if (g) "u" === f ? (f = this.text.substring(this.index + 1, this.index + 5), f.match(/[\da-f]{4}/i) || this.throwError("Invalid unicode escape [\\u" + f + "]"), 
                this.index += 4, d += String.fromCharCode(parseInt(f, 16))) : d = (g = Wd[f]) ? d + g : d + f, 
                g = !1; else if ("\\" === f) g = !0; else {
                    if (f === a) {
                        this.index++;
                        this.tokens.push({
                            index: c,
                            text: e,
                            string: d,
                            json: !0,
                            fn: function() {
                                return d;
                            }
                        });
                        return;
                    }
                    d += f;
                }
                this.index++;
            }
            this.throwError("Unterminated quote", c);
        }
    };
    var Ya = function(a, c, d) {
        this.lexer = a;
        this.$filter = c;
        this.options = d;
    };
    Ya.ZERO = function() {
        return 0;
    };
    Ya.prototype = {
        constructor: Ya,
        parse: function(a, c) {
            this.text = a;
            this.json = c;
            this.tokens = this.lexer.lex(a);
            c && (this.assignment = this.logicalOR, this.functionCall = this.fieldAccess = this.objectIndex = this.filterChain = function() {
                this.throwError("is not valid json", {
                    text: a,
                    index: 0
                });
            });
            var d = c ? this.primary() : this.statements();
            0 !== this.tokens.length && this.throwError("is an unexpected token", this.tokens[0]);
            d.literal = !!d.literal;
            d.constant = !!d.constant;
            return d;
        },
        primary: function() {
            var a;
            if (this.expect("(")) a = this.filterChain(), this.consume(")"); else if (this.expect("[")) a = this.arrayDeclaration(); else if (this.expect("{")) a = this.object(); else {
                var c = this.expect();
                (a = c.fn) || this.throwError("not a primary expression", c);
                c.json && (a.constant = !0, a.literal = !0);
            }
            for (var d; c = this.expect("(", "[", "."); ) "(" === c.text ? (a = this.functionCall(a, d), 
            d = null) : "[" === c.text ? (d = a, a = this.objectIndex(a)) : "." === c.text ? (d = a, 
            a = this.fieldAccess(a)) : this.throwError("IMPOSSIBLE");
            return a;
        },
        throwError: function(a, c) {
            throw za("syntax", c.text, a, c.index + 1, this.text, this.text.substring(c.index));
        },
        peekToken: function() {
            if (0 === this.tokens.length) throw za("ueoe", this.text);
            return this.tokens[0];
        },
        peek: function(a, c, d, e) {
            if (0 < this.tokens.length) {
                var g = this.tokens[0], f = g.text;
                if (f === a || f === c || f === d || f === e || !(a || c || d || e)) return g;
            }
            return !1;
        },
        expect: function(a, c, d, e) {
            return (a = this.peek(a, c, d, e)) ? (this.json && !a.json && this.throwError("is not valid json", a), 
            this.tokens.shift(), a) : !1;
        },
        consume: function(a) {
            this.expect(a) || this.throwError("is unexpected, expecting [" + a + "]", this.peek());
        },
        unaryFn: function(a, c) {
            return t(function(d, e) {
                return a(d, e, c);
            }, {
                constant: c.constant
            });
        },
        ternaryFn: function(a, c, d) {
            return t(function(e, g) {
                return a(e, g) ? c(e, g) : d(e, g);
            }, {
                constant: a.constant && c.constant && d.constant
            });
        },
        binaryFn: function(a, c, d) {
            return t(function(e, g) {
                return c(e, g, a, d);
            }, {
                constant: a.constant && d.constant
            });
        },
        statements: function() {
            for (var a = []; ;) if (0 < this.tokens.length && !this.peek("}", ")", ";", "]") && a.push(this.filterChain()), 
            !this.expect(";")) return 1 === a.length ? a[0] : function(c, d) {
                for (var e, g = 0; g < a.length; g++) {
                    var f = a[g];
                    f && (e = f(c, d));
                }
                return e;
            };
        },
        filterChain: function() {
            for (var a = this.expression(), c; ;) if (c = this.expect("|")) a = this.binaryFn(a, c.fn, this.filter()); else return a;
        },
        filter: function() {
            for (var a = this.expect(), c = this.$filter(a.text), d = []; ;) if (a = this.expect(":")) d.push(this.expression()); else {
                var e = function(a, e, h) {
                    h = [ h ];
                    for (var m = 0; m < d.length; m++) h.push(d[m](a, e));
                    return c.apply(a, h);
                };
                return function() {
                    return e;
                };
            }
        },
        expression: function() {
            return this.assignment();
        },
        assignment: function() {
            var a = this.ternary(), c, d;
            return (d = this.expect("=")) ? (a.assign || this.throwError("implies assignment but [" + this.text.substring(0, d.index) + "] can not be assigned to", d), 
            c = this.ternary(), function(d, g) {
                return a.assign(d, c(d, g), g);
            }) : a;
        },
        ternary: function() {
            var a = this.logicalOR(), c, d;
            if (this.expect("?")) {
                c = this.ternary();
                if (d = this.expect(":")) return this.ternaryFn(a, c, this.ternary());
                this.throwError("expected :", d);
            } else return a;
        },
        logicalOR: function() {
            for (var a = this.logicalAND(), c; ;) if (c = this.expect("||")) a = this.binaryFn(a, c.fn, this.logicalAND()); else return a;
        },
        logicalAND: function() {
            var a = this.equality(), c;
            if (c = this.expect("&&")) a = this.binaryFn(a, c.fn, this.logicalAND());
            return a;
        },
        equality: function() {
            var a = this.relational(), c;
            if (c = this.expect("==", "!=", "===", "!==")) a = this.binaryFn(a, c.fn, this.equality());
            return a;
        },
        relational: function() {
            var a = this.additive(), c;
            if (c = this.expect("<", ">", "<=", ">=")) a = this.binaryFn(a, c.fn, this.relational());
            return a;
        },
        additive: function() {
            for (var a = this.multiplicative(), c; c = this.expect("+", "-"); ) a = this.binaryFn(a, c.fn, this.multiplicative());
            return a;
        },
        multiplicative: function() {
            for (var a = this.unary(), c; c = this.expect("*", "/", "%"); ) a = this.binaryFn(a, c.fn, this.unary());
            return a;
        },
        unary: function() {
            var a;
            return this.expect("+") ? this.primary() : (a = this.expect("-")) ? this.binaryFn(Ya.ZERO, a.fn, this.unary()) : (a = this.expect("!")) ? this.unaryFn(a.fn, this.unary()) : this.primary();
        },
        fieldAccess: function(a) {
            var c = this, d = this.expect().text, e = xc(d, this.options, this.text);
            return t(function(c, d, h) {
                return e(h || a(c, d), d);
            }, {
                assign: function(e, f, h) {
                    return kb(a(e, h), d, f, c.text, c.options);
                }
            });
        },
        objectIndex: function(a) {
            var c = this, d = this.expression();
            this.consume("]");
            return t(function(e, g) {
                var f = a(e, g), h = d(e, g), m;
                if (!f) return r;
                (f = Xa(f[h], c.text)) && (f.then && c.options.unwrapPromises) && (m = f, "$$v" in f || (m.$$v = r, 
                m.then(function(a) {
                    m.$$v = a;
                })), f = f.$$v);
                return f;
            }, {
                assign: function(e, g, f) {
                    var h = d(e, f);
                    return Xa(a(e, f), c.text)[h] = g;
                }
            });
        },
        functionCall: function(a, c) {
            var d = [];
            if (")" !== this.peekToken().text) {
                do d.push(this.expression()); while (this.expect(","));
            }
            this.consume(")");
            var e = this;
            return function(g, f) {
                for (var h = [], m = c ? c(g, f) : g, k = 0; k < d.length; k++) h.push(d[k](g, f));
                k = a(g, f, m) || w;
                Xa(m, e.text);
                Xa(k, e.text);
                h = k.apply ? k.apply(m, h) : k(h[0], h[1], h[2], h[3], h[4]);
                return Xa(h, e.text);
            };
        },
        arrayDeclaration: function() {
            var a = [], c = !0;
            if ("]" !== this.peekToken().text) {
                do {
                    var d = this.expression();
                    a.push(d);
                    d.constant || (c = !1);
                } while (this.expect(","));
            }
            this.consume("]");
            return t(function(c, d) {
                for (var f = [], h = 0; h < a.length; h++) f.push(a[h](c, d));
                return f;
            }, {
                literal: !0,
                constant: c
            });
        },
        object: function() {
            var a = [], c = !0;
            if ("}" !== this.peekToken().text) {
                do {
                    var d = this.expect(), d = d.string || d.text;
                    this.consume(":");
                    var e = this.expression();
                    a.push({
                        key: d,
                        value: e
                    });
                    e.constant || (c = !1);
                } while (this.expect(","));
            }
            this.consume("}");
            return t(function(c, d) {
                for (var e = {}, m = 0; m < a.length; m++) {
                    var k = a[m];
                    e[k.key] = k.value(c, d);
                }
                return e;
            }, {
                literal: !0,
                constant: c
            });
        }
    };
    var Kb = {}, sa = F("$sce"), ea = {
        HTML: "html",
        CSS: "css",
        URL: "url",
        RESOURCE_URL: "resourceUrl",
        JS: "js"
    }, Y = Q.createElement("a"), Ac = ya(Z.location.href, !0);
    Bc.$inject = [ "$provide" ];
    Cc.$inject = [ "$locale" ];
    Ec.$inject = [ "$locale" ];
    var Hc = ".", Qd = {
        yyyy: W("FullYear", 4),
        yy: W("FullYear", 2, 0, !0),
        y: W("FullYear", 1),
        MMMM: lb("Month"),
        MMM: lb("Month", !0),
        MM: W("Month", 2, 1),
        M: W("Month", 1, 1),
        dd: W("Date", 2),
        d: W("Date", 1),
        HH: W("Hours", 2),
        H: W("Hours", 1),
        hh: W("Hours", 2, -12),
        h: W("Hours", 1, -12),
        mm: W("Minutes", 2),
        m: W("Minutes", 1),
        ss: W("Seconds", 2),
        s: W("Seconds", 1),
        sss: W("Milliseconds", 3),
        EEEE: lb("Day"),
        EEE: lb("Day", !0),
        a: function(a, c) {
            return 12 > a.getHours() ? c.AMPMS[0] : c.AMPMS[1];
        },
        Z: function(a) {
            a = -1 * a.getTimezoneOffset();
            return a = (0 <= a ? "+" : "") + (Mb(Math[0 < a ? "floor" : "ceil"](a / 60), 2) + Mb(Math.abs(a % 60), 2));
        }
    }, Pd = /((?:[^yMdHhmsaZE']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z))(.*)/, Od = /^\-?\d+$/;
    Dc.$inject = [ "$locale" ];
    var Md = $(x), Nd = $(Ia);
    Fc.$inject = [ "$parse" ];
    var Xd = $({
        restrict: "E",
        compile: function(a, c) {
            8 >= M && (c.href || c.name || c.$set("href", ""), a.append(Q.createComment("IE fix")));
            if (!c.href && !c.name) return function(a, c) {
                c.on("click", function(a) {
                    c.attr("href") || a.preventDefault();
                });
            };
        }
    }), Ob = {};
    q(gb, function(a, c) {
        if ("multiple" != a) {
            var d = ma("ng-" + c);
            Ob[d] = function() {
                return {
                    priority: 100,
                    link: function(a, g, f) {
                        a.$watch(f[d], function(a) {
                            f.$set(c, !!a);
                        });
                    }
                };
            };
        }
    });
    q([ "src", "srcset", "href" ], function(a) {
        var c = ma("ng-" + a);
        Ob[c] = function() {
            return {
                priority: 99,
                link: function(d, e, g) {
                    g.$observe(c, function(c) {
                        c && (g.$set(a, c), M && e.prop(a, g[a]));
                    });
                }
            };
        };
    });
    var ob = {
        $addControl: w,
        $removeControl: w,
        $setValidity: w,
        $setDirty: w,
        $setPristine: w
    };
    Ic.$inject = [ "$element", "$attrs", "$scope" ];
    var Kc = function(a) {
        return [ "$timeout", function(c) {
            return {
                name: "form",
                restrict: a ? "EAC" : "E",
                controller: Ic,
                compile: function() {
                    return {
                        pre: function(a, e, g, f) {
                            if (!g.action) {
                                var h = function(a) {
                                    a.preventDefault ? a.preventDefault() : a.returnValue = !1;
                                };
                                Jc(e[0], "submit", h);
                                e.on("$destroy", function() {
                                    c(function() {
                                        Bb(e[0], "submit", h);
                                    }, 0, !1);
                                });
                            }
                            var m = e.parent().controller("form"), k = g.name || g.ngForm;
                            k && kb(a, k, f, k);
                            if (m) e.on("$destroy", function() {
                                m.$removeControl(f);
                                k && kb(a, k, r, k);
                                t(f, ob);
                            });
                        }
                    };
                }
            };
        } ];
    }, Yd = Kc(), Zd = Kc(!0), $d = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/, ae = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/, be = /^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/, Lc = {
        text: qb,
        number: function(a, c, d, e, g, f) {
            qb(a, c, d, e, g, f);
            e.$parsers.push(function(a) {
                var c = e.$isEmpty(a);
                if (c || be.test(a)) return e.$setValidity("number", !0), "" === a ? null : c ? a : parseFloat(a);
                e.$setValidity("number", !1);
                return r;
            });
            e.$formatters.push(function(a) {
                return e.$isEmpty(a) ? "" : "" + a;
            });
            d.min && (a = function(a) {
                var c = parseFloat(d.min);
                return pa(e, "min", e.$isEmpty(a) || a >= c, a);
            }, e.$parsers.push(a), e.$formatters.push(a));
            d.max && (a = function(a) {
                var c = parseFloat(d.max);
                return pa(e, "max", e.$isEmpty(a) || a <= c, a);
            }, e.$parsers.push(a), e.$formatters.push(a));
            e.$formatters.push(function(a) {
                return pa(e, "number", e.$isEmpty(a) || sb(a), a);
            });
        },
        url: function(a, c, d, e, g, f) {
            qb(a, c, d, e, g, f);
            a = function(a) {
                return pa(e, "url", e.$isEmpty(a) || $d.test(a), a);
            };
            e.$formatters.push(a);
            e.$parsers.push(a);
        },
        email: function(a, c, d, e, g, f) {
            qb(a, c, d, e, g, f);
            a = function(a) {
                return pa(e, "email", e.$isEmpty(a) || ae.test(a), a);
            };
            e.$formatters.push(a);
            e.$parsers.push(a);
        },
        radio: function(a, c, d, e) {
            z(d.name) && c.attr("name", Za());
            c.on("click", function() {
                c[0].checked && a.$apply(function() {
                    e.$setViewValue(d.value);
                });
            });
            e.$render = function() {
                c[0].checked = d.value == e.$viewValue;
            };
            d.$observe("value", e.$render);
        },
        checkbox: function(a, c, d, e) {
            var g = d.ngTrueValue, f = d.ngFalseValue;
            D(g) || (g = !0);
            D(f) || (f = !1);
            c.on("click", function() {
                a.$apply(function() {
                    e.$setViewValue(c[0].checked);
                });
            });
            e.$render = function() {
                c[0].checked = e.$viewValue;
            };
            e.$isEmpty = function(a) {
                return a !== g;
            };
            e.$formatters.push(function(a) {
                return a === g;
            });
            e.$parsers.push(function(a) {
                return a ? g : f;
            });
        },
        hidden: w,
        button: w,
        submit: w,
        reset: w
    }, Mc = [ "$browser", "$sniffer", function(a, c) {
        return {
            restrict: "E",
            require: "?ngModel",
            link: function(d, e, g, f) {
                f && (Lc[x(g.type)] || Lc.text)(d, e, g, f, c, a);
            }
        };
    } ], nb = "ng-valid", mb = "ng-invalid", Ja = "ng-pristine", pb = "ng-dirty", ce = [ "$scope", "$exceptionHandler", "$attrs", "$element", "$parse", function(a, c, d, e, g) {
        function f(a, c) {
            c = c ? "-" + db(c, "-") : "";
            e.removeClass((a ? mb : nb) + c).addClass((a ? nb : mb) + c);
        }
        this.$modelValue = this.$viewValue = Number.NaN;
        this.$parsers = [];
        this.$formatters = [];
        this.$viewChangeListeners = [];
        this.$pristine = !0;
        this.$dirty = !1;
        this.$valid = !0;
        this.$invalid = !1;
        this.$name = d.name;
        var h = g(d.ngModel), m = h.assign;
        if (!m) throw F("ngModel")("nonassign", d.ngModel, ga(e));
        this.$render = w;
        this.$isEmpty = function(a) {
            return z(a) || "" === a || null === a || a !== a;
        };
        var k = e.inheritedData("$formController") || ob, l = 0, n = this.$error = {};
        e.addClass(Ja);
        f(!0);
        this.$setValidity = function(a, c) {
            n[a] !== !c && (c ? (n[a] && l--, l || (f(!0), this.$valid = !0, this.$invalid = !1)) : (f(!1), 
            this.$invalid = !0, this.$valid = !1, l++), n[a] = !c, f(c, a), k.$setValidity(a, c, this));
        };
        this.$setPristine = function() {
            this.$dirty = !1;
            this.$pristine = !0;
            e.removeClass(pb).addClass(Ja);
        };
        this.$setViewValue = function(d) {
            this.$viewValue = d;
            this.$pristine && (this.$dirty = !0, this.$pristine = !1, e.removeClass(Ja).addClass(pb), 
            k.$setDirty());
            q(this.$parsers, function(a) {
                d = a(d);
            });
            this.$modelValue !== d && (this.$modelValue = d, m(a, d), q(this.$viewChangeListeners, function(a) {
                try {
                    a();
                } catch (d) {
                    c(d);
                }
            }));
        };
        var p = this;
        a.$watch(function() {
            var c = h(a);
            if (p.$modelValue !== c) {
                var d = p.$formatters, e = d.length;
                for (p.$modelValue = c; e--; ) c = d[e](c);
                p.$viewValue !== c && (p.$viewValue = c, p.$render());
            }
            return c;
        });
    } ], de = function() {
        return {
            require: [ "ngModel", "^?form" ],
            controller: ce,
            link: function(a, c, d, e) {
                var g = e[0], f = e[1] || ob;
                f.$addControl(g);
                a.$on("$destroy", function() {
                    f.$removeControl(g);
                });
            }
        };
    }, ee = $({
        require: "ngModel",
        link: function(a, c, d, e) {
            e.$viewChangeListeners.push(function() {
                a.$eval(d.ngChange);
            });
        }
    }), Nc = function() {
        return {
            require: "?ngModel",
            link: function(a, c, d, e) {
                if (e) {
                    d.required = !0;
                    var g = function(a) {
                        if (d.required && e.$isEmpty(a)) e.$setValidity("required", !1); else return e.$setValidity("required", !0), 
                        a;
                    };
                    e.$formatters.push(g);
                    e.$parsers.unshift(g);
                    d.$observe("required", function() {
                        g(e.$viewValue);
                    });
                }
            }
        };
    }, fe = function() {
        return {
            require: "ngModel",
            link: function(a, c, d, e) {
                var g = (a = /\/(.*)\//.exec(d.ngList)) && RegExp(a[1]) || d.ngList || ",";
                e.$parsers.push(function(a) {
                    if (!z(a)) {
                        var c = [];
                        a && q(a.split(g), function(a) {
                            a && c.push(aa(a));
                        });
                        return c;
                    }
                });
                e.$formatters.push(function(a) {
                    return K(a) ? a.join(", ") : r;
                });
                e.$isEmpty = function(a) {
                    return !a || !a.length;
                };
            }
        };
    }, ge = /^(true|false|\d+)$/, he = function() {
        return {
            priority: 100,
            compile: function(a, c) {
                return ge.test(c.ngValue) ? function(a, c, g) {
                    g.$set("value", a.$eval(g.ngValue));
                } : function(a, c, g) {
                    a.$watch(g.ngValue, function(a) {
                        g.$set("value", a);
                    });
                };
            }
        };
    }, ie = ta(function(a, c, d) {
        c.addClass("ng-binding").data("$binding", d.ngBind);
        a.$watch(d.ngBind, function(a) {
            c.text(a == r ? "" : a);
        });
    }), je = [ "$interpolate", function(a) {
        return function(c, d, e) {
            c = a(d.attr(e.$attr.ngBindTemplate));
            d.addClass("ng-binding").data("$binding", c);
            e.$observe("ngBindTemplate", function(a) {
                d.text(a);
            });
        };
    } ], ke = [ "$sce", "$parse", function(a, c) {
        return function(d, e, g) {
            e.addClass("ng-binding").data("$binding", g.ngBindHtml);
            var f = c(g.ngBindHtml);
            d.$watch(function() {
                return (f(d) || "").toString();
            }, function(c) {
                e.html(a.getTrustedHtml(f(d)) || "");
            });
        };
    } ], le = Nb("", !0), me = Nb("Odd", 0), ne = Nb("Even", 1), oe = ta({
        compile: function(a, c) {
            c.$set("ngCloak", r);
            a.removeClass("ng-cloak");
        }
    }), pe = [ function() {
        return {
            scope: !0,
            controller: "@",
            priority: 500
        };
    } ], Oc = {};
    q("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "), function(a) {
        var c = ma("ng-" + a);
        Oc[c] = [ "$parse", function(d) {
            return {
                compile: function(e, g) {
                    var f = d(g[c]);
                    return function(c, d, e) {
                        d.on(x(a), function(a) {
                            c.$apply(function() {
                                f(c, {
                                    $event: a
                                });
                            });
                        });
                    };
                }
            };
        } ];
    });
    var qe = [ "$animate", function(a) {
        return {
            transclude: "element",
            priority: 600,
            terminal: !0,
            restrict: "A",
            $$tlb: !0,
            link: function(c, d, e, g, f) {
                var h, m;
                c.$watch(e.ngIf, function(g) {
                    Oa(g) ? m || (m = c.$new(), f(m, function(c) {
                        c[c.length++] = Q.createComment(" end ngIf: " + e.ngIf + " ");
                        h = {
                            clone: c
                        };
                        a.enter(c, d.parent(), d);
                    })) : (m && (m.$destroy(), m = null), h && (a.leave(wb(h.clone)), h = null));
                });
            }
        };
    } ], re = [ "$http", "$templateCache", "$anchorScroll", "$animate", "$sce", function(a, c, d, e, g) {
        return {
            restrict: "ECA",
            priority: 400,
            terminal: !0,
            transclude: "element",
            controller: Ca.noop,
            compile: function(f, h) {
                var m = h.ngInclude || h.src, k = h.onload || "", l = h.autoscroll;
                return function(f, h, q, r, y) {
                    var A = 0, u, t, H = function() {
                        u && (u.$destroy(), u = null);
                        t && (e.leave(t), t = null);
                    };
                    f.$watch(g.parseAsResourceUrl(m), function(g) {
                        var m = function() {
                            !B(l) || l && !f.$eval(l) || d();
                        }, q = ++A;
                        g ? (a.get(g, {
                            cache: c
                        }).success(function(a) {
                            if (q === A) {
                                var c = f.$new();
                                r.template = a;
                                a = y(c, function(a) {
                                    H();
                                    e.enter(a, null, h, m);
                                });
                                u = c;
                                t = a;
                                u.$emit("$includeContentLoaded");
                                f.$eval(k);
                            }
                        }).error(function() {
                            q === A && H();
                        }), f.$emit("$includeContentRequested")) : (H(), r.template = null);
                    });
                };
            }
        };
    } ], se = [ "$compile", function(a) {
        return {
            restrict: "ECA",
            priority: -400,
            require: "ngInclude",
            link: function(c, d, e, g) {
                d.html(g.template);
                a(d.contents())(c);
            }
        };
    } ], te = ta({
        priority: 450,
        compile: function() {
            return {
                pre: function(a, c, d) {
                    a.$eval(d.ngInit);
                }
            };
        }
    }), ue = ta({
        terminal: !0,
        priority: 1e3
    }), ve = [ "$locale", "$interpolate", function(a, c) {
        var d = /{}/g;
        return {
            restrict: "EA",
            link: function(e, g, f) {
                var h = f.count, m = f.$attr.when && g.attr(f.$attr.when), k = f.offset || 0, l = e.$eval(m) || {}, n = {}, p = c.startSymbol(), s = c.endSymbol(), r = /^when(Minus)?(.+)$/;
                q(f, function(a, c) {
                    r.test(c) && (l[x(c.replace("when", "").replace("Minus", "-"))] = g.attr(f.$attr[c]));
                });
                q(l, function(a, e) {
                    n[e] = c(a.replace(d, p + h + "-" + k + s));
                });
                e.$watch(function() {
                    var c = parseFloat(e.$eval(h));
                    if (isNaN(c)) return "";
                    c in l || (c = a.pluralCat(c - k));
                    return n[c](e, g, !0);
                }, function(a) {
                    g.text(a);
                });
            }
        };
    } ], we = [ "$parse", "$animate", function(a, c) {
        var d = F("ngRepeat");
        return {
            transclude: "element",
            priority: 1e3,
            terminal: !0,
            $$tlb: !0,
            link: function(e, g, f, h, m) {
                var k = f.ngRepeat, l = k.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/), n, p, s, r, y, t, u = {
                    $id: Fa
                };
                if (!l) throw d("iexp", k);
                f = l[1];
                h = l[2];
                (l = l[3]) ? (n = a(l), p = function(a, c, d) {
                    t && (u[t] = a);
                    u[y] = c;
                    u.$index = d;
                    return n(e, u);
                }) : (s = function(a, c) {
                    return Fa(c);
                }, r = function(a) {
                    return a;
                });
                l = f.match(/^(?:([\$\w]+)|\(([\$\w]+)\s*,\s*([\$\w]+)\))$/);
                if (!l) throw d("iidexp", f);
                y = l[3] || l[1];
                t = l[2];
                var B = {};
                e.$watchCollection(h, function(a) {
                    var f, h, l = g[0], n, u = {}, z, P, D, x, T, w, F = [];
                    if (rb(a)) T = a, n = p || s; else {
                        n = p || r;
                        T = [];
                        for (D in a) a.hasOwnProperty(D) && "$" != D.charAt(0) && T.push(D);
                        T.sort();
                    }
                    z = T.length;
                    h = F.length = T.length;
                    for (f = 0; f < h; f++) if (D = a === T ? f : T[f], x = a[D], x = n(D, x, f), xa(x, "`track by` id"), 
                    B.hasOwnProperty(x)) w = B[x], delete B[x], u[x] = w, F[f] = w; else {
                        if (u.hasOwnProperty(x)) throw q(F, function(a) {
                            a && a.scope && (B[a.id] = a);
                        }), d("dupes", k, x);
                        F[f] = {
                            id: x
                        };
                        u[x] = !1;
                    }
                    for (D in B) B.hasOwnProperty(D) && (w = B[D], f = wb(w.clone), c.leave(f), q(f, function(a) {
                        a.$$NG_REMOVED = !0;
                    }), w.scope.$destroy());
                    f = 0;
                    for (h = T.length; f < h; f++) {
                        D = a === T ? f : T[f];
                        x = a[D];
                        w = F[f];
                        F[f - 1] && (l = F[f - 1].clone[F[f - 1].clone.length - 1]);
                        if (w.scope) {
                            P = w.scope;
                            n = l;
                            do n = n.nextSibling; while (n && n.$$NG_REMOVED);
                            w.clone[0] != n && c.move(wb(w.clone), null, A(l));
                            l = w.clone[w.clone.length - 1];
                        } else P = e.$new();
                        P[y] = x;
                        t && (P[t] = D);
                        P.$index = f;
                        P.$first = 0 === f;
                        P.$last = f === z - 1;
                        P.$middle = !(P.$first || P.$last);
                        P.$odd = !(P.$even = 0 === (f & 1));
                        w.scope || m(P, function(a) {
                            a[a.length++] = Q.createComment(" end ngRepeat: " + k + " ");
                            c.enter(a, null, A(l));
                            l = a;
                            w.scope = P;
                            w.clone = a;
                            u[w.id] = w;
                        });
                    }
                    B = u;
                });
            }
        };
    } ], xe = [ "$animate", function(a) {
        return function(c, d, e) {
            c.$watch(e.ngShow, function(c) {
                a[Oa(c) ? "removeClass" : "addClass"](d, "ng-hide");
            });
        };
    } ], ye = [ "$animate", function(a) {
        return function(c, d, e) {
            c.$watch(e.ngHide, function(c) {
                a[Oa(c) ? "addClass" : "removeClass"](d, "ng-hide");
            });
        };
    } ], ze = ta(function(a, c, d) {
        a.$watch(d.ngStyle, function(a, d) {
            d && a !== d && q(d, function(a, d) {
                c.css(d, "");
            });
            a && c.css(a);
        }, !0);
    }), Ae = [ "$animate", function(a) {
        return {
            restrict: "EA",
            require: "ngSwitch",
            controller: [ "$scope", function() {
                this.cases = {};
            } ],
            link: function(c, d, e, g) {
                var f, h, m = [];
                c.$watch(e.ngSwitch || e.on, function(d) {
                    for (var l = 0, n = m.length; l < n; l++) m[l].$destroy(), a.leave(h[l]);
                    h = [];
                    m = [];
                    if (f = g.cases["!" + d] || g.cases["?"]) c.$eval(e.change), q(f, function(d) {
                        var e = c.$new();
                        m.push(e);
                        d.transclude(e, function(c) {
                            var e = d.element;
                            h.push(c);
                            a.enter(c, e.parent(), e);
                        });
                    });
                });
            }
        };
    } ], Be = ta({
        transclude: "element",
        priority: 800,
        require: "^ngSwitch",
        link: function(a, c, d, e, g) {
            e.cases["!" + d.ngSwitchWhen] = e.cases["!" + d.ngSwitchWhen] || [];
            e.cases["!" + d.ngSwitchWhen].push({
                transclude: g,
                element: c
            });
        }
    }), Ce = ta({
        transclude: "element",
        priority: 800,
        require: "^ngSwitch",
        link: function(a, c, d, e, g) {
            e.cases["?"] = e.cases["?"] || [];
            e.cases["?"].push({
                transclude: g,
                element: c
            });
        }
    }), De = ta({
        controller: [ "$element", "$transclude", function(a, c) {
            if (!c) throw F("ngTransclude")("orphan", ga(a));
            this.$transclude = c;
        } ],
        link: function(a, c, d, e) {
            e.$transclude(function(a) {
                c.empty();
                c.append(a);
            });
        }
    }), Ee = [ "$templateCache", function(a) {
        return {
            restrict: "E",
            terminal: !0,
            compile: function(c, d) {
                "text/ng-template" == d.type && a.put(d.id, c[0].text);
            }
        };
    } ], Fe = F("ngOptions"), Ge = $({
        terminal: !0
    }), He = [ "$compile", "$parse", function(a, c) {
        var d = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/, e = {
            $setViewValue: w
        };
        return {
            restrict: "E",
            require: [ "select", "?ngModel" ],
            controller: [ "$element", "$scope", "$attrs", function(a, c, d) {
                var m = this, k = {}, l = e, n;
                m.databound = d.ngModel;
                m.init = function(a, c, d) {
                    l = a;
                    n = d;
                };
                m.addOption = function(c) {
                    xa(c, '"option value"');
                    k[c] = !0;
                    l.$viewValue == c && (a.val(c), n.parent() && n.remove());
                };
                m.removeOption = function(a) {
                    this.hasOption(a) && (delete k[a], l.$viewValue == a && this.renderUnknownOption(a));
                };
                m.renderUnknownOption = function(c) {
                    c = "? " + Fa(c) + " ?";
                    n.val(c);
                    a.prepend(n);
                    a.val(c);
                    n.prop("selected", !0);
                };
                m.hasOption = function(a) {
                    return k.hasOwnProperty(a);
                };
                c.$on("$destroy", function() {
                    m.renderUnknownOption = w;
                });
            } ],
            link: function(e, f, h, m) {
                function k(a, c, d, e) {
                    d.$render = function() {
                        var a = d.$viewValue;
                        e.hasOption(a) ? (x.parent() && x.remove(), c.val(a), "" === a && w.prop("selected", !0)) : z(a) && w ? c.val("") : e.renderUnknownOption(a);
                    };
                    c.on("change", function() {
                        a.$apply(function() {
                            x.parent() && x.remove();
                            d.$setViewValue(c.val());
                        });
                    });
                }
                function l(a, c, d) {
                    var e;
                    d.$render = function() {
                        var a = new Sa(d.$viewValue);
                        q(c.find("option"), function(c) {
                            c.selected = B(a.get(c.value));
                        });
                    };
                    a.$watch(function() {
                        ua(e, d.$viewValue) || (e = fa(d.$viewValue), d.$render());
                    });
                    c.on("change", function() {
                        a.$apply(function() {
                            var a = [];
                            q(c.find("option"), function(c) {
                                c.selected && a.push(c.value);
                            });
                            d.$setViewValue(a);
                        });
                    });
                }
                function n(e, f, g) {
                    function h() {
                        var a = {
                            "": []
                        }, c = [ "" ], d, k, r, t, v;
                        t = g.$modelValue;
                        v = A(e) || [];
                        var C = n ? Pb(v) : v, F, I, z;
                        I = {};
                        r = !1;
                        var E, H;
                        if (s) if (w && K(t)) for (r = new Sa([]), z = 0; z < t.length; z++) I[m] = t[z], 
                        r.put(w(e, I), t[z]); else r = new Sa(t);
                        for (z = 0; F = C.length, z < F; z++) {
                            k = z;
                            if (n) {
                                k = C[z];
                                if ("$" === k.charAt(0)) continue;
                                I[n] = k;
                            }
                            I[m] = v[k];
                            d = p(e, I) || "";
                            (k = a[d]) || (k = a[d] = [], c.push(d));
                            s ? d = B(r.remove(w ? w(e, I) : q(e, I))) : (w ? (d = {}, d[m] = t, d = w(e, d) === w(e, I)) : d = t === q(e, I), 
                            r = r || d);
                            E = l(e, I);
                            E = B(E) ? E : "";
                            k.push({
                                id: w ? w(e, I) : n ? C[z] : z,
                                label: E,
                                selected: d
                            });
                        }
                        s || (y || null === t ? a[""].unshift({
                            id: "",
                            label: "",
                            selected: !r
                        }) : r || a[""].unshift({
                            id: "?",
                            label: "",
                            selected: !0
                        }));
                        I = 0;
                        for (C = c.length; I < C; I++) {
                            d = c[I];
                            k = a[d];
                            x.length <= I ? (t = {
                                element: D.clone().attr("label", d),
                                label: k.label
                            }, v = [ t ], x.push(v), f.append(t.element)) : (v = x[I], t = v[0], t.label != d && t.element.attr("label", t.label = d));
                            E = null;
                            z = 0;
                            for (F = k.length; z < F; z++) r = k[z], (d = v[z + 1]) ? (E = d.element, d.label !== r.label && E.text(d.label = r.label), 
                            d.id !== r.id && E.val(d.id = r.id), E[0].selected !== r.selected && E.prop("selected", d.selected = r.selected)) : ("" === r.id && y ? H = y : (H = u.clone()).val(r.id).attr("selected", r.selected).text(r.label), 
                            v.push({
                                element: H,
                                label: r.label,
                                id: r.id,
                                selected: r.selected
                            }), E ? E.after(H) : t.element.append(H), E = H);
                            for (z++; v.length > z; ) v.pop().element.remove();
                        }
                        for (;x.length > I; ) x.pop()[0].element.remove();
                    }
                    var k;
                    if (!(k = t.match(d))) throw Fe("iexp", t, ga(f));
                    var l = c(k[2] || k[1]), m = k[4] || k[6], n = k[5], p = c(k[3] || ""), q = c(k[2] ? k[1] : m), A = c(k[7]), w = k[8] ? c(k[8]) : null, x = [ [ {
                        element: f,
                        label: ""
                    } ] ];
                    y && (a(y)(e), y.removeClass("ng-scope"), y.remove());
                    f.empty();
                    f.on("change", function() {
                        e.$apply(function() {
                            var a, c = A(e) || [], d = {}, h, k, l, p, t, u, v;
                            if (s) for (k = [], p = 0, u = x.length; p < u; p++) for (a = x[p], l = 1, t = a.length; l < t; l++) {
                                if ((h = a[l].element)[0].selected) {
                                    h = h.val();
                                    n && (d[n] = h);
                                    if (w) for (v = 0; v < c.length && (d[m] = c[v], w(e, d) != h); v++) ; else d[m] = c[h];
                                    k.push(q(e, d));
                                }
                            } else if (h = f.val(), "?" == h) k = r; else if ("" === h) k = null; else if (w) for (v = 0; v < c.length; v++) {
                                if (d[m] = c[v], w(e, d) == h) {
                                    k = q(e, d);
                                    break;
                                }
                            } else d[m] = c[h], n && (d[n] = h), k = q(e, d);
                            g.$setViewValue(k);
                        });
                    });
                    g.$render = h;
                    e.$watch(h);
                }
                if (m[1]) {
                    var p = m[0];
                    m = m[1];
                    var s = h.multiple, t = h.ngOptions, y = !1, w, u = A(Q.createElement("option")), D = A(Q.createElement("optgroup")), x = u.clone();
                    h = 0;
                    for (var v = f.children(), F = v.length; h < F; h++) if ("" === v[h].value) {
                        w = y = v.eq(h);
                        break;
                    }
                    p.init(m, y, x);
                    s && (m.$isEmpty = function(a) {
                        return !a || 0 === a.length;
                    });
                    t ? n(e, f, m) : s ? l(e, f, m) : k(e, f, m, p);
                }
            }
        };
    } ], Ie = [ "$interpolate", function(a) {
        var c = {
            addOption: w,
            removeOption: w
        };
        return {
            restrict: "E",
            priority: 100,
            compile: function(d, e) {
                if (z(e.value)) {
                    var g = a(d.text(), !0);
                    g || e.$set("value", d.text());
                }
                return function(a, d, e) {
                    var k = d.parent(), l = k.data("$selectController") || k.parent().data("$selectController");
                    l && l.databound ? d.prop("selected", !1) : l = c;
                    g ? a.$watch(g, function(a, c) {
                        e.$set("value", a);
                        a !== c && l.removeOption(c);
                        l.addOption(a);
                    }) : l.addOption(e.value);
                    d.on("$destroy", function() {
                        l.removeOption(e.value);
                    });
                };
            }
        };
    } ], Je = $({
        restrict: "E",
        terminal: !0
    });
    (Da = Z.jQuery) ? (A = Da, t(Da.fn, {
        scope: Ga.scope,
        isolateScope: Ga.isolateScope,
        controller: Ga.controller,
        injector: Ga.injector,
        inheritedData: Ga.inheritedData
    }), xb("remove", !0, !0, !1), xb("empty", !1, !1, !1), xb("html", !1, !1, !0)) : A = O;
    Ca.element = A;
    (function(a) {
        t(a, {
            bootstrap: Zb,
            copy: fa,
            extend: t,
            equals: ua,
            element: A,
            forEach: q,
            injector: $b,
            noop: w,
            bind: cb,
            toJson: qa,
            fromJson: Vb,
            identity: Ba,
            isUndefined: z,
            isDefined: B,
            isString: D,
            isFunction: L,
            isObject: X,
            isNumber: sb,
            isElement: Qc,
            isArray: K,
            version: Sd,
            isDate: La,
            lowercase: x,
            uppercase: Ia,
            callbacks: {
                counter: 0
            },
            $$minErr: F,
            $$csp: Ub
        });
        Ua = Vc(Z);
        try {
            Ua("ngLocale");
        } catch (c) {
            Ua("ngLocale", []).provider("$locale", sd);
        }
        Ua("ng", [ "ngLocale" ], [ "$provide", function(a) {
            a.provider({
                $$sanitizeUri: Cd
            });
            a.provider("$compile", jc).directive({
                a: Xd,
                input: Mc,
                textarea: Mc,
                form: Yd,
                script: Ee,
                select: He,
                style: Je,
                option: Ie,
                ngBind: ie,
                ngBindHtml: ke,
                ngBindTemplate: je,
                ngClass: le,
                ngClassEven: ne,
                ngClassOdd: me,
                ngCloak: oe,
                ngController: pe,
                ngForm: Zd,
                ngHide: ye,
                ngIf: qe,
                ngInclude: re,
                ngInit: te,
                ngNonBindable: ue,
                ngPluralize: ve,
                ngRepeat: we,
                ngShow: xe,
                ngStyle: ze,
                ngSwitch: Ae,
                ngSwitchWhen: Be,
                ngSwitchDefault: Ce,
                ngOptions: Ge,
                ngTransclude: De,
                ngModel: de,
                ngList: fe,
                ngChange: ee,
                required: Nc,
                ngRequired: Nc,
                ngValue: he
            }).directive({
                ngInclude: se
            }).directive(Ob).directive(Oc);
            a.provider({
                $anchorScroll: dd,
                $animate: Ud,
                $browser: fd,
                $cacheFactory: gd,
                $controller: jd,
                $document: kd,
                $exceptionHandler: ld,
                $filter: Bc,
                $interpolate: qd,
                $interval: rd,
                $http: md,
                $httpBackend: od,
                $location: ud,
                $log: vd,
                $parse: yd,
                $rootScope: Bd,
                $q: zd,
                $sce: Fd,
                $sceDelegate: Ed,
                $sniffer: Gd,
                $templateCache: hd,
                $timeout: Hd,
                $window: Id
            });
        } ]);
    })(Ca);
    A(Q).ready(function() {
        Tc(Q, Zb);
    });
})(window, document);

!angular.$$csp() && angular.element(document).find("head").prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\\:form{display:block;}</style>');

angular.module("angucomplete", []).directive("angucomplete", function($parse, $http, $sce, $timeout) {
    return {
        restrict: "EA",
        scope: {
            id: "@id",
            placeholder: "@placeholder",
            selectedObject: "=selectedobject",
            url: "@url",
            dataField: "@datafield",
            titleField: "@titlefield",
            descriptionField: "@descriptionfield",
            imageField: "@imagefield",
            imageUri: "@imageuri",
            inputClass: "@inputclass",
            userPause: "@pause",
            localData: "=localdata",
            searchFields: "@searchfields",
            minLengthUser: "@minlength",
            matchClass: "@matchclass"
        },
        template: '<div class="angucomplete-holder"><input id="{{id}}_value" ng-model="searchStr" type="text" placeholder="{{placeholder}}" class="{{inputClass}}" onmouseup="this.select();" ng-focus="resetHideResults()" ng-blur="hideResults()" /><div id="{{id}}_dropdown" class="angucomplete-dropdown" ng-if="showDropdown"><div class="angucomplete-searching" ng-show="searching">Searching...</div><div class="angucomplete-searching" ng-show="!searching && (!results || results.length == 0)">No results found</div><div class="angucomplete-row" ng-repeat="result in results" ng-mousedown="selectResult(result)" ng-mouseover="hoverRow()" ng-class="{\'angucomplete-selected-row\': $index == currentIndex}"><div ng-if="imageField" class="angucomplete-image-holder"><img ng-if="result.image && result.image != \'\'" ng-src="{{result.image}}" class="angucomplete-image"/><div ng-if="!result.image && result.image != \'\'" class="angucomplete-image-default"></div></div><div class="angucomplete-title" ng-if="matchClass" ng-bind-html="result.title"></div><div class="angucomplete-title" ng-if="!matchClass">{{ result.title }}</div><div ng-if="result.description && result.description != \'\'" class="angucomplete-description">{{result.description}}</div></div></div></div>',
        link: function($scope, elem, attrs) {
            $scope.lastSearchTerm = null;
            $scope.currentIndex = null;
            $scope.justChanged = false;
            $scope.searchTimer = null;
            $scope.hideTimer = null;
            $scope.searching = false;
            $scope.pause = 500;
            $scope.minLength = 3;
            $scope.searchStr = null;
            if ($scope.minLengthUser && $scope.minLengthUser != "") {
                $scope.minLength = $scope.minLengthUser;
            }
            if ($scope.userPause) {
                $scope.pause = $scope.userPause;
            }
            isNewSearchNeeded = function(newTerm, oldTerm) {
                return newTerm.length >= $scope.minLength && newTerm != oldTerm;
            };
            $scope.processResults = function(responseData, str) {
                if (responseData && responseData.length > 0) {
                    $scope.results = [];
                    var titleFields = [];
                    if ($scope.titleField && $scope.titleField != "") {
                        titleFields = $scope.titleField.split(",");
                    }
                    for (var i = 0; i < responseData.length; i++) {
                        var titleCode = [];
                        for (var t = 0; t < titleFields.length; t++) {
                            titleCode.push(responseData[i][titleFields[t]]);
                        }
                        var description = "";
                        if ($scope.descriptionField) {
                            description = responseData[i][$scope.descriptionField];
                        }
                        var imageUri = "";
                        if ($scope.imageUri) {
                            imageUri = $scope.imageUri;
                        }
                        var image = "";
                        if ($scope.imageField) {
                            image = imageUri + responseData[i][$scope.imageField];
                        }
                        var text = titleCode.join(" ");
                        if ($scope.matchClass) {
                            var re = new RegExp(str, "i");
                            var strPart = text.match(re)[0];
                            text = $sce.trustAsHtml(text.replace(re, '<span class="' + $scope.matchClass + '">' + strPart + "</span>"));
                        }
                        var resultRow = {
                            title: text,
                            description: description,
                            image: image,
                            originalObject: responseData[i]
                        };
                        $scope.results[$scope.results.length] = resultRow;
                    }
                } else {
                    $scope.results = [];
                }
            };
            $scope.searchTimerComplete = function(str) {
                if (str.length >= $scope.minLength) {
                    if ($scope.localData) {
                        var searchFields = $scope.searchFields.split(",");
                        var matches = [];
                        for (var i = 0; i < $scope.localData.length; i++) {
                            var match = false;
                            for (var s = 0; s < searchFields.length; s++) {
                                match = match || typeof $scope.localData[i][searchFields[s]] === "string" && typeof str === "string" && $scope.localData[i][searchFields[s]].toLowerCase().indexOf(str.toLowerCase()) >= 0;
                            }
                            if (match) {
                                matches[matches.length] = $scope.localData[i];
                            }
                        }
                        $scope.searching = false;
                        $scope.processResults(matches, str);
                    } else {
                        $http.get($scope.url + str, {}).success(function(responseData, status, headers, config) {
                            $scope.searching = false;
                            $scope.processResults($scope.dataField ? responseData[$scope.dataField] : responseData, str);
                        }).error(function(data, status, headers, config) {
                            console.log("error");
                        });
                    }
                }
            };
            $scope.hideResults = function() {
                $scope.hideTimer = $timeout(function() {
                    $scope.showDropdown = false;
                }, $scope.pause);
            };
            $scope.resetHideResults = function() {
                if ($scope.hideTimer) {
                    $timeout.cancel($scope.hideTimer);
                }
            };
            $scope.hoverRow = function(index) {
                $scope.currentIndex = index;
            };
            $scope.keyPressed = function(event) {
                if (!(event.which == 38 || event.which == 40 || event.which == 13)) {
                    if (!$scope.searchStr || $scope.searchStr == "") {
                        $scope.showDropdown = false;
                        $scope.lastSearchTerm = null;
                    } else if (isNewSearchNeeded($scope.searchStr, $scope.lastSearchTerm)) {
                        $scope.lastSearchTerm = $scope.searchStr;
                        $scope.showDropdown = true;
                        $scope.currentIndex = -1;
                        $scope.results = [];
                        if ($scope.searchTimer) {
                            $timeout.cancel($scope.searchTimer);
                        }
                        $scope.searching = true;
                        $scope.searchTimer = $timeout(function() {
                            $scope.searchTimerComplete($scope.searchStr);
                        }, $scope.pause);
                    }
                } else {
                    event.preventDefault();
                }
            };
            $scope.selectResult = function(result) {
                if ($scope.matchClass) {
                    result.title = result.title.toString().replace(/(<([^>]+)>)/gi, "");
                }
                $scope.searchStr = $scope.lastSearchTerm = result.title;
                $scope.selectedObject = result;
                $scope.showDropdown = false;
                $scope.results = [];
            };
            var inputField = elem.find("input");
            inputField.on("keyup", $scope.keyPressed);
            elem.on("keyup", function(event) {
                if (event.which === 40) {
                    if ($scope.results && $scope.currentIndex + 1 < $scope.results.length) {
                        $scope.currentIndex++;
                        $scope.$apply();
                        event.preventDefault;
                        event.stopPropagation();
                    }
                    $scope.$apply();
                } else if (event.which == 38) {
                    if ($scope.currentIndex >= 1) {
                        $scope.currentIndex--;
                        $scope.$apply();
                        event.preventDefault;
                        event.stopPropagation();
                    }
                } else if (event.which == 13) {
                    if ($scope.results && $scope.currentIndex >= 0 && $scope.currentIndex < $scope.results.length) {
                        $scope.selectResult($scope.results[$scope.currentIndex]);
                        $scope.$apply();
                        event.preventDefault;
                        event.stopPropagation();
                    } else {
                        $scope.results = [];
                        $scope.$apply();
                        event.preventDefault;
                        event.stopPropagation();
                    }
                } else if (event.which == 27) {
                    $scope.results = [];
                    $scope.showDropdown = false;
                    $scope.$apply();
                } else if (event.which == 8) {
                    $scope.selectedObject = null;
                    $scope.$apply();
                }
            });
        }
    };
});

(function(E, p, F) {
    "use strict";
    p.module("ngAnimate", [ "ng" ]).config([ "$provide", "$animateProvider", function(R, L) {
        function f(f) {
            for (var l = 0; l < f.length; l++) {
                var h = f[l];
                if (h.nodeType == W) return h;
            }
        }
        var q = p.noop, l = p.forEach, aa = L.$$selectors, W = 1, h = "$$ngAnimateState", C = "ng-animate", k = {
            running: !0
        };
        R.decorator("$animate", [ "$delegate", "$injector", "$sniffer", "$rootElement", "$timeout", "$rootScope", "$document", function(w, E, G, s, M, m, N) {
            function F(a) {
                if (a) {
                    var d = [], b = {};
                    a = a.substr(1).split(".");
                    (G.transitions || G.animations) && a.push("");
                    for (var e = 0; e < a.length; e++) {
                        var g = a[e], f = aa[g];
                        f && !b[g] && (d.push(E.get(f)), b[g] = !0);
                    }
                    return d;
                }
            }
            function r(a, d, b, e, g, k, m) {
                function p(a) {
                    u();
                    if (!0 === a) v(); else {
                        if (a = b.data(h)) a.done = v, b.data(h, a);
                        s(t, "after", v);
                    }
                }
                function s(e, f, g) {
                    var n = f + "End";
                    l(e, function(l, h) {
                        var c = function() {
                            a: {
                                var c = f + "Complete", a = e[h];
                                a[c] = !0;
                                (a[n] || q)();
                                for (a = 0; a < e.length; a++) if (!e[a][c]) break a;
                                g();
                            }
                        };
                        "before" != f || "enter" != a && "move" != a ? l[f] ? l[n] = B ? l[f](b, d, c) : l[f](b, c) : c() : c();
                    });
                }
                function w() {
                    m && M(m, 0, !1);
                }
                function u() {
                    u.hasBeenRun || (u.hasBeenRun = !0, k());
                }
                function v() {
                    if (!v.hasBeenRun) {
                        v.hasBeenRun = !0;
                        var a = b.data(h);
                        a && (B ? D(b) : (a.closeAnimationTimeout = M(function() {
                            D(b);
                        }, 0, !1), b.data(h, a)));
                        w();
                    }
                }
                var n, z, r = f(b);
                r && (n = r.className, z = n + " " + d);
                if (r && O(z)) {
                    z = (" " + z).replace(/\s+/g, ".");
                    e || (e = g ? g.parent() : b.parent());
                    z = F(z);
                    var B = "addClass" == a || "removeClass" == a;
                    g = b.data(h) || {};
                    if (Q(b, e) || 0 === z.length) u(), v(); else {
                        var t = [];
                        B && (g.disabled || g.running && g.structural) || l(z, function(e) {
                            if (!e.allowCancel || e.allowCancel(b, a, d)) {
                                var f = e[a];
                                "leave" == a ? (e = f, f = null) : e = e["before" + a.charAt(0).toUpperCase() + a.substr(1)];
                                t.push({
                                    before: e,
                                    after: f
                                });
                            }
                        });
                        0 === t.length ? (u(), w()) : (e = " " + n + " ", g.running && (M.cancel(g.closeAnimationTimeout), 
                        D(b), I(g.animations), g.beforeComplete ? (g.done || q)(!0) : B && !g.structural && (e = "removeClass" == g.event ? e.replace(g.className, "") : e + g.className + " ")), 
                        n = " " + d + " ", "addClass" == a && 0 <= e.indexOf(n) || "removeClass" == a && -1 == e.indexOf(n) ? (u(), 
                        w()) : (b.addClass(C), b.data(h, {
                            running: !0,
                            event: a,
                            className: d,
                            structural: !B,
                            animations: t,
                            done: p
                        }), s(t, "before", p)));
                    }
                } else u(), v();
            }
            function J(a) {
                a = f(a);
                l(a.querySelectorAll("." + C), function(a) {
                    a = p.element(a);
                    var b = a.data(h);
                    b && (I(b.animations), D(a));
                });
            }
            function I(a) {
                l(a, function(d) {
                    a.beforeComplete || (d.beforeEnd || q)(!0);
                    a.afterComplete || (d.afterEnd || q)(!0);
                });
            }
            function D(a) {
                f(a) == f(s) ? k.disabled || (k.running = !1, k.structural = !1) : (a.removeClass(C), 
                a.removeData(h));
            }
            function Q(a, d) {
                if (k.disabled) return !0;
                if (f(a) == f(s)) return k.disabled || k.running;
                do {
                    if (0 === d.length) break;
                    var b = f(d) == f(s), e = b ? k : d.data(h), e = e && (!!e.disabled || !!e.running);
                    if (b || e) return e;
                    if (b) break;
                } while (d = d.parent());
                return !0;
            }
            s.data(h, k);
            m.$$postDigest(function() {
                m.$$postDigest(function() {
                    k.running = !1;
                });
            });
            var K = L.classNameFilter(), O = K ? function(a) {
                return K.test(a);
            } : function() {
                return !0;
            };
            return {
                enter: function(a, d, b, e) {
                    this.enabled(!1, a);
                    w.enter(a, d, b);
                    m.$$postDigest(function() {
                        r("enter", "ng-enter", a, d, b, q, e);
                    });
                },
                leave: function(a, d) {
                    J(a);
                    this.enabled(!1, a);
                    m.$$postDigest(function() {
                        r("leave", "ng-leave", a, null, null, function() {
                            w.leave(a);
                        }, d);
                    });
                },
                move: function(a, d, b, e) {
                    J(a);
                    this.enabled(!1, a);
                    w.move(a, d, b);
                    m.$$postDigest(function() {
                        r("move", "ng-move", a, d, b, q, e);
                    });
                },
                addClass: function(a, d, b) {
                    r("addClass", d, a, null, null, function() {
                        w.addClass(a, d);
                    }, b);
                },
                removeClass: function(a, d, b) {
                    r("removeClass", d, a, null, null, function() {
                        w.removeClass(a, d);
                    }, b);
                },
                enabled: function(a, d) {
                    switch (arguments.length) {
                      case 2:
                        if (a) D(d); else {
                            var b = d.data(h) || {};
                            b.disabled = !0;
                            d.data(h, b);
                        }
                        break;

                      case 1:
                        k.disabled = !a;
                        break;

                      default:
                        a = !k.disabled;
                    }
                    return !!a;
                }
            };
        } ]);
        L.register("", [ "$window", "$sniffer", "$timeout", function(h, k, G) {
            function s(c, a) {
                G.cancel(V);
                T.push(a);
                var y = f(c);
                c = p.element(y);
                U.push(c);
                y = c.data(n);
                P = Math.max(P, (y.maxDelay + y.maxDuration) * R * B);
                y.animationCount = t;
                V = G(function() {
                    l(T, function(c) {
                        c();
                    });
                    var c = [], a = t;
                    l(U, function(a) {
                        c.push(a);
                    });
                    G(function() {
                        M(c, a);
                        c = null;
                    }, P, !1);
                    T = [];
                    U = [];
                    V = null;
                    H = {};
                    P = 0;
                    t++;
                }, 10, !1);
            }
            function M(c, a) {
                l(c, function(c) {
                    (c = c.data(n)) && c.animationCount == a && (c.closeAnimationFn || q)();
                });
            }
            function m(c, a) {
                var y = a ? H[a] : null;
                if (!y) {
                    var b = 0, d = 0, f = 0, g = 0, n, k, m, p;
                    l(c, function(c) {
                        if (c.nodeType == W) {
                            c = h.getComputedStyle(c) || {};
                            m = c[e + X];
                            b = Math.max(N(m), b);
                            p = c[e + S];
                            n = c[e + Z];
                            d = Math.max(N(n), d);
                            k = c[x + Z];
                            g = Math.max(N(k), g);
                            var a = N(c[x + X]);
                            0 < a && (a *= parseInt(c[x + u], 10) || 1);
                            f = Math.max(a, f);
                        }
                    });
                    y = {
                        total: 0,
                        transitionPropertyStyle: p,
                        transitionDurationStyle: m,
                        transitionDelayStyle: n,
                        transitionDelay: d,
                        transitionDuration: b,
                        animationDelayStyle: k,
                        animationDelay: g,
                        animationDuration: f
                    };
                    a && (H[a] = y);
                }
                return y;
            }
            function N(c) {
                var a = 0;
                c = p.isString(c) ? c.split(/\s*,\s*/) : [];
                l(c, function(c) {
                    a = Math.max(parseFloat(c) || 0, a);
                });
                return a;
            }
            function L(c) {
                var a = c.parent(), b = a.data(v);
                b || (a.data(v, ++Y), b = Y);
                return b + "-" + f(c).className;
            }
            function r(c, a) {
                var b = L(c), d = b + " " + a, g = {}, $ = H[d] ? ++H[d].total : 0;
                if (0 < $) {
                    var h = a + "-stagger", g = b + " " + h;
                    (b = !H[g]) && c.addClass(h);
                    g = m(c, g);
                    b && c.removeClass(h);
                }
                c.addClass(a);
                d = m(c, d);
                h = Math.max(d.transitionDelay, d.animationDelay);
                b = Math.max(d.transitionDuration, d.animationDuration);
                if (0 === b) return c.removeClass(a), !1;
                var k = "";
                0 < d.transitionDuration ? f(c).style[e + S] = "none" : f(c).style[x] = "none 0s";
                l(a.split(" "), function(c, a) {
                    k += (0 < a ? " " : "") + c + "-active";
                });
                c.data(n, {
                    className: a,
                    activeClassName: k,
                    maxDuration: b,
                    maxDelay: h,
                    classes: a + " " + k,
                    timings: d,
                    stagger: g,
                    ii: $
                });
                return !0;
            }
            function J(c) {
                var a = e + S;
                c = f(c);
                c.style[a] && 0 < c.style[a].length && (c.style[a] = "");
            }
            function I(c) {
                var a = x;
                c = f(c);
                c.style[a] && 0 < c.style[a].length && (c.style[a] = "");
            }
            function D(c, a, d) {
                function e(b) {
                    c.off(u, h);
                    c.removeClass(r);
                    b = c;
                    b.removeClass(a);
                    b.removeData(n);
                    b = f(c);
                    for (var d in q) b.style.removeProperty(q[d]);
                }
                function h(a) {
                    a.stopPropagation();
                    var c = a.originalEvent || a;
                    a = c.$manualTimeStamp || c.timeStamp || Date.now();
                    c = parseFloat(c.elapsedTime.toFixed(z));
                    Math.max(a - w, 0) >= v && c >= s && d();
                }
                var k = c.data(n), l = f(c);
                if (-1 != l.className.indexOf(a) && k) {
                    var m = k.timings, p = k.stagger, s = k.maxDuration, r = k.activeClassName, v = Math.max(m.transitionDelay, m.animationDelay) * B, w = Date.now(), u = C + " " + g, t = k.ii, A = "", q = [];
                    if (0 < m.transitionDuration) {
                        var x = m.transitionPropertyStyle;
                        -1 == x.indexOf("all") && (A += b + "transition-property: " + x + ";", A += b + "transition-duration: " + m.transitionDurationStyle + ";", 
                        q.push(b + "transition-property"), q.push(b + "transition-duration"));
                    }
                    0 < t && (0 < p.transitionDelay && 0 === p.transitionDuration && (A += b + "transition-delay: " + Q(m.transitionDelayStyle, p.transitionDelay, t) + "; ", 
                    q.push(b + "transition-delay")), 0 < p.animationDelay && 0 === p.animationDuration && (A += b + "animation-delay: " + Q(m.animationDelayStyle, p.animationDelay, t) + "; ", 
                    q.push(b + "animation-delay")));
                    0 < q.length && (m = l.getAttribute("style") || "", l.setAttribute("style", m + " " + A));
                    c.on(u, h);
                    c.addClass(r);
                    k.closeAnimationFn = function() {
                        e();
                        d();
                    };
                    return e;
                }
                d();
            }
            function Q(a, b, d) {
                var e = "";
                l(a.split(","), function(a, c) {
                    e += (0 < c ? "," : "") + (d * b + parseInt(a, 10)) + "s";
                });
                return e;
            }
            function K(a, b) {
                if (r(a, b)) return function(d) {
                    d && (a.removeClass(b), a.removeData(n));
                };
            }
            function O(a, b, d) {
                if (a.data(n)) return D(a, b, d);
                a.removeClass(b);
                a.removeData(n);
                d();
            }
            function a(a, b, d) {
                var e = K(a, b);
                if (e) {
                    var f = e;
                    s(a, function() {
                        J(a);
                        I(a);
                        f = O(a, b, d);
                    });
                    return function(a) {
                        (f || q)(a);
                    };
                }
                d();
            }
            function d(a, b) {
                var d = "";
                a = p.isArray(a) ? a : a.split(/\s+/);
                l(a, function(a, c) {
                    a && 0 < a.length && (d += (0 < c ? " " : "") + a + b);
                });
                return d;
            }
            var b = "", e, g, x, C;
            E.ontransitionend === F && E.onwebkittransitionend !== F ? (b = "-webkit-", e = "WebkitTransition", 
            g = "webkitTransitionEnd transitionend") : (e = "transition", g = "transitionend");
            E.onanimationend === F && E.onwebkitanimationend !== F ? (b = "-webkit-", x = "WebkitAnimation", 
            C = "webkitAnimationEnd animationend") : (x = "animation", C = "animationend");
            var X = "Duration", S = "Property", Z = "Delay", u = "IterationCount", v = "$$ngAnimateKey", n = "$$ngAnimateCSS3Data", z = 3, R = 1.5, B = 1e3, t = 0, H = {}, Y = 0, T = [], U = [], V, P = 0;
            return {
                allowCancel: function(a, b, e) {
                    var g = (a.data(n) || {}).classes;
                    if (!g || 0 <= [ "enter", "leave", "move" ].indexOf(b)) return !0;
                    var k = a.parent(), h = p.element(f(a).cloneNode());
                    h.attr("style", "position:absolute; top:-9999px; left:-9999px");
                    h.removeAttr("id");
                    h.empty();
                    l(g.split(" "), function(a) {
                        h.removeClass(a);
                    });
                    h.addClass(d(e, "addClass" == b ? "-add" : "-remove"));
                    k.append(h);
                    a = m(h);
                    h.remove();
                    return 0 < Math.max(a.transitionDuration, a.animationDuration);
                },
                enter: function(c, b) {
                    return a(c, "ng-enter", b);
                },
                leave: function(c, b) {
                    return a(c, "ng-leave", b);
                },
                move: function(c, b) {
                    return a(c, "ng-move", b);
                },
                beforeAddClass: function(a, b, e) {
                    if (b = K(a, d(b, "-add"))) return s(a, function() {
                        J(a);
                        I(a);
                        e();
                    }), b;
                    e();
                },
                addClass: function(a, b, e) {
                    return O(a, d(b, "-add"), e);
                },
                beforeRemoveClass: function(a, b, e) {
                    if (b = K(a, d(b, "-remove"))) return s(a, function() {
                        J(a);
                        I(a);
                        e();
                    }), b;
                    e();
                },
                removeClass: function(a, b, e) {
                    return O(a, d(b, "-remove"), e);
                }
            };
        } ]);
    } ]);
})(window, window.angular);

(function(window, angular, undefined) {
    "use strict";
    angular.mock = {};
    angular.mock.$BrowserProvider = function() {
        this.$get = function() {
            return new angular.mock.$Browser();
        };
    };
    angular.mock.$Browser = function() {
        var self = this;
        this.isMock = true;
        self.$$url = "http://server/";
        self.$$lastUrl = self.$$url;
        self.pollFns = [];
        self.$$completeOutstandingRequest = angular.noop;
        self.$$incOutstandingRequestCount = angular.noop;
        self.onUrlChange = function(listener) {
            self.pollFns.push(function() {
                if (self.$$lastUrl != self.$$url) {
                    self.$$lastUrl = self.$$url;
                    listener(self.$$url);
                }
            });
            return listener;
        };
        self.cookieHash = {};
        self.lastCookieHash = {};
        self.deferredFns = [];
        self.deferredNextId = 0;
        self.defer = function(fn, delay) {
            delay = delay || 0;
            self.deferredFns.push({
                time: self.defer.now + delay,
                fn: fn,
                id: self.deferredNextId
            });
            self.deferredFns.sort(function(a, b) {
                return a.time - b.time;
            });
            return self.deferredNextId++;
        };
        self.defer.now = 0;
        self.defer.cancel = function(deferId) {
            var fnIndex;
            angular.forEach(self.deferredFns, function(fn, index) {
                if (fn.id === deferId) fnIndex = index;
            });
            if (fnIndex !== undefined) {
                self.deferredFns.splice(fnIndex, 1);
                return true;
            }
            return false;
        };
        self.defer.flush = function(delay) {
            if (angular.isDefined(delay)) {
                self.defer.now += delay;
            } else {
                if (self.deferredFns.length) {
                    self.defer.now = self.deferredFns[self.deferredFns.length - 1].time;
                } else {
                    throw new Error("No deferred tasks to be flushed");
                }
            }
            while (self.deferredFns.length && self.deferredFns[0].time <= self.defer.now) {
                self.deferredFns.shift().fn();
            }
        };
        self.$$baseHref = "";
        self.baseHref = function() {
            return this.$$baseHref;
        };
    };
    angular.mock.$Browser.prototype = {
        poll: function poll() {
            angular.forEach(this.pollFns, function(pollFn) {
                pollFn();
            });
        },
        addPollFn: function(pollFn) {
            this.pollFns.push(pollFn);
            return pollFn;
        },
        url: function(url, replace) {
            if (url) {
                this.$$url = url;
                return this;
            }
            return this.$$url;
        },
        cookies: function(name, value) {
            if (name) {
                if (angular.isUndefined(value)) {
                    delete this.cookieHash[name];
                } else {
                    if (angular.isString(value) && value.length <= 4096) {
                        this.cookieHash[name] = value;
                    }
                }
            } else {
                if (!angular.equals(this.cookieHash, this.lastCookieHash)) {
                    this.lastCookieHash = angular.copy(this.cookieHash);
                    this.cookieHash = angular.copy(this.cookieHash);
                }
                return this.cookieHash;
            }
        },
        notifyWhenNoOutstandingRequests: function(fn) {
            fn();
        }
    };
    angular.mock.$ExceptionHandlerProvider = function() {
        var handler;
        this.mode = function(mode) {
            switch (mode) {
              case "rethrow":
                handler = function(e) {
                    throw e;
                };
                break;

              case "log":
                var errors = [];
                handler = function(e) {
                    if (arguments.length == 1) {
                        errors.push(e);
                    } else {
                        errors.push([].slice.call(arguments, 0));
                    }
                };
                handler.errors = errors;
                break;

              default:
                throw new Error("Unknown mode '" + mode + "', only 'log'/'rethrow' modes are allowed!");
            }
        };
        this.$get = function() {
            return handler;
        };
        this.mode("rethrow");
    };
    angular.mock.$LogProvider = function() {
        var debug = true;
        function concat(array1, array2, index) {
            return array1.concat(Array.prototype.slice.call(array2, index));
        }
        this.debugEnabled = function(flag) {
            if (angular.isDefined(flag)) {
                debug = flag;
                return this;
            } else {
                return debug;
            }
        };
        this.$get = function() {
            var $log = {
                log: function() {
                    $log.log.logs.push(concat([], arguments, 0));
                },
                warn: function() {
                    $log.warn.logs.push(concat([], arguments, 0));
                },
                info: function() {
                    $log.info.logs.push(concat([], arguments, 0));
                },
                error: function() {
                    $log.error.logs.push(concat([], arguments, 0));
                },
                debug: function() {
                    if (debug) {
                        $log.debug.logs.push(concat([], arguments, 0));
                    }
                }
            };
            $log.reset = function() {
                $log.log.logs = [];
                $log.info.logs = [];
                $log.warn.logs = [];
                $log.error.logs = [];
                $log.debug.logs = [];
            };
            $log.assertEmpty = function() {
                var errors = [];
                angular.forEach([ "error", "warn", "info", "log", "debug" ], function(logLevel) {
                    angular.forEach($log[logLevel].logs, function(log) {
                        angular.forEach(log, function(logItem) {
                            errors.push("MOCK $log (" + logLevel + "): " + String(logItem) + "\n" + (logItem.stack || ""));
                        });
                    });
                });
                if (errors.length) {
                    errors.unshift("Expected $log to be empty! Either a message was logged unexpectedly, or " + "an expected log message was not checked and removed:");
                    errors.push("");
                    throw new Error(errors.join("\n---------\n"));
                }
            };
            $log.reset();
            return $log;
        };
    };
    angular.mock.$IntervalProvider = function() {
        this.$get = [ "$rootScope", "$q", function($rootScope, $q) {
            var repeatFns = [], nextRepeatId = 0, now = 0;
            var $interval = function(fn, delay, count, invokeApply) {
                var deferred = $q.defer(), promise = deferred.promise, iteration = 0, skipApply = angular.isDefined(invokeApply) && !invokeApply;
                count = angular.isDefined(count) ? count : 0, promise.then(null, null, fn);
                promise.$$intervalId = nextRepeatId;
                function tick() {
                    deferred.notify(iteration++);
                    if (count > 0 && iteration >= count) {
                        var fnIndex;
                        deferred.resolve(iteration);
                        angular.forEach(repeatFns, function(fn, index) {
                            if (fn.id === promise.$$intervalId) fnIndex = index;
                        });
                        if (fnIndex !== undefined) {
                            repeatFns.splice(fnIndex, 1);
                        }
                    }
                    if (!skipApply) $rootScope.$apply();
                }
                repeatFns.push({
                    nextTime: now + delay,
                    delay: delay,
                    fn: tick,
                    id: nextRepeatId,
                    deferred: deferred
                });
                repeatFns.sort(function(a, b) {
                    return a.nextTime - b.nextTime;
                });
                nextRepeatId++;
                return promise;
            };
            $interval.cancel = function(promise) {
                var fnIndex;
                angular.forEach(repeatFns, function(fn, index) {
                    if (fn.id === promise.$$intervalId) fnIndex = index;
                });
                if (fnIndex !== undefined) {
                    repeatFns[fnIndex].deferred.reject("canceled");
                    repeatFns.splice(fnIndex, 1);
                    return true;
                }
                return false;
            };
            $interval.flush = function(millis) {
                now += millis;
                while (repeatFns.length && repeatFns[0].nextTime <= now) {
                    var task = repeatFns[0];
                    task.fn();
                    task.nextTime += task.delay;
                    repeatFns.sort(function(a, b) {
                        return a.nextTime - b.nextTime;
                    });
                }
                return millis;
            };
            return $interval;
        } ];
    };
    var R_ISO8061_STR = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?:\:?(\d\d)(?:\:?(\d\d)(?:\.(\d{3}))?)?)?(Z|([+-])(\d\d):?(\d\d)))?$/;
    function jsonStringToDate(string) {
        var match;
        if (match = string.match(R_ISO8061_STR)) {
            var date = new Date(0), tzHour = 0, tzMin = 0;
            if (match[9]) {
                tzHour = int(match[9] + match[10]);
                tzMin = int(match[9] + match[11]);
            }
            date.setUTCFullYear(int(match[1]), int(match[2]) - 1, int(match[3]));
            date.setUTCHours(int(match[4] || 0) - tzHour, int(match[5] || 0) - tzMin, int(match[6] || 0), int(match[7] || 0));
            return date;
        }
        return string;
    }
    function int(str) {
        return parseInt(str, 10);
    }
    function padNumber(num, digits, trim) {
        var neg = "";
        if (num < 0) {
            neg = "-";
            num = -num;
        }
        num = "" + num;
        while (num.length < digits) num = "0" + num;
        if (trim) num = num.substr(num.length - digits);
        return neg + num;
    }
    angular.mock.TzDate = function(offset, timestamp) {
        var self = new Date(0);
        if (angular.isString(timestamp)) {
            var tsStr = timestamp;
            self.origDate = jsonStringToDate(timestamp);
            timestamp = self.origDate.getTime();
            if (isNaN(timestamp)) throw {
                name: "Illegal Argument",
                message: "Arg '" + tsStr + "' passed into TzDate constructor is not a valid date string"
            };
        } else {
            self.origDate = new Date(timestamp);
        }
        var localOffset = new Date(timestamp).getTimezoneOffset();
        self.offsetDiff = localOffset * 60 * 1e3 - offset * 1e3 * 60 * 60;
        self.date = new Date(timestamp + self.offsetDiff);
        self.getTime = function() {
            return self.date.getTime() - self.offsetDiff;
        };
        self.toLocaleDateString = function() {
            return self.date.toLocaleDateString();
        };
        self.getFullYear = function() {
            return self.date.getFullYear();
        };
        self.getMonth = function() {
            return self.date.getMonth();
        };
        self.getDate = function() {
            return self.date.getDate();
        };
        self.getHours = function() {
            return self.date.getHours();
        };
        self.getMinutes = function() {
            return self.date.getMinutes();
        };
        self.getSeconds = function() {
            return self.date.getSeconds();
        };
        self.getMilliseconds = function() {
            return self.date.getMilliseconds();
        };
        self.getTimezoneOffset = function() {
            return offset * 60;
        };
        self.getUTCFullYear = function() {
            return self.origDate.getUTCFullYear();
        };
        self.getUTCMonth = function() {
            return self.origDate.getUTCMonth();
        };
        self.getUTCDate = function() {
            return self.origDate.getUTCDate();
        };
        self.getUTCHours = function() {
            return self.origDate.getUTCHours();
        };
        self.getUTCMinutes = function() {
            return self.origDate.getUTCMinutes();
        };
        self.getUTCSeconds = function() {
            return self.origDate.getUTCSeconds();
        };
        self.getUTCMilliseconds = function() {
            return self.origDate.getUTCMilliseconds();
        };
        self.getDay = function() {
            return self.date.getDay();
        };
        if (self.toISOString) {
            self.toISOString = function() {
                return padNumber(self.origDate.getUTCFullYear(), 4) + "-" + padNumber(self.origDate.getUTCMonth() + 1, 2) + "-" + padNumber(self.origDate.getUTCDate(), 2) + "T" + padNumber(self.origDate.getUTCHours(), 2) + ":" + padNumber(self.origDate.getUTCMinutes(), 2) + ":" + padNumber(self.origDate.getUTCSeconds(), 2) + "." + padNumber(self.origDate.getUTCMilliseconds(), 3) + "Z";
            };
        }
        var unimplementedMethods = [ "getUTCDay", "getYear", "setDate", "setFullYear", "setHours", "setMilliseconds", "setMinutes", "setMonth", "setSeconds", "setTime", "setUTCDate", "setUTCFullYear", "setUTCHours", "setUTCMilliseconds", "setUTCMinutes", "setUTCMonth", "setUTCSeconds", "setYear", "toDateString", "toGMTString", "toJSON", "toLocaleFormat", "toLocaleString", "toLocaleTimeString", "toSource", "toString", "toTimeString", "toUTCString", "valueOf" ];
        angular.forEach(unimplementedMethods, function(methodName) {
            self[methodName] = function() {
                throw new Error("Method '" + methodName + "' is not implemented in the TzDate mock");
            };
        });
        return self;
    };
    angular.mock.TzDate.prototype = Date.prototype;
    angular.mock.animate = angular.module("mock.animate", [ "ng" ]).config([ "$provide", function($provide) {
        $provide.decorator("$animate", function($delegate) {
            var animate = {
                queue: [],
                enabled: $delegate.enabled,
                flushNext: function(name) {
                    var tick = animate.queue.shift();
                    if (!tick) throw new Error("No animation to be flushed");
                    if (tick.method !== name) {
                        throw new Error('The next animation is not "' + name + '", but is "' + tick.method + '"');
                    }
                    tick.fn();
                    return tick;
                }
            };
            angular.forEach([ "enter", "leave", "move", "addClass", "removeClass" ], function(method) {
                animate[method] = function() {
                    var params = arguments;
                    animate.queue.push({
                        method: method,
                        params: params,
                        element: angular.isElement(params[0]) && params[0],
                        parent: angular.isElement(params[1]) && params[1],
                        after: angular.isElement(params[2]) && params[2],
                        fn: function() {
                            $delegate[method].apply($delegate, params);
                        }
                    });
                };
            });
            return animate;
        });
    } ]);
    angular.mock.dump = function(object) {
        return serialize(object);
        function serialize(object) {
            var out;
            if (angular.isElement(object)) {
                object = angular.element(object);
                out = angular.element("<div></div>");
                angular.forEach(object, function(element) {
                    out.append(angular.element(element).clone());
                });
                out = out.html();
            } else if (angular.isArray(object)) {
                out = [];
                angular.forEach(object, function(o) {
                    out.push(serialize(o));
                });
                out = "[ " + out.join(", ") + " ]";
            } else if (angular.isObject(object)) {
                if (angular.isFunction(object.$eval) && angular.isFunction(object.$apply)) {
                    out = serializeScope(object);
                } else if (object instanceof Error) {
                    out = object.stack || "" + object.name + ": " + object.message;
                } else {
                    out = angular.toJson(object, true);
                }
            } else {
                out = String(object);
            }
            return out;
        }
        function serializeScope(scope, offset) {
            offset = offset || "  ";
            var log = [ offset + "Scope(" + scope.$id + "): {" ];
            for (var key in scope) {
                if (Object.prototype.hasOwnProperty.call(scope, key) && !key.match(/^(\$|this)/)) {
                    log.push("  " + key + ": " + angular.toJson(scope[key]));
                }
            }
            var child = scope.$$childHead;
            while (child) {
                log.push(serializeScope(child, offset + "  "));
                child = child.$$nextSibling;
            }
            log.push("}");
            return log.join("\n" + offset);
        }
    };
    angular.mock.$HttpBackendProvider = function() {
        this.$get = [ "$rootScope", createHttpBackendMock ];
    };
    function createHttpBackendMock($rootScope, $delegate, $browser) {
        var definitions = [], expectations = [], responses = [], responsesPush = angular.bind(responses, responses.push), copy = angular.copy;
        function createResponse(status, data, headers) {
            if (angular.isFunction(status)) return status;
            return function() {
                return angular.isNumber(status) ? [ status, data, headers ] : [ 200, status, data ];
            };
        }
        function $httpBackend(method, url, data, callback, headers, timeout, withCredentials) {
            var xhr = new MockXhr(), expectation = expectations[0], wasExpected = false;
            function prettyPrint(data) {
                return angular.isString(data) || angular.isFunction(data) || data instanceof RegExp ? data : angular.toJson(data);
            }
            function wrapResponse(wrapped) {
                if (!$browser && timeout && timeout.then) timeout.then(handleTimeout);
                return handleResponse;
                function handleResponse() {
                    var response = wrapped.response(method, url, data, headers);
                    xhr.$$respHeaders = response[2];
                    callback(copy(response[0]), copy(response[1]), xhr.getAllResponseHeaders());
                }
                function handleTimeout() {
                    for (var i = 0, ii = responses.length; i < ii; i++) {
                        if (responses[i] === handleResponse) {
                            responses.splice(i, 1);
                            callback(-1, undefined, "");
                            break;
                        }
                    }
                }
            }
            if (expectation && expectation.match(method, url)) {
                if (!expectation.matchData(data)) throw new Error("Expected " + expectation + " with different data\n" + "EXPECTED: " + prettyPrint(expectation.data) + "\nGOT:      " + data);
                if (!expectation.matchHeaders(headers)) throw new Error("Expected " + expectation + " with different headers\n" + "EXPECTED: " + prettyPrint(expectation.headers) + "\nGOT:      " + prettyPrint(headers));
                expectations.shift();
                if (expectation.response) {
                    responses.push(wrapResponse(expectation));
                    return;
                }
                wasExpected = true;
            }
            var i = -1, definition;
            while (definition = definitions[++i]) {
                if (definition.match(method, url, data, headers || {})) {
                    if (definition.response) {
                        ($browser ? $browser.defer : responsesPush)(wrapResponse(definition));
                    } else if (definition.passThrough) {
                        $delegate(method, url, data, callback, headers, timeout, withCredentials);
                    } else throw new Error("No response defined !");
                    return;
                }
            }
            throw wasExpected ? new Error("No response defined !") : new Error("Unexpected request: " + method + " " + url + "\n" + (expectation ? "Expected " + expectation : "No more request expected"));
        }
        $httpBackend.when = function(method, url, data, headers) {
            var definition = new MockHttpExpectation(method, url, data, headers), chain = {
                respond: function(status, data, headers) {
                    definition.response = createResponse(status, data, headers);
                }
            };
            if ($browser) {
                chain.passThrough = function() {
                    definition.passThrough = true;
                };
            }
            definitions.push(definition);
            return chain;
        };
        createShortMethods("when");
        $httpBackend.expect = function(method, url, data, headers) {
            var expectation = new MockHttpExpectation(method, url, data, headers);
            expectations.push(expectation);
            return {
                respond: function(status, data, headers) {
                    expectation.response = createResponse(status, data, headers);
                }
            };
        };
        createShortMethods("expect");
        $httpBackend.flush = function(count) {
            $rootScope.$digest();
            if (!responses.length) throw new Error("No pending request to flush !");
            if (angular.isDefined(count)) {
                while (count--) {
                    if (!responses.length) throw new Error("No more pending request to flush !");
                    responses.shift()();
                }
            } else {
                while (responses.length) {
                    responses.shift()();
                }
            }
            $httpBackend.verifyNoOutstandingExpectation();
        };
        $httpBackend.verifyNoOutstandingExpectation = function() {
            $rootScope.$digest();
            if (expectations.length) {
                throw new Error("Unsatisfied requests: " + expectations.join(", "));
            }
        };
        $httpBackend.verifyNoOutstandingRequest = function() {
            if (responses.length) {
                throw new Error("Unflushed requests: " + responses.length);
            }
        };
        $httpBackend.resetExpectations = function() {
            expectations.length = 0;
            responses.length = 0;
        };
        return $httpBackend;
        function createShortMethods(prefix) {
            angular.forEach([ "GET", "DELETE", "JSONP" ], function(method) {
                $httpBackend[prefix + method] = function(url, headers) {
                    return $httpBackend[prefix](method, url, undefined, headers);
                };
            });
            angular.forEach([ "PUT", "POST", "PATCH" ], function(method) {
                $httpBackend[prefix + method] = function(url, data, headers) {
                    return $httpBackend[prefix](method, url, data, headers);
                };
            });
        }
    }
    function MockHttpExpectation(method, url, data, headers) {
        this.data = data;
        this.headers = headers;
        this.match = function(m, u, d, h) {
            if (method != m) return false;
            if (!this.matchUrl(u)) return false;
            if (angular.isDefined(d) && !this.matchData(d)) return false;
            if (angular.isDefined(h) && !this.matchHeaders(h)) return false;
            return true;
        };
        this.matchUrl = function(u) {
            if (!url) return true;
            if (angular.isFunction(url.test)) return url.test(u);
            return url == u;
        };
        this.matchHeaders = function(h) {
            if (angular.isUndefined(headers)) return true;
            if (angular.isFunction(headers)) return headers(h);
            return angular.equals(headers, h);
        };
        this.matchData = function(d) {
            if (angular.isUndefined(data)) return true;
            if (data && angular.isFunction(data.test)) return data.test(d);
            if (data && angular.isFunction(data)) return data(d);
            if (data && !angular.isString(data)) return angular.equals(data, angular.fromJson(d));
            return data == d;
        };
        this.toString = function() {
            return method + " " + url;
        };
    }
    function createMockXhr() {
        return new MockXhr();
    }
    function MockXhr() {
        MockXhr.$$lastInstance = this;
        this.open = function(method, url, async) {
            this.$$method = method;
            this.$$url = url;
            this.$$async = async;
            this.$$reqHeaders = {};
            this.$$respHeaders = {};
        };
        this.send = function(data) {
            this.$$data = data;
        };
        this.setRequestHeader = function(key, value) {
            this.$$reqHeaders[key] = value;
        };
        this.getResponseHeader = function(name) {
            var header = this.$$respHeaders[name];
            if (header) return header;
            name = angular.lowercase(name);
            header = this.$$respHeaders[name];
            if (header) return header;
            header = undefined;
            angular.forEach(this.$$respHeaders, function(headerVal, headerName) {
                if (!header && angular.lowercase(headerName) == name) header = headerVal;
            });
            return header;
        };
        this.getAllResponseHeaders = function() {
            var lines = [];
            angular.forEach(this.$$respHeaders, function(value, key) {
                lines.push(key + ": " + value);
            });
            return lines.join("\n");
        };
        this.abort = angular.noop;
    }
    angular.mock.$TimeoutDecorator = function($delegate, $browser) {
        $delegate.flush = function(delay) {
            $browser.defer.flush(delay);
        };
        $delegate.verifyNoPendingTasks = function() {
            if ($browser.deferredFns.length) {
                throw new Error("Deferred tasks to flush (" + $browser.deferredFns.length + "): " + formatPendingTasksAsString($browser.deferredFns));
            }
        };
        function formatPendingTasksAsString(tasks) {
            var result = [];
            angular.forEach(tasks, function(task) {
                result.push("{id: " + task.id + ", " + "time: " + task.time + "}");
            });
            return result.join(", ");
        }
        return $delegate;
    };
    angular.mock.$RootElementProvider = function() {
        this.$get = function() {
            return angular.element("<div ng-app></div>");
        };
    };
    angular.module("ngMock", [ "ng" ]).provider({
        $browser: angular.mock.$BrowserProvider,
        $exceptionHandler: angular.mock.$ExceptionHandlerProvider,
        $log: angular.mock.$LogProvider,
        $interval: angular.mock.$IntervalProvider,
        $httpBackend: angular.mock.$HttpBackendProvider,
        $rootElement: angular.mock.$RootElementProvider
    }).config([ "$provide", function($provide) {
        $provide.decorator("$timeout", angular.mock.$TimeoutDecorator);
    } ]);
    angular.module("ngMockE2E", [ "ng" ]).config([ "$provide", function($provide) {
        $provide.decorator("$httpBackend", angular.mock.e2e.$httpBackendDecorator);
    } ]);
    angular.mock.e2e = {};
    angular.mock.e2e.$httpBackendDecorator = [ "$rootScope", "$delegate", "$browser", createHttpBackendMock ];
    angular.mock.clearDataCache = function() {
        var key, cache = angular.element.cache;
        for (key in cache) {
            if (Object.prototype.hasOwnProperty.call(cache, key)) {
                var handle = cache[key].handle;
                handle && angular.element(handle.elem).off();
                delete cache[key];
            }
        }
    };
    if (window.jasmine || window.mocha) {
        var currentSpec = null, isSpecRunning = function() {
            return currentSpec && (window.mocha || currentSpec.queue.running);
        };
        beforeEach(function() {
            currentSpec = this;
        });
        afterEach(function() {
            var injector = currentSpec.$injector;
            currentSpec.$injector = null;
            currentSpec.$modules = null;
            currentSpec = null;
            if (injector) {
                injector.get("$rootElement").off();
                injector.get("$browser").pollFns.length = 0;
            }
            angular.mock.clearDataCache();
            angular.forEach(angular.element.fragments, function(val, key) {
                delete angular.element.fragments[key];
            });
            MockXhr.$$lastInstance = null;
            angular.forEach(angular.callbacks, function(val, key) {
                delete angular.callbacks[key];
            });
            angular.callbacks.counter = 0;
        });
        window.module = angular.mock.module = function() {
            var moduleFns = Array.prototype.slice.call(arguments, 0);
            return isSpecRunning() ? workFn() : workFn;
            function workFn() {
                if (currentSpec.$injector) {
                    throw new Error("Injector already created, can not register a module!");
                } else {
                    var modules = currentSpec.$modules || (currentSpec.$modules = []);
                    angular.forEach(moduleFns, function(module) {
                        if (angular.isObject(module) && !angular.isArray(module)) {
                            modules.push(function($provide) {
                                angular.forEach(module, function(value, key) {
                                    $provide.value(key, value);
                                });
                            });
                        } else {
                            modules.push(module);
                        }
                    });
                }
            }
        };
        var ErrorAddingDeclarationLocationStack = function(e, errorForStack) {
            this.message = e.message;
            this.name = e.name;
            if (e.line) this.line = e.line;
            if (e.sourceId) this.sourceId = e.sourceId;
            if (e.stack && errorForStack) this.stack = e.stack + "\n" + errorForStack.stack;
            if (e.stackArray) this.stackArray = e.stackArray;
        };
        ErrorAddingDeclarationLocationStack.prototype.toString = Error.prototype.toString;
        window.inject = angular.mock.inject = function() {
            var blockFns = Array.prototype.slice.call(arguments, 0);
            var errorForStack = new Error("Declaration Location");
            return isSpecRunning() ? workFn() : workFn;
            function workFn() {
                var modules = currentSpec.$modules || [];
                modules.unshift("ngMock");
                modules.unshift("ng");
                var injector = currentSpec.$injector;
                if (!injector) {
                    injector = currentSpec.$injector = angular.injector(modules);
                }
                for (var i = 0, ii = blockFns.length; i < ii; i++) {
                    try {
                        injector.invoke(blockFns[i] || angular.noop, this);
                    } catch (e) {
                        if (e.stack && errorForStack) {
                            throw new ErrorAddingDeclarationLocationStack(e, errorForStack);
                        }
                        throw e;
                    } finally {
                        errorForStack = null;
                    }
                }
            }
        };
    }
})(window, window.angular);

(function(H, a, A) {
    "use strict";
    function D(p, g) {
        g = g || {};
        a.forEach(g, function(a, c) {
            delete g[c];
        });
        for (var c in p) p.hasOwnProperty(c) && ("$" !== c.charAt(0) && "$" !== c.charAt(1)) && (g[c] = p[c]);
        return g;
    }
    var v = a.$$minErr("$resource"), C = /^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;
    a.module("ngResource", [ "ng" ]).factory("$resource", [ "$http", "$q", function(p, g) {
        function c(a, c) {
            this.template = a;
            this.defaults = c || {};
            this.urlParams = {};
        }
        function t(n, w, l) {
            function r(h, d) {
                var e = {};
                d = x({}, w, d);
                s(d, function(b, d) {
                    u(b) && (b = b());
                    var k;
                    if (b && b.charAt && "@" == b.charAt(0)) {
                        k = h;
                        var a = b.substr(1);
                        if (null == a || "" === a || "hasOwnProperty" === a || !C.test("." + a)) throw v("badmember", a);
                        for (var a = a.split("."), f = 0, c = a.length; f < c && k !== A; f++) {
                            var g = a[f];
                            k = null !== k ? k[g] : A;
                        }
                    } else k = b;
                    e[d] = k;
                });
                return e;
            }
            function e(a) {
                return a.resource;
            }
            function f(a) {
                D(a || {}, this);
            }
            var F = new c(n);
            l = x({}, B, l);
            s(l, function(h, d) {
                var c = /^(POST|PUT|PATCH)$/i.test(h.method);
                f[d] = function(b, d, k, w) {
                    var q = {}, n, l, y;
                    switch (arguments.length) {
                      case 4:
                        y = w, l = k;

                      case 3:
                      case 2:
                        if (u(d)) {
                            if (u(b)) {
                                l = b;
                                y = d;
                                break;
                            }
                            l = d;
                            y = k;
                        } else {
                            q = b;
                            n = d;
                            l = k;
                            break;
                        }

                      case 1:
                        u(b) ? l = b : c ? n = b : q = b;
                        break;

                      case 0:
                        break;

                      default:
                        throw v("badargs", arguments.length);
                    }
                    var t = this instanceof f, m = t ? n : h.isArray ? [] : new f(n), z = {}, B = h.interceptor && h.interceptor.response || e, C = h.interceptor && h.interceptor.responseError || A;
                    s(h, function(a, b) {
                        "params" != b && ("isArray" != b && "interceptor" != b) && (z[b] = G(a));
                    });
                    c && (z.data = n);
                    F.setUrlParams(z, x({}, r(n, h.params || {}), q), h.url);
                    q = p(z).then(function(b) {
                        var d = b.data, k = m.$promise;
                        if (d) {
                            if (a.isArray(d) !== !!h.isArray) throw v("badcfg", h.isArray ? "array" : "object", a.isArray(d) ? "array" : "object");
                            h.isArray ? (m.length = 0, s(d, function(b) {
                                m.push(new f(b));
                            })) : (D(d, m), m.$promise = k);
                        }
                        m.$resolved = !0;
                        b.resource = m;
                        return b;
                    }, function(b) {
                        m.$resolved = !0;
                        (y || E)(b);
                        return g.reject(b);
                    });
                    q = q.then(function(b) {
                        var a = B(b);
                        (l || E)(a, b.headers);
                        return a;
                    }, C);
                    return t ? q : (m.$promise = q, m.$resolved = !1, m);
                };
                f.prototype["$" + d] = function(b, a, k) {
                    u(b) && (k = a, a = b, b = {});
                    b = f[d].call(this, b, this, a, k);
                    return b.$promise || b;
                };
            });
            f.bind = function(a) {
                return t(n, x({}, w, a), l);
            };
            return f;
        }
        var B = {
            get: {
                method: "GET"
            },
            save: {
                method: "POST"
            },
            query: {
                method: "GET",
                isArray: !0
            },
            remove: {
                method: "DELETE"
            },
            "delete": {
                method: "DELETE"
            }
        }, E = a.noop, s = a.forEach, x = a.extend, G = a.copy, u = a.isFunction;
        c.prototype = {
            setUrlParams: function(c, g, l) {
                var r = this, e = l || r.template, f, p, h = r.urlParams = {};
                s(e.split(/\W/), function(a) {
                    if ("hasOwnProperty" === a) throw v("badname");
                    !/^\d+$/.test(a) && (a && RegExp("(^|[^\\\\]):" + a + "(\\W|$)").test(e)) && (h[a] = !0);
                });
                e = e.replace(/\\:/g, ":");
                g = g || {};
                s(r.urlParams, function(d, c) {
                    f = g.hasOwnProperty(c) ? g[c] : r.defaults[c];
                    a.isDefined(f) && null !== f ? (p = encodeURIComponent(f).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "%20").replace(/%26/gi, "&").replace(/%3D/gi, "=").replace(/%2B/gi, "+"), 
                    e = e.replace(RegExp(":" + c + "(\\W|$)", "g"), p + "$1")) : e = e.replace(RegExp("(/?):" + c + "(\\W|$)", "g"), function(a, c, d) {
                        return "/" == d.charAt(0) ? d : c + d;
                    });
                });
                e = e.replace(/\/+$/, "") || "/";
                e = e.replace(/\/\.(?=\w+($|\?))/, ".");
                c.url = e.replace(/\/\\\./, "/.");
                s(g, function(a, e) {
                    r.urlParams[e] || (c.params = c.params || {}, c.params[e] = a);
                });
            }
        };
        return t;
    } ]);
})(window, window.angular);

(function(h, e, A) {
    "use strict";
    function u(w, q, k) {
        return {
            restrict: "ECA",
            terminal: !0,
            priority: 400,
            transclude: "element",
            link: function(a, c, b, f, n) {
                function y() {
                    l && (l.$destroy(), l = null);
                    g && (k.leave(g), g = null);
                }
                function v() {
                    var b = w.current && w.current.locals;
                    if (e.isDefined(b && b.$template)) {
                        var b = a.$new(), f = w.current;
                        g = n(b, function(d) {
                            k.enter(d, null, g || c, function() {
                                !e.isDefined(t) || t && !a.$eval(t) || q();
                            });
                            y();
                        });
                        l = f.scope = b;
                        l.$emit("$viewContentLoaded");
                        l.$eval(h);
                    } else y();
                }
                var l, g, t = b.autoscroll, h = b.onload || "";
                a.$on("$routeChangeSuccess", v);
                v();
            }
        };
    }
    function z(e, h, k) {
        return {
            restrict: "ECA",
            priority: -400,
            link: function(a, c) {
                var b = k.current, f = b.locals;
                c.html(f.$template);
                var n = e(c.contents());
                b.controller && (f.$scope = a, f = h(b.controller, f), b.controllerAs && (a[b.controllerAs] = f), 
                c.data("$ngControllerController", f), c.children().data("$ngControllerController", f));
                n(a);
            }
        };
    }
    h = e.module("ngRoute", [ "ng" ]).provider("$route", function() {
        function h(a, c) {
            return e.extend(new (e.extend(function() {}, {
                prototype: a
            }))(), c);
        }
        function q(a, e) {
            var b = e.caseInsensitiveMatch, f = {
                originalPath: a,
                regexp: a
            }, h = f.keys = [];
            a = a.replace(/([().])/g, "\\$1").replace(/(\/)?:(\w+)([\?|\*])?/g, function(a, e, b, c) {
                a = "?" === c ? c : null;
                c = "*" === c ? c : null;
                h.push({
                    name: b,
                    optional: !!a
                });
                e = e || "";
                return "" + (a ? "" : e) + "(?:" + (a ? e : "") + (c && "(.+?)" || "([^/]+)") + (a || "") + ")" + (a || "");
            }).replace(/([\/$\*])/g, "\\$1");
            f.regexp = RegExp("^" + a + "$", b ? "i" : "");
            return f;
        }
        var k = {};
        this.when = function(a, c) {
            k[a] = e.extend({
                reloadOnSearch: !0
            }, c, a && q(a, c));
            if (a) {
                var b = "/" == a[a.length - 1] ? a.substr(0, a.length - 1) : a + "/";
                k[b] = e.extend({
                    redirectTo: a
                }, q(b, c));
            }
            return this;
        };
        this.otherwise = function(a) {
            this.when(null, a);
            return this;
        };
        this.$get = [ "$rootScope", "$location", "$routeParams", "$q", "$injector", "$http", "$templateCache", "$sce", function(a, c, b, f, n, q, v, l) {
            function g() {
                var d = t(), m = r.current;
                if (d && m && d.$$route === m.$$route && e.equals(d.pathParams, m.pathParams) && !d.reloadOnSearch && !x) m.params = d.params, 
                e.copy(m.params, b), a.$broadcast("$routeUpdate", m); else if (d || m) x = !1, a.$broadcast("$routeChangeStart", d, m), 
                (r.current = d) && d.redirectTo && (e.isString(d.redirectTo) ? c.path(u(d.redirectTo, d.params)).search(d.params).replace() : c.url(d.redirectTo(d.pathParams, c.path(), c.search())).replace()), 
                f.when(d).then(function() {
                    if (d) {
                        var a = e.extend({}, d.resolve), c, b;
                        e.forEach(a, function(d, c) {
                            a[c] = e.isString(d) ? n.get(d) : n.invoke(d);
                        });
                        e.isDefined(c = d.template) ? e.isFunction(c) && (c = c(d.params)) : e.isDefined(b = d.templateUrl) && (e.isFunction(b) && (b = b(d.params)), 
                        b = l.getTrustedResourceUrl(b), e.isDefined(b) && (d.loadedTemplateUrl = b, c = q.get(b, {
                            cache: v
                        }).then(function(a) {
                            return a.data;
                        })));
                        e.isDefined(c) && (a.$template = c);
                        return f.all(a);
                    }
                }).then(function(c) {
                    d == r.current && (d && (d.locals = c, e.copy(d.params, b)), a.$broadcast("$routeChangeSuccess", d, m));
                }, function(c) {
                    d == r.current && a.$broadcast("$routeChangeError", d, m, c);
                });
            }
            function t() {
                var a, b;
                e.forEach(k, function(f, k) {
                    var p;
                    if (p = !b) {
                        var s = c.path();
                        p = f.keys;
                        var l = {};
                        if (f.regexp) if (s = f.regexp.exec(s)) {
                            for (var g = 1, q = s.length; g < q; ++g) {
                                var n = p[g - 1], r = "string" == typeof s[g] ? decodeURIComponent(s[g]) : s[g];
                                n && r && (l[n.name] = r);
                            }
                            p = l;
                        } else p = null; else p = null;
                        p = a = p;
                    }
                    p && (b = h(f, {
                        params: e.extend({}, c.search(), a),
                        pathParams: a
                    }), b.$$route = f);
                });
                return b || k[null] && h(k[null], {
                    params: {},
                    pathParams: {}
                });
            }
            function u(a, c) {
                var b = [];
                e.forEach((a || "").split(":"), function(a, d) {
                    if (0 === d) b.push(a); else {
                        var e = a.match(/(\w+)(.*)/), f = e[1];
                        b.push(c[f]);
                        b.push(e[2] || "");
                        delete c[f];
                    }
                });
                return b.join("");
            }
            var x = !1, r = {
                routes: k,
                reload: function() {
                    x = !0;
                    a.$evalAsync(g);
                }
            };
            a.$on("$locationChangeSuccess", g);
            return r;
        } ];
    });
    h.provider("$routeParams", function() {
        this.$get = function() {
            return {};
        };
    });
    h.directive("ngView", u);
    h.directive("ngView", z);
    u.$inject = [ "$route", "$anchorScroll", "$animate" ];
    z.$inject = [ "$compile", "$controller", "$route" ];
})(window, window.angular);

(function(y, v, z) {
    "use strict";
    function t(g, a, b) {
        q.directive(g, [ "$parse", "$swipe", function(l, n) {
            var r = 75, h = .3, d = 30;
            return function(p, m, k) {
                function e(e) {
                    if (!u) return !1;
                    var c = Math.abs(e.y - u.y);
                    e = (e.x - u.x) * a;
                    return f && c < r && 0 < e && e > d && c / e < h;
                }
                var c = l(k[g]), u, f;
                n.bind(m, {
                    start: function(e, c) {
                        u = e;
                        f = !0;
                    },
                    cancel: function(e) {
                        f = !1;
                    },
                    end: function(a, f) {
                        e(a) && p.$apply(function() {
                            m.triggerHandler(b);
                            c(p, {
                                $event: f
                            });
                        });
                    }
                });
            };
        } ]);
    }
    var q = v.module("ngTouch", []);
    q.factory("$swipe", [ function() {
        function g(a) {
            var b = a.touches && a.touches.length ? a.touches : [ a ];
            a = a.changedTouches && a.changedTouches[0] || a.originalEvent && a.originalEvent.changedTouches && a.originalEvent.changedTouches[0] || b[0].originalEvent || b[0];
            return {
                x: a.clientX,
                y: a.clientY
            };
        }
        return {
            bind: function(a, b) {
                var l, n, r, h, d = !1;
                a.on("touchstart mousedown", function(a) {
                    r = g(a);
                    d = !0;
                    n = l = 0;
                    h = r;
                    b.start && b.start(r, a);
                });
                a.on("touchcancel", function(a) {
                    d = !1;
                    b.cancel && b.cancel(a);
                });
                a.on("touchmove mousemove", function(a) {
                    if (d && r) {
                        var m = g(a);
                        l += Math.abs(m.x - h.x);
                        n += Math.abs(m.y - h.y);
                        h = m;
                        10 > l && 10 > n || (n > l ? (d = !1, b.cancel && b.cancel(a)) : (a.preventDefault(), 
                        b.move && b.move(m, a)));
                    }
                });
                a.on("touchend mouseup", function(a) {
                    d && (d = !1, b.end && b.end(g(a), a));
                });
            }
        };
    } ]);
    q.config([ "$provide", function(g) {
        g.decorator("ngClickDirective", [ "$delegate", function(a) {
            a.shift();
            return a;
        } ]);
    } ]);
    q.directive("ngClick", [ "$parse", "$timeout", "$rootElement", function(g, a, b) {
        function l(a, c, b) {
            for (var f = 0; f < a.length; f += 2) if (Math.abs(a[f] - c) < d && Math.abs(a[f + 1] - b) < d) return a.splice(f, f + 2), 
            !0;
            return !1;
        }
        function n(a) {
            if (!(Date.now() - m > h)) {
                var c = a.touches && a.touches.length ? a.touches : [ a ], b = c[0].clientX, c = c[0].clientY;
                1 > b && 1 > c || l(k, b, c) || (a.stopPropagation(), a.preventDefault(), a.target && a.target.blur());
            }
        }
        function r(b) {
            b = b.touches && b.touches.length ? b.touches : [ b ];
            var c = b[0].clientX, d = b[0].clientY;
            k.push(c, d);
            a(function() {
                for (var a = 0; a < k.length; a += 2) if (k[a] == c && k[a + 1] == d) {
                    k.splice(a, a + 2);
                    break;
                }
            }, h, !1);
        }
        var h = 2500, d = 25, p = "ng-click-active", m, k;
        return function(a, c, d) {
            function f() {
                q = !1;
                c.removeClass(p);
            }
            var h = g(d.ngClick), q = !1, s, t, w, x;
            c.on("touchstart", function(a) {
                q = !0;
                s = a.target ? a.target : a.srcElement;
                3 == s.nodeType && (s = s.parentNode);
                c.addClass(p);
                t = Date.now();
                a = a.touches && a.touches.length ? a.touches : [ a ];
                a = a[0].originalEvent || a[0];
                w = a.clientX;
                x = a.clientY;
            });
            c.on("touchmove", function(a) {
                f();
            });
            c.on("touchcancel", function(a) {
                f();
            });
            c.on("touchend", function(a) {
                var h = Date.now() - t, e = a.changedTouches && a.changedTouches.length ? a.changedTouches : a.touches && a.touches.length ? a.touches : [ a ], g = e[0].originalEvent || e[0], e = g.clientX, g = g.clientY, p = Math.sqrt(Math.pow(e - w, 2) + Math.pow(g - x, 2));
                q && (750 > h && 12 > p) && (k || (b[0].addEventListener("click", n, !0), b[0].addEventListener("touchstart", r, !0), 
                k = []), m = Date.now(), l(k, e, g), s && s.blur(), v.isDefined(d.disabled) && !1 !== d.disabled || c.triggerHandler("click", [ a ]));
                f();
            });
            c.onclick = function(a) {};
            c.on("click", function(b, c) {
                a.$apply(function() {
                    h(a, {
                        $event: c || b
                    });
                });
            });
            c.on("mousedown", function(a) {
                c.addClass(p);
            });
            c.on("mousemove mouseup", function(a) {
                c.removeClass(p);
            });
        };
    } ]);
    t("ngSwipeLeft", -1, "swipeleft");
    t("ngSwipeRight", 1, "swiperight");
})(window, window.angular);

!function(a, b) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a);
    } : b(a);
}("undefined" != typeof window ? window : this, function(a, b) {
    var c = [], d = c.slice, e = c.concat, f = c.push, g = c.indexOf, h = {}, i = h.toString, j = h.hasOwnProperty, k = {}, l = "1.11.3", m = function(a, b) {
        return new m.fn.init(a, b);
    }, n = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, o = /^-ms-/, p = /-([\da-z])/gi, q = function(a, b) {
        return b.toUpperCase();
    };
    m.fn = m.prototype = {
        jquery: l,
        constructor: m,
        selector: "",
        length: 0,
        toArray: function() {
            return d.call(this);
        },
        get: function(a) {
            return null != a ? 0 > a ? this[a + this.length] : this[a] : d.call(this);
        },
        pushStack: function(a) {
            var b = m.merge(this.constructor(), a);
            return b.prevObject = this, b.context = this.context, b;
        },
        each: function(a, b) {
            return m.each(this, a, b);
        },
        map: function(a) {
            return this.pushStack(m.map(this, function(b, c) {
                return a.call(b, c, b);
            }));
        },
        slice: function() {
            return this.pushStack(d.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(a) {
            var b = this.length, c = +a + (0 > a ? b : 0);
            return this.pushStack(c >= 0 && b > c ? [ this[c] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor(null);
        },
        push: f,
        sort: c.sort,
        splice: c.splice
    }, m.extend = m.fn.extend = function() {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || m.isFunction(g) || (g = {}), 
        h === i && (g = this, h--); i > h; h++) if (null != (e = arguments[h])) for (d in e) a = g[d], 
        c = e[d], g !== c && (j && c && (m.isPlainObject(c) || (b = m.isArray(c))) ? (b ? (b = !1, 
        f = a && m.isArray(a) ? a : []) : f = a && m.isPlainObject(a) ? a : {}, g[d] = m.extend(j, f, c)) : void 0 !== c && (g[d] = c));
        return g;
    }, m.extend({
        expando: "jQuery" + (l + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(a) {
            throw new Error(a);
        },
        noop: function() {},
        isFunction: function(a) {
            return "function" === m.type(a);
        },
        isArray: Array.isArray || function(a) {
            return "array" === m.type(a);
        },
        isWindow: function(a) {
            return null != a && a == a.window;
        },
        isNumeric: function(a) {
            return !m.isArray(a) && a - parseFloat(a) + 1 >= 0;
        },
        isEmptyObject: function(a) {
            var b;
            for (b in a) return !1;
            return !0;
        },
        isPlainObject: function(a) {
            var b;
            if (!a || "object" !== m.type(a) || a.nodeType || m.isWindow(a)) return !1;
            try {
                if (a.constructor && !j.call(a, "constructor") && !j.call(a.constructor.prototype, "isPrototypeOf")) return !1;
            } catch (c) {
                return !1;
            }
            if (k.ownLast) for (b in a) return j.call(a, b);
            for (b in a) ;
            return void 0 === b || j.call(a, b);
        },
        type: function(a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? h[i.call(a)] || "object" : typeof a;
        },
        globalEval: function(b) {
            b && m.trim(b) && (a.execScript || function(b) {
                a.eval.call(a, b);
            })(b);
        },
        camelCase: function(a) {
            return a.replace(o, "ms-").replace(p, q);
        },
        nodeName: function(a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
        },
        each: function(a, b, c) {
            var d, e = 0, f = a.length, g = r(a);
            if (c) {
                if (g) {
                    for (;f > e; e++) if (d = b.apply(a[e], c), d === !1) break;
                } else for (e in a) if (d = b.apply(a[e], c), d === !1) break;
            } else if (g) {
                for (;f > e; e++) if (d = b.call(a[e], e, a[e]), d === !1) break;
            } else for (e in a) if (d = b.call(a[e], e, a[e]), d === !1) break;
            return a;
        },
        trim: function(a) {
            return null == a ? "" : (a + "").replace(n, "");
        },
        makeArray: function(a, b) {
            var c = b || [];
            return null != a && (r(Object(a)) ? m.merge(c, "string" == typeof a ? [ a ] : a) : f.call(c, a)), 
            c;
        },
        inArray: function(a, b, c) {
            var d;
            if (b) {
                if (g) return g.call(b, a, c);
                for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++) if (c in b && b[c] === a) return c;
            }
            return -1;
        },
        merge: function(a, b) {
            var c = +b.length, d = 0, e = a.length;
            while (c > d) a[e++] = b[d++];
            if (c !== c) while (void 0 !== b[d]) a[e++] = b[d++];
            return a.length = e, a;
        },
        grep: function(a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
            return e;
        },
        map: function(a, b, c) {
            var d, f = 0, g = a.length, h = r(a), i = [];
            if (h) for (;g > f; f++) d = b(a[f], f, c), null != d && i.push(d); else for (f in a) d = b(a[f], f, c), 
            null != d && i.push(d);
            return e.apply([], i);
        },
        guid: 1,
        proxy: function(a, b) {
            var c, e, f;
            return "string" == typeof b && (f = a[b], b = a, a = f), m.isFunction(a) ? (c = d.call(arguments, 2), 
            e = function() {
                return a.apply(b || this, c.concat(d.call(arguments)));
            }, e.guid = a.guid = a.guid || m.guid++, e) : void 0;
        },
        now: function() {
            return +new Date();
        },
        support: k
    }), m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(a, b) {
        h["[object " + b + "]"] = b.toLowerCase();
    });
    function r(a) {
        var b = "length" in a && a.length, c = m.type(a);
        return "function" === c || m.isWindow(a) ? !1 : 1 === a.nodeType && b ? !0 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a;
    }
    var s = function(a) {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date(), v = a.document, w = 0, x = 0, y = ha(), z = ha(), A = ha(), B = function(a, b) {
            return a === b && (l = !0), 0;
        }, C = 1 << 31, D = {}.hasOwnProperty, E = [], F = E.pop, G = E.push, H = E.push, I = E.slice, J = function(a, b) {
            for (var c = 0, d = a.length; d > c; c++) if (a[c] === b) return c;
            return -1;
        }, K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", L = "[\\x20\\t\\r\\n\\f]", M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", N = M.replace("w", "w#"), O = "\\[" + L + "*(" + M + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + N + "))|)" + L + "*\\]", P = ":(" + M + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + O + ")*)|.*)\\)|)", Q = new RegExp(L + "+", "g"), R = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"), S = new RegExp("^" + L + "*," + L + "*"), T = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"), U = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"), V = new RegExp(P), W = new RegExp("^" + N + "$"), X = {
            ID: new RegExp("^#(" + M + ")"),
            CLASS: new RegExp("^\\.(" + M + ")"),
            TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + O),
            PSEUDO: new RegExp("^" + P),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + K + ")$", "i"),
            needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
        }, Y = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, $ = /^[^{]+\{\s*\[native \w/, _ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, aa = /[+~]/, ba = /'|\\/g, ca = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"), da = function(a, b, c) {
            var d = "0x" + b - 65536;
            return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320);
        }, ea = function() {
            m();
        };
        try {
            H.apply(E = I.call(v.childNodes), v.childNodes), E[v.childNodes.length].nodeType;
        } catch (fa) {
            H = {
                apply: E.length ? function(a, b) {
                    G.apply(a, I.call(b));
                } : function(a, b) {
                    var c = a.length, d = 0;
                    while (a[c++] = b[d++]) ;
                    a.length = c - 1;
                }
            };
        }
        function ga(a, b, d, e) {
            var f, h, j, k, l, o, r, s, w, x;
            if ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, d = d || [], k = b.nodeType, 
            "string" != typeof a || !a || 1 !== k && 9 !== k && 11 !== k) return d;
            if (!e && p) {
                if (11 !== k && (f = _.exec(a))) if (j = f[1]) {
                    if (9 === k) {
                        if (h = b.getElementById(j), !h || !h.parentNode) return d;
                        if (h.id === j) return d.push(h), d;
                    } else if (b.ownerDocument && (h = b.ownerDocument.getElementById(j)) && t(b, h) && h.id === j) return d.push(h), 
                    d;
                } else {
                    if (f[2]) return H.apply(d, b.getElementsByTagName(a)), d;
                    if ((j = f[3]) && c.getElementsByClassName) return H.apply(d, b.getElementsByClassName(j)), 
                    d;
                }
                if (c.qsa && (!q || !q.test(a))) {
                    if (s = r = u, w = b, x = 1 !== k && a, 1 === k && "object" !== b.nodeName.toLowerCase()) {
                        o = g(a), (r = b.getAttribute("id")) ? s = r.replace(ba, "\\$&") : b.setAttribute("id", s), 
                        s = "[id='" + s + "'] ", l = o.length;
                        while (l--) o[l] = s + ra(o[l]);
                        w = aa.test(a) && pa(b.parentNode) || b, x = o.join(",");
                    }
                    if (x) try {
                        return H.apply(d, w.querySelectorAll(x)), d;
                    } catch (y) {} finally {
                        r || b.removeAttribute("id");
                    }
                }
            }
            return i(a.replace(R, "$1"), b, d, e);
        }
        function ha() {
            var a = [];
            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e;
            }
            return b;
        }
        function ia(a) {
            return a[u] = !0, a;
        }
        function ja(a) {
            var b = n.createElement("div");
            try {
                return !!a(b);
            } catch (c) {
                return !1;
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null;
            }
        }
        function ka(a, b) {
            var c = a.split("|"), e = a.length;
            while (e--) d.attrHandle[c[e]] = b;
        }
        function la(a, b) {
            var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || C) - (~a.sourceIndex || C);
            if (d) return d;
            if (c) while (c = c.nextSibling) if (c === b) return -1;
            return a ? 1 : -1;
        }
        function ma(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a;
            };
        }
        function na(a) {
            return function(b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a;
            };
        }
        function oa(a) {
            return ia(function(b) {
                return b = +b, ia(function(c, d) {
                    var e, f = a([], c.length, b), g = f.length;
                    while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]));
                });
            });
        }
        function pa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a;
        }
        c = ga.support = {}, f = ga.isXML = function(a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return b ? "HTML" !== b.nodeName : !1;
        }, m = ga.setDocument = function(a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = g.documentElement, 
            e = g.defaultView, e && e !== e.top && (e.addEventListener ? e.addEventListener("unload", ea, !1) : e.attachEvent && e.attachEvent("onunload", ea)), 
            p = !f(g), c.attributes = ja(function(a) {
                return a.className = "i", !a.getAttribute("className");
            }), c.getElementsByTagName = ja(function(a) {
                return a.appendChild(g.createComment("")), !a.getElementsByTagName("*").length;
            }), c.getElementsByClassName = $.test(g.getElementsByClassName), c.getById = ja(function(a) {
                return o.appendChild(a).id = u, !g.getElementsByName || !g.getElementsByName(u).length;
            }), c.getById ? (d.find.ID = function(a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c && c.parentNode ? [ c ] : [];
                }
            }, d.filter.ID = function(a) {
                var b = a.replace(ca, da);
                return function(a) {
                    return a.getAttribute("id") === b;
                };
            }) : (delete d.find.ID, d.filter.ID = function(a) {
                var b = a.replace(ca, da);
                return function(a) {
                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b;
                };
            }), d.find.TAG = c.getElementsByTagName ? function(a, b) {
                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0;
            } : function(a, b) {
                var c, d = [], e = 0, f = b.getElementsByTagName(a);
                if ("*" === a) {
                    while (c = f[e++]) 1 === c.nodeType && d.push(c);
                    return d;
                }
                return f;
            }, d.find.CLASS = c.getElementsByClassName && function(a, b) {
                return p ? b.getElementsByClassName(a) : void 0;
            }, r = [], q = [], (c.qsa = $.test(g.querySelectorAll)) && (ja(function(a) {
                o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\f]' msallowcapture=''><option selected=''></option></select>", 
                a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + L + "*(?:''|\"\")"), 
                a.querySelectorAll("[selected]").length || q.push("\\[" + L + "*(?:value|" + K + ")"), 
                a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), 
                a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]");
            }), ja(function(a) {
                var b = g.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + L + "*[*^$|!~]?="), 
                a.querySelectorAll(":enabled").length || q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), 
                q.push(",.*:");
            })), (c.matchesSelector = $.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function(a) {
                c.disconnectedMatch = s.call(a, "div"), s.call(a, "[s!='']:x"), r.push("!=", P);
            }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), 
            b = $.test(o.compareDocumentPosition), t = b || $.test(o.contains) ? function(a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
            } : function(a, b) {
                if (b) while (b = b.parentNode) if (b === a) return !0;
                return !1;
            }, B = b ? function(a, b) {
                if (a === b) return l = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 
                1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === g || a.ownerDocument === v && t(v, a) ? -1 : b === g || b.ownerDocument === v && t(v, b) ? 1 : k ? J(k, a) - J(k, b) : 0 : 4 & d ? -1 : 1);
            } : function(a, b) {
                if (a === b) return l = !0, 0;
                var c, d = 0, e = a.parentNode, f = b.parentNode, h = [ a ], i = [ b ];
                if (!e || !f) return a === g ? -1 : b === g ? 1 : e ? -1 : f ? 1 : k ? J(k, a) - J(k, b) : 0;
                if (e === f) return la(a, b);
                c = a;
                while (c = c.parentNode) h.unshift(c);
                c = b;
                while (c = c.parentNode) i.unshift(c);
                while (h[d] === i[d]) d++;
                return d ? la(h[d], i[d]) : h[d] === v ? -1 : i[d] === v ? 1 : 0;
            }, g) : n;
        }, ga.matches = function(a, b) {
            return ga(a, null, null, b);
        }, ga.matchesSelector = function(a, b) {
            if ((a.ownerDocument || a) !== n && m(a), b = b.replace(U, "='$1']"), !(!c.matchesSelector || !p || r && r.test(b) || q && q.test(b))) try {
                var d = s.call(a, b);
                if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d;
            } catch (e) {}
            return ga(b, n, null, [ a ]).length > 0;
        }, ga.contains = function(a, b) {
            return (a.ownerDocument || a) !== n && m(a), t(a, b);
        }, ga.attr = function(a, b) {
            (a.ownerDocument || a) !== n && m(a);
            var e = d.attrHandle[b.toLowerCase()], f = e && D.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
            return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
        }, ga.error = function(a) {
            throw new Error("Syntax error, unrecognized expression: " + a);
        }, ga.uniqueSort = function(a) {
            var b, d = [], e = 0, f = 0;
            if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
                while (b = a[f++]) b === a[f] && (e = d.push(f));
                while (e--) a.splice(d[e], 1);
            }
            return k = null, a;
        }, e = ga.getText = function(a) {
            var b, c = "", d = 0, f = a.nodeType;
            if (f) {
                if (1 === f || 9 === f || 11 === f) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += e(a);
                } else if (3 === f || 4 === f) return a.nodeValue;
            } else while (b = a[d++]) c += e(b);
            return c;
        }, d = ga.selectors = {
            cacheLength: 50,
            createPseudo: ia,
            match: X,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(a) {
                    return a[1] = a[1].replace(ca, da), a[3] = (a[3] || a[4] || a[5] || "").replace(ca, da), 
                    "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
                },
                CHILD: function(a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), 
                    a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), 
                    a;
                },
                PSEUDO: function(a) {
                    var b, c = !a[6] && a[2];
                    return X.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && V.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), 
                    a[2] = c.slice(0, b)), a.slice(0, 3));
                }
            },
            filter: {
                TAG: function(a) {
                    var b = a.replace(ca, da).toLowerCase();
                    return "*" === a ? function() {
                        return !0;
                    } : function(a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b;
                    };
                },
                CLASS: function(a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + L + ")" + a + "(" + L + "|$)")) && y(a, function(a) {
                        return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "");
                    });
                },
                ATTR: function(a, b, c) {
                    return function(d) {
                        var e = ga.attr(d, a);
                        return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(Q, " ") + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0;
                    };
                },
                CHILD: function(a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
                    return 1 === d && 0 === e ? function(a) {
                        return !!a.parentNode;
                    } : function(b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
                        if (q) {
                            if (f) {
                                while (p) {
                                    l = b;
                                    while (l = l[p]) if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling";
                                }
                                return !0;
                            }
                            if (o = [ g ? q.firstChild : q.lastChild ], g && s) {
                                k = q[u] || (q[u] = {}), j = k[a] || [], n = j[0] === w && j[1], m = j[0] === w && j[2], 
                                l = n && q.childNodes[n];
                                while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) if (1 === l.nodeType && ++m && l === b) {
                                    k[a] = [ w, n, m ];
                                    break;
                                }
                            } else if (s && (j = (b[u] || (b[u] = {}))[a]) && j[0] === w) m = j[1]; else while (l = ++n && l && l[p] || (m = n = 0) || o.pop()) if ((h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) && ++m && (s && ((l[u] || (l[u] = {}))[a] = [ w, m ]), 
                            l === b)) break;
                            return m -= e, m === d || m % d === 0 && m / d >= 0;
                        }
                    };
                },
                PSEUDO: function(a, b) {
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                    return e[u] ? e(b) : e.length > 1 ? (c = [ a, a, "", b ], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function(a, c) {
                        var d, f = e(a, b), g = f.length;
                        while (g--) d = J(a, f[g]), a[d] = !(c[d] = f[g]);
                    }) : function(a) {
                        return e(a, 0, c);
                    }) : e;
                }
            },
            pseudos: {
                not: ia(function(a) {
                    var b = [], c = [], d = h(a.replace(R, "$1"));
                    return d[u] ? ia(function(a, b, c, e) {
                        var f, g = d(a, null, e, []), h = a.length;
                        while (h--) (f = g[h]) && (a[h] = !(b[h] = f));
                    }) : function(a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop();
                    };
                }),
                has: ia(function(a) {
                    return function(b) {
                        return ga(a, b).length > 0;
                    };
                }),
                contains: ia(function(a) {
                    return a = a.replace(ca, da), function(b) {
                        return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
                    };
                }),
                lang: ia(function(a) {
                    return W.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(ca, da).toLowerCase(), 
                    function(b) {
                        var c;
                        do if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), 
                        c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                        return !1;
                    };
                }),
                target: function(b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id;
                },
                root: function(a) {
                    return a === o;
                },
                focus: function(a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
                },
                enabled: function(a) {
                    return a.disabled === !1;
                },
                disabled: function(a) {
                    return a.disabled === !0;
                },
                checked: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected;
                },
                selected: function(a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0;
                },
                empty: function(a) {
                    for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1;
                    return !0;
                },
                parent: function(a) {
                    return !d.pseudos.empty(a);
                },
                header: function(a) {
                    return Z.test(a.nodeName);
                },
                input: function(a) {
                    return Y.test(a.nodeName);
                },
                button: function(a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b;
                },
                text: function(a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
                },
                first: oa(function() {
                    return [ 0 ];
                }),
                last: oa(function(a, b) {
                    return [ b - 1 ];
                }),
                eq: oa(function(a, b, c) {
                    return [ 0 > c ? c + b : c ];
                }),
                even: oa(function(a, b) {
                    for (var c = 0; b > c; c += 2) a.push(c);
                    return a;
                }),
                odd: oa(function(a, b) {
                    for (var c = 1; b > c; c += 2) a.push(c);
                    return a;
                }),
                lt: oa(function(a, b, c) {
                    for (var d = 0 > c ? c + b : c; --d >= 0; ) a.push(d);
                    return a;
                }),
                gt: oa(function(a, b, c) {
                    for (var d = 0 > c ? c + b : c; ++d < b; ) a.push(d);
                    return a;
                })
            }
        }, d.pseudos.nth = d.pseudos.eq;
        for (b in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) d.pseudos[b] = ma(b);
        for (b in {
            submit: !0,
            reset: !0
        }) d.pseudos[b] = na(b);
        function qa() {}
        qa.prototype = d.filters = d.pseudos, d.setFilters = new qa(), g = ga.tokenize = function(a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) return b ? 0 : k.slice(0);
            h = a, i = [], j = d.preFilter;
            while (h) {
                (!c || (e = S.exec(h))) && (e && (h = h.slice(e[0].length) || h), i.push(f = [])), 
                c = !1, (e = T.exec(h)) && (c = e.shift(), f.push({
                    value: c,
                    type: e[0].replace(R, " ")
                }), h = h.slice(c.length));
                for (g in d.filter) !(e = X[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), 
                f.push({
                    value: c,
                    type: g,
                    matches: e
                }), h = h.slice(c.length));
                if (!c) break;
            }
            return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
        };
        function ra(a) {
            for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value;
            return d;
        }
        function sa(a, b, c) {
            var d = b.dir, e = c && "parentNode" === d, f = x++;
            return b.first ? function(b, c, f) {
                while (b = b[d]) if (1 === b.nodeType || e) return a(b, c, f);
            } : function(b, c, g) {
                var h, i, j = [ w, f ];
                if (g) {
                    while (b = b[d]) if ((1 === b.nodeType || e) && a(b, c, g)) return !0;
                } else while (b = b[d]) if (1 === b.nodeType || e) {
                    if (i = b[u] || (b[u] = {}), (h = i[d]) && h[0] === w && h[1] === f) return j[2] = h[2];
                    if (i[d] = j, j[2] = a(b, c, g)) return !0;
                }
            };
        }
        function ta(a) {
            return a.length > 1 ? function(b, c, d) {
                var e = a.length;
                while (e--) if (!a[e](b, c, d)) return !1;
                return !0;
            } : a[0];
        }
        function ua(a, b, c) {
            for (var d = 0, e = b.length; e > d; d++) ga(a, b[d], c);
            return c;
        }
        function va(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++) (f = a[h]) && (!c || c(f, d, e)) && (g.push(f), 
            j && b.push(h));
            return g;
        }
        function wa(a, b, c, d, e, f) {
            return d && !d[u] && (d = wa(d)), e && !e[u] && (e = wa(e, f)), ia(function(f, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, p = f || ua(b || "*", h.nodeType ? [ h ] : h, []), q = !a || !f && b ? p : va(p, m, a, h, i), r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) {
                    j = va(r, n), d(j, [], h, i), k = j.length;
                    while (k--) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            j = [], k = r.length;
                            while (k--) (l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i);
                        }
                        k = r.length;
                        while (k--) (l = r[k]) && (j = e ? J(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l));
                    }
                } else r = va(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : H.apply(g, r);
            });
        }
        function xa(a) {
            for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = sa(function(a) {
                return a === b;
            }, h, !0), l = sa(function(a) {
                return J(b, a) > -1;
            }, h, !0), m = [ function(a, c, d) {
                var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                return b = null, e;
            } ]; f > i; i++) if (c = d.relative[a[i].type]) m = [ sa(ta(m), c) ]; else {
                if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
                    for (e = ++i; f > e; e++) if (d.relative[a[e].type]) break;
                    return wa(i > 1 && ta(m), i > 1 && ra(a.slice(0, i - 1).concat({
                        value: " " === a[i - 2].type ? "*" : ""
                    })).replace(R, "$1"), c, e > i && xa(a.slice(i, e)), f > e && xa(a = a.slice(e)), f > e && ra(a));
                }
                m.push(c);
            }
            return ta(m);
        }
        function ya(a, b) {
            var c = b.length > 0, e = a.length > 0, f = function(f, g, h, i, k) {
                var l, m, o, p = 0, q = "0", r = f && [], s = [], t = j, u = f || e && d.find.TAG("*", k), v = w += null == t ? 1 : Math.random() || .1, x = u.length;
                for (k && (j = g !== n && g); q !== x && null != (l = u[q]); q++) {
                    if (e && l) {
                        m = 0;
                        while (o = a[m++]) if (o(l, g, h)) {
                            i.push(l);
                            break;
                        }
                        k && (w = v);
                    }
                    c && ((l = !o && l) && p--, f && r.push(l));
                }
                if (p += q, c && q !== p) {
                    m = 0;
                    while (o = b[m++]) o(r, s, g, h);
                    if (f) {
                        if (p > 0) while (q--) r[q] || s[q] || (s[q] = F.call(i));
                        s = va(s);
                    }
                    H.apply(i, s), k && !f && s.length > 0 && p + b.length > 1 && ga.uniqueSort(i);
                }
                return k && (w = v, j = t), r;
            };
            return c ? ia(f) : f;
        }
        return h = ga.compile = function(a, b) {
            var c, d = [], e = [], f = A[a + " "];
            if (!f) {
                b || (b = g(a)), c = b.length;
                while (c--) f = xa(b[c]), f[u] ? d.push(f) : e.push(f);
                f = A(a, ya(e, d)), f.selector = a;
            }
            return f;
        }, i = ga.select = function(a, b, e, f) {
            var i, j, k, l, m, n = "function" == typeof a && a, o = !f && g(a = n.selector || a);
            if (e = e || [], 1 === o.length) {
                if (j = o[0] = o[0].slice(0), j.length > 2 && "ID" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) {
                    if (b = (d.find.ID(k.matches[0].replace(ca, da), b) || [])[0], !b) return e;
                    n && (b = b.parentNode), a = a.slice(j.shift().value.length);
                }
                i = X.needsContext.test(a) ? 0 : j.length;
                while (i--) {
                    if (k = j[i], d.relative[l = k.type]) break;
                    if ((m = d.find[l]) && (f = m(k.matches[0].replace(ca, da), aa.test(j[0].type) && pa(b.parentNode) || b))) {
                        if (j.splice(i, 1), a = f.length && ra(j), !a) return H.apply(e, f), e;
                        break;
                    }
                }
            }
            return (n || h(a, o))(f, b, !p, e, aa.test(a) && pa(b.parentNode) || b), e;
        }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, 
        m(), c.sortDetached = ja(function(a) {
            return 1 & a.compareDocumentPosition(n.createElement("div"));
        }), ja(function(a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href");
        }) || ka("type|href|height|width", function(a, b, c) {
            return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
        }), c.attributes && ja(function(a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
        }) || ka("value", function(a, b, c) {
            return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue;
        }), ja(function(a) {
            return null == a.getAttribute("disabled");
        }) || ka(K, function(a, b, c) {
            var d;
            return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
        }), ga;
    }(a);
    m.find = s, m.expr = s.selectors, m.expr[":"] = m.expr.pseudos, m.unique = s.uniqueSort, 
    m.text = s.getText, m.isXMLDoc = s.isXML, m.contains = s.contains;
    var t = m.expr.match.needsContext, u = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, v = /^.[^:#\[\.,]*$/;
    function w(a, b, c) {
        if (m.isFunction(b)) return m.grep(a, function(a, d) {
            return !!b.call(a, d, a) !== c;
        });
        if (b.nodeType) return m.grep(a, function(a) {
            return a === b !== c;
        });
        if ("string" == typeof b) {
            if (v.test(b)) return m.filter(b, a, c);
            b = m.filter(b, a);
        }
        return m.grep(a, function(a) {
            return m.inArray(a, b) >= 0 !== c;
        });
    }
    m.filter = function(a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? m.find.matchesSelector(d, a) ? [ d ] : [] : m.find.matches(a, m.grep(b, function(a) {
            return 1 === a.nodeType;
        }));
    }, m.fn.extend({
        find: function(a) {
            var b, c = [], d = this, e = d.length;
            if ("string" != typeof a) return this.pushStack(m(a).filter(function() {
                for (b = 0; e > b; b++) if (m.contains(d[b], this)) return !0;
            }));
            for (b = 0; e > b; b++) m.find(a, d[b], c);
            return c = this.pushStack(e > 1 ? m.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, 
            c;
        },
        filter: function(a) {
            return this.pushStack(w(this, a || [], !1));
        },
        not: function(a) {
            return this.pushStack(w(this, a || [], !0));
        },
        is: function(a) {
            return !!w(this, "string" == typeof a && t.test(a) ? m(a) : a || [], !1).length;
        }
    });
    var x, y = a.document, z = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, A = m.fn.init = function(a, b) {
        var c, d;
        if (!a) return this;
        if ("string" == typeof a) {
            if (c = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [ null, a, null ] : z.exec(a), 
            !c || !c[1] && b) return !b || b.jquery ? (b || x).find(a) : this.constructor(b).find(a);
            if (c[1]) {
                if (b = b instanceof m ? b[0] : b, m.merge(this, m.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : y, !0)), 
                u.test(c[1]) && m.isPlainObject(b)) for (c in b) m.isFunction(this[c]) ? this[c](b[c]) : this.attr(c, b[c]);
                return this;
            }
            if (d = y.getElementById(c[2]), d && d.parentNode) {
                if (d.id !== c[2]) return x.find(a);
                this.length = 1, this[0] = d;
            }
            return this.context = y, this.selector = a, this;
        }
        return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : m.isFunction(a) ? "undefined" != typeof x.ready ? x.ready(a) : a(m) : (void 0 !== a.selector && (this.selector = a.selector, 
        this.context = a.context), m.makeArray(a, this));
    };
    A.prototype = m.fn, x = m(y);
    var B = /^(?:parents|prev(?:Until|All))/, C = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    m.extend({
        dir: function(a, b, c) {
            var d = [], e = a[b];
            while (e && 9 !== e.nodeType && (void 0 === c || 1 !== e.nodeType || !m(e).is(c))) 1 === e.nodeType && d.push(e), 
            e = e[b];
            return d;
        },
        sibling: function(a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c;
        }
    }), m.fn.extend({
        has: function(a) {
            var b, c = m(a, this), d = c.length;
            return this.filter(function() {
                for (b = 0; d > b; b++) if (m.contains(this, c[b])) return !0;
            });
        },
        closest: function(a, b) {
            for (var c, d = 0, e = this.length, f = [], g = t.test(a) || "string" != typeof a ? m(a, b || this.context) : 0; e > d; d++) for (c = this[d]; c && c !== b; c = c.parentNode) if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && m.find.matchesSelector(c, a))) {
                f.push(c);
                break;
            }
            return this.pushStack(f.length > 1 ? m.unique(f) : f);
        },
        index: function(a) {
            return a ? "string" == typeof a ? m.inArray(this[0], m(a)) : m.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
        },
        add: function(a, b) {
            return this.pushStack(m.unique(m.merge(this.get(), m(a, b))));
        },
        addBack: function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
        }
    });
    function D(a, b) {
        do a = a[b]; while (a && 1 !== a.nodeType);
        return a;
    }
    m.each({
        parent: function(a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null;
        },
        parents: function(a) {
            return m.dir(a, "parentNode");
        },
        parentsUntil: function(a, b, c) {
            return m.dir(a, "parentNode", c);
        },
        next: function(a) {
            return D(a, "nextSibling");
        },
        prev: function(a) {
            return D(a, "previousSibling");
        },
        nextAll: function(a) {
            return m.dir(a, "nextSibling");
        },
        prevAll: function(a) {
            return m.dir(a, "previousSibling");
        },
        nextUntil: function(a, b, c) {
            return m.dir(a, "nextSibling", c);
        },
        prevUntil: function(a, b, c) {
            return m.dir(a, "previousSibling", c);
        },
        siblings: function(a) {
            return m.sibling((a.parentNode || {}).firstChild, a);
        },
        children: function(a) {
            return m.sibling(a.firstChild);
        },
        contents: function(a) {
            return m.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : m.merge([], a.childNodes);
        }
    }, function(a, b) {
        m.fn[a] = function(c, d) {
            var e = m.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = m.filter(d, e)), 
            this.length > 1 && (C[a] || (e = m.unique(e)), B.test(a) && (e = e.reverse())), 
            this.pushStack(e);
        };
    });
    var E = /\S+/g, F = {};
    function G(a) {
        var b = F[a] = {};
        return m.each(a.match(E) || [], function(a, c) {
            b[c] = !0;
        }), b;
    }
    m.Callbacks = function(a) {
        a = "string" == typeof a ? F[a] || G(a) : m.extend({}, a);
        var b, c, d, e, f, g, h = [], i = !a.once && [], j = function(l) {
            for (c = a.memory && l, d = !0, f = g || 0, g = 0, e = h.length, b = !0; h && e > f; f++) if (h[f].apply(l[0], l[1]) === !1 && a.stopOnFalse) {
                c = !1;
                break;
            }
            b = !1, h && (i ? i.length && j(i.shift()) : c ? h = [] : k.disable());
        }, k = {
            add: function() {
                if (h) {
                    var d = h.length;
                    !function f(b) {
                        m.each(b, function(b, c) {
                            var d = m.type(c);
                            "function" === d ? a.unique && k.has(c) || h.push(c) : c && c.length && "string" !== d && f(c);
                        });
                    }(arguments), b ? e = h.length : c && (g = d, j(c));
                }
                return this;
            },
            remove: function() {
                return h && m.each(arguments, function(a, c) {
                    var d;
                    while ((d = m.inArray(c, h, d)) > -1) h.splice(d, 1), b && (e >= d && e--, f >= d && f--);
                }), this;
            },
            has: function(a) {
                return a ? m.inArray(a, h) > -1 : !(!h || !h.length);
            },
            empty: function() {
                return h = [], e = 0, this;
            },
            disable: function() {
                return h = i = c = void 0, this;
            },
            disabled: function() {
                return !h;
            },
            lock: function() {
                return i = void 0, c || k.disable(), this;
            },
            locked: function() {
                return !i;
            },
            fireWith: function(a, c) {
                return !h || d && !i || (c = c || [], c = [ a, c.slice ? c.slice() : c ], b ? i.push(c) : j(c)), 
                this;
            },
            fire: function() {
                return k.fireWith(this, arguments), this;
            },
            fired: function() {
                return !!d;
            }
        };
        return k;
    }, m.extend({
        Deferred: function(a) {
            var b = [ [ "resolve", "done", m.Callbacks("once memory"), "resolved" ], [ "reject", "fail", m.Callbacks("once memory"), "rejected" ], [ "notify", "progress", m.Callbacks("memory") ] ], c = "pending", d = {
                state: function() {
                    return c;
                },
                always: function() {
                    return e.done(arguments).fail(arguments), this;
                },
                then: function() {
                    var a = arguments;
                    return m.Deferred(function(c) {
                        m.each(b, function(b, f) {
                            var g = m.isFunction(a[b]) && a[b];
                            e[f[1]](function() {
                                var a = g && g.apply(this, arguments);
                                a && m.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [ a ] : arguments);
                            });
                        }), a = null;
                    }).promise();
                },
                promise: function(a) {
                    return null != a ? m.extend(a, d) : d;
                }
            }, e = {};
            return d.pipe = d.then, m.each(b, function(a, f) {
                var g = f[2], h = f[3];
                d[f[1]] = g.add, h && g.add(function() {
                    c = h;
                }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function() {
                    return e[f[0] + "With"](this === e ? d : this, arguments), this;
                }, e[f[0] + "With"] = g.fireWith;
            }), d.promise(e), a && a.call(e, e), e;
        },
        when: function(a) {
            var b = 0, c = d.call(arguments), e = c.length, f = 1 !== e || a && m.isFunction(a.promise) ? e : 0, g = 1 === f ? a : m.Deferred(), h = function(a, b, c) {
                return function(e) {
                    b[a] = this, c[a] = arguments.length > 1 ? d.call(arguments) : e, c === i ? g.notifyWith(b, c) : --f || g.resolveWith(b, c);
                };
            }, i, j, k;
            if (e > 1) for (i = new Array(e), j = new Array(e), k = new Array(e); e > b; b++) c[b] && m.isFunction(c[b].promise) ? c[b].promise().done(h(b, k, c)).fail(g.reject).progress(h(b, j, i)) : --f;
            return f || g.resolveWith(k, c), g.promise();
        }
    });
    var H;
    m.fn.ready = function(a) {
        return m.ready.promise().done(a), this;
    }, m.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(a) {
            a ? m.readyWait++ : m.ready(!0);
        },
        ready: function(a) {
            if (a === !0 ? !--m.readyWait : !m.isReady) {
                if (!y.body) return setTimeout(m.ready);
                m.isReady = !0, a !== !0 && --m.readyWait > 0 || (H.resolveWith(y, [ m ]), m.fn.triggerHandler && (m(y).triggerHandler("ready"), 
                m(y).off("ready")));
            }
        }
    });
    function I() {
        y.addEventListener ? (y.removeEventListener("DOMContentLoaded", J, !1), a.removeEventListener("load", J, !1)) : (y.detachEvent("onreadystatechange", J), 
        a.detachEvent("onload", J));
    }
    function J() {
        (y.addEventListener || "load" === event.type || "complete" === y.readyState) && (I(), 
        m.ready());
    }
    m.ready.promise = function(b) {
        if (!H) if (H = m.Deferred(), "complete" === y.readyState) setTimeout(m.ready); else if (y.addEventListener) y.addEventListener("DOMContentLoaded", J, !1), 
        a.addEventListener("load", J, !1); else {
            y.attachEvent("onreadystatechange", J), a.attachEvent("onload", J);
            var c = !1;
            try {
                c = null == a.frameElement && y.documentElement;
            } catch (d) {}
            c && c.doScroll && !function e() {
                if (!m.isReady) {
                    try {
                        c.doScroll("left");
                    } catch (a) {
                        return setTimeout(e, 50);
                    }
                    I(), m.ready();
                }
            }();
        }
        return H.promise(b);
    };
    var K = "undefined", L;
    for (L in m(k)) break;
    k.ownLast = "0" !== L, k.inlineBlockNeedsLayout = !1, m(function() {
        var a, b, c, d;
        c = y.getElementsByTagName("body")[0], c && c.style && (b = y.createElement("div"), 
        d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", 
        c.appendChild(d).appendChild(b), typeof b.style.zoom !== K && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", 
        k.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(d));
    }), function() {
        var a = y.createElement("div");
        if (null == k.deleteExpando) {
            k.deleteExpando = !0;
            try {
                delete a.test;
            } catch (b) {
                k.deleteExpando = !1;
            }
        }
        a = null;
    }(), m.acceptData = function(a) {
        var b = m.noData[(a.nodeName + " ").toLowerCase()], c = +a.nodeType || 1;
        return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b;
    };
    var M = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, N = /([A-Z])/g;
    function O(a, b, c) {
        if (void 0 === c && 1 === a.nodeType) {
            var d = "data-" + b.replace(N, "-$1").toLowerCase();
            if (c = a.getAttribute(d), "string" == typeof c) {
                try {
                    c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : M.test(c) ? m.parseJSON(c) : c;
                } catch (e) {}
                m.data(a, b, c);
            } else c = void 0;
        }
        return c;
    }
    function P(a) {
        var b;
        for (b in a) if (("data" !== b || !m.isEmptyObject(a[b])) && "toJSON" !== b) return !1;
        return !0;
    }
    function Q(a, b, d, e) {
        if (m.acceptData(a)) {
            var f, g, h = m.expando, i = a.nodeType, j = i ? m.cache : a, k = i ? a[h] : a[h] && h;
            if (k && j[k] && (e || j[k].data) || void 0 !== d || "string" != typeof b) return k || (k = i ? a[h] = c.pop() || m.guid++ : h), 
            j[k] || (j[k] = i ? {} : {
                toJSON: m.noop
            }), ("object" == typeof b || "function" == typeof b) && (e ? j[k] = m.extend(j[k], b) : j[k].data = m.extend(j[k].data, b)), 
            g = j[k], e || (g.data || (g.data = {}), g = g.data), void 0 !== d && (g[m.camelCase(b)] = d), 
            "string" == typeof b ? (f = g[b], null == f && (f = g[m.camelCase(b)])) : f = g, 
            f;
        }
    }
    function R(a, b, c) {
        if (m.acceptData(a)) {
            var d, e, f = a.nodeType, g = f ? m.cache : a, h = f ? a[m.expando] : m.expando;
            if (g[h]) {
                if (b && (d = c ? g[h] : g[h].data)) {
                    m.isArray(b) ? b = b.concat(m.map(b, m.camelCase)) : b in d ? b = [ b ] : (b = m.camelCase(b), 
                    b = b in d ? [ b ] : b.split(" ")), e = b.length;
                    while (e--) delete d[b[e]];
                    if (c ? !P(d) : !m.isEmptyObject(d)) return;
                }
                (c || (delete g[h].data, P(g[h]))) && (f ? m.cleanData([ a ], !0) : k.deleteExpando || g != g.window ? delete g[h] : g[h] = null);
            }
        }
    }
    m.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(a) {
            return a = a.nodeType ? m.cache[a[m.expando]] : a[m.expando], !!a && !P(a);
        },
        data: function(a, b, c) {
            return Q(a, b, c);
        },
        removeData: function(a, b) {
            return R(a, b);
        },
        _data: function(a, b, c) {
            return Q(a, b, c, !0);
        },
        _removeData: function(a, b) {
            return R(a, b, !0);
        }
    }), m.fn.extend({
        data: function(a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = m.data(f), 1 === f.nodeType && !m._data(f, "parsedAttrs"))) {
                    c = g.length;
                    while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = m.camelCase(d.slice(5)), 
                    O(f, d, e[d])));
                    m._data(f, "parsedAttrs", !0);
                }
                return e;
            }
            return "object" == typeof a ? this.each(function() {
                m.data(this, a);
            }) : arguments.length > 1 ? this.each(function() {
                m.data(this, a, b);
            }) : f ? O(f, a, m.data(f, a)) : void 0;
        },
        removeData: function(a) {
            return this.each(function() {
                m.removeData(this, a);
            });
        }
    }), m.extend({
        queue: function(a, b, c) {
            var d;
            return a ? (b = (b || "fx") + "queue", d = m._data(a, b), c && (!d || m.isArray(c) ? d = m._data(a, b, m.makeArray(c)) : d.push(c)), 
            d || []) : void 0;
        },
        dequeue: function(a, b) {
            b = b || "fx";
            var c = m.queue(a, b), d = c.length, e = c.shift(), f = m._queueHooks(a, b), g = function() {
                m.dequeue(a, b);
            };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), 
            delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire();
        },
        _queueHooks: function(a, b) {
            var c = b + "queueHooks";
            return m._data(a, c) || m._data(a, c, {
                empty: m.Callbacks("once memory").add(function() {
                    m._removeData(a, b + "queue"), m._removeData(a, c);
                })
            });
        }
    }), m.fn.extend({
        queue: function(a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? m.queue(this[0], a) : void 0 === b ? this : this.each(function() {
                var c = m.queue(this, a, b);
                m._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && m.dequeue(this, a);
            });
        },
        dequeue: function(a) {
            return this.each(function() {
                m.dequeue(this, a);
            });
        },
        clearQueue: function(a) {
            return this.queue(a || "fx", []);
        },
        promise: function(a, b) {
            var c, d = 1, e = m.Deferred(), f = this, g = this.length, h = function() {
                --d || e.resolveWith(f, [ f ]);
            };
            "string" != typeof a && (b = a, a = void 0), a = a || "fx";
            while (g--) c = m._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b);
        }
    });
    var S = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, T = [ "Top", "Right", "Bottom", "Left" ], U = function(a, b) {
        return a = b || a, "none" === m.css(a, "display") || !m.contains(a.ownerDocument, a);
    }, V = m.access = function(a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = null == c;
        if ("object" === m.type(c)) {
            e = !0;
            for (h in c) m.access(a, b, h, c[h], !0, f, g);
        } else if (void 0 !== d && (e = !0, m.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), 
        b = null) : (j = b, b = function(a, b, c) {
            return j.call(m(a), c);
        })), b)) for (;i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    }, W = /^(?:checkbox|radio)$/i;
    !function() {
        var a = y.createElement("input"), b = y.createElement("div"), c = y.createDocumentFragment();
        if (b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", 
        k.leadingWhitespace = 3 === b.firstChild.nodeType, k.tbody = !b.getElementsByTagName("tbody").length, 
        k.htmlSerialize = !!b.getElementsByTagName("link").length, k.html5Clone = "<:nav></:nav>" !== y.createElement("nav").cloneNode(!0).outerHTML, 
        a.type = "checkbox", a.checked = !0, c.appendChild(a), k.appendChecked = a.checked, 
        b.innerHTML = "<textarea>x</textarea>", k.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue, 
        c.appendChild(b), b.innerHTML = "<input type='radio' checked='checked' name='t'/>", 
        k.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, k.noCloneEvent = !0, 
        b.attachEvent && (b.attachEvent("onclick", function() {
            k.noCloneEvent = !1;
        }), b.cloneNode(!0).click()), null == k.deleteExpando) {
            k.deleteExpando = !0;
            try {
                delete b.test;
            } catch (d) {
                k.deleteExpando = !1;
            }
        }
    }(), function() {
        var b, c, d = y.createElement("div");
        for (b in {
            submit: !0,
            change: !0,
            focusin: !0
        }) c = "on" + b, (k[b + "Bubbles"] = c in a) || (d.setAttribute(c, "t"), k[b + "Bubbles"] = d.attributes[c].expando === !1);
        d = null;
    }();
    var X = /^(?:input|select|textarea)$/i, Y = /^key/, Z = /^(?:mouse|pointer|contextmenu)|click/, $ = /^(?:focusinfocus|focusoutblur)$/, _ = /^([^.]*)(?:\.(.+)|)$/;
    function aa() {
        return !0;
    }
    function ba() {
        return !1;
    }
    function ca() {
        try {
            return y.activeElement;
        } catch (a) {}
    }
    m.event = {
        global: {},
        add: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, n, o, p, q, r = m._data(a);
            if (r) {
                c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = m.guid++), 
                (g = r.events) || (g = r.events = {}), (k = r.handle) || (k = r.handle = function(a) {
                    return typeof m === K || a && m.event.triggered === a.type ? void 0 : m.event.dispatch.apply(k.elem, arguments);
                }, k.elem = a), b = (b || "").match(E) || [ "" ], h = b.length;
                while (h--) f = _.exec(b[h]) || [], o = q = f[1], p = (f[2] || "").split(".").sort(), 
                o && (j = m.event.special[o] || {}, o = (e ? j.delegateType : j.bindType) || o, 
                j = m.event.special[o] || {}, l = m.extend({
                    type: o,
                    origType: q,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && m.expr.match.needsContext.test(e),
                    namespace: p.join(".")
                }, i), (n = g[o]) || (n = g[o] = [], n.delegateCount = 0, j.setup && j.setup.call(a, d, p, k) !== !1 || (a.addEventListener ? a.addEventListener(o, k, !1) : a.attachEvent && a.attachEvent("on" + o, k))), 
                j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? n.splice(n.delegateCount++, 0, l) : n.push(l), 
                m.event.global[o] = !0);
                a = null;
            }
        },
        remove: function(a, b, c, d, e) {
            var f, g, h, i, j, k, l, n, o, p, q, r = m.hasData(a) && m._data(a);
            if (r && (k = r.events)) {
                b = (b || "").match(E) || [ "" ], j = b.length;
                while (j--) if (h = _.exec(b[j]) || [], o = q = h[1], p = (h[2] || "").split(".").sort(), 
                o) {
                    l = m.event.special[o] || {}, o = (d ? l.delegateType : l.bindType) || o, n = k[o] || [], 
                    h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = n.length;
                    while (f--) g = n[f], !e && q !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (n.splice(f, 1), 
                    g.selector && n.delegateCount--, l.remove && l.remove.call(a, g));
                    i && !n.length && (l.teardown && l.teardown.call(a, p, r.handle) !== !1 || m.removeEvent(a, o, r.handle), 
                    delete k[o]);
                } else for (o in k) m.event.remove(a, o + b[j], c, d, !0);
                m.isEmptyObject(k) && (delete r.handle, m._removeData(a, "events"));
            }
        },
        trigger: function(b, c, d, e) {
            var f, g, h, i, k, l, n, o = [ d || y ], p = j.call(b, "type") ? b.type : b, q = j.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = l = d = d || y, 3 !== d.nodeType && 8 !== d.nodeType && !$.test(p + m.event.triggered) && (p.indexOf(".") >= 0 && (q = p.split("."), 
            p = q.shift(), q.sort()), g = p.indexOf(":") < 0 && "on" + p, b = b[m.expando] ? b : new m.Event(p, "object" == typeof b && b), 
            b.isTrigger = e ? 2 : 3, b.namespace = q.join("."), b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, 
            b.result = void 0, b.target || (b.target = d), c = null == c ? [ b ] : m.makeArray(c, [ b ]), 
            k = m.event.special[p] || {}, e || !k.trigger || k.trigger.apply(d, c) !== !1)) {
                if (!e && !k.noBubble && !m.isWindow(d)) {
                    for (i = k.delegateType || p, $.test(i + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), 
                    l = h;
                    l === (d.ownerDocument || y) && o.push(l.defaultView || l.parentWindow || a);
                }
                n = 0;
                while ((h = o[n++]) && !b.isPropagationStopped()) b.type = n > 1 ? i : k.bindType || p, 
                f = (m._data(h, "events") || {})[b.type] && m._data(h, "handle"), f && f.apply(h, c), 
                f = g && h[g], f && f.apply && m.acceptData(h) && (b.result = f.apply(h, c), b.result === !1 && b.preventDefault());
                if (b.type = p, !e && !b.isDefaultPrevented() && (!k._default || k._default.apply(o.pop(), c) === !1) && m.acceptData(d) && g && d[p] && !m.isWindow(d)) {
                    l = d[g], l && (d[g] = null), m.event.triggered = p;
                    try {
                        d[p]();
                    } catch (r) {}
                    m.event.triggered = void 0, l && (d[g] = l);
                }
                return b.result;
            }
        },
        dispatch: function(a) {
            a = m.event.fix(a);
            var b, c, e, f, g, h = [], i = d.call(arguments), j = (m._data(this, "events") || {})[a.type] || [], k = m.event.special[a.type] || {};
            if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) {
                h = m.event.handlers.call(this, a, j), b = 0;
                while ((f = h[b++]) && !a.isPropagationStopped()) {
                    a.currentTarget = f.elem, g = 0;
                    while ((e = f.handlers[g++]) && !a.isImmediatePropagationStopped()) (!a.namespace_re || a.namespace_re.test(e.namespace)) && (a.handleObj = e, 
                    a.data = e.data, c = ((m.event.special[e.origType] || {}).handle || e.handler).apply(f.elem, i), 
                    void 0 !== c && (a.result = c) === !1 && (a.preventDefault(), a.stopPropagation()));
                }
                return k.postDispatch && k.postDispatch.call(this, a), a.result;
            }
        },
        handlers: function(a, b) {
            var c, d, e, f, g = [], h = b.delegateCount, i = a.target;
            if (h && i.nodeType && (!a.button || "click" !== a.type)) for (;i != this; i = i.parentNode || this) if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) {
                for (e = [], f = 0; h > f; f++) d = b[f], c = d.selector + " ", void 0 === e[c] && (e[c] = d.needsContext ? m(c, this).index(i) >= 0 : m.find(c, this, null, [ i ]).length), 
                e[c] && e.push(d);
                e.length && g.push({
                    elem: i,
                    handlers: e
                });
            }
            return h < b.length && g.push({
                elem: this,
                handlers: b.slice(h)
            }), g;
        },
        fix: function(a) {
            if (a[m.expando]) return a;
            var b, c, d, e = a.type, f = a, g = this.fixHooks[e];
            g || (this.fixHooks[e] = g = Z.test(e) ? this.mouseHooks : Y.test(e) ? this.keyHooks : {}), 
            d = g.props ? this.props.concat(g.props) : this.props, a = new m.Event(f), b = d.length;
            while (b--) c = d[b], a[c] = f[c];
            return a.target || (a.target = f.srcElement || y), 3 === a.target.nodeType && (a.target = a.target.parentNode), 
            a.metaKey = !!a.metaKey, g.filter ? g.filter(a, f) : a;
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(a, b) {
                return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), 
                a;
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(a, b) {
                var c, d, e, f = b.button, g = b.fromElement;
                return null == a.pageX && null != b.clientX && (d = a.target.ownerDocument || y, 
                e = d.documentElement, c = d.body, a.pageX = b.clientX + (e && e.scrollLeft || c && c.scrollLeft || 0) - (e && e.clientLeft || c && c.clientLeft || 0), 
                a.pageY = b.clientY + (e && e.scrollTop || c && c.scrollTop || 0) - (e && e.clientTop || c && c.clientTop || 0)), 
                !a.relatedTarget && g && (a.relatedTarget = g === a.target ? b.toElement : g), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), 
                a;
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== ca() && this.focus) try {
                        return this.focus(), !1;
                    } catch (a) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === ca() && this.blur ? (this.blur(), !1) : void 0;
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return m.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), 
                    !1) : void 0;
                },
                _default: function(a) {
                    return m.nodeName(a.target, "a");
                }
            },
            beforeunload: {
                postDispatch: function(a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
                }
            }
        },
        simulate: function(a, b, c, d) {
            var e = m.extend(new m.Event(), c, {
                type: a,
                isSimulated: !0,
                originalEvent: {}
            });
            d ? m.event.trigger(e, null, b) : m.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault();
        }
    }, m.removeEvent = y.removeEventListener ? function(a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c, !1);
    } : function(a, b, c) {
        var d = "on" + b;
        a.detachEvent && (typeof a[d] === K && (a[d] = null), a.detachEvent(d, c));
    }, m.Event = function(a, b) {
        return this instanceof m.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, 
        this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? aa : ba) : this.type = a, 
        b && m.extend(this, b), this.timeStamp = a && a.timeStamp || m.now(), void (this[m.expando] = !0)) : new m.Event(a, b);
    }, m.Event.prototype = {
        isDefaultPrevented: ba,
        isPropagationStopped: ba,
        isImmediatePropagationStopped: ba,
        preventDefault: function() {
            var a = this.originalEvent;
            this.isDefaultPrevented = aa, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1);
        },
        stopPropagation: function() {
            var a = this.originalEvent;
            this.isPropagationStopped = aa, a && (a.stopPropagation && a.stopPropagation(), 
            a.cancelBubble = !0);
        },
        stopImmediatePropagation: function() {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = aa, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), 
            this.stopPropagation();
        }
    }, m.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(a, b) {
        m.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function(a) {
                var c, d = this, e = a.relatedTarget, f = a.handleObj;
                return (!e || e !== d && !m.contains(d, e)) && (a.type = f.origType, c = f.handler.apply(this, arguments), 
                a.type = b), c;
            }
        };
    }), k.submitBubbles || (m.event.special.submit = {
        setup: function() {
            return m.nodeName(this, "form") ? !1 : void m.event.add(this, "click._submit keypress._submit", function(a) {
                var b = a.target, c = m.nodeName(b, "input") || m.nodeName(b, "button") ? b.form : void 0;
                c && !m._data(c, "submitBubbles") && (m.event.add(c, "submit._submit", function(a) {
                    a._submit_bubble = !0;
                }), m._data(c, "submitBubbles", !0));
            });
        },
        postDispatch: function(a) {
            a._submit_bubble && (delete a._submit_bubble, this.parentNode && !a.isTrigger && m.event.simulate("submit", this.parentNode, a, !0));
        },
        teardown: function() {
            return m.nodeName(this, "form") ? !1 : void m.event.remove(this, "._submit");
        }
    }), k.changeBubbles || (m.event.special.change = {
        setup: function() {
            return X.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (m.event.add(this, "propertychange._change", function(a) {
                "checked" === a.originalEvent.propertyName && (this._just_changed = !0);
            }), m.event.add(this, "click._change", function(a) {
                this._just_changed && !a.isTrigger && (this._just_changed = !1), m.event.simulate("change", this, a, !0);
            })), !1) : void m.event.add(this, "beforeactivate._change", function(a) {
                var b = a.target;
                X.test(b.nodeName) && !m._data(b, "changeBubbles") && (m.event.add(b, "change._change", function(a) {
                    !this.parentNode || a.isSimulated || a.isTrigger || m.event.simulate("change", this.parentNode, a, !0);
                }), m._data(b, "changeBubbles", !0));
            });
        },
        handle: function(a) {
            var b = a.target;
            return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0;
        },
        teardown: function() {
            return m.event.remove(this, "._change"), !X.test(this.nodeName);
        }
    }), k.focusinBubbles || m.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        var c = function(a) {
            m.event.simulate(b, a.target, m.event.fix(a), !0);
        };
        m.event.special[b] = {
            setup: function() {
                var d = this.ownerDocument || this, e = m._data(d, b);
                e || d.addEventListener(a, c, !0), m._data(d, b, (e || 0) + 1);
            },
            teardown: function() {
                var d = this.ownerDocument || this, e = m._data(d, b) - 1;
                e ? m._data(d, b, e) : (d.removeEventListener(a, c, !0), m._removeData(d, b));
            }
        };
    }), m.fn.extend({
        on: function(a, b, c, d, e) {
            var f, g;
            if ("object" == typeof a) {
                "string" != typeof b && (c = c || b, b = void 0);
                for (f in a) this.on(f, b, c, a[f], e);
                return this;
            }
            if (null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, 
            c = void 0) : (d = c, c = b, b = void 0)), d === !1) d = ba; else if (!d) return this;
            return 1 === e && (g = d, d = function(a) {
                return m().off(a), g.apply(this, arguments);
            }, d.guid = g.guid || (g.guid = m.guid++)), this.each(function() {
                m.event.add(this, a, d, c, b);
            });
        },
        one: function(a, b, c, d) {
            return this.on(a, b, c, d, 1);
        },
        off: function(a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, m(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), 
            this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this;
            }
            return (b === !1 || "function" == typeof b) && (c = b, b = void 0), c === !1 && (c = ba), 
            this.each(function() {
                m.event.remove(this, a, c, b);
            });
        },
        trigger: function(a, b) {
            return this.each(function() {
                m.event.trigger(a, b, this);
            });
        },
        triggerHandler: function(a, b) {
            var c = this[0];
            return c ? m.event.trigger(a, b, c, !0) : void 0;
        }
    });
    function da(a) {
        var b = ea.split("|"), c = a.createDocumentFragment();
        if (c.createElement) while (b.length) c.createElement(b.pop());
        return c;
    }
    var ea = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", fa = / jQuery\d+="(?:null|\d+)"/g, ga = new RegExp("<(?:" + ea + ")[\\s/>]", "i"), ha = /^\s+/, ia = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, ja = /<([\w:]+)/, ka = /<tbody/i, la = /<|&#?\w+;/, ma = /<(?:script|style|link)/i, na = /checked\s*(?:[^=]|=\s*.checked.)/i, oa = /^$|\/(?:java|ecma)script/i, pa = /^true\/(.*)/, qa = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, ra = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        legend: [ 1, "<fieldset>", "</fieldset>" ],
        area: [ 1, "<map>", "</map>" ],
        param: [ 1, "<object>", "</object>" ],
        thead: [ 1, "<table>", "</table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        _default: k.htmlSerialize ? [ 0, "", "" ] : [ 1, "X<div>", "</div>" ]
    }, sa = da(y), ta = sa.appendChild(y.createElement("div"));
    ra.optgroup = ra.option, ra.tbody = ra.tfoot = ra.colgroup = ra.caption = ra.thead, 
    ra.th = ra.td;
    function ua(a, b) {
        var c, d, e = 0, f = typeof a.getElementsByTagName !== K ? a.getElementsByTagName(b || "*") : typeof a.querySelectorAll !== K ? a.querySelectorAll(b || "*") : void 0;
        if (!f) for (f = [], c = a.childNodes || a; null != (d = c[e]); e++) !b || m.nodeName(d, b) ? f.push(d) : m.merge(f, ua(d, b));
        return void 0 === b || b && m.nodeName(a, b) ? m.merge([ a ], f) : f;
    }
    function va(a) {
        W.test(a.type) && (a.defaultChecked = a.checked);
    }
    function wa(a, b) {
        return m.nodeName(a, "table") && m.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a;
    }
    function xa(a) {
        return a.type = (null !== m.find.attr(a, "type")) + "/" + a.type, a;
    }
    function ya(a) {
        var b = pa.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a;
    }
    function za(a, b) {
        for (var c, d = 0; null != (c = a[d]); d++) m._data(c, "globalEval", !b || m._data(b[d], "globalEval"));
    }
    function Aa(a, b) {
        if (1 === b.nodeType && m.hasData(a)) {
            var c, d, e, f = m._data(a), g = m._data(b, f), h = f.events;
            if (h) {
                delete g.handle, g.events = {};
                for (c in h) for (d = 0, e = h[c].length; e > d; d++) m.event.add(b, c, h[c][d]);
            }
            g.data && (g.data = m.extend({}, g.data));
        }
    }
    function Ba(a, b) {
        var c, d, e;
        if (1 === b.nodeType) {
            if (c = b.nodeName.toLowerCase(), !k.noCloneEvent && b[m.expando]) {
                e = m._data(b);
                for (d in e.events) m.removeEvent(b, d, e.handle);
                b.removeAttribute(m.expando);
            }
            "script" === c && b.text !== a.text ? (xa(b).text = a.text, ya(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), 
            k.html5Clone && a.innerHTML && !m.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && W.test(a.type) ? (b.defaultChecked = b.checked = a.checked, 
            b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : ("input" === c || "textarea" === c) && (b.defaultValue = a.defaultValue);
        }
    }
    m.extend({
        clone: function(a, b, c) {
            var d, e, f, g, h, i = m.contains(a.ownerDocument, a);
            if (k.html5Clone || m.isXMLDoc(a) || !ga.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (ta.innerHTML = a.outerHTML, 
            ta.removeChild(f = ta.firstChild)), !(k.noCloneEvent && k.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || m.isXMLDoc(a))) for (d = ua(f), 
            h = ua(a), g = 0; null != (e = h[g]); ++g) d[g] && Ba(e, d[g]);
            if (b) if (c) for (h = h || ua(a), d = d || ua(f), g = 0; null != (e = h[g]); g++) Aa(e, d[g]); else Aa(a, f);
            return d = ua(f, "script"), d.length > 0 && za(d, !i && ua(a, "script")), d = h = e = null, 
            f;
        },
        buildFragment: function(a, b, c, d) {
            for (var e, f, g, h, i, j, l, n = a.length, o = da(b), p = [], q = 0; n > q; q++) if (f = a[q], 
            f || 0 === f) if ("object" === m.type(f)) m.merge(p, f.nodeType ? [ f ] : f); else if (la.test(f)) {
                h = h || o.appendChild(b.createElement("div")), i = (ja.exec(f) || [ "", "" ])[1].toLowerCase(), 
                l = ra[i] || ra._default, h.innerHTML = l[1] + f.replace(ia, "<$1></$2>") + l[2], 
                e = l[0];
                while (e--) h = h.lastChild;
                if (!k.leadingWhitespace && ha.test(f) && p.push(b.createTextNode(ha.exec(f)[0])), 
                !k.tbody) {
                    f = "table" !== i || ka.test(f) ? "<table>" !== l[1] || ka.test(f) ? 0 : h : h.firstChild, 
                    e = f && f.childNodes.length;
                    while (e--) m.nodeName(j = f.childNodes[e], "tbody") && !j.childNodes.length && f.removeChild(j);
                }
                m.merge(p, h.childNodes), h.textContent = "";
                while (h.firstChild) h.removeChild(h.firstChild);
                h = o.lastChild;
            } else p.push(b.createTextNode(f));
            h && o.removeChild(h), k.appendChecked || m.grep(ua(p, "input"), va), q = 0;
            while (f = p[q++]) if ((!d || -1 === m.inArray(f, d)) && (g = m.contains(f.ownerDocument, f), 
            h = ua(o.appendChild(f), "script"), g && za(h), c)) {
                e = 0;
                while (f = h[e++]) oa.test(f.type || "") && c.push(f);
            }
            return h = null, o;
        },
        cleanData: function(a, b) {
            for (var d, e, f, g, h = 0, i = m.expando, j = m.cache, l = k.deleteExpando, n = m.event.special; null != (d = a[h]); h++) if ((b || m.acceptData(d)) && (f = d[i], 
            g = f && j[f])) {
                if (g.events) for (e in g.events) n[e] ? m.event.remove(d, e) : m.removeEvent(d, e, g.handle);
                j[f] && (delete j[f], l ? delete d[i] : typeof d.removeAttribute !== K ? d.removeAttribute(i) : d[i] = null, 
                c.push(f));
            }
        }
    }), m.fn.extend({
        text: function(a) {
            return V(this, function(a) {
                return void 0 === a ? m.text(this) : this.empty().append((this[0] && this[0].ownerDocument || y).createTextNode(a));
            }, null, a, arguments.length);
        },
        append: function() {
            return this.domManip(arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = wa(this, a);
                    b.appendChild(a);
                }
            });
        },
        prepend: function() {
            return this.domManip(arguments, function(a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = wa(this, a);
                    b.insertBefore(a, b.firstChild);
                }
            });
        },
        before: function() {
            return this.domManip(arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this);
            });
        },
        after: function() {
            return this.domManip(arguments, function(a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
            });
        },
        remove: function(a, b) {
            for (var c, d = a ? m.filter(a, this) : this, e = 0; null != (c = d[e]); e++) b || 1 !== c.nodeType || m.cleanData(ua(c)), 
            c.parentNode && (b && m.contains(c.ownerDocument, c) && za(ua(c, "script")), c.parentNode.removeChild(c));
            return this;
        },
        empty: function() {
            for (var a, b = 0; null != (a = this[b]); b++) {
                1 === a.nodeType && m.cleanData(ua(a, !1));
                while (a.firstChild) a.removeChild(a.firstChild);
                a.options && m.nodeName(a, "select") && (a.options.length = 0);
            }
            return this;
        },
        clone: function(a, b) {
            return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function() {
                return m.clone(this, a, b);
            });
        },
        html: function(a) {
            return V(this, function(a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(fa, "") : void 0;
                if (!("string" != typeof a || ma.test(a) || !k.htmlSerialize && ga.test(a) || !k.leadingWhitespace && ha.test(a) || ra[(ja.exec(a) || [ "", "" ])[1].toLowerCase()])) {
                    a = a.replace(ia, "<$1></$2>");
                    try {
                        for (;d > c; c++) b = this[c] || {}, 1 === b.nodeType && (m.cleanData(ua(b, !1)), 
                        b.innerHTML = a);
                        b = 0;
                    } catch (e) {}
                }
                b && this.empty().append(a);
            }, null, a, arguments.length);
        },
        replaceWith: function() {
            var a = arguments[0];
            return this.domManip(arguments, function(b) {
                a = this.parentNode, m.cleanData(ua(this)), a && a.replaceChild(b, this);
            }), a && (a.length || a.nodeType) ? this : this.remove();
        },
        detach: function(a) {
            return this.remove(a, !0);
        },
        domManip: function(a, b) {
            a = e.apply([], a);
            var c, d, f, g, h, i, j = 0, l = this.length, n = this, o = l - 1, p = a[0], q = m.isFunction(p);
            if (q || l > 1 && "string" == typeof p && !k.checkClone && na.test(p)) return this.each(function(c) {
                var d = n.eq(c);
                q && (a[0] = p.call(this, c, d.html())), d.domManip(a, b);
            });
            if (l && (i = m.buildFragment(a, this[0].ownerDocument, !1, this), c = i.firstChild, 
            1 === i.childNodes.length && (i = c), c)) {
                for (g = m.map(ua(i, "script"), xa), f = g.length; l > j; j++) d = i, j !== o && (d = m.clone(d, !0, !0), 
                f && m.merge(g, ua(d, "script"))), b.call(this[j], d, j);
                if (f) for (h = g[g.length - 1].ownerDocument, m.map(g, ya), j = 0; f > j; j++) d = g[j], 
                oa.test(d.type || "") && !m._data(d, "globalEval") && m.contains(h, d) && (d.src ? m._evalUrl && m._evalUrl(d.src) : m.globalEval((d.text || d.textContent || d.innerHTML || "").replace(qa, "")));
                i = c = null;
            }
            return this;
        }
    }), m.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(a, b) {
        m.fn[a] = function(a) {
            for (var c, d = 0, e = [], g = m(a), h = g.length - 1; h >= d; d++) c = d === h ? this : this.clone(!0), 
            m(g[d])[b](c), f.apply(e, c.get());
            return this.pushStack(e);
        };
    });
    var Ca, Da = {};
    function Ea(b, c) {
        var d, e = m(c.createElement(b)).appendTo(c.body), f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? d.display : m.css(e[0], "display");
        return e.detach(), f;
    }
    function Fa(a) {
        var b = y, c = Da[a];
        return c || (c = Ea(a, b), "none" !== c && c || (Ca = (Ca || m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), 
        b = (Ca[0].contentWindow || Ca[0].contentDocument).document, b.write(), b.close(), 
        c = Ea(a, b), Ca.detach()), Da[a] = c), c;
    }
    !function() {
        var a;
        k.shrinkWrapBlocks = function() {
            if (null != a) return a;
            a = !1;
            var b, c, d;
            return c = y.getElementsByTagName("body")[0], c && c.style ? (b = y.createElement("div"), 
            d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", 
            c.appendChild(d).appendChild(b), typeof b.style.zoom !== K && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", 
            b.appendChild(y.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), 
            c.removeChild(d), a) : void 0;
        };
    }();
    var Ga = /^margin/, Ha = new RegExp("^(" + S + ")(?!px)[a-z%]+$", "i"), Ia, Ja, Ka = /^(top|right|bottom|left)$/;
    a.getComputedStyle ? (Ia = function(b) {
        return b.ownerDocument.defaultView.opener ? b.ownerDocument.defaultView.getComputedStyle(b, null) : a.getComputedStyle(b, null);
    }, Ja = function(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Ia(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, c && ("" !== g || m.contains(a.ownerDocument, a) || (g = m.style(a, b)), 
        Ha.test(g) && Ga.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, 
        g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 === g ? g : g + "";
    }) : y.documentElement.currentStyle && (Ia = function(a) {
        return a.currentStyle;
    }, Ja = function(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Ia(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), 
        Ha.test(g) && !Ka.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), 
        h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), 
        void 0 === g ? g : g + "" || "auto";
    });
    function La(a, b) {
        return {
            get: function() {
                var c = a();
                if (null != c) return c ? void delete this.get : (this.get = b).apply(this, arguments);
            }
        };
    }
    !function() {
        var b, c, d, e, f, g, h;
        if (b = y.createElement("div"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", 
        d = b.getElementsByTagName("a")[0], c = d && d.style) {
            c.cssText = "float:left;opacity:.5", k.opacity = "0.5" === c.opacity, k.cssFloat = !!c.cssFloat, 
            b.style.backgroundClip = "content-box", b.cloneNode(!0).style.backgroundClip = "", 
            k.clearCloneStyle = "content-box" === b.style.backgroundClip, k.boxSizing = "" === c.boxSizing || "" === c.MozBoxSizing || "" === c.WebkitBoxSizing, 
            m.extend(k, {
                reliableHiddenOffsets: function() {
                    return null == g && i(), g;
                },
                boxSizingReliable: function() {
                    return null == f && i(), f;
                },
                pixelPosition: function() {
                    return null == e && i(), e;
                },
                reliableMarginRight: function() {
                    return null == h && i(), h;
                }
            });
            function i() {
                var b, c, d, i;
                c = y.getElementsByTagName("body")[0], c && c.style && (b = y.createElement("div"), 
                d = y.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", 
                c.appendChild(d).appendChild(b), b.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", 
                e = f = !1, h = !0, a.getComputedStyle && (e = "1%" !== (a.getComputedStyle(b, null) || {}).top, 
                f = "4px" === (a.getComputedStyle(b, null) || {
                    width: "4px"
                }).width, i = b.appendChild(y.createElement("div")), i.style.cssText = b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", 
                i.style.marginRight = i.style.width = "0", b.style.width = "1px", h = !parseFloat((a.getComputedStyle(i, null) || {}).marginRight), 
                b.removeChild(i)), b.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", 
                i = b.getElementsByTagName("td"), i[0].style.cssText = "margin:0;border:0;padding:0;display:none", 
                g = 0 === i[0].offsetHeight, g && (i[0].style.display = "", i[1].style.display = "none", 
                g = 0 === i[0].offsetHeight), c.removeChild(d));
            }
        }
    }(), m.swap = function(a, b, c, d) {
        var e, f, g = {};
        for (f in b) g[f] = a.style[f], a.style[f] = b[f];
        e = c.apply(a, d || []);
        for (f in b) a.style[f] = g[f];
        return e;
    };
    var Ma = /alpha\([^)]*\)/i, Na = /opacity\s*=\s*([^)]*)/, Oa = /^(none|table(?!-c[ea]).+)/, Pa = new RegExp("^(" + S + ")(.*)$", "i"), Qa = new RegExp("^([+-])=(" + S + ")", "i"), Ra = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, Sa = {
        letterSpacing: "0",
        fontWeight: "400"
    }, Ta = [ "Webkit", "O", "Moz", "ms" ];
    function Ua(a, b) {
        if (b in a) return b;
        var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = Ta.length;
        while (e--) if (b = Ta[e] + c, b in a) return b;
        return d;
    }
    function Va(a, b) {
        for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = m._data(d, "olddisplay"), 
        c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && U(d) && (f[g] = m._data(d, "olddisplay", Fa(d.nodeName)))) : (e = U(d), 
        (c && "none" !== c || !e) && m._data(d, "olddisplay", e ? c : m.css(d, "display"))));
        for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
        return a;
    }
    function Wa(a, b, c) {
        var d = Pa.exec(b);
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b;
    }
    function Xa(a, b, c, d, e) {
        for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += m.css(a, c + T[f], !0, e)), 
        d ? ("content" === c && (g -= m.css(a, "padding" + T[f], !0, e)), "margin" !== c && (g -= m.css(a, "border" + T[f] + "Width", !0, e))) : (g += m.css(a, "padding" + T[f], !0, e), 
        "padding" !== c && (g += m.css(a, "border" + T[f] + "Width", !0, e)));
        return g;
    }
    function Ya(a, b, c) {
        var d = !0, e = "width" === b ? a.offsetWidth : a.offsetHeight, f = Ia(a), g = k.boxSizing && "border-box" === m.css(a, "boxSizing", !1, f);
        if (0 >= e || null == e) {
            if (e = Ja(a, b, f), (0 > e || null == e) && (e = a.style[b]), Ha.test(e)) return e;
            d = g && (k.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0;
        }
        return e + Xa(a, b, c || (g ? "border" : "content"), d, f) + "px";
    }
    m.extend({
        cssHooks: {
            opacity: {
                get: function(a, b) {
                    if (b) {
                        var c = Ja(a, "opacity");
                        return "" === c ? "1" : c;
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": k.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = m.camelCase(b), i = a.style;
                if (b = m.cssProps[h] || (m.cssProps[h] = Ua(i, h)), g = m.cssHooks[b] || m.cssHooks[h], 
                void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
                if (f = typeof c, "string" === f && (e = Qa.exec(c)) && (c = (e[1] + 1) * e[2] + parseFloat(m.css(a, b)), 
                f = "number"), null != c && c === c && ("number" !== f || m.cssNumber[h] || (c += "px"), 
                k.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), 
                !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try {
                    i[b] = c;
                } catch (j) {}
            }
        },
        css: function(a, b, c, d) {
            var e, f, g, h = m.camelCase(b);
            return b = m.cssProps[h] || (m.cssProps[h] = Ua(a.style, h)), g = m.cssHooks[b] || m.cssHooks[h], 
            g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = Ja(a, b, d)), "normal" === f && b in Sa && (f = Sa[b]), 
            "" === c || c ? (e = parseFloat(f), c === !0 || m.isNumeric(e) ? e || 0 : f) : f;
        }
    }), m.each([ "height", "width" ], function(a, b) {
        m.cssHooks[b] = {
            get: function(a, c, d) {
                return c ? Oa.test(m.css(a, "display")) && 0 === a.offsetWidth ? m.swap(a, Ra, function() {
                    return Ya(a, b, d);
                }) : Ya(a, b, d) : void 0;
            },
            set: function(a, c, d) {
                var e = d && Ia(a);
                return Wa(a, c, d ? Xa(a, b, d, k.boxSizing && "border-box" === m.css(a, "boxSizing", !1, e), e) : 0);
            }
        };
    }), k.opacity || (m.cssHooks.opacity = {
        get: function(a, b) {
            return Na.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : "";
        },
        set: function(a, b) {
            var c = a.style, d = a.currentStyle, e = m.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "", f = d && d.filter || c.filter || "";
            c.zoom = 1, (b >= 1 || "" === b) && "" === m.trim(f.replace(Ma, "")) && c.removeAttribute && (c.removeAttribute("filter"), 
            "" === b || d && !d.filter) || (c.filter = Ma.test(f) ? f.replace(Ma, e) : f + " " + e);
        }
    }), m.cssHooks.marginRight = La(k.reliableMarginRight, function(a, b) {
        return b ? m.swap(a, {
            display: "inline-block"
        }, Ja, [ a, "marginRight" ]) : void 0;
    }), m.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(a, b) {
        m.cssHooks[a + b] = {
            expand: function(c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [ c ]; 4 > d; d++) e[a + T[d] + b] = f[d] || f[d - 2] || f[0];
                return e;
            }
        }, Ga.test(a) || (m.cssHooks[a + b].set = Wa);
    }), m.fn.extend({
        css: function(a, b) {
            return V(this, function(a, b, c) {
                var d, e, f = {}, g = 0;
                if (m.isArray(b)) {
                    for (d = Ia(a), e = b.length; e > g; g++) f[b[g]] = m.css(a, b[g], !1, d);
                    return f;
                }
                return void 0 !== c ? m.style(a, b, c) : m.css(a, b);
            }, a, b, arguments.length > 1);
        },
        show: function() {
            return Va(this, !0);
        },
        hide: function() {
            return Va(this);
        },
        toggle: function(a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
                U(this) ? m(this).show() : m(this).hide();
            });
        }
    });
    function Za(a, b, c, d, e) {
        return new Za.prototype.init(a, b, c, d, e);
    }
    m.Tween = Za, Za.prototype = {
        constructor: Za,
        init: function(a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), 
            this.end = d, this.unit = f || (m.cssNumber[c] ? "" : "px");
        },
        cur: function() {
            var a = Za.propHooks[this.prop];
            return a && a.get ? a.get(this) : Za.propHooks._default.get(this);
        },
        run: function(a) {
            var b, c = Za.propHooks[this.prop];
            return this.options.duration ? this.pos = b = m.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, 
            this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), 
            c && c.set ? c.set(this) : Za.propHooks._default.set(this), this;
        }
    }, Za.prototype.init.prototype = Za.prototype, Za.propHooks = {
        _default: {
            get: function(a) {
                var b;
                return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = m.css(a.elem, a.prop, ""), 
                b && "auto" !== b ? b : 0) : a.elem[a.prop];
            },
            set: function(a) {
                m.fx.step[a.prop] ? m.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[m.cssProps[a.prop]] || m.cssHooks[a.prop]) ? m.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now;
            }
        }
    }, Za.propHooks.scrollTop = Za.propHooks.scrollLeft = {
        set: function(a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
        }
    }, m.easing = {
        linear: function(a) {
            return a;
        },
        swing: function(a) {
            return .5 - Math.cos(a * Math.PI) / 2;
        }
    }, m.fx = Za.prototype.init, m.fx.step = {};
    var $a, _a, ab = /^(?:toggle|show|hide)$/, bb = new RegExp("^(?:([+-])=|)(" + S + ")([a-z%]*)$", "i"), cb = /queueHooks$/, db = [ ib ], eb = {
        "*": [ function(a, b) {
            var c = this.createTween(a, b), d = c.cur(), e = bb.exec(b), f = e && e[3] || (m.cssNumber[a] ? "" : "px"), g = (m.cssNumber[a] || "px" !== f && +d) && bb.exec(m.css(c.elem, a)), h = 1, i = 20;
            if (g && g[3] !== f) {
                f = f || g[3], e = e || [], g = +d || 1;
                do h = h || ".5", g /= h, m.style(c.elem, a, g + f); while (h !== (h = c.cur() / d) && 1 !== h && --i);
            }
            return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2]), 
            c;
        } ]
    };
    function fb() {
        return setTimeout(function() {
            $a = void 0;
        }), $a = m.now();
    }
    function gb(a, b) {
        var c, d = {
            height: a
        }, e = 0;
        for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = T[e], d["margin" + c] = d["padding" + c] = a;
        return b && (d.opacity = d.width = a), d;
    }
    function hb(a, b, c) {
        for (var d, e = (eb[b] || []).concat(eb["*"]), f = 0, g = e.length; g > f; f++) if (d = e[f].call(c, b, a)) return d;
    }
    function ib(a, b, c) {
        var d, e, f, g, h, i, j, l, n = this, o = {}, p = a.style, q = a.nodeType && U(a), r = m._data(a, "fxshow");
        c.queue || (h = m._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, 
        h.empty.fire = function() {
            h.unqueued || i();
        }), h.unqueued++, n.always(function() {
            n.always(function() {
                h.unqueued--, m.queue(a, "fx").length || h.empty.fire();
            });
        })), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [ p.overflow, p.overflowX, p.overflowY ], 
        j = m.css(a, "display"), l = "none" === j ? m._data(a, "olddisplay") || Fa(a.nodeName) : j, 
        "inline" === l && "none" === m.css(a, "float") && (k.inlineBlockNeedsLayout && "inline" !== Fa(a.nodeName) ? p.zoom = 1 : p.display = "inline-block")), 
        c.overflow && (p.overflow = "hidden", k.shrinkWrapBlocks() || n.always(function() {
            p.overflow = c.overflow[0], p.overflowX = c.overflow[1], p.overflowY = c.overflow[2];
        }));
        for (d in b) if (e = b[d], ab.exec(e)) {
            if (delete b[d], f = f || "toggle" === e, e === (q ? "hide" : "show")) {
                if ("show" !== e || !r || void 0 === r[d]) continue;
                q = !0;
            }
            o[d] = r && r[d] || m.style(a, d);
        } else j = void 0;
        if (m.isEmptyObject(o)) "inline" === ("none" === j ? Fa(a.nodeName) : j) && (p.display = j); else {
            r ? "hidden" in r && (q = r.hidden) : r = m._data(a, "fxshow", {}), f && (r.hidden = !q), 
            q ? m(a).show() : n.done(function() {
                m(a).hide();
            }), n.done(function() {
                var b;
                m._removeData(a, "fxshow");
                for (b in o) m.style(a, b, o[b]);
            });
            for (d in o) g = hb(q ? r[d] : 0, d, n), d in r || (r[d] = g.start, q && (g.end = g.start, 
            g.start = "width" === d || "height" === d ? 1 : 0));
        }
    }
    function jb(a, b) {
        var c, d, e, f, g;
        for (c in a) if (d = m.camelCase(c), e = b[d], f = a[c], m.isArray(f) && (e = f[1], 
        f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = m.cssHooks[d], g && "expand" in g) {
            f = g.expand(f), delete a[d];
            for (c in f) c in a || (a[c] = f[c], b[c] = e);
        } else b[d] = e;
    }
    function kb(a, b, c) {
        var d, e, f = 0, g = db.length, h = m.Deferred().always(function() {
            delete i.elem;
        }), i = function() {
            if (e) return !1;
            for (var b = $a || fb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);
            return h.notifyWith(a, [ j, f, c ]), 1 > f && i ? c : (h.resolveWith(a, [ j ]), 
            !1);
        }, j = h.promise({
            elem: a,
            props: m.extend({}, b),
            opts: m.extend(!0, {
                specialEasing: {}
            }, c),
            originalProperties: b,
            originalOptions: c,
            startTime: $a || fb(),
            duration: c.duration,
            tweens: [],
            createTween: function(b, c) {
                var d = m.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                return j.tweens.push(d), d;
            },
            stop: function(b) {
                var c = 0, d = b ? j.tweens.length : 0;
                if (e) return this;
                for (e = !0; d > c; c++) j.tweens[c].run(1);
                return b ? h.resolveWith(a, [ j, b ]) : h.rejectWith(a, [ j, b ]), this;
            }
        }), k = j.props;
        for (jb(k, j.opts.specialEasing); g > f; f++) if (d = db[f].call(j, a, k, j.opts)) return d;
        return m.map(k, hb, j), m.isFunction(j.opts.start) && j.opts.start.call(a, j), m.fx.timer(m.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always);
    }
    m.Animation = m.extend(kb, {
        tweener: function(a, b) {
            m.isFunction(a) ? (b = a, a = [ "*" ]) : a = a.split(" ");
            for (var c, d = 0, e = a.length; e > d; d++) c = a[d], eb[c] = eb[c] || [], eb[c].unshift(b);
        },
        prefilter: function(a, b) {
            b ? db.unshift(a) : db.push(a);
        }
    }), m.speed = function(a, b, c) {
        var d = a && "object" == typeof a ? m.extend({}, a) : {
            complete: c || !c && b || m.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !m.isFunction(b) && b
        };
        return d.duration = m.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in m.fx.speeds ? m.fx.speeds[d.duration] : m.fx.speeds._default, 
        (null == d.queue || d.queue === !0) && (d.queue = "fx"), d.old = d.complete, d.complete = function() {
            m.isFunction(d.old) && d.old.call(this), d.queue && m.dequeue(this, d.queue);
        }, d;
    }, m.fn.extend({
        fadeTo: function(a, b, c, d) {
            return this.filter(U).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d);
        },
        animate: function(a, b, c, d) {
            var e = m.isEmptyObject(a), f = m.speed(b, c, d), g = function() {
                var b = kb(this, m.extend({}, a), f);
                (e || m._data(this, "finish")) && b.stop(!0);
            };
            return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g);
        },
        stop: function(a, b, c) {
            var d = function(a) {
                var b = a.stop;
                delete a.stop, b(c);
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), 
            this.each(function() {
                var b = !0, e = null != a && a + "queueHooks", f = m.timers, g = m._data(this);
                if (e) g[e] && g[e].stop && d(g[e]); else for (e in g) g[e] && g[e].stop && cb.test(e) && d(g[e]);
                for (e = f.length; e--; ) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), 
                b = !1, f.splice(e, 1));
                (b || !c) && m.dequeue(this, a);
            });
        },
        finish: function(a) {
            return a !== !1 && (a = a || "fx"), this.each(function() {
                var b, c = m._data(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = m.timers, g = d ? d.length : 0;
                for (c.finish = !0, m.queue(this, a, []), e && e.stop && e.stop.call(this, !0), 
                b = f.length; b--; ) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), 
                f.splice(b, 1));
                for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish;
            });
        }
    }), m.each([ "toggle", "show", "hide" ], function(a, b) {
        var c = m.fn[b];
        m.fn[b] = function(a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(gb(b, !0), a, d, e);
        };
    }), m.each({
        slideDown: gb("show"),
        slideUp: gb("hide"),
        slideToggle: gb("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(a, b) {
        m.fn[a] = function(a, c, d) {
            return this.animate(b, a, c, d);
        };
    }), m.timers = [], m.fx.tick = function() {
        var a, b = m.timers, c = 0;
        for ($a = m.now(); c < b.length; c++) a = b[c], a() || b[c] !== a || b.splice(c--, 1);
        b.length || m.fx.stop(), $a = void 0;
    }, m.fx.timer = function(a) {
        m.timers.push(a), a() ? m.fx.start() : m.timers.pop();
    }, m.fx.interval = 13, m.fx.start = function() {
        _a || (_a = setInterval(m.fx.tick, m.fx.interval));
    }, m.fx.stop = function() {
        clearInterval(_a), _a = null;
    }, m.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, m.fn.delay = function(a, b) {
        return a = m.fx ? m.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function(b, c) {
            var d = setTimeout(b, a);
            c.stop = function() {
                clearTimeout(d);
            };
        });
    }, function() {
        var a, b, c, d, e;
        b = y.createElement("div"), b.setAttribute("className", "t"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", 
        d = b.getElementsByTagName("a")[0], c = y.createElement("select"), e = c.appendChild(y.createElement("option")), 
        a = b.getElementsByTagName("input")[0], d.style.cssText = "top:1px", k.getSetAttribute = "t" !== b.className, 
        k.style = /top/.test(d.getAttribute("style")), k.hrefNormalized = "/a" === d.getAttribute("href"), 
        k.checkOn = !!a.value, k.optSelected = e.selected, k.enctype = !!y.createElement("form").enctype, 
        c.disabled = !0, k.optDisabled = !e.disabled, a = y.createElement("input"), a.setAttribute("value", ""), 
        k.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), 
        k.radioValue = "t" === a.value;
    }();
    var lb = /\r/g;
    m.fn.extend({
        val: function(a) {
            var b, c, d, e = this[0];
            {
                if (arguments.length) return d = m.isFunction(a), this.each(function(c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, m(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : m.isArray(e) && (e = m.map(e, function(a) {
                        return null == a ? "" : a + "";
                    })), b = m.valHooks[this.type] || m.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e));
                });
                if (e) return b = m.valHooks[e.type] || m.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, 
                "string" == typeof c ? c.replace(lb, "") : null == c ? "" : c);
            }
        }
    }), m.extend({
        valHooks: {
            option: {
                get: function(a) {
                    var b = m.find.attr(a, "value");
                    return null != b ? b : m.trim(m.text(a));
                }
            },
            select: {
                get: function(a) {
                    for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++) if (c = d[i], 
                    !(!c.selected && i !== e || (k.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && m.nodeName(c.parentNode, "optgroup"))) {
                        if (b = m(c).val(), f) return b;
                        g.push(b);
                    }
                    return g;
                },
                set: function(a, b) {
                    var c, d, e = a.options, f = m.makeArray(b), g = e.length;
                    while (g--) if (d = e[g], m.inArray(m.valHooks.option.get(d), f) >= 0) try {
                        d.selected = c = !0;
                    } catch (h) {
                        d.scrollHeight;
                    } else d.selected = !1;
                    return c || (a.selectedIndex = -1), e;
                }
            }
        }
    }), m.each([ "radio", "checkbox" ], function() {
        m.valHooks[this] = {
            set: function(a, b) {
                return m.isArray(b) ? a.checked = m.inArray(m(a).val(), b) >= 0 : void 0;
            }
        }, k.checkOn || (m.valHooks[this].get = function(a) {
            return null === a.getAttribute("value") ? "on" : a.value;
        });
    });
    var mb, nb, ob = m.expr.attrHandle, pb = /^(?:checked|selected)$/i, qb = k.getSetAttribute, rb = k.input;
    m.fn.extend({
        attr: function(a, b) {
            return V(this, m.attr, a, b, arguments.length > 1);
        },
        removeAttr: function(a) {
            return this.each(function() {
                m.removeAttr(this, a);
            });
        }
    }), m.extend({
        attr: function(a, b, c) {
            var d, e, f = a.nodeType;
            if (a && 3 !== f && 8 !== f && 2 !== f) return typeof a.getAttribute === K ? m.prop(a, b, c) : (1 === f && m.isXMLDoc(a) || (b = b.toLowerCase(), 
            d = m.attrHooks[b] || (m.expr.match.bool.test(b) ? nb : mb)), void 0 === c ? d && "get" in d && null !== (e = d.get(a, b)) ? e : (e = m.find.attr(a, b), 
            null == e ? void 0 : e) : null !== c ? d && "set" in d && void 0 !== (e = d.set(a, c, b)) ? e : (a.setAttribute(b, c + ""), 
            c) : void m.removeAttr(a, b));
        },
        removeAttr: function(a, b) {
            var c, d, e = 0, f = b && b.match(E);
            if (f && 1 === a.nodeType) while (c = f[e++]) d = m.propFix[c] || c, m.expr.match.bool.test(c) ? rb && qb || !pb.test(c) ? a[d] = !1 : a[m.camelCase("default-" + c)] = a[d] = !1 : m.attr(a, c, ""), 
            a.removeAttribute(qb ? c : d);
        },
        attrHooks: {
            type: {
                set: function(a, b) {
                    if (!k.radioValue && "radio" === b && m.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b;
                    }
                }
            }
        }
    }), nb = {
        set: function(a, b, c) {
            return b === !1 ? m.removeAttr(a, c) : rb && qb || !pb.test(c) ? a.setAttribute(!qb && m.propFix[c] || c, c) : a[m.camelCase("default-" + c)] = a[c] = !0, 
            c;
        }
    }, m.each(m.expr.match.bool.source.match(/\w+/g), function(a, b) {
        var c = ob[b] || m.find.attr;
        ob[b] = rb && qb || !pb.test(b) ? function(a, b, d) {
            var e, f;
            return d || (f = ob[b], ob[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, 
            ob[b] = f), e;
        } : function(a, b, c) {
            return c ? void 0 : a[m.camelCase("default-" + b)] ? b.toLowerCase() : null;
        };
    }), rb && qb || (m.attrHooks.value = {
        set: function(a, b, c) {
            return m.nodeName(a, "input") ? void (a.defaultValue = b) : mb && mb.set(a, b, c);
        }
    }), qb || (mb = {
        set: function(a, b, c) {
            var d = a.getAttributeNode(c);
            return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", 
            "value" === c || b === a.getAttribute(c) ? b : void 0;
        }
    }, ob.id = ob.name = ob.coords = function(a, b, c) {
        var d;
        return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null;
    }, m.valHooks.button = {
        get: function(a, b) {
            var c = a.getAttributeNode(b);
            return c && c.specified ? c.value : void 0;
        },
        set: mb.set
    }, m.attrHooks.contenteditable = {
        set: function(a, b, c) {
            mb.set(a, "" === b ? !1 : b, c);
        }
    }, m.each([ "width", "height" ], function(a, b) {
        m.attrHooks[b] = {
            set: function(a, c) {
                return "" === c ? (a.setAttribute(b, "auto"), c) : void 0;
            }
        };
    })), k.style || (m.attrHooks.style = {
        get: function(a) {
            return a.style.cssText || void 0;
        },
        set: function(a, b) {
            return a.style.cssText = b + "";
        }
    });
    var sb = /^(?:input|select|textarea|button|object)$/i, tb = /^(?:a|area)$/i;
    m.fn.extend({
        prop: function(a, b) {
            return V(this, m.prop, a, b, arguments.length > 1);
        },
        removeProp: function(a) {
            return a = m.propFix[a] || a, this.each(function() {
                try {
                    this[a] = void 0, delete this[a];
                } catch (b) {}
            });
        }
    }), m.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(a, b, c) {
            var d, e, f, g = a.nodeType;
            if (a && 3 !== g && 8 !== g && 2 !== g) return f = 1 !== g || !m.isXMLDoc(a), f && (b = m.propFix[b] || b, 
            e = m.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b];
        },
        propHooks: {
            tabIndex: {
                get: function(a) {
                    var b = m.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : sb.test(a.nodeName) || tb.test(a.nodeName) && a.href ? 0 : -1;
                }
            }
        }
    }), k.hrefNormalized || m.each([ "href", "src" ], function(a, b) {
        m.propHooks[b] = {
            get: function(a) {
                return a.getAttribute(b, 4);
            }
        };
    }), k.optSelected || (m.propHooks.selected = {
        get: function(a) {
            var b = a.parentNode;
            return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null;
        }
    }), m.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        m.propFix[this.toLowerCase()] = this;
    }), k.enctype || (m.propFix.enctype = "encoding");
    var ub = /[\t\r\n\f]/g;
    m.fn.extend({
        addClass: function(a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = "string" == typeof a && a;
            if (m.isFunction(a)) return this.each(function(b) {
                m(this).addClass(a.call(this, b, this.className));
            });
            if (j) for (b = (a || "").match(E) || []; i > h; h++) if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(ub, " ") : " ")) {
                f = 0;
                while (e = b[f++]) d.indexOf(" " + e + " ") < 0 && (d += e + " ");
                g = m.trim(d), c.className !== g && (c.className = g);
            }
            return this;
        },
        removeClass: function(a) {
            var b, c, d, e, f, g, h = 0, i = this.length, j = 0 === arguments.length || "string" == typeof a && a;
            if (m.isFunction(a)) return this.each(function(b) {
                m(this).removeClass(a.call(this, b, this.className));
            });
            if (j) for (b = (a || "").match(E) || []; i > h; h++) if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(ub, " ") : "")) {
                f = 0;
                while (e = b[f++]) while (d.indexOf(" " + e + " ") >= 0) d = d.replace(" " + e + " ", " ");
                g = a ? m.trim(d) : "", c.className !== g && (c.className = g);
            }
            return this;
        },
        toggleClass: function(a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : this.each(m.isFunction(a) ? function(c) {
                m(this).toggleClass(a.call(this, c, this.className, b), b);
            } : function() {
                if ("string" === c) {
                    var b, d = 0, e = m(this), f = a.match(E) || [];
                    while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
                } else (c === K || "boolean" === c) && (this.className && m._data(this, "__className__", this.className), 
                this.className = this.className || a === !1 ? "" : m._data(this, "__className__") || "");
            });
        },
        hasClass: function(a) {
            for (var b = " " + a + " ", c = 0, d = this.length; d > c; c++) if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(ub, " ").indexOf(b) >= 0) return !0;
            return !1;
        }
    }), m.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(a, b) {
        m.fn[b] = function(a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
        };
    }), m.fn.extend({
        hover: function(a, b) {
            return this.mouseenter(a).mouseleave(b || a);
        },
        bind: function(a, b, c) {
            return this.on(a, null, b, c);
        },
        unbind: function(a, b) {
            return this.off(a, null, b);
        },
        delegate: function(a, b, c, d) {
            return this.on(b, a, c, d);
        },
        undelegate: function(a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
        }
    });
    var vb = m.now(), wb = /\?/, xb = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    m.parseJSON = function(b) {
        if (a.JSON && a.JSON.parse) return a.JSON.parse(b + "");
        var c, d = null, e = m.trim(b + "");
        return e && !m.trim(e.replace(xb, function(a, b, e, f) {
            return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "");
        })) ? Function("return " + e)() : m.error("Invalid JSON: " + b);
    }, m.parseXML = function(b) {
        var c, d;
        if (!b || "string" != typeof b) return null;
        try {
            a.DOMParser ? (d = new DOMParser(), c = d.parseFromString(b, "text/xml")) : (c = new ActiveXObject("Microsoft.XMLDOM"), 
            c.async = "false", c.loadXML(b));
        } catch (e) {
            c = void 0;
        }
        return c && c.documentElement && !c.getElementsByTagName("parsererror").length || m.error("Invalid XML: " + b), 
        c;
    };
    var yb, zb, Ab = /#.*$/, Bb = /([?&])_=[^&]*/, Cb = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, Db = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Eb = /^(?:GET|HEAD)$/, Fb = /^\/\//, Gb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Hb = {}, Ib = {}, Jb = "*/".concat("*");
    try {
        zb = location.href;
    } catch (Kb) {
        zb = y.createElement("a"), zb.href = "", zb = zb.href;
    }
    yb = Gb.exec(zb.toLowerCase()) || [];
    function Lb(a) {
        return function(b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0, f = b.toLowerCase().match(E) || [];
            if (m.isFunction(c)) while (d = f[e++]) "+" === d.charAt(0) ? (d = d.slice(1) || "*", 
            (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c);
        };
    }
    function Mb(a, b, c, d) {
        var e = {}, f = a === Ib;
        function g(h) {
            var i;
            return e[h] = !0, m.each(a[h] || [], function(a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), 
                g(j), !1);
            }), i;
        }
        return g(b.dataTypes[0]) || !e["*"] && g("*");
    }
    function Nb(a, b) {
        var c, d, e = m.ajaxSettings.flatOptions || {};
        for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
        return c && m.extend(!0, a, c), a;
    }
    function Ob(a, b, c) {
        var d, e, f, g, h = a.contents, i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
        if (e) for (g in h) if (h[g] && h[g].test(e)) {
            i.unshift(g);
            break;
        }
        if (i[0] in c) f = i[0]; else {
            for (g in c) {
                if (!i[0] || a.converters[g + " " + i[0]]) {
                    f = g;
                    break;
                }
                d || (d = g);
            }
            f = f || d;
        }
        return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0;
    }
    function Pb(a, b, c, d) {
        var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
        if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f) if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), 
        i = f, f = k.shift()) if ("*" === f) f = i; else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g) for (e in j) if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                break;
            }
            if (g !== !0) if (g && a["throws"]) b = g(b); else try {
                b = g(b);
            } catch (l) {
                return {
                    state: "parsererror",
                    error: g ? l : "No conversion from " + i + " to " + f
                };
            }
        }
        return {
            state: "success",
            data: b
        };
    }
    m.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: zb,
            type: "GET",
            isLocal: Db.test(yb[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Jb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": m.parseJSON,
                "text xml": m.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(a, b) {
            return b ? Nb(Nb(a, m.ajaxSettings), b) : Nb(m.ajaxSettings, a);
        },
        ajaxPrefilter: Lb(Hb),
        ajaxTransport: Lb(Ib),
        ajax: function(a, b) {
            "object" == typeof a && (b = a, a = void 0), b = b || {};
            var c, d, e, f, g, h, i, j, k = m.ajaxSetup({}, b), l = k.context || k, n = k.context && (l.nodeType || l.jquery) ? m(l) : m.event, o = m.Deferred(), p = m.Callbacks("once memory"), q = k.statusCode || {}, r = {}, s = {}, t = 0, u = "canceled", v = {
                readyState: 0,
                getResponseHeader: function(a) {
                    var b;
                    if (2 === t) {
                        if (!j) {
                            j = {};
                            while (b = Cb.exec(f)) j[b[1].toLowerCase()] = b[2];
                        }
                        b = j[a.toLowerCase()];
                    }
                    return null == b ? null : b;
                },
                getAllResponseHeaders: function() {
                    return 2 === t ? f : null;
                },
                setRequestHeader: function(a, b) {
                    var c = a.toLowerCase();
                    return t || (a = s[c] = s[c] || a, r[a] = b), this;
                },
                overrideMimeType: function(a) {
                    return t || (k.mimeType = a), this;
                },
                statusCode: function(a) {
                    var b;
                    if (a) if (2 > t) for (b in a) q[b] = [ q[b], a[b] ]; else v.always(a[v.status]);
                    return this;
                },
                abort: function(a) {
                    var b = a || u;
                    return i && i.abort(b), x(0, b), this;
                }
            };
            if (o.promise(v).complete = p.add, v.success = v.done, v.error = v.fail, k.url = ((a || k.url || zb) + "").replace(Ab, "").replace(Fb, yb[1] + "//"), 
            k.type = b.method || b.type || k.method || k.type, k.dataTypes = m.trim(k.dataType || "*").toLowerCase().match(E) || [ "" ], 
            null == k.crossDomain && (c = Gb.exec(k.url.toLowerCase()), k.crossDomain = !(!c || c[1] === yb[1] && c[2] === yb[2] && (c[3] || ("http:" === c[1] ? "80" : "443")) === (yb[3] || ("http:" === yb[1] ? "80" : "443")))), 
            k.data && k.processData && "string" != typeof k.data && (k.data = m.param(k.data, k.traditional)), 
            Mb(Hb, k, b, v), 2 === t) return v;
            h = m.event && k.global, h && 0 === m.active++ && m.event.trigger("ajaxStart"), 
            k.type = k.type.toUpperCase(), k.hasContent = !Eb.test(k.type), e = k.url, k.hasContent || (k.data && (e = k.url += (wb.test(e) ? "&" : "?") + k.data, 
            delete k.data), k.cache === !1 && (k.url = Bb.test(e) ? e.replace(Bb, "$1_=" + vb++) : e + (wb.test(e) ? "&" : "?") + "_=" + vb++)), 
            k.ifModified && (m.lastModified[e] && v.setRequestHeader("If-Modified-Since", m.lastModified[e]), 
            m.etag[e] && v.setRequestHeader("If-None-Match", m.etag[e])), (k.data && k.hasContent && k.contentType !== !1 || b.contentType) && v.setRequestHeader("Content-Type", k.contentType), 
            v.setRequestHeader("Accept", k.dataTypes[0] && k.accepts[k.dataTypes[0]] ? k.accepts[k.dataTypes[0]] + ("*" !== k.dataTypes[0] ? ", " + Jb + "; q=0.01" : "") : k.accepts["*"]);
            for (d in k.headers) v.setRequestHeader(d, k.headers[d]);
            if (k.beforeSend && (k.beforeSend.call(l, v, k) === !1 || 2 === t)) return v.abort();
            u = "abort";
            for (d in {
                success: 1,
                error: 1,
                complete: 1
            }) v[d](k[d]);
            if (i = Mb(Ib, k, b, v)) {
                v.readyState = 1, h && n.trigger("ajaxSend", [ v, k ]), k.async && k.timeout > 0 && (g = setTimeout(function() {
                    v.abort("timeout");
                }, k.timeout));
                try {
                    t = 1, i.send(r, x);
                } catch (w) {
                    if (!(2 > t)) throw w;
                    x(-1, w);
                }
            } else x(-1, "No Transport");
            function x(a, b, c, d) {
                var j, r, s, u, w, x = b;
                2 !== t && (t = 2, g && clearTimeout(g), i = void 0, f = d || "", v.readyState = a > 0 ? 4 : 0, 
                j = a >= 200 && 300 > a || 304 === a, c && (u = Ob(k, v, c)), u = Pb(k, u, v, j), 
                j ? (k.ifModified && (w = v.getResponseHeader("Last-Modified"), w && (m.lastModified[e] = w), 
                w = v.getResponseHeader("etag"), w && (m.etag[e] = w)), 204 === a || "HEAD" === k.type ? x = "nocontent" : 304 === a ? x = "notmodified" : (x = u.state, 
                r = u.data, s = u.error, j = !s)) : (s = x, (a || !x) && (x = "error", 0 > a && (a = 0))), 
                v.status = a, v.statusText = (b || x) + "", j ? o.resolveWith(l, [ r, x, v ]) : o.rejectWith(l, [ v, x, s ]), 
                v.statusCode(q), q = void 0, h && n.trigger(j ? "ajaxSuccess" : "ajaxError", [ v, k, j ? r : s ]), 
                p.fireWith(l, [ v, x ]), h && (n.trigger("ajaxComplete", [ v, k ]), --m.active || m.event.trigger("ajaxStop")));
            }
            return v;
        },
        getJSON: function(a, b, c) {
            return m.get(a, b, c, "json");
        },
        getScript: function(a, b) {
            return m.get(a, void 0, b, "script");
        }
    }), m.each([ "get", "post" ], function(a, b) {
        m[b] = function(a, c, d, e) {
            return m.isFunction(c) && (e = e || d, d = c, c = void 0), m.ajax({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            });
        };
    }), m._evalUrl = function(a) {
        return m.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        });
    }, m.fn.extend({
        wrapAll: function(a) {
            if (m.isFunction(a)) return this.each(function(b) {
                m(this).wrapAll(a.call(this, b));
            });
            if (this[0]) {
                var b = m(a, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
                    var a = this;
                    while (a.firstChild && 1 === a.firstChild.nodeType) a = a.firstChild;
                    return a;
                }).append(this);
            }
            return this;
        },
        wrapInner: function(a) {
            return this.each(m.isFunction(a) ? function(b) {
                m(this).wrapInner(a.call(this, b));
            } : function() {
                var b = m(this), c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a);
            });
        },
        wrap: function(a) {
            var b = m.isFunction(a);
            return this.each(function(c) {
                m(this).wrapAll(b ? a.call(this, c) : a);
            });
        },
        unwrap: function() {
            return this.parent().each(function() {
                m.nodeName(this, "body") || m(this).replaceWith(this.childNodes);
            }).end();
        }
    }), m.expr.filters.hidden = function(a) {
        return a.offsetWidth <= 0 && a.offsetHeight <= 0 || !k.reliableHiddenOffsets() && "none" === (a.style && a.style.display || m.css(a, "display"));
    }, m.expr.filters.visible = function(a) {
        return !m.expr.filters.hidden(a);
    };
    var Qb = /%20/g, Rb = /\[\]$/, Sb = /\r?\n/g, Tb = /^(?:submit|button|image|reset|file)$/i, Ub = /^(?:input|select|textarea|keygen)/i;
    function Vb(a, b, c, d) {
        var e;
        if (m.isArray(b)) m.each(b, function(b, e) {
            c || Rb.test(a) ? d(a, e) : Vb(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d);
        }); else if (c || "object" !== m.type(b)) d(a, b); else for (e in b) Vb(a + "[" + e + "]", b[e], c, d);
    }
    m.param = function(a, b) {
        var c, d = [], e = function(a, b) {
            b = m.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b);
        };
        if (void 0 === b && (b = m.ajaxSettings && m.ajaxSettings.traditional), m.isArray(a) || a.jquery && !m.isPlainObject(a)) m.each(a, function() {
            e(this.name, this.value);
        }); else for (c in a) Vb(c, a[c], b, e);
        return d.join("&").replace(Qb, "+");
    }, m.fn.extend({
        serialize: function() {
            return m.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                var a = m.prop(this, "elements");
                return a ? m.makeArray(a) : this;
            }).filter(function() {
                var a = this.type;
                return this.name && !m(this).is(":disabled") && Ub.test(this.nodeName) && !Tb.test(a) && (this.checked || !W.test(a));
            }).map(function(a, b) {
                var c = m(this).val();
                return null == c ? null : m.isArray(c) ? m.map(c, function(a) {
                    return {
                        name: b.name,
                        value: a.replace(Sb, "\r\n")
                    };
                }) : {
                    name: b.name,
                    value: c.replace(Sb, "\r\n")
                };
            }).get();
        }
    }), m.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function() {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && Zb() || $b();
    } : Zb;
    var Wb = 0, Xb = {}, Yb = m.ajaxSettings.xhr();
    a.attachEvent && a.attachEvent("onunload", function() {
        for (var a in Xb) Xb[a](void 0, !0);
    }), k.cors = !!Yb && "withCredentials" in Yb, Yb = k.ajax = !!Yb, Yb && m.ajaxTransport(function(a) {
        if (!a.crossDomain || k.cors) {
            var b;
            return {
                send: function(c, d) {
                    var e, f = a.xhr(), g = ++Wb;
                    if (f.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields) for (e in a.xhrFields) f[e] = a.xhrFields[e];
                    a.mimeType && f.overrideMimeType && f.overrideMimeType(a.mimeType), a.crossDomain || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest");
                    for (e in c) void 0 !== c[e] && f.setRequestHeader(e, c[e] + "");
                    f.send(a.hasContent && a.data || null), b = function(c, e) {
                        var h, i, j;
                        if (b && (e || 4 === f.readyState)) if (delete Xb[g], b = void 0, f.onreadystatechange = m.noop, 
                        e) 4 !== f.readyState && f.abort(); else {
                            j = {}, h = f.status, "string" == typeof f.responseText && (j.text = f.responseText);
                            try {
                                i = f.statusText;
                            } catch (k) {
                                i = "";
                            }
                            h || !a.isLocal || a.crossDomain ? 1223 === h && (h = 204) : h = j.text ? 200 : 404;
                        }
                        j && d(h, i, j, f.getAllResponseHeaders());
                    }, a.async ? 4 === f.readyState ? setTimeout(b) : f.onreadystatechange = Xb[g] = b : b();
                },
                abort: function() {
                    b && b(void 0, !0);
                }
            };
        }
    });
    function Zb() {
        try {
            return new a.XMLHttpRequest();
        } catch (b) {}
    }
    function $b() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP");
        } catch (b) {}
    }
    m.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(a) {
                return m.globalEval(a), a;
            }
        }
    }), m.ajaxPrefilter("script", function(a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1);
    }), m.ajaxTransport("script", function(a) {
        if (a.crossDomain) {
            var b, c = y.head || m("head")[0] || y.documentElement;
            return {
                send: function(d, e) {
                    b = y.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), 
                    b.src = a.url, b.onload = b.onreadystatechange = function(a, c) {
                        (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, 
                        b.parentNode && b.parentNode.removeChild(b), b = null, c || e(200, "success"));
                    }, c.insertBefore(b, c.firstChild);
                },
                abort: function() {
                    b && b.onload(void 0, !0);
                }
            };
        }
    });
    var _b = [], ac = /(=)\?(?=&|$)|\?\?/;
    m.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var a = _b.pop() || m.expando + "_" + vb++;
            return this[a] = !0, a;
        }
    }), m.ajaxPrefilter("json jsonp", function(b, c, d) {
        var e, f, g, h = b.jsonp !== !1 && (ac.test(b.url) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && ac.test(b.data) && "data");
        return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = m.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, 
        h ? b[h] = b[h].replace(ac, "$1" + e) : b.jsonp !== !1 && (b.url += (wb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), 
        b.converters["script json"] = function() {
            return g || m.error(e + " was not called"), g[0];
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
            g = arguments;
        }, d.always(function() {
            a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, _b.push(e)), g && m.isFunction(f) && f(g[0]), 
            g = f = void 0;
        }), "script") : void 0;
    }), m.parseHTML = function(a, b, c) {
        if (!a || "string" != typeof a) return null;
        "boolean" == typeof b && (c = b, b = !1), b = b || y;
        var d = u.exec(a), e = !c && [];
        return d ? [ b.createElement(d[1]) ] : (d = m.buildFragment([ a ], b, e), e && e.length && m(e).remove(), 
        m.merge([], d.childNodes));
    };
    var bc = m.fn.load;
    m.fn.load = function(a, b, c) {
        if ("string" != typeof a && bc) return bc.apply(this, arguments);
        var d, e, f, g = this, h = a.indexOf(" ");
        return h >= 0 && (d = m.trim(a.slice(h, a.length)), a = a.slice(0, h)), m.isFunction(b) ? (c = b, 
        b = void 0) : b && "object" == typeof b && (f = "POST"), g.length > 0 && m.ajax({
            url: a,
            type: f,
            dataType: "html",
            data: b
        }).done(function(a) {
            e = arguments, g.html(d ? m("<div>").append(m.parseHTML(a)).find(d) : a);
        }).complete(c && function(a, b) {
            g.each(c, e || [ a.responseText, b, a ]);
        }), this;
    }, m.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(a, b) {
        m.fn[b] = function(a) {
            return this.on(b, a);
        };
    }), m.expr.filters.animated = function(a) {
        return m.grep(m.timers, function(b) {
            return a === b.elem;
        }).length;
    };
    var cc = a.document.documentElement;
    function dc(a) {
        return m.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1;
    }
    m.offset = {
        setOffset: function(a, b, c) {
            var d, e, f, g, h, i, j, k = m.css(a, "position"), l = m(a), n = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = m.css(a, "top"), 
            i = m.css(a, "left"), j = ("absolute" === k || "fixed" === k) && m.inArray("auto", [ f, i ]) > -1, 
            j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), 
            m.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (n.top = b.top - h.top + g), 
            null != b.left && (n.left = b.left - h.left + e), "using" in b ? b.using.call(a, n) : l.css(n);
        }
    }, m.fn.extend({
        offset: function(a) {
            if (arguments.length) return void 0 === a ? this : this.each(function(b) {
                m.offset.setOffset(this, a, b);
            });
            var b, c, d = {
                top: 0,
                left: 0
            }, e = this[0], f = e && e.ownerDocument;
            if (f) return b = f.documentElement, m.contains(b, e) ? (typeof e.getBoundingClientRect !== K && (d = e.getBoundingClientRect()), 
            c = dc(f), {
                top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
                left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
            }) : d;
        },
        position: function() {
            if (this[0]) {
                var a, b, c = {
                    top: 0,
                    left: 0
                }, d = this[0];
                return "fixed" === m.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), 
                b = this.offset(), m.nodeName(a[0], "html") || (c = a.offset()), c.top += m.css(a[0], "borderTopWidth", !0), 
                c.left += m.css(a[0], "borderLeftWidth", !0)), {
                    top: b.top - c.top - m.css(d, "marginTop", !0),
                    left: b.left - c.left - m.css(d, "marginLeft", !0)
                };
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var a = this.offsetParent || cc;
                while (a && !m.nodeName(a, "html") && "static" === m.css(a, "position")) a = a.offsetParent;
                return a || cc;
            });
        }
    }), m.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(a, b) {
        var c = /Y/.test(b);
        m.fn[a] = function(d) {
            return V(this, function(a, d, e) {
                var f = dc(a);
                return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void (f ? f.scrollTo(c ? m(f).scrollLeft() : e, c ? e : m(f).scrollTop()) : a[d] = e);
            }, a, d, arguments.length, null);
        };
    }), m.each([ "top", "left" ], function(a, b) {
        m.cssHooks[b] = La(k.pixelPosition, function(a, c) {
            return c ? (c = Ja(a, b), Ha.test(c) ? m(a).position()[b] + "px" : c) : void 0;
        });
    }), m.each({
        Height: "height",
        Width: "width"
    }, function(a, b) {
        m.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function(c, d) {
            m.fn[d] = function(d, e) {
                var f = arguments.length && (c || "boolean" != typeof d), g = c || (d === !0 || e === !0 ? "margin" : "border");
                return V(this, function(b, c, d) {
                    var e;
                    return m.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, 
                    Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? m.css(b, c, g) : m.style(b, c, d, g);
                }, b, f ? d : void 0, f, null);
            };
        });
    }), m.fn.size = function() {
        return this.length;
    }, m.fn.andSelf = m.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return m;
    });
    var ec = a.jQuery, fc = a.$;
    return m.noConflict = function(b) {
        return a.$ === m && (a.$ = fc), b && a.jQuery === m && (a.jQuery = ec), m;
    }, typeof b === K && (a.jQuery = a.$ = m), m;
});

(function() {
    var l = new function() {
        function d(a) {
            return a ? 0 : -1;
        }
        var f = this.priority = function(a, b) {
            for (var c = a.exprs, e = 0, f = 0, d = c.length; f < d; f++) {
                var g = c[f];
                if (!~(g = g.e(g.v, b instanceof Date ? b.getTime() : b, b))) return -1;
                e += g;
            }
            return e;
        }, e = this.parse = function(a, b) {
            a || (a = {
                $eq: a
            });
            var c = [];
            if (a.constructor == Object) for (var d in a) {
                var m = k[d] ? d : "$trav", j = a[d], g = j;
                if (h[m]) {
                    if (~d.indexOf(".")) {
                        g = d.split(".");
                        d = g.shift();
                        for (var n = {}, l = n, p = 0, s = g.length - 1; p < s; p++) l = l[g[p]] = {};
                        l[g[p]] = j;
                        g = j = n;
                    }
                    if (j instanceof Array) {
                        g = [];
                        for (n = j.length; n--; ) g.push(e(j[n]));
                    } else g = e(j, d);
                }
                c.push(r(m, d, g));
            } else c.push(r("$eq", d, a));
            var q = {
                exprs: c,
                k: b,
                test: function(a) {
                    return !!~q.priority(a);
                },
                priority: function(a) {
                    return f(q, a);
                }
            };
            return q;
        }, h = this.traversable = {
            $and: !0,
            $or: !0,
            $nor: !0,
            $trav: !0,
            $not: !0
        }, k = this.testers = {
            $eq: function(a, b) {
                return d(a.test(b));
            },
            $ne: function(a, b) {
                return d(!a.test(b));
            },
            $lt: function(a, b) {
                return a > b ? 0 : -1;
            },
            $gt: function(a, b) {
                return a < b ? 0 : -1;
            },
            $lte: function(a, b) {
                return a >= b ? 0 : -1;
            },
            $gte: function(a, b) {
                return a <= b ? 0 : -1;
            },
            $exists: function(a, b) {
                return a === (null != b) ? 0 : -1;
            },
            $in: function(a, b) {
                if (b instanceof Array) for (var c = b.length; c--; ) {
                    if (~a.indexOf(b[c])) return c;
                } else return d(~a.indexOf(b));
                return -1;
            },
            $not: function(a, b) {
                if (!a.test) throw Error("$not test should include an expression, not a value. Use $ne instead.");
                return d(!a.test(b));
            },
            $type: function(a, b, c) {
                return c ? c instanceof a || c.constructor == a ? 0 : -1 : -1;
            },
            $nin: function(a, b) {
                return ~k.$in(a, b) ? -1 : 0;
            },
            $mod: function(a, b) {
                return b % a[0] == a[1] ? 0 : -1;
            },
            $all: function(a, b) {
                for (var c = a.length; c--; ) if (-1 == b.indexOf(a[c])) return -1;
                return 0;
            },
            $size: function(a, b) {
                return b ? a == b.length ? 0 : -1 : -1;
            },
            $or: function(a, b) {
                for (var c = a.length, d = c; c--; ) if (~f(a[c], b)) return c;
                return 0 == d ? 0 : -1;
            },
            $nor: function(a, b) {
                for (var c = a.length; c--; ) if (~f(a[c], b)) return -1;
                return 0;
            },
            $and: function(a, b) {
                for (var c = a.length; c--; ) if (!~f(a[c], b)) return -1;
                return 0;
            },
            $trav: function(a, b) {
                if (b instanceof Array) {
                    for (var c = b.length; c--; ) {
                        var d = b[c];
                        if (d[a.k] && ~f(a, d[a.k])) return c;
                    }
                    return -1;
                }
                return f(a, b ? b[a.k] : void 0);
            }
        }, m = {
            $eq: function(a) {
                return a instanceof RegExp ? a : {
                    test: a instanceof Function ? a : function(b) {
                        return b instanceof Array ? ~b.indexOf(a) : a == b;
                    }
                };
            },
            $ne: function(a) {
                return m.$eq(a);
            }
        }, r = function(a, b, c) {
            c = c instanceof Date ? c.getTime() : c;
            return {
                k: b,
                v: m[a] ? m[a](c) : c,
                e: k[a]
            };
        };
    }(), h = function(d, f, e) {
        "object" != typeof f && (e = f, f = void 0);
        if (e) {
            if ("function" != typeof e) throw Error("Unknown sift selector " + e);
        } else e = function(d) {
            return d;
        };
        var h = e, k = l.parse(d);
        e = function(d) {
            for (var e = [], a, b, c = 0, f = d.length; c < f; c++) a = h(d[c]), ~(b = k.priority(a)) && e.push({
                value: a,
                priority: b
            });
            e.sort(function(a, b) {
                return a.priority > b.priority ? -1 : 1;
            });
            d = Array(e.length);
            for (c = e.length; c--; ) d[c] = e[c].value;
            return d;
        };
        e.test = k.test;
        e.score = k.priority;
        e.query = d;
        return f ? e(f) : e;
    };
    h.use = function(d) {
        d.operators && h.useOperators(d.operators);
    };
    h.useOperators = function(d) {
        for (var f in d) h.useOperator(f, d[f]);
    };
    h.useOperator = function(d, f) {
        var e = {}, e = "object" == typeof f ? f : {
            test: f
        }, h = "$" + d;
        l.testers[h] = e.test;
        if (e.traversable || e.traverse) l.traversable[h] = !0;
    };
    "undefined" != typeof module && "undefined" != typeof module.exports ? module.exports = h : "undefined" != typeof window && (window.sift = h);
})();

(function() {
    var a = this, b = function() {
        var c = function(a) {
            var c = b();
            return null != a && c.init(a), c;
        };
        c.API_ENDPOINT = "https://baas.kinvey.com", c.API_VERSION = "3", c.SDK_VERSION = "1.1.4", 
        c.appKey = null, c.appSecret = null, c.masterSecret = null;
        var d = "appdata", e = "blob", f = "rpc", g = "user", h = null, i = !1, j = function(a) {
            var b = x.get("activeUser");
            return b.then(function(b) {
                if (null == b) return c.setActiveUser(null);
                var d = c.setActiveUser({
                    _id: b[0],
                    _kmd: {
                        authtoken: b[1]
                    }
                });
                if (!1 === a.refresh) return c.getActiveUser();
                var e = a.success, f = a.error;
                return delete a.success, delete a.error, c.User.me(a).then(function(b) {
                    return a.success = e, a.error = f, b;
                }, function(b) {
                    return c.Error.INVALID_CREDENTIALS === b.name && c.setActiveUser(d), a.success = e, 
                    a.error = f, c.Defer.reject(b);
                });
            });
        };
        c.getActiveUser = function() {
            if (!1 === i) throw new c.Error("Kinvey.getActiveUser can only be called after the promise returned by Kinvey.init fulfills or rejects.");
            return h;
        }, c.setActiveUser = function(a) {
            if (null != a && (null == a._id || null == a._kmd || null == a._kmd.authtoken)) throw new c.Error("user argument must contain: _id, _kmd.authtoken.");
            !1 === i && (i = !0);
            var b = c.getActiveUser();
            return h = a, null != a ? x.save("activeUser", [ a._id, a._kmd.authtoken ]) : x.destroy("activeUser"), 
            b;
        }, c.init = function(a) {
            if (a = a || {}, null == a.appKey) throw new c.Error("options argument must contain: appKey.");
            if (null == a.appSecret && null == a.masterSecret) throw new c.Error("options argument must contain: appSecret and/or masterSecret.");
            i = !1, c.appKey = a.appKey, c.appSecret = null != a.appSecret ? a.appSecret : null, 
            c.masterSecret = null != a.masterSecret ? a.masterSecret : null, c.encryptionKey = null != a.encryptionKey ? a.encryptionKey : null;
            var b = c.Sync.init(a.sync).then(function() {
                return j(a);
            });
            return n(b, a);
        }, c.ping = function(a) {
            a = a || {}, a.nocache = null == c.appKey ? !1 : a.nocache;
            var b = c.Persistence.read({
                namespace: d,
                auth: null != c.appKey ? y.All : y.None
            }, a);
            return n(b, a);
        }, c.Error = function(a) {
            this.name = "Kinvey.Error", this.message = a, this.stack = new Error().stack;
        }, c.Error.prototype = new Error(), c.Error.prototype.constructor = c.Error, c.Error.ENTITY_NOT_FOUND = "EntityNotFound", 
        c.Error.COLLECTION_NOT_FOUND = "CollectionNotFound", c.Error.APP_NOT_FOUND = "AppNotFound", 
        c.Error.USER_NOT_FOUND = "UserNotFound", c.Error.BLOB_NOT_FOUND = "BlobNotFound", 
        c.Error.INVALID_CREDENTIALS = "InvalidCredentials", c.Error.KINVEY_INTERNAL_ERROR_RETRY = "KinveyInternalErrorRetry", 
        c.Error.KINVEY_INTERNAL_ERROR_STOP = "KinveyInternalErrorStop", c.Error.USER_ALREADY_EXISTS = "UserAlreadyExists", 
        c.Error.USER_UNAVAILABLE = "UserUnavailable", c.Error.DUPLICATE_END_USERS = "DuplicateEndUsers", 
        c.Error.INSUFFICIENT_CREDENTIALS = "InsufficientCredentials", c.Error.WRITES_TO_COLLECTION_DISALLOWED = "WritesToCollectionDisallowed", 
        c.Error.INDIRECT_COLLECTION_ACCESS_DISALLOWED = "IndirectCollectionAccessDisallowed", 
        c.Error.APP_PROBLEM = "AppProblem", c.Error.PARAMETER_VALUE_OUT_OF_RANGE = "ParameterValueOutOfRange", 
        c.Error.CORS_DISABLED = "CORSDisabled", c.Error.INVALID_QUERY_SYNTAX = "InvalidQuerySyntax", 
        c.Error.MISSING_QUERY = "MissingQuery", c.Error.JSON_PARSE_ERROR = "JSONParseError", 
        c.Error.MISSING_REQUEST_HEADER = "MissingRequestHeader", c.Error.INCOMPLETE_REQUEST_BODY = "IncompleteRequestBody", 
        c.Error.MISSING_REQUEST_PARAMETER = "MissingRequestParameter", c.Error.INVALID_IDENTIFIER = "InvalidIdentifier", 
        c.Error.BAD_REQUEST = "BadRequest", c.Error.FEATURE_UNAVAILABLE = "FeatureUnavailable", 
        c.Error.API_VERSION_NOT_IMPLEMENTED = "APIVersionNotImplemented", c.Error.API_VERSION_NOT_AVAILABLE = "APIVersionNotAvailable", 
        c.Error.INPUT_VALIDATION_FAILED = "InputValidationFailed", c.Error.BL_RUNTIME_ERROR = "BLRuntimeError", 
        c.Error.BL_SYNTAX_ERROR = "BLSyntaxError", c.Error.BL_TIMEOUT_ERROR = "BLTimeoutError", 
        c.Error.OAUTH_TOKEN_REFRESH_ERROR = "OAuthTokenRefreshError", c.Error.BL_VIOLATION_ERROR = "BLViolationError", 
        c.Error.BL_INTERNAL_ERROR = "BLInternalError", c.Error.THIRD_PARTY_TOS_UNACKED = "ThirdPartyTOSUnacked", 
        c.Error.STALE_REQUEST = "StaleRequest", c.Error.DATA_LINK_PARSE_ERROR = "DataLinkParseError", 
        c.Error.NOT_IMPLEMENTED_ERROR = "NotImplementedError", c.Error.EMAIL_VERIFICATION_REQUIRED = "EmailVerificationRequired", 
        c.Error.SORT_LIMIT_EXCEEDED = "SortLimitExceeded", c.Error.INVALID_SHORT_URL = "InvalidShortURL", 
        c.Error.INVALID_OR_MISSING_NONCE = "InvalidOrMissingNonce", c.Error.MISSING_CONFIGURATION = "MissingConfiguration", 
        c.Error.ENDPOINT_DOES_NOT_EXIST = "EndpointDoesNotExist", c.Error.DISALLOWED_QUERY_SYNTAX = "DisallowedQuerySyntax", 
        c.Error.MALFORMED_AUTHENTICATION_HEADER = "MalformedAuthenticationHeader", c.Error.APP_ARCHIVED = "AppArchived", 
        c.Error.BL_NOT_SUPPORTED_FOR_ROUTE = "BLNotSupportedForRoute", c.Error.USER_LOCKED_DOWN = "UserLockedDown", 
        c.Error.ALREADY_LOGGED_IN = "AlreadyLoggedIn", c.Error.DATABASE_ERROR = "DatabaseError", 
        c.Error.MISSING_APP_CREDENTIALS = "MissingAppCredentials", c.Error.MISSING_MASTER_CREDENTIALS = "MissingMasterCredentials", 
        c.Error.NO_ACTIVE_USER = "NoActiveUser", c.Error.REQUEST_ABORT_ERROR = "RequestAbortError", 
        c.Error.REQUEST_ERROR = "RequestError", c.Error.REQUEST_TIMEOUT_ERROR = "RequestTimeoutError", 
        c.Error.SOCIAL_ERROR = "SocialError", c.Error.SYNC_ERROR = "SyncError";
        var k = {};
        k[c.Error.ALREADY_LOGGED_IN] = {
            name: c.Error.ALREADY_LOGGED_IN,
            description: "You are already logged in with another user.",
            debug: "If you want to switch users, logout the active user first using `Kinvey.User.logout`, then try again."
        }, k[c.Error.DATABASE_ERROR] = {
            name: c.Error.DATABASE_ERROR,
            description: "The database used for local persistence encountered an error.",
            debug: ""
        }, k[c.Error.MISSING_APP_CREDENTIALS] = {
            name: c.Error.MISSING_APP_CREDENTIALS,
            description: "Missing credentials: `Kinvey.appKey` and/or `Kinvey.appSecret`.",
            debug: "Did you forget to call `Kinvey.init`?"
        }, k[c.Error.MISSING_MASTER_CREDENTIALS] = {
            name: c.Error.MISSING_MASTER_CREDENTIALS,
            description: "Missing credentials: `Kinvey.appKey` and/or `Kinvey.masterSecret`.",
            debug: "Did you forget to call `Kinvey.init` with your Master Secret?"
        }, k[c.Error.NO_ACTIVE_USER] = {
            name: c.Error.NO_ACTIVE_USER,
            description: "You need to be logged in to execute this request.",
            debug: "Try creating a user using `Kinvey.User.signup`, or login an existing user using `Kinvey.User.login`."
        }, k[c.Error.REQUEST_ABORT_ERROR] = {
            name: c.Error.REQUEST_TIMEOUT_ERROR,
            description: "The request was aborted.",
            debug: ""
        }, k[c.Error.REQUEST_ERROR] = {
            name: c.Error.REQUEST_ERROR,
            description: "The request failed.",
            debug: ""
        }, k[c.Error.REQUEST_TIMEOUT_ERROR] = {
            name: c.Error.REQUEST_TIMEOUT_ERROR,
            description: "The request timed out.",
            debug: ""
        }, k[c.Error.SOCIAL_ERROR] = {
            name: c.Error.SOCIAL_ERROR,
            description: "The social identity cannot be obtained.",
            debug: ""
        }, k[c.Error.SYNC_ERROR] = {
            name: c.Error.SYNC_ERROR,
            description: "The synchronization operation cannot be completed.",
            debug: ""
        };
        var l, m = function(a, b) {
            var c = k[a] || {
                name: a
            };
            return b = b || {}, {
                name: c.name,
                description: b.description || c.description || "",
                debug: b.debug || c.debug || ""
            };
        };
        l = "function" == typeof a.setImmediate ? a.setImmediate : "undefined" != typeof process && process.nextTick ? process.nextTick : function(b) {
            a.setTimeout(b, 0);
        };
        var n = function(a, b) {
            return a.then(function(a) {
                b.success && b.success(a);
            }, function(a) {
                b.error && b.error(a);
            }).then(null, function(a) {
                l(function() {
                    throw a;
                });
            }), a;
        }, o = Array.isArray || function(a) {
            return "[object Array]" === Object.prototype.toString.call(a);
        }, p = function(a) {
            return "function" != typeof / . / ? "function" == typeof a : "[object Function]" === Object.prototype.toString.call(a);
        }, q = function(a) {
            return "[object Number]" === Object.prototype.toString.call(a) && !isNaN(a);
        }, r = function(a) {
            return Object(a) === a;
        }, s = function(a) {
            return "[object RegExp]" === Object.prototype.toString.call(a);
        }, t = function(a) {
            return "[object String]" === Object.prototype.toString.call(a);
        }, u = function(a) {
            if (null == a) return !0;
            if (o(a) || t(a)) return 0 === a.length;
            for (var b in a) if (a.hasOwnProperty(b)) return !1;
            return !0;
        }, v = function(a) {
            return function() {
                throw new c.Error("Method not implemented: " + a);
            };
        }, w = function(a) {
            return function(b) {
                var d = this;
                b = b || {}, a.forEach(function(a) {
                    if ("function" != typeof b[a]) throw new c.Error("Adapter must implement method: " + a);
                }), a.forEach(function(a) {
                    d[a] = function() {
                        return b[a].apply(b, arguments);
                    };
                });
            };
        }, x = {
            destroy: function(a) {
                return x._destroy(x._key(a));
            },
            get: function(a) {
                return x._get(x._key(a));
            },
            save: function(a, b) {
                return x._save(x._key(a), b);
            },
            _destroy: v("Storage.destroy"),
            _get: v("Storage.get"),
            _key: function(a) {
                return [ "Kinvey", c.appKey, a ].join(".");
            },
            _save: v("Storage.set"),
            use: w([ "_destroy", "_get", "_save" ])
        };
        c.Defer = {
            all: function(a) {
                if (!o(a)) throw new c.Error("promises argument must be of type: Array.");
                var b = a.length;
                if (0 === b) return c.Defer.resolve([]);
                var d = c.Defer.deferred(), e = [];
                return a.forEach(function(a, c) {
                    a.then(function(a) {
                        b -= 1, e[c] = a, 0 === b && d.resolve(e);
                    }, function(a) {
                        d.reject(a);
                    });
                }), d.promise;
            },
            resolve: function(a) {
                var b = c.Defer.deferred();
                return b.resolve(a), b.promise;
            },
            reject: function(a) {
                var b = c.Defer.deferred();
                return b.reject(a), b.promise;
            },
            deferred: v("Kinvey.Defer.deferred"),
            use: w([ "deferred" ])
        };
        var y = {
            All: function() {
                return y.Session().then(null, y.Basic);
            },
            App: function() {
                if (null == c.appKey || null == c.appSecret) {
                    var a = m(c.Error.MISSING_APP_CREDENTIALS);
                    return c.Defer.reject(a);
                }
                var b = c.Defer.resolve({
                    scheme: "Basic",
                    username: c.appKey,
                    password: c.appSecret
                });
                return b;
            },
            Basic: function() {
                return y.Master().then(null, y.App);
            },
            Default: function() {
                return y.Session().then(null, function(a) {
                    return y.Master().then(null, function() {
                        return c.Defer.reject(a);
                    });
                });
            },
            Master: function() {
                if (null == c.appKey || null == c.masterSecret) {
                    var a = m(c.Error.MISSING_MASTER_CREDENTIALS);
                    return c.Defer.reject(a);
                }
                var b = c.Defer.resolve({
                    scheme: "Basic",
                    username: c.appKey,
                    password: c.masterSecret
                });
                return b;
            },
            None: function() {
                return c.Defer.resolve(null);
            },
            Session: function() {
                var a = c.getActiveUser();
                if (null === a) {
                    var b = m(c.Error.NO_ACTIVE_USER);
                    return c.Defer.reject(b);
                }
                var d = c.Defer.resolve({
                    scheme: "Kinvey",
                    credentials: a._kmd.authtoken
                });
                return d;
            }
        }, z = function() {
            var b, c, d, e, f, g = [], h = function(a) {
                a = a.toLowerCase();
                var b = /(chrome)\/([\w]+)/, c = /(firefox)\/([\w.]+)/, d = /(msie) ([\w.]+)/i, e = /(opera)(?:.*version)?[ \/]([\w.]+)/, f = /(safari)\/([\w.]+)/;
                return b.exec(a) || c.exec(a) || d.exec(a) || e.exec(a) || f.exec(a) || [];
            };
            if ("undefined" != typeof a.cordova && "undefined" != typeof a.device) {
                var i = a.device;
                g.push("phonegap/" + i.cordova), c = i.platform, d = i.version, e = i.model, f = i.uuid;
            } else "undefined" != typeof Titanium ? (g.push("titanium/" + Titanium.getVersion()), 
            "mobileweb" === Titanium.Platform.getName() ? (b = h(Titanium.Platform.getModel()), 
            c = b[1], d = b[2], e = Titanium.Platform.getOstype()) : (c = Titanium.Platform.getOsname(), 
            d = Titanium.Platform.getVersion(), e = Titanium.Platform.getManufacturer()), f = Titanium.Platform.getId()) : "undefined" != typeof forge ? (g.push("triggerio/" + (forge.config.platform_version || "")), 
            f = forge.config.uuid) : "undefined" != typeof process && (c = process.title, d = process.version, 
            e = process.platform);
            "undefined" != typeof angular && g.push("angularjs/" + angular.version.full), "undefined" != typeof Backbone && g.push("backbonejs/" + Backbone.VERSION), 
            "undefined" != typeof Ember && g.push("emberjs/" + Ember.VERSION), "undefined" != typeof jQuery && g.push("jquery/" + jQuery.fn.jquery), 
            "undefined" != typeof ko && g.push("knockout/" + ko.version), "undefined" != typeof Zepto && g.push("zeptojs"), 
            null == c && a.navigator && (b = h(a.navigator.userAgent), c = b[1], d = b[2], e = a.navigator.platform);
            var j = [ "js-angular/1.1.4" ];
            return 0 !== g.length && j.push("(" + g.sort().join(", ") + ")"), j.concat([ c, d, e, f ].map(function(a) {
                return null != a ? a.toString().replace(/\s/g, "_").toLowerCase() : "unknown";
            })).join(" ");
        };
        c.Acl = function(a) {
            if (null != a && !r(a)) throw new c.Error("document argument must be of type: Object.");
            a = a || {}, a._acl = a._acl || {}, this._acl = a._acl;
        }, c.Acl.prototype = {
            addReader: function(a) {
                return this._acl.r = this._acl.r || [], -1 === this._acl.r.indexOf(a) && this._acl.r.push(a), 
                this;
            },
            addReaderGroup: function(a) {
                return this._acl.groups = this._acl.groups || {}, this._acl.groups.r = this._acl.groups.r || [], 
                -1 === this._acl.groups.r.indexOf(a) && this._acl.groups.r.push(a), this;
            },
            addWriterGroup: function(a) {
                return this._acl.groups = this._acl.groups || {}, this._acl.groups.w = this._acl.groups.w || [], 
                -1 === this._acl.groups.w.indexOf(a) && this._acl.groups.w.push(a), this;
            },
            addWriter: function(a) {
                return this._acl.w = this._acl.w || [], -1 === this._acl.w.indexOf(a) && this._acl.w.push(a), 
                this;
            },
            getCreator: function() {
                return this._acl.creator || null;
            },
            getReaders: function() {
                return this._acl.r || [];
            },
            getReaderGroups: function() {
                return this._acl.groups ? this._acl.groups.r : [];
            },
            getWriterGroups: function() {
                return this._acl.groups ? this._acl.groups.w : [];
            },
            getWriters: function() {
                return this._acl.w || [];
            },
            isGloballyReadable: function() {
                return this._acl.gr || !1;
            },
            isGloballyWritable: function() {
                return this._acl.gw || !1;
            },
            removeReader: function(a) {
                var b;
                return this._acl.r && -1 !== (b = this._acl.r.indexOf(a)) && this._acl.r.splice(b, 1), 
                this;
            },
            removeReaderGroup: function(a) {
                var b;
                return this._acl.groups && this._acl.groups.r && -1 !== (b = this._acl.groups.r.indexOf(a)) && this._acl.groups.r.splice(b, 1), 
                this;
            },
            removeWriterGroup: function(a) {
                var b;
                return this._acl.groups && this._acl.groups.w && -1 !== (b = this._acl.groups.w.indexOf(a)) && this._acl.groups.w.splice(b, 1), 
                this;
            },
            removeWriter: function(a) {
                var b;
                return this._acl.w && -1 !== (b = this._acl.w.indexOf(a)) && this._acl.w.splice(b, 1), 
                this;
            },
            setGloballyReadable: function(a) {
                return this._acl.gr = a || !1, this;
            },
            setGloballyWritable: function(a) {
                return this._acl.gw = a || !1, this;
            },
            toJSON: function() {
                return this._acl;
            }
        }, c.Group = function() {
            this._query = null, this._initial = {}, this._key = {}, this._reduce = function() {}.toString();
        }, c.Group.prototype = {
            by: function(a) {
                return this._key[a] = !0, this;
            },
            initial: function(a, b) {
                if ("undefined" == typeof b && !r(a)) throw new c.Error("objectOrKey argument must be of type: Object.");
                return r(a) ? this._initial = a : this._initial[a] = b, this;
            },
            postProcess: function(a) {
                return null === this._query ? a : this._query._postProcess(a);
            },
            query: function(a) {
                if (!(a instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                return this._query = a, this;
            },
            reduce: function(a) {
                if (p(a) && (a = a.toString()), !t(a)) throw new c.Error("fn argument must be of type: function or string.");
                return this._reduce = a, this;
            },
            toJSON: function() {
                return {
                    key: this._key,
                    initial: this._initial,
                    reduce: this._reduce,
                    condition: null !== this._query ? this._query.toJSON().filter : {}
                };
            }
        }, c.Group.count = function(a) {
            var b = new c.Group();
            return null != a && b.by(a), b.initial({
                result: 0
            }), b.reduce(function(a, b) {
                b.result += 1;
            }), b;
        }, c.Group.sum = function(a) {
            a = a.replace("'", "\\'");
            var b = new c.Group();
            return b.initial({
                result: 0
            }), b.reduce('function(doc, out) { out.result += doc["' + a + '"]; }'), b;
        }, c.Group.min = function(a) {
            a = a.replace("'", "\\'");
            var b = new c.Group();
            return b.initial({
                result: "Infinity"
            }), b.reduce('function(doc, out) { out.result = Math.min(out.result, doc["' + a + '"]); }'), 
            b;
        }, c.Group.max = function(a) {
            a = a.replace("'", "\\'");
            var b = new c.Group();
            return b.initial({
                result: "-Infinity"
            }), b.reduce('function(doc, out) { out.result = Math.max(out.result, doc["' + a + '"]); }'), 
            b;
        }, c.Group.average = function(a) {
            a = a.replace("'", "\\'");
            var b = new c.Group();
            return b.initial({
                count: 0,
                result: 0
            }), b.reduce('function(doc, out) {  out.result = (out.result * out.count + doc["' + a + '"]) / (out.count + 1);  out.count += 1;}'), 
            b;
        }, c.execute = function(a, b, d) {
            d = d || {};
            var e = c.Persistence.create({
                namespace: f,
                collection: "custom",
                id: a,
                data: b,
                auth: y.Default
            }, d).then(null, function(a) {
                return c.Error.REQUEST_ERROR === a.name && r(a.debug) ? c.Defer.reject(a.debug) : c.Defer.reject(a);
            });
            return n(e, d);
        }, c.DataStore = {
            find: function(a, b, e) {
                if (null != b && !(b instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                e = e || {};
                var f = c.Persistence.read({
                    namespace: d,
                    collection: a,
                    query: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e);
                return n(f, e);
            },
            get: function(a, b, e) {
                e = e || {};
                var f = c.Persistence.read({
                    namespace: d,
                    collection: a,
                    id: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e);
                return n(f, e);
            },
            save: function(a, b, e) {
                if (e = e || {}, null != b._id) return c.DataStore.update(a, b, e);
                var f = c.Persistence.create({
                    namespace: d,
                    collection: a,
                    data: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e);
                return n(f, e);
            },
            update: function(a, b, e) {
                if (null == b._id) throw new c.Error("document argument must contain: _id");
                e = e || {};
                var f = c.Persistence.update({
                    namespace: d,
                    collection: a,
                    id: b._id,
                    data: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e);
                return n(f, e);
            },
            clean: function(a, b, e) {
                if (e = e || {}, b = b || new c.Query(), !(b instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                var f = c.Persistence.destroy({
                    namespace: d,
                    collection: a,
                    query: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e);
                return n(f, e);
            },
            destroy: function(a, b, e) {
                e = e || {};
                var f = c.Persistence.destroy({
                    namespace: d,
                    collection: a,
                    id: b,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, e).then(null, function(a) {
                    return e.silent && c.Error.ENTITY_NOT_FOUND === a.name ? {
                        count: 0
                    } : c.Defer.reject(a);
                });
                return n(f, e);
            },
            count: function(a, b, e) {
                if (null != b && !(b instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                e = e || {};
                var f = c.Persistence.read({
                    namespace: d,
                    collection: a,
                    id: "_count",
                    query: b,
                    auth: y.Default,
                    local: {
                        req: !0
                    }
                }, e).then(function(a) {
                    return a.count;
                });
                return n(f, e);
            },
            group: function(a, b, e) {
                if (!(b instanceof c.Group)) throw new c.Error("aggregation argument must be of type: Kinvey.Group.");
                e = e || {};
                var f = c.Persistence.create({
                    namespace: d,
                    collection: a,
                    id: "_group",
                    data: b.toJSON(),
                    auth: y.Default,
                    local: {
                        req: !0
                    }
                }, e).then(function(a) {
                    return b.postProcess(a);
                });
                return n(f, e);
            }
        }, c.File = {
            destroy: function(a, b) {
                b = b || {};
                var d = c.Persistence.destroy({
                    namespace: e,
                    id: a,
                    auth: y.Default
                }, b).then(null, function(a) {
                    return b.silent && c.Error.BLOB_NOT_FOUND === a.name ? {
                        count: 0
                    } : c.Defer.reject(a);
                });
                return n(d, b);
            },
            download: function(a, b) {
                b = b || {};
                var d = {};
                !1 !== b.tls && (d.tls = !0), b.ttl && (d.ttl_in_seconds = b.ttl);
                var f = c.Persistence.read({
                    namespace: e,
                    id: a,
                    flags: d,
                    auth: y.Default
                }, b).then(function(a) {
                    if (b.stream) return a;
                    var d = b.success, e = b.error;
                    return delete b.success, delete b.error, c.File.downloadByUrl(a, b).then(function(a) {
                        return b.success = d, b.error = e, a;
                    }, function(a) {
                        return b.success = d, b.error = e, c.Defer.reject(a);
                    });
                });
                return n(f, b);
            },
            downloadByUrl: function(a, b) {
                var d = r(a) ? a : {
                    _downloadURL: a
                };
                b = b || {}, b.file = d.mimeType || "application-octet-stream", b.headers = b.headers || {}, 
                delete b.headers["Content-Type"];
                var e = d._downloadURL, f = c.Persistence.Net.request("GET", e, null, b.headers, b);
                return f = f.then(function(a) {
                    return d._data = a, d;
                }, function(a) {
                    var b = m(c.Error.REQUEST_ERROR, {
                        description: "This file could not be downloaded from the provided URL.",
                        debug: a
                    });
                    return c.Defer.reject(b);
                }), n(f, b);
            },
            find: function(a, b) {
                if (null != a && !(a instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                b = b || {};
                var d = {};
                !1 !== b.tls && (d.tls = !0), b.ttl && (d.ttl_in_seconds = b.ttl);
                var f = c.Persistence.read({
                    namespace: e,
                    query: a,
                    flags: d,
                    auth: y.Default
                }, b).then(function(a) {
                    if (b.download) {
                        var d = b.success, e = b.error;
                        delete b.success, delete b.error;
                        var f = a.map(function(a) {
                            return c.File.downloadByUrl(a, b);
                        });
                        return c.Defer.all(f).then(function(a) {
                            return b.success = d, b.error = e, a;
                        }, function(a) {
                            return b.success = d, b.error = e, c.Defer.reject(a);
                        });
                    }
                    return a;
                });
                return n(f, b);
            },
            stream: function(a, b) {
                return b = b || {}, b.stream = !0, c.File.download(a, b);
            },
            upload: function(a, b, d) {
                a = a || {}, b = b || {}, d = d || {}, null != b._filename || null == a._filename && null == a.name || (b._filename = a._filename || a.name), 
                null != b.size || null == a.size && null == a.length || (b.size = a.size || a.length), 
                b.mimeType = b.mimeType || a.mimeType || a.type || "application/octet-stream", d["public"] && (b._public = !0), 
                d.contentType = b.mimeType;
                var f = null != b._id ? c.Persistence.update({
                    namespace: e,
                    id: b._id,
                    data: b,
                    flags: !1 !== d.tls ? {
                        tls: !0
                    } : null,
                    auth: y.Default
                }, d) : c.Persistence.create({
                    namespace: e,
                    data: b,
                    flags: !1 !== d.tls ? {
                        tls: !0
                    } : null,
                    auth: y.Default
                }, d);
                return f = f.then(function(b) {
                    var e = b._uploadURL, f = b._requiredHeaders || {};
                    f["Content-Type"] = d.contentType, delete b._expiresAt, delete b._requiredHeaders, 
                    delete b._uploadURL;
                    var g = c.Persistence.Net.request("PUT", e, a, f, d);
                    return g.then(function() {
                        return b._data = a, b;
                    });
                }), n(f, d);
            }
        };
        var A = function(b) {
            var c = Date.parse(b);
            if (c) return new Date(c);
            var d = /^(\d{4}\-\d\d\-\d\d([tT][\d:\.]*)?)([zZ]|([+\-])(\d\d):?(\d\d))?$/, e = b.match(d);
            if (null != e[1]) {
                var f = e[1].split(/\D/).map(function(b) {
                    return a.parseInt(b, 10) || 0;
                });
                if (f[1] -= 1, f = new Date(Date.UTC.apply(Date, f)), null != e[5]) {
                    var g = a.parseInt(e[5], 10) / 100 * 60;
                    g += null != e[6] ? a.parseInt(e[6], 10) : 0, g *= "+" === e[4] ? -1 : 1, g && f.setUTCMinutes(f.getUTCMinutes() * g);
                }
                return f;
            }
            return 0 / 0;
        };
        c.Metadata = function(a) {
            if (!r(a)) throw new c.Error("document argument must be of type: Object.");
            this._acl = null, this._document = a;
        }, c.Metadata.prototype = {
            getAcl: function() {
                return null === this._acl && (this._acl = new c.Acl(this._document)), this._acl;
            },
            getCreatedAt: function() {
                return null != this._document._kmd && null != this._document._kmd.ect ? A(this._document._kmd.ect) : null;
            },
            getEmailVerification: function() {
                return null != this._document._kmd && null != this._document._kmd.emailVerification ? this._document._kmd.emailVerification.status : null;
            },
            getLastModified: function() {
                return null != this._document._kmd && null != this._document._kmd.lmt ? A(this._document._kmd.lmt) : null;
            },
            setAcl: function(a) {
                if (!(a instanceof c.Acl)) throw new c.Error("acl argument must be of type: Kinvey.Acl.");
                return this._acl = null, this._document._acl = a.toJSON(), this;
            },
            toJSON: function() {
                return this._document;
            }
        };
        var B = [ "facebook", "google", "linkedIn", "twitter" ], C = {
            use: w(B)
        };
        B.forEach(function(a) {
            C[a] = v("Social." + a);
        }), c.Social = {
            connect: function(a, b, d) {
                if (d = d || {}, d.create = "undefined" != typeof d.create ? d.create : !0, !c.Social.isSupported(b)) throw new c.Error("provider argument is not supported.");
                var e = d.success, f = d.error;
                delete d.success, delete d.error;
                var g = C[b](d).then(function(e) {
                    a = a || {};
                    var f = c.getActiveUser();
                    return a._socialIdentity = a._socialIdentity || {}, a._socialIdentity[b] = e, null !== f && f._id === a._id ? (d._provider = b, 
                    c.User.update(a, d)) : (a._socialIdentity = {}, a._socialIdentity[b] = e, c.User.login(a, null, d).then(null, function(b) {
                        return d.create && c.Error.USER_NOT_FOUND === b.name ? c.User.signup(a, d) : c.Defer.reject(b);
                    }));
                });
                return g = g.then(function(a) {
                    return d.success = e, d.error = f, a;
                }), n(g, d);
            },
            disconnect: function(a, b, d) {
                if (!c.Social.isSupported(b)) throw new c.Error("provider argument is not supported.");
                if (a._socialIdentity = a._socialIdentity || {}, a._socialIdentity[b] = null, null == a._id) {
                    var e = c.Defer.resolve(a);
                    return n(e, d);
                }
                return c.User.update(a, d);
            },
            facebook: function(a, b) {
                return c.Social.connect(a, "facebook", b);
            },
            google: function(a, b) {
                return c.Social.connect(a, "google", b);
            },
            isSupported: function(a) {
                return -1 !== B.indexOf(a);
            },
            linkedIn: function(a, b) {
                return c.Social.connect(a, "linkedIn", b);
            },
            twitter: function(a, b) {
                return c.Social.connect(a, "twitter", b);
            }
        }, c.User = {
            signup: function(a, b) {
                return b = b || {}, b.state = !0, c.User.create(a, b);
            },
            login: function(a, b, d) {
                if (r(a) ? d = "undefined" != typeof d ? d : b : a = {
                    username: a,
                    password: b
                }, d = d || {}, null == a.username && null == a.password && null == a._socialIdentity) throw new c.Error("Argument must contain: username and password, or _socialIdentity.");
                if (null !== c.getActiveUser()) {
                    var e = m(c.Error.ALREADY_LOGGED_IN);
                    return n(c.Defer.reject(e), d);
                }
                var f = c.Persistence.create({
                    namespace: g,
                    collection: d._provider ? null : "login",
                    data: a,
                    flags: d._provider ? {
                        provider: d._provider
                    } : {},
                    auth: y.App,
                    local: {
                        res: !0
                    }
                }, d).then(function(a) {
                    return c.setActiveUser(a), a;
                });
                return n(f, d);
            },
            logout: function(a) {
                a = a || {};
                var b;
                return b = a.silent && null === c.getActiveUser() ? c.Defer.resolve(null) : c.Persistence.create({
                    namespace: g,
                    collection: "_logout",
                    auth: y.Session
                }, a).then(null, function(b) {
                    return !a.force || c.Error.INVALID_CREDENTIALS !== b.name && c.Error.EMAIL_VERIFICATION_REQUIRED !== b.name ? c.Defer.reject(b) : null;
                }).then(function() {
                    var a = c.setActiveUser(null);
                    return null !== a && delete a._kmd.authtoken, a;
                }), n(b, a);
            },
            me: function(a) {
                a = a || {};
                var b = c.Persistence.read({
                    namespace: g,
                    collection: "_me",
                    auth: y.Session,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, a).then(function(a) {
                    return a._kmd = a._kmd || {}, null == a._kmd.authtoken && (a._kmd.authtoken = c.getActiveUser()._kmd.authtoken), 
                    c.setActiveUser(a), a;
                });
                return n(b, a);
            },
            verifyEmail: function(a, b) {
                b = b || {};
                var d = c.Persistence.create({
                    namespace: f,
                    collection: a,
                    id: "user-email-verification-initiate",
                    auth: y.App
                }, b);
                return n(d, b);
            },
            forgotUsername: function(a, b) {
                b = b || {};
                var d = c.Persistence.create({
                    namespace: f,
                    id: "user-forgot-username",
                    data: {
                        email: a
                    },
                    auth: y.App
                }, b);
                return n(d, b);
            },
            resetPassword: function(a, b) {
                b = b || {};
                var d = c.Persistence.create({
                    namespace: f,
                    collection: a,
                    id: "user-password-reset-initiate",
                    auth: y.App
                }, b);
                return n(d, b);
            },
            exists: function(a, b) {
                b = b || {};
                var d = c.Persistence.create({
                    namespace: f,
                    id: "check-username-exists",
                    data: {
                        username: a
                    },
                    auth: y.App
                }, b).then(function(a) {
                    return a.usernameExists;
                });
                return n(d, b);
            },
            create: function(a, b) {
                if (b = b || {}, !1 !== b.state && null !== c.getActiveUser()) {
                    var d = m(c.Error.ALREADY_LOGGED_IN);
                    return n(c.Defer.reject(d), b);
                }
                var e = c.Persistence.create({
                    namespace: g,
                    data: a || {},
                    auth: y.App
                }, b).then(function(a) {
                    return !1 !== b.state && c.setActiveUser(a), a;
                });
                return n(e, b);
            },
            update: function(a, b) {
                if (null == a._id) throw new c.Error("data argument must contain: _id");
                b = b || {};
                var d = [];
                if (null != a._socialIdentity) for (var e in a._socialIdentity) a._socialIdentity.hasOwnProperty(e) && null != a._socialIdentity[e] && e !== b._provider && (d.push({
                    provider: e,
                    access_token: a._socialIdentity[e].access_token,
                    access_token_secret: a._socialIdentity[e].access_token_secret
                }), delete a._socialIdentity[e].access_token, delete a._socialIdentity[e].access_token_secret);
                var f = c.Persistence.update({
                    namespace: g,
                    id: a._id,
                    data: a,
                    auth: y.Default,
                    local: {
                        res: !0
                    }
                }, b).then(function(a) {
                    d.forEach(function(b) {
                        var c = b.provider;
                        null != a._socialIdentity && null != a._socialIdentity[c] && [ "access_token", "access_token_secret" ].forEach(function(d) {
                            null != b[d] && (a._socialIdentity[c][d] = b[d]);
                        });
                    });
                    var b = c.getActiveUser();
                    return null !== b && b._id === a._id && c.setActiveUser(a), a;
                });
                return n(f, b);
            },
            find: function(a, b) {
                if (null != a && !(a instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                b = b || {};
                var d;
                return d = b.discover ? c.Persistence.create({
                    namespace: g,
                    collection: "_lookup",
                    data: null != a ? a.toJSON().filter : null,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, b) : c.Persistence.read({
                    namespace: g,
                    query: a,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, b), n(d, b);
            },
            get: function(a, b) {
                b = b || {};
                var d = c.Persistence.read({
                    namespace: g,
                    id: a,
                    auth: y.Default,
                    local: {
                        req: !0,
                        res: !0
                    }
                }, b);
                return n(d, b);
            },
            destroy: function(a, b) {
                b = b || {};
                var d = c.Persistence.destroy({
                    namespace: g,
                    id: a,
                    flags: b.hard ? {
                        hard: !0
                    } : {},
                    auth: y.Default,
                    local: {
                        res: !0
                    }
                }, b).then(function(b) {
                    var d = c.getActiveUser();
                    return null !== d && d._id === a && c.setActiveUser(null), b;
                }, function(a) {
                    return b.silent && c.Error.USER_NOT_FOUND === a.name ? null : c.Defer.reject(a);
                });
                return n(d, b);
            },
            restore: function(a, b) {
                b = b || {};
                var d = c.Persistence.create({
                    namespace: g,
                    collection: a,
                    id: "_restore",
                    auth: y.Master
                }, b);
                return n(d, b);
            },
            count: function(a, b) {
                if (null != a && !(a instanceof c.Query)) throw new c.Error("query argument must be of type: Kinvey.Query.");
                b = b || {};
                var d = c.Persistence.read({
                    namespace: g,
                    id: "_count",
                    query: a,
                    auth: y.Default,
                    local: {
                        req: !0
                    }
                }, b).then(function(a) {
                    return a.count;
                });
                return n(d, b);
            },
            group: function(a, b) {
                if (!(a instanceof c.Group)) throw new c.Error("aggregation argument must be of type: Kinvey.Group.");
                b = b || {};
                var d = c.Persistence.create({
                    namespace: g,
                    id: "_group",
                    data: a.toJSON(),
                    auth: y.Default,
                    local: {
                        req: !0
                    }
                }, b).then(function(b) {
                    return a.postProcess(b);
                });
                return n(d, b);
            }
        }, c.Query = function(a) {
            a = a || {}, this._filter = a.filter || {}, this._sort = a.sort || {}, this._limit = a.limit || null, 
            this._skip = a.skip || 0, this._parent = null;
        }, c.Query.prototype = {
            equalTo: function(a, b) {
                return this._filter[a] = b, this;
            },
            contains: function(a, b) {
                if (!o(b)) throw new c.Error("values argument must be of type: Array.");
                return this._addFilter(a, "$in", b);
            },
            containsAll: function(a, b) {
                if (!o(b)) throw new c.Error("values argument must be of type: Array.");
                return this._addFilter(a, "$all", b);
            },
            greaterThan: function(a, b) {
                if (!q(b) && !t(b)) throw new c.Error("value argument must be of type: number or string.");
                return this._addFilter(a, "$gt", b);
            },
            greaterThanOrEqualTo: function(a, b) {
                if (!q(b) && !t(b)) throw new c.Error("value argument must be of type: number or string.");
                return this._addFilter(a, "$gte", b);
            },
            lessThan: function(a, b) {
                if (!q(b) && !t(b)) throw new c.Error("value argument must be of type: number or string.");
                return this._addFilter(a, "$lt", b);
            },
            lessThanOrEqualTo: function(a, b) {
                if (!q(b) && !t(b)) throw new c.Error("value argument must be of type: number or string.");
                return this._addFilter(a, "$lte", b);
            },
            notEqualTo: function(a, b) {
                return this._addFilter(a, "$ne", b);
            },
            notContainedIn: function(a, b) {
                if (!o(b)) throw new c.Error("values argument must be of type: Array.");
                return this._addFilter(a, "$nin", b);
            },
            and: function() {
                return this._join("$and", Array.prototype.slice.call(arguments));
            },
            nor: function() {
                return null !== this._parent && null != this._parent._filter.$and ? this._parent.nor.apply(this._parent, arguments) : this._join("$nor", Array.prototype.slice.call(arguments));
            },
            or: function() {
                return null !== this._parent ? this._parent.or.apply(this._parent, arguments) : this._join("$or", Array.prototype.slice.call(arguments));
            },
            exists: function(a, b) {
                return b = "undefined" == typeof b ? !0 : b || !1, this._addFilter(a, "$exists", b);
            },
            mod: function(a, b, d) {
                if (t(b) && (b = parseFloat(b)), "undefined" == typeof d ? d = 0 : t(d) && (d = parseFloat(d)), 
                !q(b)) throw new c.Error("divisor arguments must be of type: number.");
                if (!q(d)) throw new c.Error("remainder argument must be of type: number.");
                return this._addFilter(a, "$mod", [ b, d ]);
            },
            matches: function(a, b, c) {
                if (s(b) || (b = new RegExp(b)), c = c || {}, (b.ignoreCase || c.ignoreCase) && !1 !== c.ignoreCase) throw new Error("ignoreCase flag is not supported.");
                if (0 !== b.source.indexOf("^")) throw new Error("regExp must be an anchored expression.");
                var d = [];
                (b.multiline || c.multiline) && !1 !== c.multiline && d.push("m"), c.extended && d.push("x"), 
                c.dotMatchesAll && d.push("s");
                var e = this._addFilter(a, "$regex", b.source);
                return 0 !== d.length && this._addFilter(a, "$options", d.join("")), e;
            },
            near: function(a, b, d) {
                if (!o(b) || null == b[0] || null == b[1]) throw new c.Error("coord argument must be of type: Array.<number, number>.");
                b[0] = parseFloat(b[0]), b[1] = parseFloat(b[1]);
                var e = this._addFilter(a, "$nearSphere", [ b[0], b[1] ]);
                return null != d && this._addFilter(a, "$maxDistance", d), e;
            },
            withinBox: function(a, b, d) {
                if (!o(b) || null == b[0] || null == b[1]) throw new c.Error("bottomLeftCoord argument must be of type: Array.<number, number>.");
                if (!o(d) || null == d[0] || null == d[1]) throw new c.Error("upperRightCoord argument must be of type: Array.<number, number>.");
                b[0] = parseFloat(b[0]), b[1] = parseFloat(b[1]), d[0] = parseFloat(d[0]), d[1] = parseFloat(d[1]);
                var e = [ [ b[0], b[1] ], [ d[0], d[1] ] ];
                return this._addFilter(a, "$within", {
                    $box: e
                });
            },
            withinPolygon: function(a, b) {
                if (!o(b) || 3 > b.length) throw new c.Error("coords argument must be of type: Array.Array.<number, number>.");
                return b = b.map(function(a) {
                    if (null == a[0] || null == a[1]) throw new c.Error("coords argument must be of type: Array.Array.<number, number>.");
                    return [ parseFloat(a[0]), parseFloat(a[1]) ];
                }), this._addFilter(a, "$within", {
                    $polygon: b
                });
            },
            size: function(a, b) {
                if (t(b) && (b = parseFloat(b)), !q(b)) throw new c.Error("size argument must be of type: number.");
                return this._addFilter(a, "$size", b);
            },
            limit: function(a) {
                if (a = a || null, t(a) && (a = parseFloat(a)), null != a && !q(a)) throw new c.Error("limit argument must be of type: number.");
                return null !== this._parent ? this._parent.limit(a) : this._limit = a, this;
            },
            skip: function(a) {
                if (t(a) && (a = parseFloat(a)), !q(a)) throw new c.Error("skip argument must be of type: number.");
                return null !== this._parent ? this._parent.skip(a) : this._skip = a, this;
            },
            ascending: function(a) {
                return null !== this._parent ? this._parent.ascending(a) : this._sort[a] = 1, this;
            },
            descending: function(a) {
                return null !== this._parent ? this._parent.descending(a) : this._sort[a] = -1, 
                this;
            },
            sort: function(a) {
                if (null != a && !r(a)) throw new c.Error("sort argument must be of type: Object.");
                return null !== this._parent ? this._parent.sort(a) : this._sort = a || {}, this;
            },
            toJSON: function() {
                return null !== this._parent ? this._parent.toJSON() : {
                    filter: this._filter,
                    sort: this._sort,
                    skip: this._skip,
                    limit: this._limit
                };
            },
            _addFilter: function(a, b, c) {
                return r(this._filter[a]) || (this._filter[a] = {}), this._filter[a][b] = c, this;
            },
            _join: function(a, b) {
                var d = this;
                b = b.map(function(a) {
                    if (!(a instanceof c.Query)) {
                        if (!r(a)) throw new c.Error("query argument must be of type: Kinvey.Query[] or Object[].");
                        a = new c.Query(a);
                    }
                    return a.toJSON().filter;
                }), 0 === b.length && (d = new c.Query(), b = [ d.toJSON().filter ], d._parent = this);
                var e = {};
                for (var f in this._filter) this._filter.hasOwnProperty(f) && (e[f] = this._filter[f], 
                delete this._filter[f]);
                return this._filter[a] = [ e ].concat(b), d;
            },
            _postProcess: function(a) {
                if (!o(a)) throw new c.Error("response argument must be of type: Array.");
                var b = this;
                return a = a.sort(function(a, c) {
                    for (var d in b._sort) if (b._sort.hasOwnProperty(d)) {
                        if ("undefined" != typeof a[d] && "undefined" == typeof c[d]) return -1;
                        if ("undefined" != typeof c[d] && "undefined" == typeof a[d]) return 1;
                        if (a[d] !== c[d]) {
                            var e = b._sort[d];
                            return (a[d] < c[d] ? -1 : 1) * e;
                        }
                    }
                    return 0;
                }), null !== this._limit ? a.slice(this._skip, this._skip + this._limit) : a.slice(this._skip);
            }
        };
        var D = function(a, b, c) {
            if (!b) return a = "undefined" == typeof c ? a : c;
            for (var d, e = a, f = b.split("."); (d = f.shift()) && null != e && e.hasOwnProperty(d); ) {
                if (0 === f.length) return e[d] = "undefined" == typeof c ? e[d] : c, e[d];
                e = e[d];
            }
            return null;
        }, E = {
            get: function(a, b) {
                if (o(a)) {
                    var d = a.map(function(a) {
                        return E.get(a, b);
                    });
                    return c.Defer.all(d);
                }
                b = b || {}, b.exclude = b.exclude || [], b.relations = b.relations || {};
                var e = b.error, f = b.relations, h = b.success;
                delete b.error, delete b.relations, delete b.success;
                var i = [];
                Object.keys(f).forEach(function(a) {
                    var b = a.split(".").length;
                    i[b] = (i[b] || []).concat(a);
                });
                var j = c.Defer.resolve(null);
                return i.forEach(function(d) {
                    j = j.then(function() {
                        var e = d.map(function(d) {
                            var e = D(a, d), f = o(e), h = (f ? e : [ e ]).map(function(a) {
                                if (null == a || "KinveyRef" !== a._type || -1 !== b.exclude.indexOf(d)) return c.Defer.resolve(a);
                                var e;
                                return e = g === a._collection ? c.User.get(a._id, b) : c.DataStore.get(a._collection, a._id, b), 
                                e.then(null, function() {
                                    return c.Defer.resolve(a);
                                });
                            });
                            return c.Defer.all(h).then(function(b) {
                                D(a, d, f ? b : b[0]);
                            });
                        });
                        return c.Defer.all(e);
                    });
                }), j.then(function() {
                    return b.error = e, b.relations = f, b.success = h, a;
                }, function(a) {
                    return b.error = e, b.relations = f, b.success = h, c.Defer.reject(a);
                });
            },
            save: function(a, b, d) {
                if (o(b)) {
                    var e = b.map(function(b) {
                        return E.save(a, b, d);
                    });
                    return c.Defer.all(e);
                }
                d = d || {}, d.exclude = d.exclude || [], d.relations = d.relations || {};
                var f = d.error, h = d.relations, i = d.success;
                delete d.error, delete d.relations, delete d.success;
                var j = [];
                h[""] = a, Object.keys(h).forEach(function(a) {
                    var b = "" === a ? 0 : a.split(".").length;
                    j[b] = (j[b] || []).concat(a);
                });
                var k = {}, l = c.Defer.resolve(null);
                return j.reverse().forEach(function(a) {
                    l = l.then(function() {
                        var e = a.map(function(a) {
                            var e = h[a], f = D(b, a), i = o(f), j = (i ? f : [ f ]).map(function(b) {
                                if (null == b || "KinveyRef" === b._type || -1 !== d.exclude.indexOf(a)) return c.Defer.resolve(b);
                                var f, h = d.offline && !1 === d.track;
                                if (g !== e || h) f = c.DataStore.save(e, b, d); else {
                                    var i = null == b._id;
                                    d.state = i && "" !== a ? d.state || !1 : d.state, f = c.User[i ? "create" : "update"](b, d);
                                }
                                return f.then(null, function(e) {
                                    return d.force && "" !== a ? b : c.Defer.reject(e);
                                });
                            });
                            return c.Defer.all(j).then(function(c) {
                                var d = c.map(function(a) {
                                    return null == a || null == a._id ? a : {
                                        _type: "KinveyRef",
                                        _collection: e,
                                        _id: a._id
                                    };
                                });
                                i || (d = d[0], c = c[0]), D(b, a, d), k[a] = c;
                            });
                        });
                        return c.Defer.all(e);
                    });
                }), l.then(function() {
                    var a = k[""];
                    return j.reverse().forEach(function(b) {
                        b.forEach(function(b) {
                            D(a, b, k[b]);
                        });
                    }), delete h[""], d.error = f, d.relations = h, d.success = i, a;
                }, function(a) {
                    return delete h[""], d.error = f, d.relations = h, d.success = i, c.Defer.reject(a);
                });
            }
        }, F = function(a) {
            var b = c.Sync.isEnabled(), d = c.Sync.isOnline();
            return a.fallback = b && d && !1 !== a.fallback, a.offline = b && (!d || a.offline), 
            a.refresh = b && d && !1 !== a.refresh, a;
        };
        c.Persistence = {
            create: function(a, b) {
                if (b.relations) {
                    var d = g === a.namespace ? g : a.collection;
                    return E.save(d, a.data, b);
                }
                if (a.local = a.local || {}, b = F(b), a.local.req && b.offline) return c.Persistence.Local.create(a, b).then(null, function(d) {
                    return b.fallback && "_group" === a.id ? (b.offline = !1, c.Persistence.create(a, b)) : c.Defer.reject(d);
                });
                var e = c.Persistence.Net.create(a, b);
                return a.local.res && b.refresh ? e.then(function(d) {
                    return a.data = d, c.Persistence.Local.create(a, b).then(function() {
                        return d;
                    });
                }) : e;
            },
            read: function(a, b) {
                if (a.local = a.local || {}, b = F(b), a.local.req && b.offline) return c.Persistence.Local.read(a, b).then(null, function(d) {
                    return b.fallback ? (b.offline = !1, c.Persistence.read(a, b)) : c.Defer.reject(d);
                });
                var d = c.Persistence.Net.read(a, b);
                return a.local.res && b.refresh ? d.then(function(d) {
                    var e;
                    if (b.relations) {
                        var f = b.offline;
                        b.offline = !0, b.track = !1;
                        var h = g === a.namespace ? g : a.collection;
                        e = E.save(h, d, b).then(function() {
                            b.offline = f, delete b.track;
                        });
                    } else a.data = d, e = c.Persistence.Local.create(a, b);
                    return e.then(function() {
                        return d;
                    });
                }, function(d) {
                    return c.Error.ENTITY_NOT_FOUND === d.name ? c.Persistence.Local.destroy(a, b).then(function() {
                        return c.Defer.reject(d);
                    }) : c.Defer.reject(d);
                }) : d;
            },
            update: function(a, b) {
                if (b.relations) {
                    var d = g === a.namespace ? g : a.collection;
                    return E.save(d, a.data, b);
                }
                if (a.local = a.local || {}, b = F(b), a.local.req && b.offline) return c.Persistence.Local.update(a, b);
                var e = c.Persistence.Net.update(a, b);
                return a.local.res && b.refresh ? e.then(function(d) {
                    return a.data = d, c.Persistence.Local.update(a, b).then(function() {
                        return d;
                    });
                }) : e;
            },
            destroy: function(a, b) {
                if (a.local = a.local || {}, b = F(b), a.local.req && b.offline) return c.Persistence.Local.destroy(a, b);
                var d = c.Persistence.Net.destroy(a, b);
                return a.local.res && b.refresh ? d.then(function(d) {
                    return c.Persistence.Local.destroy(a, b).then(function() {
                        return d;
                    }, function(a) {
                        return c.Error.ENTITY_NOT_FOUND === a.name ? d : c.Defer.reject(a);
                    });
                }) : d;
            }
        };
        var G = {
            batch: v("Database.batch"),
            clean: v("Database.clean"),
            count: v("Database.count"),
            destroy: v("Database.destroy"),
            destruct: v("Database.destruct"),
            find: v("Database.find"),
            findAndModify: v("Database.findAndModify"),
            get: v("Database.get"),
            group: v("Database.group"),
            save: v("Database.save"),
            update: v("Database.update"),
            use: w([ "batch", "clean", "count", "destroy", "destruct", "find", "findAndModify", "get", "group", "save", "update" ])
        };
        c.Persistence.Local = {
            create: function(a, b) {
                b = b || {};
                var c = g === a.namespace ? g : a.collection;
                if ("_group" === a.id) return G.group(c, a.data, b);
                var d = o(a.data) ? "batch" : "save", e = G[d](c, a.data, b);
                return e.then(function(a) {
                    return b.offline && !1 !== b.track ? I.notify(c, a, b).then(function() {
                        return a;
                    }) : a;
                });
            },
            read: function(a, b) {
                b = b || {};
                var d = g === a.namespace ? g : a.collection;
                if ("_count" === a.id) return G.count(d, a.query, b);
                if ("_me" === a.collection) {
                    var e = c.getActiveUser();
                    if (null !== e) return G.get(d, e._id, b).then(null, function(a) {
                        return a.name === c.Error.ENTITY_NOT_FOUND ? e : c.Defer.reject(a);
                    });
                    var f = m(c.Error.NO_ACTIVE_USER);
                    return c.Defer.reject(f);
                }
                var h;
                return h = null == a.id ? G.find(d, a.query, b) : G.get(d, a.id, b), h.then(function(a) {
                    return b.relations ? E.get(a, b) : a;
                });
            },
            update: function(a, b) {
                b = b || {};
                var c = g === a.namespace ? g : a.collection, d = G.update(c, a.data, b);
                return d.then(function(a) {
                    return b.offline && !1 !== b.track ? I.notify(c, a, b).then(function() {
                        return a;
                    }) : a;
                });
            },
            destroy: function(a, b) {
                b = b || {};
                var c, d = g === a.namespace ? g : a.collection;
                return c = null == a.id ? G.clean(d, a.query, b) : G.destroy(d, a.id, b), c.then(function(a) {
                    return b.offline && !1 !== b.track ? I.notify(d, a.documents, b).then(function() {
                        return a;
                    }) : a;
                });
            }
        };
        var H = null;
        c.Persistence.Net = {
            create: function(a, b) {
                return a.method = "POST", c.Persistence.Net._request(a, b);
            },
            read: function(a, b) {
                if (a.flags = a.flags || {}, b = b || {}, null != a.collection && (!1 !== b.fileTls && (a.flags.kinveyfile_tls = !0), 
                b.fileTtl && (a.flags.kinveyfile_ttl = b.fileTtl)), b.relations) {
                    b.exclude = b.exclude || [];
                    var d = Object.keys(b.relations).filter(function(a) {
                        return -1 === b.exclude.indexOf(a);
                    });
                    0 !== d.length && (a.flags.retainReferences = !1, a.flags.resolve = d.join(","));
                }
                return a.method = "GET", c.Persistence.Net._request(a, b);
            },
            update: function(a, b) {
                return a.method = "PUT", c.Persistence.Net._request(a, b);
            },
            destroy: function(a, b) {
                return a.method = "DELETE", c.Persistence.Net._request(a, b);
            },
            _request: function(a, b) {
                if (null == a.method) throw new c.Error("request argument must contain: method.");
                if (null == a.namespace) throw new c.Error("request argument must contain: namespace.");
                if (null == a.auth) throw new c.Error("request argument must contain: auth.");
                var d;
                if (null == c.appKey && y.None !== a.auth) return d = m(c.Error.MISSING_APP_CREDENTIALS), 
                c.Defer.reject(d);
                if (null == c.masterSecret && b.skipBL) return d = m(c.Error.MISSING_MASTER_CREDENTIALS), 
                c.Defer.reject(d);
                b.trace = b.trace || !1;
                var e = [ a.namespace, c.appKey, a.collection, a.id ];
                e = e.filter(function(a) {
                    return null != a;
                }).map(c.Persistence.Net.encode);
                var f = [ c.API_ENDPOINT ].concat(e).join("/") + "/", g = a.flags || {};
                if (a.query) {
                    var h = a.query.toJSON();
                    g.query = h.filter, null !== h.limit && (g.limit = h.limit), 0 !== h.skip && (g.skip = h.skip), 
                    u(h.sort) || (g.sort = h.sort);
                }
                !1 !== b.nocache && (g._ = Math.random().toString(36).substr(2));
                var i = [];
                for (var j in g) if (g.hasOwnProperty(j)) {
                    var k = t(g[j]) ? g[j] : JSON.stringify(g[j]);
                    i.push(c.Persistence.Net.encode(j) + "=" + c.Persistence.Net.encode(k));
                }
                0 < i.length && (f += "?" + i.join("&")), null === H && (H = z());
                var l = {
                    Accept: "application/json",
                    "X-Kinvey-API-Version": c.API_VERSION,
                    "X-Kinvey-Device-Information": H
                };
                null != a.data && (l["Content-Type"] = "application/json; charset=utf-8"), b.contentType && (l["X-Kinvey-Content-Type"] = b.contentType), 
                b.skipBL && (l["X-Kinvey-Skip-Business-Logic"] = "true"), b.trace && (l["X-Kinvey-Include-Headers-In-Response"] = "X-Kinvey-Request-Id", 
                l["X-Kinvey-ResponseWrapper"] = "true");
                var n = a.auth().then(function(a) {
                    if (null !== a) {
                        var b = a.credentials;
                        null != a.username && (b = c.Persistence.Net.base64(a.username + ":" + a.password)), 
                        l.Authorization = a.scheme + " " + b;
                    }
                });
                return n.then(function() {
                    var d = c.Persistence.Net.request(a.method, f, a.data, l, b).then(function(a) {
                        try {
                            a = JSON.parse(a);
                        } catch (c) {}
                        return b.trace && r(a) ? a.result : a;
                    }, function(a) {
                        try {
                            a = JSON.parse(a);
                        } catch (d) {}
                        var e = null;
                        if (b.trace && (e = a.headers["X-Kinvey-Request-Id"], a = a.result), null != a && null != a.error) a = {
                            name: a.error,
                            description: a.description || "",
                            debug: a.debug || ""
                        }, b.trace && (a.requestId = e); else {
                            var f = {
                                abort: c.Error.REQUEST_ABORT_ERROR,
                                error: c.Error.REQUEST_ERROR,
                                timeout: c.Error.REQUEST_TIMEOUT_ERROR
                            };
                            a = m(f[a] || f.error, {
                                debug: a
                            });
                        }
                        return c.Defer.reject(a);
                    });
                    return d.then(null, function(a) {
                        if (c.Error.USER_LOCKED_DOWN === a.name) {
                            if (c.setActiveUser(null), "undefined" != typeof G) {
                                var b = function() {
                                    c.Defer.reject(a);
                                };
                                return c.Sync.destruct().then(b, b);
                            }
                        } else c.Error.INVALID_CREDENTIALS === a.name && (a.debug += " It is possible the tokens used to execute the request are expired. In that case, please run `Kinvey.User.logout({ force: true })`, and then log back in  using`Kinvey.User.login(username, password)` to solve this issue.");
                        return c.Defer.reject(a);
                    });
                });
            },
            base64: v("Kinvey.Persistence.Net.base64"),
            encode: v("Kinvey.Persistence.Net.encode"),
            request: v("Kinvey.Persistence.Net.request"),
            use: w([ "base64", "encode", "request" ])
        };
        var I = {
            enabled: !1,
            online: !0,
            system: "system.sync",
            count: function(a, b) {
                if (b = b || {}, null != a) return G.get(I.system, a, b).then(function(a) {
                    return a.size;
                }, function(a) {
                    return c.Error.ENTITY_NOT_FOUND === a.name ? 0 : c.Defer.reject(a);
                });
                var d = c.Group.sum("size").toJSON();
                return G.group(I.system, d, b).then(function(a) {
                    return a[0] ? a[0].result : 0;
                });
            },
            execute: function(a) {
                var b = new c.Query().greaterThan("size", 0);
                return G.find(I.system, b, a).then(function(b) {
                    var d = b.map(function(b) {
                        return I._collection(b._id, b.documents, a);
                    });
                    return c.Defer.all(d);
                });
            },
            notify: function(a, b, c) {
                return G.findAndModify(I.system, a, function(c) {
                    return b = o(b) ? b : [ b ], c = c || {
                        _id: a,
                        documents: {},
                        size: 0
                    }, b.forEach(function(a) {
                        c.documents.hasOwnProperty(a._id) || (c.size += 1);
                        var b = null != a._kmd ? a._kmd.lmt : null;
                        c.documents[a._id] = b;
                    }), c;
                }, c).then(function() {
                    return null;
                });
            },
            _collection: function(a, b, e) {
                var f = {
                    collection: a,
                    success: [],
                    error: []
                }, h = Object.keys(b), i = {
                    namespace: g === a ? g : d,
                    collection: g === a ? null : a,
                    query: new c.Query().contains("_id", h),
                    auth: y.Default
                }, j = [ c.Persistence.Local.read(i, e), c.Persistence.Net.read(i, e) ];
                return c.Defer.all(j).then(function(a) {
                    var b = {
                        local: {},
                        net: {}
                    };
                    return a[0].forEach(function(a) {
                        b.local[a._id] = a;
                    }), a[1].forEach(function(a) {
                        b.net[a._id] = a;
                    }), b;
                }).then(function(d) {
                    var g = h.map(function(c) {
                        var g = {
                            id: c,
                            timestamp: b[c]
                        };
                        return I._document(a, g, d.local[c] || null, d.net[c] || null, e).then(null, function(a) {
                            return f.error.push(a.id), null;
                        });
                    });
                    return c.Defer.all(g);
                }).then(function(b) {
                    var d = b.filter(function(a) {
                        return null != a && null !== a.document;
                    }), f = b.filter(function(a) {
                        return null != a && null === a.document;
                    }), g = [ I._save(a, d, e), I._destroy(a, f, e) ];
                    return c.Defer.all(g);
                }).then(function(b) {
                    return f.success = f.success.concat(b[0].success, b[1].success), f.error = f.error.concat(b[0].error, b[1].error), 
                    G.findAndModify(I.system, a, function(a) {
                        return f.success.forEach(function(b) {
                            a.documents.hasOwnProperty(b) && (a.size -= 1, delete a.documents[b]);
                        }), a;
                    }, e);
                }).then(function() {
                    return f;
                });
            },
            _destroy: function(a, b, e) {
                if (b = b.map(function(a) {
                    return a.id;
                }), 0 === b.length) return c.Defer.resolve({
                    success: [],
                    error: []
                });
                var f = {
                    namespace: g === a ? g : d,
                    collection: g === a ? null : a,
                    query: new c.Query().contains("_id", b),
                    auth: y.Default
                }, h = [ c.Persistence.Local.destroy(f, e), c.Persistence.Net.destroy(f, e) ];
                return c.Defer.all(h).then(function() {
                    return {
                        success: b,
                        error: []
                    };
                }, function() {
                    return {
                        success: [],
                        error: b
                    };
                });
            },
            _document: function(a, b, d, e, f) {
                return null === e || null != e._kmd && b.timestamp === e._kmd.lmt ? c.Defer.resolve({
                    id: b.id,
                    document: d
                }) : null != f.conflict ? f.conflict(a, d, e).then(function(a) {
                    return {
                        id: b.id,
                        document: a
                    };
                }, function() {
                    return c.Defer.reject({
                        id: b.id,
                        document: [ d, e ]
                    });
                }) : c.Defer.reject({
                    id: b.id,
                    document: [ d, e ]
                });
            },
            _save: function(a, b, e) {
                b = b.map(function(a) {
                    return a.document;
                });
                var f = [], h = b.map(function(b) {
                    return c.Persistence.Net.update({
                        namespace: g === a ? g : d,
                        collection: g === a ? null : a,
                        id: b._id,
                        data: b,
                        auth: y.Default
                    }, e).then(null, function() {
                        return f.push(b._id), null;
                    });
                });
                return c.Defer.all(h).then(function(b) {
                    return c.Persistence.Local.create({
                        namespace: g === a ? g : d,
                        collection: g === a ? null : a,
                        data: b,
                        auth: y.Default
                    }, e);
                }).then(function(a) {
                    return {
                        success: a.map(function(a) {
                            return a._id;
                        }),
                        error: f
                    };
                }, function() {
                    return {
                        success: [],
                        error: b.map(function(a) {
                            return a._id;
                        })
                    };
                });
            }
        };
        c.Sync = {
            count: function(a, b) {
                b = b || {};
                var c = I.count(a, b);
                return n(c, b);
            },
            destruct: function(a) {
                a = a || {};
                var b = G.destruct(a);
                return n(b, a);
            },
            execute: function(a) {
                if (!c.Sync.isOnline()) {
                    var b = m(c.Error.SYNC_ERROR, {
                        debug: "Sync is not enabled, or the application resides in offline mode."
                    });
                    return c.Defer.reject(b);
                }
                a = a || {};
                var d;
                return null != a.user ? (d = c.User.login(a.user).then(function() {
                    return delete a.user, c.Sync.execute(a);
                }), delete a.success, n(d, a)) : (d = I.execute(a), n(d, a));
            },
            init: function(a) {
                return a = a || {}, I.enabled = null != a ? a.enable : !1, I.online = "undefined" != typeof a.online ? a.online : I.online, 
                c.Defer.resolve(null);
            },
            isEnabled: function() {
                return I.enabled;
            },
            isOnline: function() {
                return I.online;
            },
            offline: function() {
                if (!c.Sync.isEnabled()) {
                    var a = m(c.Error.SYNC_ERROR, {
                        debug: "Sync is not enabled."
                    });
                    return c.Defer.reject(a);
                }
                return I.online = !1, c.Defer.resolve(null);
            },
            online: function(a) {
                if (!c.Sync.isEnabled()) {
                    var b = m(c.Error.SYNC_ERROR, {
                        debug: "Sync is not enabled."
                    });
                    return c.Defer.reject(b);
                }
                a = a || {};
                var d = I.online;
                return I.online = !0, !1 !== a.sync && d !== I.online ? c.Sync.execute(a) : c.Defer.resolve(null);
            },
            clientAlwaysWins: function(a, b) {
                return c.Defer.resolve(b);
            },
            serverAlwaysWins: function(a, b, d) {
                return c.Defer.resolve(d);
            }
        };
        var J = {
            db: null,
            dbName: function() {
                if (null == c.appKey) throw new c.Error("Kinvey.appKey must not be null.");
                return "Kinvey." + c.appKey;
            },
            size: 5242880,
            open: function() {
                return a.openDatabase(J.dbName(), 1, "", J.size);
            },
            transaction: function(a, b, d, e) {
                var f;
                if (!t(a) || !/^[a-zA-Z0-9\-]{1,128}/.test(a)) return f = m(c.Error.INVALID_IDENTIFIER, {
                    description: "The collection name has an invalid format.",
                    debug: 'The collection name must be a string containing only alphanumeric characters and dashes, "' + a + '" given.'
                }), c.Defer.reject(f);
                var g = '"' + a + '"', h = "sqlite_master" === a, i = o(b);
                b = i ? b : [ [ b, d ] ], e = e || !1, null === J.db && (J.db = J.open());
                var j = c.Defer.deferred(), k = e || !p(J.db.readTransaction);
                return J.db[k ? "transaction" : "readTransaction"](function(a) {
                    e && !h && a.executeSql("CREATE TABLE IF NOT EXISTS " + g + " (key BLOB PRIMARY KEY NOT NULL, value BLOB NOT NULL)");
                    var c = b.length, d = [];
                    b.forEach(function(b) {
                        var e = b[0].replace("#{collection}", g);
                        a.executeSql(e, b[1], function(a, b) {
                            var e = {
                                rowCount: b.rowsAffected,
                                result: []
                            };
                            if (b.rows.length) for (var f = 0; f < b.rows.length; f += 1) {
                                var g = b.rows.item(f).value, k = h ? g : JSON.parse(g);
                                e.result.push(k);
                            }
                            d.push(e), c -= 1, 0 === c && j.resolve(i ? d : d.shift());
                        });
                    });
                }, function(b) {
                    b = t(b) ? b : b.message, f = -1 !== b.indexOf("no such table") ? m(c.Error.COLLECTION_NOT_FOUND, {
                        description: "This collection not found for this app backend",
                        debug: {
                            collection: a
                        }
                    }) : m(c.Error.DATABASE_ERROR, {
                        debug: b
                    }), j.reject(f);
                }), j.promise;
            },
            objectID: function(a) {
                a = a || 24;
                for (var b = "abcdef0123456789", c = "", d = 0, e = b.length; a > d; d += 1) {
                    var f = Math.floor(Math.random() * e);
                    c += b.substring(f, f + 1);
                }
                return c;
            },
            batch: function(a, b, d) {
                if (0 === b.length) return c.Defer.resolve(b);
                var e = [];
                b = b.map(function(a) {
                    return a._id = a._id || J.objectID(), e.push([ "REPLACE INTO #{collection} (key, value) VALUES (?, ?)", [ a._id, JSON.stringify(a) ] ]), 
                    a;
                });
                var f = J.transaction(a, e, null, !0, d);
                return f.then(function() {
                    return b;
                });
            },
            clean: function(a, b, c) {
                return null != b && b.sort(null).limit(null).skip(0), J.find(a, b, c).then(function(b) {
                    if (0 === b.length) return {
                        count: 0,
                        documents: []
                    };
                    var d = [], e = b.map(function(a) {
                        return d.push("?"), a._id;
                    }), f = "DELETE FROM #{collection} WHERE key IN(" + d.join(",") + ")", g = J.transaction(a, f, e, !0, c);
                    return g.then(function(a) {
                        return a.rowCount = null != a.rowCount ? a.rowCount : b.length, {
                            count: a.rowCount,
                            documents: b
                        };
                    });
                });
            },
            count: function(a, b, c) {
                return null != b && b.sort(null).limit(null).skip(0), J.find(a, b, c).then(function(a) {
                    return {
                        count: a.length
                    };
                });
            },
            destroy: function(a, b, d) {
                var e = J.transaction(a, [ [ "SELECT value FROM #{collection} WHERE key = ?", [ b ] ], [ "DELETE       FROM #{collection} WHERE key = ?", [ b ] ] ], null, !0, d);
                return e.then(function(d) {
                    var e = d[1].rowCount, f = d[0].result;
                    if (e = null != e ? e : d[0].result.length, 0 === e) {
                        var g = m(c.Error.ENTITY_NOT_FOUND, {
                            description: "This entity not found in the collection",
                            debug: {
                                collection: a,
                                id: b
                            }
                        });
                        return c.Defer.reject(g);
                    }
                    return {
                        count: e,
                        documents: f
                    };
                });
            },
            destruct: function(a) {
                var b = "SELECT name AS value FROM #{collection} WHERE type = ?", c = [ "table" ], d = J.transaction("sqlite_master", b, c, !1, a);
                return d.then(function(b) {
                    var c = b.result;
                    if (0 === c.length) return null;
                    var d = c.filter(function(a) {
                        return /^[a-zA-Z0-9\-]{1,128}/.test(a);
                    }).map(function(a) {
                        return [ "DROP TABLE IF EXISTS '" + a + "'" ];
                    });
                    return J.transaction("sqlite_master", d, null, !0, a);
                }).then(function() {
                    return null;
                });
            },
            find: function(b, d, e) {
                var f = "SELECT value FROM #{collection}", g = J.transaction(b, f, [], !1, e);
                return g.then(function(b) {
                    return b = b.result, null == d ? b : (b = a.sift(d.toJSON().filter, b), d._postProcess(b));
                }, function(a) {
                    return c.Error.COLLECTION_NOT_FOUND === a.name ? [] : c.Defer.reject(a);
                });
            },
            findAndModify: function(a, b, d, e) {
                var f = J.get(a, b, e).then(null, function(a) {
                    return c.Error.ENTITY_NOT_FOUND === a.name ? null : c.Defer.reject(a);
                });
                return f.then(function(b) {
                    var c = d(b);
                    return J.save(a, c, e);
                });
            },
            get: function(a, b, d) {
                var e = "SELECT value FROM #{collection} WHERE key = ?", f = J.transaction(a, e, [ b ], !1, d);
                return f.then(function(d) {
                    var e = d.result;
                    if (0 === e.length) {
                        var f = m(c.Error.ENTITY_NOT_FOUND, {
                            description: "This entity not found in the collection",
                            debug: {
                                collection: a,
                                id: b
                            }
                        });
                        return c.Defer.reject(f);
                    }
                    return e[0];
                }, function(d) {
                    return c.Error.COLLECTION_NOT_FOUND === d.name && (d = m(c.Error.ENTITY_NOT_FOUND, {
                        description: "This entity not found in the collection",
                        debug: {
                            collection: a,
                            id: b
                        }
                    })), c.Defer.reject(d);
                });
            },
            group: function(a, b, d) {
                var e = b.reduce.replace(/function[\s\S]*?\([\s\S]*?\)/, "");
                b.reduce = new Function([ "doc", "out" ], e);
                var f = new c.Query({
                    filter: b.condition
                });
                return J.find(a, f, d).then(function(a) {
                    var c = {};
                    a.forEach(function(a) {
                        var d = {};
                        for (var e in b.key) b.key.hasOwnProperty(e) && (d[e] = a[e]);
                        var f = JSON.stringify(d);
                        if (null == c[f]) {
                            c[f] = d;
                            for (var g in b.initial) b.initial.hasOwnProperty(g) && (c[f][g] = b.initial[g]);
                        }
                        b.reduce(a, c[f]);
                    });
                    var d = [];
                    for (var e in c) c.hasOwnProperty(e) && d.push(c[e]);
                    return d;
                });
            },
            save: function(a, b, c) {
                b._id = b._id || J.objectID();
                var d = "REPLACE INTO #{collection} (key, value) VALUES (?, ?)", e = [ b._id, JSON.stringify(b) ], f = J.transaction(a, d, e, !0, c);
                return f.then(function() {
                    return b;
                });
            },
            update: function(a, b, c) {
                return J.save(a, b, c);
            }
        };
        "undefined" != typeof a.openDatabase && "undefined" != typeof a.sift && (G.use(J), 
        [ "near", "regex", "within" ].forEach(function(b) {
            a.sift.useOperator(b, function() {
                throw new c.Error(b + " query operator is not supported locally.");
            });
        }));
        var K = {
            db: null,
            dbName: function() {
                if (null == c.appKey) throw new c.Error("Kinvey.appKey must not be null.");
                return "Kinvey." + c.appKey;
            },
            impl: a.indexedDB || a.webkitIndexedDB || a.mozIndexedDB || a.oIndexedDB || a.msIndexedDB,
            inTransaction: !1,
            objectID: function(a) {
                a = a || 24;
                for (var b = "abcdef0123456789", c = "", d = 0, e = b.length; a > d; d += 1) {
                    var f = Math.floor(Math.random() * e);
                    c += b.substring(f, f + 1);
                }
                return c;
            },
            pending: [],
            transaction: function(a, b, d, e, f) {
                if (!t(a) || !/^[a-zA-Z0-9\-]{1,128}/.test(a)) return e(m(c.Error.INVALID_IDENTIFIER, {
                    description: "The collection name has an invalid format.",
                    debug: 'The collection name must be a string containing only alphanumeric characters and dashes, "' + a + '" given.'
                }));
                if (b = b || !1, null !== K.db) {
                    if (K.db.objectStoreNames.contains(a)) {
                        var g = b ? "readwrite" : "readonly", h = K.db.transaction([ a ], g), i = h.objectStore(a);
                        return d(i);
                    }
                    if (!b) return e(m(c.Error.COLLECTION_NOT_FOUND, {
                        description: "This collection not found for this app backend",
                        debug: {
                            collection: a
                        }
                    }));
                }
                if (!0 !== f && K.inTransaction) return K.pending.push(function() {
                    K.transaction(a, b, d, e);
                });
                K.inTransaction = !0;
                var j;
                if (null !== K.db) {
                    var k = K.db.version + 1;
                    K.db.close(), j = K.impl.open(K.dbName(), k);
                } else {
                    if (null == c.appKey) return K.inTransaction = !1, e(m(c.Error.MISSING_APP_CREDENTIALS));
                    j = K.impl.open(K.dbName());
                }
                j.onupgradeneeded = function() {
                    K.db = j.result, b && K.db.createObjectStore(a, {
                        keyPath: "_id"
                    });
                }, j.onsuccess = function() {
                    K.db = j.result, K.db.onversionchange = function() {
                        null !== K.db && (K.db.close(), K.db = null);
                    };
                    var c = function(a) {
                        return function(b) {
                            var c = a(b);
                            if (K.inTransaction = !1, 0 !== K.pending.length) {
                                var d = K.pending;
                                K.pending = [], d.forEach(function(a) {
                                    a();
                                });
                            }
                            return c;
                        };
                    };
                    K.transaction(a, b, c(d), c(e), !0);
                }, j.onerror = function(a) {
                    e(m(c.Error.DATABASE_ERROR, {
                        debug: a
                    }));
                };
            },
            batch: function(a, b) {
                if (0 === b.length) return c.Defer.resolve(b);
                var d = c.Defer.deferred();
                return K.transaction(a, !0, function(a) {
                    var e = a.transaction;
                    b.forEach(function(b) {
                        b._id = b._id || K.objectID(), a.put(b);
                    }), e.oncomplete = function() {
                        d.resolve(b);
                    }, e.onerror = function(a) {
                        var b = m(c.Error.DATABASE_ERROR, {
                            debug: a
                        });
                        d.reject(b);
                    };
                }, function(a) {
                    d.reject(a);
                }), d.promise;
            },
            clean: function(a, b, d) {
                return null != b && b.sort(null).limit(null).skip(0), K.find(a, b, d).then(function(b) {
                    if (0 === b.length) return {
                        count: 0,
                        documents: []
                    };
                    var d = c.Defer.deferred();
                    return K.transaction(a, !0, function(a) {
                        var e = a.transaction;
                        b.forEach(function(b) {
                            a["delete"](b._id);
                        }), e.oncomplete = function() {
                            d.resolve({
                                count: b.length,
                                documents: b
                            });
                        }, e.onerror = function(a) {
                            var b = m(c.Error.DATABASE_ERROR, {
                                debug: a
                            });
                            d.reject(b);
                        };
                    }), d.promise;
                });
            },
            count: function(a, b, c) {
                return null != b && b.sort(null).limit(null).skip(0), K.find(a, b, c).then(function(a) {
                    return {
                        count: a.length
                    };
                });
            },
            destroy: function(a, b) {
                var d = c.Defer.deferred();
                return K.transaction(a, !0, function(e) {
                    var f = e.transaction, g = e.get(b);
                    e["delete"](b), f.oncomplete = function() {
                        return null == g.result ? d.reject(m(c.Error.ENTITY_NOT_FOUND, {
                            description: "This entity not found in the collection",
                            debug: {
                                collection: a,
                                id: b
                            }
                        })) : (d.resolve({
                            count: 1,
                            documents: [ g.result ]
                        }), void 0);
                    }, f.onerror = function(a) {
                        var b = m(c.Error.DATABASE_ERROR, {
                            debug: a
                        });
                        d.reject(b);
                    };
                }, function(a) {
                    d.reject(a);
                }), d.promise;
            },
            destruct: function() {
                if (null == c.appKey) {
                    var a = m(c.Error.MISSING_APP_CREDENTIALS);
                    return c.Defer.reject(a);
                }
                var b = c.Defer.deferred();
                null !== K.db && (K.db.close(), K.db = null);
                var d = K.impl.deleteDatabase(K.dbName());
                return d.onsuccess = function() {
                    b.resolve(null);
                }, d.onerror = function(a) {
                    var d = m(c.Error.DATABASE_ERROR, {
                        debug: a
                    });
                    b.reject(d);
                }, b.promise;
            },
            find: function(b, d) {
                var e = c.Defer.deferred();
                return K.transaction(b, !1, function(a) {
                    var b = a.openCursor(), d = [];
                    b.onsuccess = function() {
                        var a = b.result;
                        null != a ? (d.push(a.value), a["continue"]()) : e.resolve(d);
                    }, b.onerror = function(a) {
                        e.reject(m(c.DATABASE_ERROR, {
                            debug: a
                        }));
                    };
                }, function(a) {
                    return c.Error.COLLECTION_NOT_FOUND === a.name ? e.resolve([]) : e.reject(a);
                }), e.promise.then(function(b) {
                    return null == d ? b : (b = a.sift(d.toJSON().filter, b), d._postProcess(b));
                });
            },
            findAndModify: function(a, b, d) {
                var e = c.Defer.deferred();
                return K.transaction(a, !0, function(a) {
                    var f = null, g = a.get(b);
                    g.onsuccess = function() {
                        f = d(g.result || null), a.put(f);
                    };
                    var h = a.transaction;
                    h.oncomplete = function() {
                        e.resolve(f);
                    }, h.onerror = function(a) {
                        var b = m(c.Error.DATABASE_ERROR, {
                            debug: a
                        });
                        e.reject(b);
                    };
                }, function(a) {
                    e.reject(a);
                }), e.promise;
            },
            get: function(a, b) {
                var d = c.Defer.deferred();
                return K.transaction(a, !1, function(e) {
                    var f = e.get(b);
                    f.onsuccess = function() {
                        return null != f.result ? d.resolve(f.result) : (d.reject(m(c.Error.ENTITY_NOT_FOUND, {
                            description: "This entity not found in the collection",
                            debug: {
                                collection: a,
                                id: b
                            }
                        })), void 0);
                    }, f.onerror = function(a) {
                        d.reject(m(c.Error.DATABASE_ERROR, {
                            debug: a
                        }));
                    };
                }, function(e) {
                    c.Error.COLLECTION_NOT_FOUND === e.name && (e = m(c.Error.ENTITY_NOT_FOUND, {
                        description: "This entity not found in the collection",
                        debug: {
                            collection: a,
                            id: b
                        }
                    })), d.reject(e);
                }), d.promise;
            },
            group: function(a, b, d) {
                var e = b.reduce.replace(/function[\s\S]*?\([\s\S]*?\)/, "");
                b.reduce = new Function([ "doc", "out" ], e);
                var f = new c.Query({
                    filter: b.condition
                });
                return K.find(a, f, d).then(function(a) {
                    var c = {};
                    a.forEach(function(a) {
                        var d = {};
                        for (var e in b.key) b.key.hasOwnProperty(e) && (d[e] = a[e]);
                        var f = JSON.stringify(d);
                        if (null == c[f]) {
                            c[f] = d;
                            for (var g in b.initial) b.initial.hasOwnProperty(g) && (c[f][g] = b.initial[g]);
                        }
                        b.reduce(a, c[f]);
                    });
                    var d = [];
                    for (var e in c) c.hasOwnProperty(e) && d.push(c[e]);
                    return d;
                });
            },
            save: function(a, b) {
                b._id = b._id || K.objectID();
                var d = c.Defer.deferred();
                return K.transaction(a, !0, function(a) {
                    var e = a.put(b);
                    e.onsuccess = function() {
                        d.resolve(b);
                    }, e.onerror = function(a) {
                        var b = m(c.Error.DATABASE_ERROR, {
                            debug: a
                        });
                        d.reject(b);
                    };
                }, function(a) {
                    d.reject(a);
                }), d.promise;
            },
            update: function(a, b, c) {
                return K.save(a, b, c);
            }
        };
        "undefined" != typeof K.impl && "undefined" != typeof a.sift && (G.use(K), [ "near", "regex", "within" ].forEach(function(b) {
            a.sift.useOperator(b, function() {
                throw new c.Error(b + " query operator is not supported locally.");
            });
        }));
        var L = {
            facebook: function(a) {
                return L.oAuth2("facebook", a);
            },
            google: function(a) {
                return L.oAuth2("google", a);
            },
            linkedIn: function(a) {
                return L.oAuth1("linkedIn", a);
            },
            twitter: function(a) {
                return L.oAuth1("twitter", a);
            },
            oAuth1: function(a, b) {
                return L.requestToken(a, b).then(function(a) {
                    if (a.error || a.denied) {
                        var b = m(c.Error.SOCIAL_ERROR, {
                            debug: a
                        });
                        return c.Defer.reject(b);
                    }
                    return {
                        oauth_token: a.oauth_token,
                        oauth_token_secret: a.oauth_token_secret,
                        oauth_verifier: a.oauth_verifier
                    };
                }).then(function(d) {
                    return c.Persistence.Net.create({
                        namespace: g,
                        data: d,
                        flags: {
                            provider: a,
                            step: "verifyToken"
                        },
                        auth: y.App
                    }, b);
                }).then(function(c) {
                    return b._provider = a, c;
                });
            },
            oAuth2: function(a, b) {
                return b.state = Math.random().toString(36).substr(2), L.requestToken(a, b).then(function(a) {
                    var d;
                    return a.state !== b.state ? (d = m(c.Error.SOCIAL_ERROR, {
                        debug: "The state parameters did not match (CSRF attack?)."
                    }), c.Defer.reject(d)) : a.error ? (d = m(c.Error.SOCIAL_ERROR, {
                        debug: a
                    }), c.Defer.reject(d)) : {
                        access_token: a.access_token,
                        expires_in: a.expires_in
                    };
                });
            },
            requestToken: function(b, d) {
                var e = "about:blank", f = a.open(e, "KinveyOAuth2"), h = d.redirect || a.location.toString();
                return c.Persistence.Net.create({
                    namespace: g,
                    data: {
                        redirect: h,
                        state: d.state
                    },
                    flags: {
                        provider: b,
                        step: "requestToken"
                    },
                    auth: y.App
                }, d).then(function(b) {
                    var g = c.Defer.deferred();
                    null != f && (f.location = b.url);
                    var h = 0, i = 100, j = a.setInterval(function() {
                        var k;
                        if (null == f) a.clearTimeout(j), k = m(c.Error.SOCIAL_ERROR, {
                            debug: "The popup was blocked."
                        }), g.reject(k); else if (f.closed) a.clearTimeout(j), k = m(c.Error.SOCIAL_ERROR, {
                            debug: "The popup was closed unexpectedly."
                        }), g.reject(k); else if (d.timeout && h > d.timeout) a.clearTimeout(j), f.close(), 
                        k = m(c.Error.SOCIAL_ERROR, {
                            debug: "The authorization request timed out."
                        }), g.reject(k); else {
                            var l = !1;
                            try {
                                l = e !== f.location.toString();
                            } catch (n) {}
                            if (l) {
                                a.clearTimeout(j);
                                var o = f.location, p = o.search.substring(1) + "&" + o.hash.substring(1), q = L.tokenize(p);
                                null != b.oauth_token_secret && (q.oauth_token_secret = b.oauth_token_secret), g.resolve(q), 
                                f.close();
                            }
                        }
                        h += i;
                    }, i);
                    return g.promise;
                });
            },
            tokenize: function(b) {
                var c = {};
                return b.split("&").forEach(function(b) {
                    var d = b.split("=", 2).map(a.decodeURIComponent);
                    d[0] && (c[d[0]] = d[1]);
                }), c;
            }
        };
        C.use(L);
        var M = angular.injector([ "ng" ]).get("$q");
        c.Defer.use({
            deferred: M.defer
        });
        var N = angular.module("kinvey", []);
        if (N.factory("$kinvey", [ "$http", "$q", function(b, d) {
            c.Defer.use({
                deferred: d.defer
            });
            var e = {
                base64: function(b) {
                    return a.btoa(b);
                },
                supportsBlob: function() {
                    try {
                        return new a.Blob() && !0;
                    } catch (b) {
                        return !1;
                    }
                }(),
                encode: a.encodeURIComponent,
                request: function(f, g, h, i, j) {
                    if (h = h || {}, i = i || {}, j = j || {}, 0 === g.indexOf(c.API_ENDPOINT) && "GET" === f) {
                        var k = a.location;
                        null != k && null != k.protocol && (i["X-Kinvey-Origin"] = k.protocol + "//" + k.host);
                    }
                    return r(h) && !(null != a.ArrayBuffer && h instanceof a.ArrayBuffer || null != a.Blob && h instanceof a.Blob) && (h = JSON.stringify(h)), 
                    b({
                        data: h,
                        headers: i,
                        method: f,
                        timeout: j.timeout,
                        url: g
                    }).then(function(b) {
                        if (b = b.data, j.file && null != b && null != a.ArrayBuffer) {
                            for (var c = new a.ArrayBuffer(b.length), d = new a.Uint8Array(c), f = 0, g = b.length; g > f; f += 1) d[f] = b.charCodeAt(f);
                            e.supportsBlob && (c = new a.Blob([ d ], {
                                type: j.file
                            })), b = c;
                        }
                        return b || null;
                    }, function(a) {
                        return d.reject(a.data || null);
                    });
                }
            };
            return c.Persistence.Net.use(e), c;
        } ]), "undefined" != typeof localStorage) {
            var O = c.Defer.resolve(null), P = {
                _destroy: function(a) {
                    return O = O.then(function() {
                        return localStorage.removeItem(a), c.Defer.resolve(null);
                    });
                },
                _get: function(a) {
                    return O = O.then(function() {
                        var b = localStorage.getItem(a);
                        return c.Defer.resolve(b ? JSON.parse(b) : null);
                    });
                },
                _save: function(a, b) {
                    return O = O.then(function() {
                        return localStorage.setItem(a, JSON.stringify(b)), c.Defer.resolve(null);
                    });
                }
            };
            x.use(P);
        }
        return c;
    }, c = b();
    "object" == typeof module && "object" == typeof module.exports ? module.exports = c : "function" == typeof define && define.amd ? define("kinvey", [], function() {
        return c;
    }) : a.Kinvey = c;
}).call(this);

function adfs(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = window.cordova.InAppBrowser.open(e + "/adfs/oauth2/authorize?response_type=code&client_id=" + d + "&redirect_uri=http://localhost/callback&resource=" + f, "_blank", "location=no");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf("http://localhost/callback")) {
                    var c = a.url.split("code=")[1];
                    b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                        method: "post",
                        url: e + "/adfs/oauth2/token",
                        data: "client_id=" + d + "&code=" + c + "&redirect_uri=http://localhost/callback&grant_type=authorization_code"
                    }).success(function(a) {
                        g.resolve(a);
                    }).error(function(a, b) {
                        g.reject("Problem authenticating");
                    })["finally"](function() {
                        setTimeout(function() {
                            h.close();
                        }, 10);
                    });
                }
            }), h.addEventListener("exit", function(a) {
                g.reject("The sign in flow was canceled");
            });
        } else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function azureAD(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = window.cordova.InAppBrowser.open("https://login.microsoftonline.com/" + e + "/oauth2/authorize?response_type=code&client_id=" + d + "&redirect_uri=http://localhost/callback", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf("http://localhost/callback")) {
                    var c = a.url.split("code=")[1];
                    console.log(c), b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                    b({
                        method: "post",
                        url: "https://login.microsoftonline.com/" + e + "/oauth2/token",
                        data: "client_id=" + d + "&code=" + c + "&redirect_uri=http://localhost/callback&grant_type=authorization_code&resource=" + f
                    }).success(function(a) {
                        g.resolve(a);
                    }).error(function(a, b) {
                        g.reject("Problem authenticating");
                    })["finally"](function() {
                        setTimeout(function() {
                            h.close();
                        }, 10);
                    });
                }
            }), h.addEventListener("exit", function(a) {
                g.reject("The sign in flow was canceled");
            });
        } else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function box(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://app.box.com/api/oauth2/authorize/?client_id=" + d + "&redirect_uri=" + i + "&state=" + f + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(i) && (requestToken = a.url.split("code=")[1], b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                b({
                    method: "post",
                    url: "https://app.box.com/api/oauth2/token",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + i + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function digitalOcean(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = "http://localhost/callback";
            void 0 !== f && f.hasOwnProperty("redirect_uri") && (h = f.redirect_uri);
            var i = window.cordova.InAppBrowser.open("https://cloud.digitalocean.com/v1/oauth/authorize?client_id=" + d + "&redirect_uri=" + h + "&response_type=code&scope=read%20write", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            i.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(h)) {
                    var c = a.url.split("code=")[1];
                    b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                        method: "post",
                        url: "https://cloud.digitalocean.com/v1/oauth/token",
                        data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + h + "&grant_type=authorization_code&code=" + c
                    }).success(function(a) {
                        g.resolve(a);
                    }).error(function(a, b) {
                        g.reject("Problem authenticating");
                    })["finally"](function() {
                        setTimeout(function() {
                            i.close();
                        }, 10);
                    });
                }
            }), i.addEventListener("exit", function(a) {
                g.reject("The sign in flow was canceled");
            });
        } else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function dribble(a, b, c) {
    function d(d, e, f, g, h) {
        var i = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var j = "http://localhost/callback", k = "https://dribbble.com/oauth/authorize", l = "https://dribbble.com/oauth/token";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (j = g.redirect_uri), void 0 === h && (h = c.createNonce(5));
            var m = f.join(",").replace(/,/g, "+"), n = window.cordova.InAppBrowser.open(k + "?client_id=" + d + "&redirect_uri=" + j + "&scope=" + m + "&state=" + h, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            n.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(j)) {
                    var c = a.url.split("code=")[1], f = c.split("&")[0];
                    b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                        method: "post",
                        url: l,
                        data: "client_id=" + d + "&redirect_uri=" + j + "&client_secret=" + e + "&code=" + f
                    }).success(function(a) {
                        i.resolve(a);
                    }).error(function(a, b) {
                        i.reject("Problem authenticating ");
                    })["finally"](function() {
                        setTimeout(function() {
                            n.close();
                        }, 10);
                    });
                }
            }), n.addEventListener("exit", function(a) {
                i.reject("The sign in flow was canceled");
            });
        } else i.reject("Could not find InAppBrowser plugin"); else i.reject("Cannot authenticate via a web browser");
        return i.promise;
    }
    return {
        signin: d
    };
}

function dropbox(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://www.dropbox.com/1/oauth2/authorize?client_id=" + b + "&redirect_uri=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve({
                        access_token: d.access_token,
                        token_type: d.token_type,
                        uid: d.uid
                    }) : e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function envato(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://api.envato.com/authorization?client_id=" + b + "&redirect_uri=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve({
                        access_token: d.access_token,
                        expires_in: d.expires_in
                    }) : e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function facebook(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback";
            void 0 !== e && e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri);
            var h = "https://www.facebook.com/v2.0/dialog/oauth?client_id=" + b + "&redirect_uri=" + g + "&response_type=token&scope=" + d.join(",");
            void 0 !== e && e.hasOwnProperty("auth_type") && (h += "&auth_type=" + e.auth_type);
            var i = window.cordova.InAppBrowser.open(h, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            i.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    i.removeEventListener("exit", function(a) {}), i.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], e = 0; e < c.length; e++) d[c[e].split("=")[0]] = c[e].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? f.resolve({
                        access_token: d.access_token,
                        expires_in: d.expires_in
                    }) : 0 !== a.url.indexOf("error_code=100") ? f.reject("Facebook returned error_code=100: Invalid permissions") : f.reject("Problem authenticating");
                }
            }), i.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function familySearch(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = "http://localhost/callback";
            void 0 !== f && f.hasOwnProperty("redirect_uri") && (h = f.redirect_uri);
            var i = window.cordova.InAppBrowser.open("https://ident.familysearch.org/cis-web/oauth2/v3/authorization?client_id=" + d + "&redirect_uri=" + h + "&response_type=code&state=" + e, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            i.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(h)) {
                    var c = a.url.split("code=")[1];
                    b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                        method: "post",
                        url: "https://ident.familysearch.org/cis-web/oauth2/v3/token",
                        data: "client_id=" + d + "&redirect_uri=" + h + "&grant_type=authorization_code&code=" + c
                    }).success(function(a) {
                        g.resolve(a);
                    }).error(function(a, b) {
                        g.reject("Problem authenticating");
                    })["finally"](function() {
                        setTimeout(function() {
                            i.close();
                        }, 10);
                    });
                }
            }), i.addEventListener("exit", function(a) {
                g.reject("The sign in flow was canceled");
            });
        } else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function foursquare(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://foursquare.com/oauth2/authenticate?client_id=" + b + "&redirect_uri=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    if (void 0 !== d.access_token && null !== d.access_token) {
                        var i = {
                            access_token: d.access_token,
                            expires_in: d.expires_in
                        };
                        e.resolve(i);
                    } else e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function github(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://github.com/login/oauth/authorize?client_id=" + d + "&redirect_uri=" + i + "&scope=" + f.join(","), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(i) && (requestToken = a.url.split("code=")[1], b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                b.defaults.headers.post.accept = "application/json", b({
                    method: "post",
                    url: "https://github.com/login/oauth/access_token",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + i + "&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function google(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback";
            void 0 !== e && e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri);
            var h = window.cordova.InAppBrowser.open("https://accounts.google.com/o/oauth2/auth?client_id=" + b + "&redirect_uri=" + g + "&scope=" + d.join(" ") + "&approval_prompt=force&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    h.removeEventListener("exit", function(a) {}), h.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], e = 0; e < c.length; e++) d[c[e].split("=")[0]] = c[e].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? f.resolve({
                        access_token: d.access_token,
                        token_type: d.token_type,
                        expires_in: d.expires_in
                    }) : f.reject("Problem authenticating");
                }
            }), h.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function imgur(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://api.imgur.com/oauth2/authorize?client_id=" + b + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve({
                        access_token: d.access_token,
                        expires_in: d.expires_in,
                        account_username: d.account_username
                    }) : e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function instagram(a, b, c) {
    function d(b, d, e) {
        var f = a.defer(), g = {
            code: "?",
            token: "#"
        };
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = "http://localhost/callback", i = "token";
            void 0 !== e && (e.hasOwnProperty("redirect_uri") && (h = e.redirect_uri), e.hasOwnProperty("response_type") && (i = e.response_type));
            var j = "";
            d && d.length > 0 && (j = "&scope" + d.join("+"));
            var k = window.cordova.InAppBrowser.open("https://api.instagram.com/oauth/authorize/?client_id=" + b + "&redirect_uri=" + h + j + "&response_type=" + i, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            k.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(h)) {
                    k.removeEventListener("exit", function(a) {}), k.close();
                    var b = a.url.split(g[i])[1], d = c.parseResponseParameters(b);
                    d.access_token ? f.resolve({
                        access_token: d.access_token
                    }) : void 0 !== d.code && null !== d.code ? f.resolve({
                        code: d.code
                    }) : f.reject("Problem authenticating");
                }
            }), k.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function jawbone(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://jawbone.com/auth/oauth2/auth?client_id=" + d + "&redirect_uri=" + i + "&response_type=code&scope=" + f.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(i)) {
                    var c = a.url.split("code=")[1];
                    b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                        method: "post",
                        url: "https://jawbone.com/auth/oauth2/token",
                        data: "client_id=" + d + "&client_secret=" + e + "&grant_type=authorization_code&code=" + c
                    }).success(function(a) {
                        h.resolve(a);
                    }).error(function(a, b) {
                        h.reject("Problem authenticating");
                    })["finally"](function() {
                        setTimeout(function() {
                            j.close();
                        }, 10);
                    });
                }
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function cordovaOauth(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M) {
    return {
        azureAD: d.signin,
        adfs: e.signin,
        dropbox: f.signin,
        digitalOcean: g.signin,
        google: h.signin,
        github: i.signin,
        facebook: j.signin,
        linkedin: k.signin,
        instagram: l.signin,
        box: m.signin,
        reddit: n.signin,
        slack: o.signin,
        twitter: p.signin,
        meetup: q.signin,
        salesforce: r.signin,
        strava: s.signin,
        withings: t.signin,
        foursquare: u.signin,
        magento: v.signin,
        vkontakte: w.signin,
        odnoklassniki: x.signin,
        imgur: y.signin,
        spotify: z.signin,
        uber: A.signin,
        windowsLive: B.signin,
        yammer: C.signin,
        venmo: D.signin,
        stripe: E.signin,
        rally: F.signin,
        familySearch: G.signin,
        envato: H.signin,
        weibo: I.sigin,
        jawbone: J.signin,
        untappd: K.signin,
        dribble: L.signin,
        pocket: M.signin
    };
}

function linkedin(a, b, c) {
    function d(d, e, f, g, h) {
        var i = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var j = "http://localhost/callback";
            void 0 !== h && h.hasOwnProperty("redirect_uri") && (j = h.redirect_uri);
            var k = window.cordova.InAppBrowser.open("https://www.linkedin.com/uas/oauth2/authorization?client_id=" + d + "&redirect_uri=" + j + "&scope=" + f.join(" ") + "&response_type=code&state=" + g, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            k.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(j) && (requestToken = a.url.split("code=")[1].split("&")[0], 
                b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                    method: "post",
                    url: "https://www.linkedin.com/uas/oauth2/accessToken",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + j + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    i.resolve(a);
                }).error(function(a, b) {
                    i.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        k.close();
                    }, 10);
                }));
            }), k.addEventListener("exit", function(a) {
                i.reject("The sign in flow was canceled");
            });
        } else i.reject("Could not find InAppBrowser plugin"); else i.reject("Cannot authenticate via a web browser");
        return i.promise;
    }
    return {
        signin: d
    };
}

function magento(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) if ("undefined" != typeof jsSHA) {
            var h = {
                oauth_callback: "http://localhost/callback",
                oauth_consumer_key: e,
                oauth_nonce: c.createNonce(5),
                oauth_signature_method: "HMAC-SHA1",
                oauth_timestamp: Math.round(new Date().getTime() / 1e3),
                oauth_version: "1.0"
            }, i = c.createSignature("POST", d + "/oauth/initiate", h, {
                oauth_callback: "http://localhost/callback"
            }, f);
            b.defaults.headers.post.Authorization = i.authorization_header, b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
            b({
                method: "post",
                url: d + "/oauth/initiate",
                data: "oauth_callback=http://localhost/callback"
            }).success(function(a) {
                for (var e = a.split("&"), i = {}, j = 0; j < e.length; j++) i[e[j].split("=")[0]] = e[j].split("=")[1];
                i.hasOwnProperty("oauth_token") === !1 && g.reject("Oauth request token was not received");
                var k = i.oauth_token_secret, l = window.cordova.InAppBrowser.open(d + "/oauth/authorize?oauth_token=" + i.oauth_token, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                l.addEventListener("loadstart", function(a) {
                    if (0 === a.url.indexOf("http://localhost/callback")) {
                        for (var e = a.url.split("?")[1], i = e.split("&"), j = {}, m = 0; m < i.length; m++) j[i[m].split("=")[0]] = i[m].split("=")[1];
                        j.hasOwnProperty("oauth_verifier") === !1 && g.reject("Browser authentication failed to complete.  No oauth_verifier was returned"), 
                        delete h.oauth_signature, delete h.oauth_callback, h.oauth_token = j.oauth_token, 
                        h.oauth_nonce = c.createNonce(5), h.oauth_verifier = j.oauth_verifier;
                        var n = c.createSignature("POST", d + "/oauth/token", h, {}, f, k);
                        b.defaults.headers.post.Authorization = n.authorization_header, b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                        b({
                            method: "post",
                            url: d + "/oauth/token"
                        }).success(function(a) {
                            for (var b = a.split("&"), c = {}, d = 0; d < b.length; d++) c[b[d].split("=")[0]] = b[d].split("=")[1];
                            c.hasOwnProperty("oauth_token_secret") === !1 && g.reject("Oauth access token was not received"), 
                            g.resolve(c);
                        }).error(function(a) {
                            g.reject(a);
                        })["finally"](function() {
                            setTimeout(function() {
                                l.close();
                            }, 10);
                        });
                    }
                }), l.addEventListener("exit", function(a) {
                    g.reject("The sign in flow was canceled");
                });
            }).error(function(a) {
                g.reject(a);
            });
        } else g.reject("Missing jsSHA JavaScript library"); else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function meetup(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://secure.meetup.com/oauth2/authorize/?client_id=" + b + "&redirect_uri=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = {}, h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve(d) : e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function odnoklassniki(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = window.cordova.InAppBrowser.open("http://www.odnoklassniki.ru/oauth/authorize?client_id=" + b + "&scope=" + d.join(",") + "&response_type=token&redirect_uri=http://localhost/callback&layout=m", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            f.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf("http://localhost/callback")) {
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], g = 0; g < c.length; g++) d[c[g].split("=")[0]] = c[g].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve({
                        access_token: d.access_token,
                        session_secret_key: d.session_secret_key
                    }) : e.reject("Problem authenticating"), setTimeout(function() {
                        f.close();
                    }, 10);
                }
            }), f.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function pocket(a, b, c) {
    function d(d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback";
            void 0 !== e && e.hasOwnProperty("redirect_url") && (g = e.redirect_url);
            var h = "consumer_key=" + d + "&redirect_uri=" + encodeURIComponent(g);
            console.log(h), b({
                method: "post",
                url: "https://getpocket.com/v3/oauth/request",
                headers: {
                    "X-Accept": "application/x-www-form-urlencoded",
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                data: h
            }).success(function(a) {
                var c = a.split("code=")[1], e = window.cordova.InAppBrowser.open("https://getpocket.com/auth/authorize?request_token=" + c + "&redirect_uri=" + g, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                e.addEventListener("loadstart", function(h) {
                    0 === h.url.indexOf(g) && (e.removeEventListener("exit", function(a) {}), a = "consumer_key=" + d + "&code=" + c, 
                    b({
                        method: "post",
                        url: "https://getpocket.com/v3/oauth/authorize",
                        headers: {
                            "X-Accept": "application/x-www-form-urlencoded",
                            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                        },
                        data: a
                    }).success(function(a) {
                        f.resolve(a);
                    }).error(function(a) {
                        f.reject(a);
                    })["finally"](function() {
                        setTimeout(function() {
                            e.close();
                        }, 10);
                    }));
                }), e.addEventListener("exit", function(a) {
                    f.reject("The sign in flow was canceled");
                });
            }).error(function(a) {
                f.reject(a);
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function rally(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://rally1.rallydev.com/login/oauth2/auth?client_id=" + d + "&redirect_uri=" + i + "&scope=" + f + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf("http://localhost/callback") && (requestToken = a.url.split("code=")[1], 
                b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                    method: "post",
                    url: "https://rally1.rallydev.com/login/oauth2/auth",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + i + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function reddit(a, b, c) {
    function d(d, e, f, g, h) {
        var i = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var j = "http://localhost/callback";
            void 0 !== h && h.hasOwnProperty("redirect_uri") && (j = h.redirect_uri);
            var k = window.cordova.InAppBrowser.open("https://ssl.reddit.com/api/v1/authorize" + (g ? ".compact" : "") + "?client_id=" + d + "&redirect_uri=" + j + "&duration=permanent&state=ngcordovaoauth&scope=" + f.join(",") + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            k.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(j) && (requestToken = a.url.split("code=")[1], b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                b.defaults.headers.post.Authorization = "Basic " + btoa(d + ":" + e), b({
                    method: "post",
                    url: "https://ssl.reddit.com/api/v1/access_token",
                    data: "redirect_uri=" + j + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    i.resolve(a);
                }).error(function(a, b) {
                    i.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        k.close();
                    }, 10);
                }));
            }), k.addEventListener("exit", function(a) {
                i.reject("The sign in flow was canceled");
            });
        } else i.reject("Could not find InAppBrowser plugin"); else i.reject("Cannot authenticate via a web browser");
        return i.promise;
    }
    return {
        signin: d
    };
}

function salesforce(a, b, c) {
    function d(b, d) {
        var e = "http://localhost/callback", f = function(a, b, c) {
            return a + "services/oauth2/authorize?display=touch&response_type=token&client_id=" + escape(b) + "&redirect_uri=" + escape(c);
        }, g = function(a, b) {
            return a.substr(0, b.length) === b;
        }, h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = window.cordova.InAppBrowser.open(f(b, d, e), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            i.addEventListener("loadstart", function(a) {
                if (g(a.url, e)) {
                    var b = {}, c = a.url.split("#")[1];
                    if (c) {
                        var d = c.split("&");
                        for (var f in d) {
                            var j = d[f].split("=");
                            b[j[0]] = unescape(j[1]);
                        }
                    }
                    "undefined" == typeof b || "undefined" == typeof b.access_token ? h.reject("Problem authenticating") : h.resolve(b), 
                    setTimeout(function() {
                        i.close();
                    }, 10);
                }
            }), i.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function slack(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://slack.com/oauth/authorize?client_id=" + d + "&redirect_uri=" + i + "&state=ngcordovaoauth&scope=" + f.join(","), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(i) && (requestToken = a.url.split("code=")[1], console.log("Request token is " + requestToken), 
                b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                    method: "post",
                    url: "https://slack.com/api/oauth.access",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + i + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function spotify(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback", h = "token", i = "", j = "";
            void 0 !== e && (e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri), e.hasOwnProperty("response_type") && (h = e.response_type), 
            e.hasOwnProperty("state") && (i = "&state=" + e.state), e.hasOwnProperty("show_dialog") && (j = "&show_dialog=" + e.show_dialog));
            var k = window.cordova.InAppBrowser.open("https://accounts.spotify.com/authorize?client_id=" + b + "&redirect_uri=" + g + "&response_type=" + h + i + "&scope=" + d.join(" ") + j, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            k.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    k.removeEventListener("exit", function(a) {}), k.close();
                    for (var b = "code" === h ? "?" : "#", c = a.url.split(b)[1], d = c.split("&"), e = [], i = 0; i < d.length; i++) e[d[i].split("=")[0]] = d[i].split("=")[1];
                    "token" === h && void 0 !== e.access_token && null !== e.access_token ? f.resolve({
                        access_token: e.access_token,
                        expires_in: e.expires_in,
                        account_username: e.account_username
                    }) : "code" === h && void 0 !== e.code && null !== e.code ? f.resolve({
                        code: e.code,
                        state: e.state
                    }) : f.reject("Problem authenticating");
                }
            }), k.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function strava(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://www.strava.com/oauth/authorize?client_id=" + d + "&redirect_uri=" + i + "&scope=" + f.join(",") + "&response_type=code&approval_prompt=force", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(i) && (requestToken = a.url.split("code=")[1], b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                b({
                    method: "post",
                    url: "https://www.strava.com/oauth/token",
                    data: "client_id=" + d + "&client_secret=" + e + "&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function stripe(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = window.cordova.InAppBrowser.open("https://connect.stripe.com/oauth/authorize?client_id=" + d + "&redirect_uri=" + i + "&scope=" + f + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            j.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf("http://localhost/callback") && (requestToken = a.url.split("code=")[1], 
                b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", b({
                    method: "post",
                    url: "https://connect.stripe.com/oauth/token",
                    data: "client_id=" + d + "&client_secret=" + e + "&redirect_uri=" + i + "&grant_type=authorization_code&code=" + requestToken
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        j.close();
                    }, 10);
                }));
            }), j.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function twitter(a, b, c) {
    function d(d, e, f) {
        var g = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var h = "http://localhost/callback";
            if (void 0 !== f && f.hasOwnProperty("redirect_uri") && (h = f.redirect_uri), "undefined" != typeof jsSHA) {
                var i = {
                    oauth_consumer_key: d,
                    oauth_nonce: c.createNonce(10),
                    oauth_signature_method: "HMAC-SHA1",
                    oauth_timestamp: Math.round(new Date().getTime() / 1e3),
                    oauth_version: "1.0"
                }, j = c.createSignature("POST", "https://api.twitter.com/oauth/request_token", i, {
                    oauth_callback: h
                }, e);
                b({
                    method: "post",
                    url: "https://api.twitter.com/oauth/request_token",
                    headers: {
                        Authorization: j.authorization_header,
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    data: "oauth_callback=" + encodeURIComponent(h)
                }).success(function(a) {
                    for (var d = a.split("&"), f = {}, j = 0; j < d.length; j++) f[d[j].split("=")[0]] = d[j].split("=")[1];
                    f.hasOwnProperty("oauth_token") === !1 && g.reject("Oauth request token was not received");
                    var k = window.cordova.InAppBrowser.open("https://api.twitter.com/oauth/authenticate?oauth_token=" + f.oauth_token, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                    k.addEventListener("loadstart", function(a) {
                        if (0 === a.url.indexOf(h)) {
                            for (var d = a.url.split("?")[1], f = d.split("&"), j = {}, l = 0; l < f.length; l++) j[f[l].split("=")[0]] = f[l].split("=")[1];
                            j.hasOwnProperty("oauth_verifier") === !1 && g.reject("Browser authentication failed to complete.  No oauth_verifier was returned"), 
                            delete i.oauth_signature, i.oauth_token = j.oauth_token;
                            var m = c.createSignature("POST", "https://api.twitter.com/oauth/access_token", i, {
                                oauth_verifier: j.oauth_verifier
                            }, e);
                            b({
                                method: "post",
                                url: "https://api.twitter.com/oauth/access_token",
                                headers: {
                                    Authorization: m.authorization_header
                                },
                                params: {
                                    oauth_verifier: j.oauth_verifier
                                }
                            }).success(function(a) {
                                for (var b = a.split("&"), c = {}, d = 0; d < b.length; d++) c[b[d].split("=")[0]] = b[d].split("=")[1];
                                c.hasOwnProperty("oauth_token_secret") === !1 && g.reject("Oauth access token was not received"), 
                                g.resolve(c);
                            }).error(function(a) {
                                g.reject(a);
                            })["finally"](function() {
                                setTimeout(function() {
                                    k.close();
                                }, 10);
                            });
                        }
                    }), k.addEventListener("exit", function(a) {
                        g.reject("The sign in flow was canceled");
                    });
                }).error(function(a) {
                    g.reject(a);
                });
            } else g.reject("Missing jsSHA JavaScript library");
        } else g.reject("Could not find InAppBrowser plugin"); else g.reject("Cannot authenticate via a web browser");
        return g.promise;
    }
    return {
        signin: d
    };
}

function uber(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback";
            void 0 !== e && e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri);
            var h = window.cordova.InAppBrowser.open("https://login.uber.com/oauth/authorize?client_id=" + b + "&redirect_uri=" + g + "&response_type=token&scope=" + d.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    h.removeEventListener("exit", function(a) {}), h.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], e = 0; e < c.length; e++) d[c[e].split("=")[0]] = c[e].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? f.resolve({
                        access_token: d.access_token,
                        token_type: d.token_type,
                        expires_in: d.expires_in,
                        scope: d.scope
                    }) : f.reject("Problem authenticating");
                }
            }), h.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function untappd(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_url") && (f = d.redirect_url);
            var g = window.cordova.InAppBrowser.open("https://untappd.com/oauth/authenticate/?client_id=" + b + "&redirect_url=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    if (void 0 !== d.access_token && null !== d.access_token) {
                        var i = {
                            access_token: d.access_token
                        };
                        e.resolve(i);
                    } else e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function venmo(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "http://localhost/callback";
            void 0 !== e && e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri);
            var h = window.cordova.InAppBrowser.open("https://api.venmo.com/v1/oauth/authorize?client_id=" + b + "&redirect_uri=" + g + "&response_type=token&scope=" + d.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    h.removeEventListener("exit", function(a) {}), h.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], e = 0; e < c.length; e++) d[c[e].split("=")[0]] = c[e].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? f.resolve({
                        access_token: d.access_token,
                        expires_in: d.expires_in
                    }) : f.reject("Problem authenticating");
                }
            }), h.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function vkontakte(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = window.cordova.InAppBrowser.open("https://oauth.vk.com/authorize?client_id=" + b + "&redirect_uri=http://oauth.vk.com/blank.html&response_type=token&scope=" + d.join(",") + "&display=touch&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            f.addEventListener("loadstart", function(a) {
                var b = a.url.split("#");
                if ("https://oauth.vk.com/blank.html" == b[0] || "http://oauth.vk.com/blank.html" == b[0]) {
                    f.removeEventListener("exit", function(a) {}), f.close();
                    for (var c = a.url.split("#")[1], d = c.split("&"), g = [], h = 0; h < d.length; h++) g[d[h].split("=")[0]] = d[h].split("=")[1];
                    if (void 0 !== g.access_token && null !== g.access_token) {
                        var i = {
                            access_token: g.access_token,
                            expires_in: g.expires_in
                        };
                        void 0 !== g.email && null !== g.email && (i.email = g.email), e.resolve(i);
                    } else e.reject("Problem authenticating");
                }
            }), f.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function weibo(a, b, c) {
    function d(d, e, f, g) {
        var h = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var i = "http://localhost/callback";
            void 0 !== g && g.hasOwnProperty("redirect_uri") && (i = g.redirect_uri);
            var j = "https://open.weibo.cn/oauth2/authorize?display=mobile&client_id=" + d + "&redirect_uri=" + i + "&scope=" + f.join(",");
            void 0 !== g && (g.hasOwnProperty("language") && (j += "&language=" + g.language), 
            g.hasOwnProperty("forcelogin") && (j += "&forcelogin=" + g.forcelogin));
            var k = window.cordova.InAppBrowser.open(j, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            k.addEventListener("loadstart", function(a) {
                0 === a.url.indexOf(i) && (requestToken = a.url.split("code=")[1], b.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                b({
                    method: "post",
                    url: "https://api.weibo.com/oauth2/access_token",
                    data: "client_id=" + d + "&client_secret=" + e + "&grant_type=authorization_code&code=" + requestToken + "&redirect_uri=" + i
                }).success(function(a) {
                    h.resolve(a);
                }).error(function(a, b) {
                    h.reject("Problem authenticating");
                })["finally"](function() {
                    setTimeout(function() {
                        k.close();
                    }, 10);
                }));
            }), k.addEventListener("exit", function(a) {
                h.reject("The sign in flow was canceled");
            });
        } else h.reject("Could not find InAppBrowser plugin"); else h.reject("Cannot authenticate via a web browser");
        return h.promise;
    }
    return {
        signin: d
    };
}

function windowslive(a, b, c) {
    function d(b, d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var g = "https://login.live.com/oauth20_desktop.srf";
            void 0 !== e && e.hasOwnProperty("redirect_uri") && (g = e.redirect_uri);
            var h = window.cordova.InAppBrowser.open("https://login.live.com/oauth20_authorize.srf?client_id=" + b + "&scope=" + d.join(",") + "&response_type=token&display=touch&redirect_uri=" + g, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            h.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(g)) {
                    h.removeEventListener("exit", function(a) {}), h.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], e = 0; e < c.length; e++) d[c[e].split("=")[0]] = c[e].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? f.resolve({
                        access_token: d.access_token,
                        expires_in: d.expires_in
                    }) : f.reject("Problem authenticating");
                }
            }), h.addEventListener("exit", function(a) {
                f.reject("The sign in flow was canceled");
            });
        } else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function withings(a, b, c) {
    function d(d, e) {
        var f = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) if ("undefined" != typeof jsSHA) {
            var g = c.generateOauthParametersInstance(d);
            g.oauth_callback = "http://localhost/callback";
            var h = "https://oauth.withings.com/account/request_token", i = c.createSignature("GET", h, {}, g, e);
            g.oauth_signature = i.signature;
            var j = c.generateUrlParameters(g);
            b({
                method: "get",
                url: h + "?" + j
            }).success(function(a) {
                var g = c.parseResponseParameters(a);
                g.oauth_token || f.reject("Oauth request token was not received");
                var h = c.generateOauthParametersInstance(d);
                h.oauth_token = g.oauth_token;
                var i = g.oauth_token_secret, j = "https://oauth.withings.com/account/authorize", k = c.createSignature("GET", j, {}, h, e);
                h.oauth_signature = k.signature;
                var l = c.generateUrlParameters(h), m = window.cordova.InAppBrowser.open(j + "?" + l, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                m.addEventListener("loadstart", function(a) {
                    if (0 === a.url.indexOf("http://localhost/callback")) {
                        var h = a.url.split("?")[1];
                        g = c.parseResponseParameters(h), g.oauth_verifier || f.reject("Browser authentication failed to complete.  No oauth_verifier was returned");
                        var j = c.generateOauthParametersInstance(d);
                        j.oauth_token = g.oauth_token;
                        var k = "https://oauth.withings.com/account/access_token", l = c.createSignature("GET", k, {}, j, e, i);
                        j.oauth_signature = l.signature;
                        var n = c.generateUrlParameters(j);
                        b({
                            method: "get",
                            url: k + "?" + n
                        }).success(function(a) {
                            var b = c.parseResponseParameters(a);
                            b.oauth_token_secret || f.reject("Oauth access token was not received"), f.resolve(b);
                        }).error(function(a) {
                            f.reject(a);
                        })["finally"](function() {
                            setTimeout(function() {
                                m.close();
                            }, 10);
                        });
                    }
                }), m.addEventListener("exit", function(a) {
                    f.reject("The sign in flow was canceled");
                });
            }).error(function(a) {
                f.reject(a);
            });
        } else f.reject("Missing jsSHA JavaScript library"); else f.reject("Could not find InAppBrowser plugin"); else f.reject("Cannot authenticate via a web browser");
        return f.promise;
    }
    return {
        signin: d
    };
}

function yammer(a, b, c) {
    function d(b, d) {
        var e = a.defer();
        if (window.cordova) if (c.isInAppBrowserInstalled()) {
            var f = "http://localhost/callback";
            void 0 !== d && d.hasOwnProperty("redirect_uri") && (f = d.redirect_uri);
            var g = window.cordova.InAppBrowser.open("https://www.yammer.com/dialog/oauth?client_id=" + b + "&redirect_uri=" + f + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
            g.addEventListener("loadstart", function(a) {
                if (0 === a.url.indexOf(f)) {
                    g.removeEventListener("exit", function(a) {}), g.close();
                    for (var b = a.url.split("#")[1], c = b.split("&"), d = [], h = 0; h < c.length; h++) d[c[h].split("=")[0]] = c[h].split("=")[1];
                    void 0 !== d.access_token && null !== d.access_token ? e.resolve({
                        access_token: d.access_token
                    }) : e.reject("Problem authenticating");
                }
            }), g.addEventListener("exit", function(a) {
                e.reject("The sign in flow was canceled");
            });
        } else e.reject("Could not find InAppBrowser plugin"); else e.reject("Cannot authenticate via a web browser");
        return e.promise;
    }
    return {
        signin: d
    };
}

function cordovaOauthUtility(a) {
    function b() {
        var a = cordova.require("cordova/plugin_list"), b = [ "cordova-plugin-inappbrowser", "org.apache.cordova.inappbrowser" ];
        if (0 === Object.keys(a.metadata).length) {
            var c = a.map(function(a) {
                return a.pluginId;
            });
            return b.some(function(a) {
                return -1 != c.indexOf(a) ? !0 : !1;
            });
        }
        return b.some(function(b) {
            return a.metadata.hasOwnProperty(b);
        });
    }
    function c(a, b, c, d, e, f) {
        if ("undefined" != typeof jsSHA) {
            for (var g = angular.copy(c), h = Object.keys(d), i = 0; i < h.length; i++) g[h[i]] = encodeURIComponent(d[h[i]]);
            var j = a + "&" + encodeURIComponent(b) + "&", k = Object.keys(g).sort();
            for (i = 0; i < k.length; i++) j += i == k.length - 1 ? encodeURIComponent(k[i] + "=" + g[k[i]]) : encodeURIComponent(k[i] + "=" + g[k[i]] + "&");
            var l = new jsSHA(j, "TEXT"), m = "";
            f && (m = encodeURIComponent(f)), c.oauth_signature = encodeURIComponent(l.getHMAC(encodeURIComponent(e) + "&" + m, "TEXT", "SHA-1", "B64"));
            var n = Object.keys(c), o = "OAuth ";
            for (i = 0; i < n.length; i++) o += i == n.length - 1 ? n[i] + '="' + c[n[i]] + '"' : n[i] + '="' + c[n[i]] + '",';
            return {
                signature_base_string: j,
                authorization_header: o,
                signature: c.oauth_signature
            };
        }
        return "Missing jsSHA JavaScript library";
    }
    function d(a) {
        for (var b = "", c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", d = 0; a > d; d++) b += c.charAt(Math.floor(Math.random() * c.length));
        return b;
    }
    function e(a) {
        var b = Object.keys(a);
        b.sort();
        for (var c = "", d = "", e = 0; e < b.length; e++) c += d + b[e] + "=" + a[b[e]], 
        d = "&";
        return c;
    }
    function f(a) {
        if (a.split) {
            for (var b = a.split("&"), c = {}, d = 0; d < b.length; d++) c[b[d].split("=")[0]] = b[d].split("=")[1];
            return c;
        }
        return {};
    }
    function g(a) {
        var b = new jsSHA(Math.round(new Date().getTime() / 1e3), "TEXT"), c = {
            oauth_consumer_key: a,
            oauth_nonce: b.getHash("SHA-1", "HEX"),
            oauth_signature_method: "HMAC-SHA1",
            oauth_timestamp: Math.round(new Date().getTime() / 1e3),
            oauth_version: "1.0"
        };
        return c;
    }
    return {
        isInAppBrowserInstalled: b,
        createSignature: c,
        createNonce: d,
        generateUrlParameters: e,
        parseResponseParameters: f,
        generateOauthParametersInstance: g
    };
}

angular.module("oauth.adfs", [ "oauth.utils" ]).factory("$adfs", adfs), adfs.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], 
angular.module("oauth.azuread", [ "oauth.utils" ]).factory("$azureAD", azureAD), 
azureAD.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.box", [ "oauth.utils" ]).factory("$box", box), 
box.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.digitalOcean", [ "oauth.utils" ]).factory("$digitalOcean", digitalOcean), 
digitalOcean.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.dribble", [ "oauth.utils" ]).factory("$dribble", dribble), 
dribble.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.dropbox", [ "oauth.utils" ]).factory("$dropbox", dropbox), 
dropbox.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.envato", [ "oauth.utils" ]).factory("$envato", envato), 
envato.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.facebook", [ "oauth.utils" ]).factory("$facebook", facebook), 
facebook.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.familySearch", [ "oauth.utils" ]).factory("$familySearch", familySearch), 
familySearch.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.foursquare", [ "oauth.utils" ]).factory("$foursquare", foursquare), 
foursquare.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.github", [ "oauth.utils" ]).factory("$github", github), 
github.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.google", [ "oauth.utils" ]).factory("$google", google), 
google.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.imgur", [ "oauth.utils" ]).factory("$imgur", imgur), 
imgur.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.instagram", [ "oauth.utils" ]).factory("$instagram", instagram), 
instagram.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.jawbone", [ "oauth.utils" ]).factory("$jawbone", jawbone), 
jawbone.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.providers", [ "oauth.utils", "oauth.azuread", "oauth.adfs", "oauth.dropbox", "oauth.digitalOcean", "oauth.google", "oauth.github", "oauth.facebook", "oauth.linkedin", "oauth.instagram", "oauth.box", "oauth.reddit", "oauth.slack", "oauth.twitter", "oauth.meetup", "oauth.salesforce", "oauth.strava", "oauth.withings", "oauth.foursquare", "oauth.magento", "oauth.vkontakte", "oauth.odnoklassniki", "oauth.imgur", "oauth.spotify", "oauth.uber", "oauth.windowslive", "oauth.yammer", "oauth.venmo", "oauth.stripe", "oauth.rally", "oauth.familySearch", "oauth.envato", "oauth.weibo", "oauth.jawbone", "oauth.untappd", "oauth.dribble", "oauth.pocket" ]).factory("$cordovaOauth", cordovaOauth), 
cordovaOauth.$inject = [ "$q", "$http", "$cordovaOauthUtility", "$azureAD", "$adfs", "$dropbox", "$digitalOcean", "$google", "$github", "$facebook", "$linkedin", "$instagram", "$box", "$reddit", "$slack", "$twitter", "$meetup", "$salesforce", "$strava", "$withings", "$foursquare", "$magento", "$vkontakte", "$odnoklassniki", "$imgur", "$spotify", "$uber", "$windowslive", "$yammer", "$venmo", "$stripe", "$rally", "$familySearch", "$envato", "$weibo", "$jawbone", "$untappd", "$dribble", "$pocket" ], 
angular.module("oauth.linkedin", [ "oauth.utils" ]).factory("$linkedin", linkedin), 
linkedin.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.magento", [ "oauth.utils" ]).factory("$magento", magento), 
magento.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.meetup", [ "oauth.utils" ]).factory("$meetup", meetup), 
meetup.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.odnoklassniki", [ "oauth.utils" ]).factory("$odnoklassniki", odnoklassniki), 
odnoklassniki.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.pocket", [ "oauth.utils" ]).factory("$pocket", pocket), 
pocket.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.rally", [ "oauth.utils" ]).factory("$rally", rally), 
rally.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.reddit", [ "oauth.utils" ]).factory("$reddit", reddit), 
reddit.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.salesforce", [ "oauth.utils" ]).factory("$salesforce", salesforce), 
salesforce.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.slack", [ "oauth.utils" ]).factory("$slack", slack), 
slack.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.spotify", [ "oauth.utils" ]).factory("$spotify", spotify), 
spotify.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.strava", [ "oauth.utils" ]).factory("$strava", strava), 
strava.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.stripe", [ "oauth.utils" ]).factory("$stripe", stripe), 
stripe.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.twitter", [ "oauth.utils" ]).factory("$twitter", twitter), 
twitter.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.uber", [ "oauth.utils" ]).factory("$uber", uber), 
uber.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.untappd", [ "oauth.utils" ]).factory("$untappd", untappd), 
untappd.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.venmo", [ "oauth.utils" ]).factory("$venmo", venmo), 
venmo.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.vkontakte", [ "oauth.utils" ]).factory("$vkontakte", vkontakte), 
vkontakte.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.weibo", [ "oauth.utils" ]).factory("$weibo", weibo), 
weibo.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.windowslive", [ "oauth.utils" ]).factory("$windowslive", windowslive), 
windowslive.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.withings", [ "oauth.utils" ]).factory("$withings", withings), 
withings.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("oauth.yammer", [ "oauth.utils" ]).factory("$yammer", yammer), 
yammer.$inject = [ "$q", "$http", "$cordovaOauthUtility" ], angular.module("ngCordovaOauth", [ "oauth.providers", "oauth.utils" ]), 
angular.module("oauth.utils", []).factory("$cordovaOauthUtility", cordovaOauthUtility), 
cordovaOauthUtility.$inject = [ "$q" ];

!function() {
    angular.module("ngCordova", [ "ngCordova.plugins" ]), angular.module("ngCordova.plugins.actionSheet", []).factory("$cordovaActionSheet", [ "$q", "$window", function(e, n) {
        return {
            show: function(t) {
                var r = e.defer();
                return n.plugins.actionsheet.show(t, function(e) {
                    r.resolve(e);
                }), r.promise;
            },
            hide: function() {
                return n.plugins.actionsheet.hide();
            }
        };
    } ]), angular.module("ngCordova.plugins.adMob", []).factory("$cordovaAdMob", [ "$q", "$window", function(e, n) {
        return {
            createBannerView: function(t) {
                var r = e.defer();
                return n.plugins.AdMob.createBannerView(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createInterstitialView: function(t) {
                var r = e.defer();
                return n.plugins.AdMob.createInterstitialView(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            requestAd: function(t) {
                var r = e.defer();
                return n.plugins.AdMob.requestAd(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showAd: function(t) {
                var r = e.defer();
                return n.plugins.AdMob.showAd(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            requestInterstitialAd: function(t) {
                var r = e.defer();
                return n.plugins.AdMob.requestInterstitialAd(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.appAvailability", []).factory("$cordovaAppAvailability", [ "$q", function(e) {
        return {
            check: function(n) {
                var t = e.defer();
                return appAvailability.check(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.appRate", []).provider("$cordovaAppRate", [ function() {
        this.setPreferences = function(e) {
            e && angular.isObject(e) && (AppRate.preferences.useLanguage = e.language || null, 
            AppRate.preferences.displayAppName = e.appName || "", AppRate.preferences.promptAgainForEachNewVersion = e.promptForNewVersion || !0, 
            AppRate.preferences.openStoreInApp = e.openStoreInApp || !1, AppRate.preferences.usesUntilPrompt = e.usesUntilPrompt || 3, 
            AppRate.preferences.useCustomRateDialog = e.useCustomRateDialog || !1, AppRate.preferences.storeAppURL.ios = e.iosURL || null, 
            AppRate.preferences.storeAppURL.android = e.androidURL || null, AppRate.preferences.storeAppURL.blackberry = e.blackberryURL || null, 
            AppRate.preferences.storeAppURL.windows8 = e.windowsURL || null);
        }, this.setCustomLocale = function(e) {
            var n = {
                title: "Rate %@",
                message: "If you enjoy using %@, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",
                cancelButtonLabel: "No, Thanks",
                laterButtonLabel: "Remind Me Later",
                rateButtonLabel: "Rate It Now"
            };
            n = angular.extend(n, e), AppRate.preferences.customLocale = n;
        }, this.$get = [ "$q", function(e) {
            return {
                promptForRating: function(n) {
                    var t = e.defer(), r = AppRate.promptForRating(n);
                    return t.resolve(r), t.promise;
                },
                navigateToAppStore: function() {
                    var n = e.defer(), t = AppRate.navigateToAppStore();
                    return n.resolve(t), n.promise;
                },
                onButtonClicked: function(e) {
                    AppRate.onButtonClicked = function(n) {
                        e.call(this, n);
                    };
                },
                onRateDialogShow: function(e) {
                    AppRate.onRateDialogShow = e();
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.appVersion", []).factory("$cordovaAppVersion", [ "$q", function(e) {
        return {
            getVersionNumber: function() {
                var n = e.defer();
                return cordova.getAppVersion.getVersionNumber(function(e) {
                    n.resolve(e);
                }), n.promise;
            },
            getVersionCode: function() {
                var n = e.defer();
                return cordova.getAppVersion.getVersionCode(function(e) {
                    n.resolve(e);
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.backgroundGeolocation", []).factory("$cordovaBackgroundGeolocation", [ "$q", "$window", function(e, n) {
        return {
            init: function() {
                n.navigator.geolocation.getCurrentPosition(function(e) {
                    return e;
                });
            },
            configure: function(t) {
                this.init();
                var r = e.defer();
                return n.plugins.backgroundGeoLocation.configure(function(e) {
                    r.notify(e), n.plugins.backgroundGeoLocation.finish();
                }, function(e) {
                    r.reject(e);
                }, t), this.start(), r.promise;
            },
            start: function() {
                var t = e.defer();
                return n.plugins.backgroundGeoLocation.start(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            stop: function() {
                var t = e.defer();
                return n.plugins.backgroundGeoLocation.stop(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.badge", []).factory("$cordovaBadge", [ "$q", function(e) {
        return {
            hasPermission: function() {
                var n = e.defer();
                return cordova.plugins.notification.badge.hasPermission(function(e) {
                    e ? n.resolve(!0) : n.reject("You do not have permission");
                }), n.promise;
            },
            promptForPermission: function() {
                return cordova.plugins.notification.badge.promptForPermission();
            },
            set: function(n, t, r) {
                var o = e.defer();
                return cordova.plugins.notification.badge.hasPermission(function(e) {
                    e ? o.resolve(cordova.plugins.notification.badge.set(n, t, r)) : o.reject("You do not have permission to set Badge");
                }), o.promise;
            },
            get: function() {
                var n = e.defer();
                return cordova.plugins.notification.badge.hasPermission(function(e) {
                    e ? cordova.plugins.notification.badge.get(function(e) {
                        n.resolve(e);
                    }) : n.reject("You do not have permission to get Badge");
                }), n.promise;
            },
            clear: function(n, t) {
                var r = e.defer();
                return cordova.plugins.notification.badge.hasPermission(function(e) {
                    e ? r.resolve(cordova.plugins.notification.badge.clear(n, t)) : r.reject("You do not have permission to clear Badge");
                }), r.promise;
            },
            increase: function(n, t, r) {
                var o = e.defer();
                return this.hasPermission().then(function() {
                    o.resolve(cordova.plugins.notification.badge.increase(n, t, r));
                }, function() {
                    o.reject("You do not have permission to increase Badge");
                }), o.promise;
            },
            decrease: function(n, t, r) {
                var o = e.defer();
                return this.hasPermission().then(function() {
                    o.resolve(cordova.plugins.notification.badge.decrease(n, t, r));
                }, function() {
                    o.reject("You do not have permission to decrease Badge");
                }), o.promise;
            },
            configure: function(e) {
                return cordova.plugins.notification.badge.configure(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.barcodeScanner", []).factory("$cordovaBarcodeScanner", [ "$q", function(e) {
        return {
            scan: function(n) {
                var t = e.defer();
                return cordova.plugins.barcodeScanner.scan(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            encode: function(n, t) {
                var r = e.defer();
                return n = n || "TEXT_TYPE", cordova.plugins.barcodeScanner.encode(n, t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.batteryStatus", []).factory("$cordovaBatteryStatus", [ "$rootScope", "$window", "$timeout", function(e, n, t) {
        var r = function(n) {
            t(function() {
                e.$broadcast("$cordovaBatteryStatus:status", n);
            });
        }, o = function(n) {
            t(function() {
                e.$broadcast("$cordovaBatteryStatus:critical", n);
            });
        }, i = function(n) {
            t(function() {
                e.$broadcast("$cordovaBatteryStatus:low", n);
            });
        };
        return document.addEventListener("deviceready", function() {
            navigator.battery && (n.addEventListener("batterystatus", r, !1), n.addEventListener("batterycritical", o, !1), 
            n.addEventListener("batterylow", i, !1));
        }, !1), !0;
    } ]).run([ "$injector", function(e) {
        e.get("$cordovaBatteryStatus");
    } ]), angular.module("ngCordova.plugins.beacon", []).factory("$cordovaBeacon", [ "$window", "$rootScope", "$timeout", "$q", function(e, n, t, r) {
        var o = null, i = null, a = null, c = null, s = null, u = null, l = null, d = null;
        return document.addEventListener("deviceready", function() {
            if (e.cordova && e.cordova.plugins && e.cordova.plugins.locationManager) {
                var r = new e.cordova.plugins.locationManager.Delegate();
                r.didDetermineStateForRegion = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didDetermineStateForRegion", e);
                    }), o && o(e);
                }, r.didStartMonitoringForRegion = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didStartMonitoringForRegion", e);
                    }), i && i(e);
                }, r.didExitRegion = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didExitRegion", e);
                    }), a && a(e);
                }, r.didEnterRegion = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didEnterRegion", e);
                    }), c && c(e);
                }, r.didRangeBeaconsInRegion = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didRangeBeaconsInRegion", e);
                    }), s && s(e);
                }, r.peripheralManagerDidStartAdvertising = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:peripheralManagerDidStartAdvertising", e);
                    }), u && u(e);
                }, r.peripheralManagerDidUpdateState = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:peripheralManagerDidUpdateState", e);
                    }), l && l(e);
                }, r.didChangeAuthorizationStatus = function(e) {
                    t(function() {
                        n.$broadcast("$cordovaBeacon:didChangeAuthorizationStatus", e);
                    }), d && d(e);
                }, e.cordova.plugins.locationManager.setDelegate(r);
            }
        }, !1), {
            setCallbackDidDetermineStateForRegion: function(e) {
                o = e;
            },
            setCallbackDidStartMonitoringForRegion: function(e) {
                i = e;
            },
            setCallbackDidExitRegion: function(e) {
                a = e;
            },
            setCallbackDidEnterRegion: function(e) {
                c = e;
            },
            setCallbackDidRangeBeaconsInRegion: function(e) {
                s = e;
            },
            setCallbackPeripheralManagerDidStartAdvertising: function(e) {
                u = e;
            },
            setCallbackPeripheralManagerDidUpdateState: function(e) {
                l = e;
            },
            setCallbackDidChangeAuthorizationStatus: function(e) {
                d = e;
            },
            createBeaconRegion: function(n, t, r, o, i) {
                return r = r || void 0, o = o || void 0, new e.cordova.plugins.locationManager.BeaconRegion(n, t, r, o, i);
            },
            isBluetoothEnabled: function() {
                return r.when(e.cordova.plugins.locationManager.isBluetoothEnabled());
            },
            enableBluetooth: function() {
                return r.when(e.cordova.plugins.locationManager.enableBluetooth());
            },
            disableBluetooth: function() {
                return r.when(e.cordova.plugins.locationManager.disableBluetooth());
            },
            startMonitoringForRegion: function(n) {
                return r.when(e.cordova.plugins.locationManager.startMonitoringForRegion(n));
            },
            stopMonitoringForRegion: function(n) {
                return r.when(e.cordova.plugins.locationManager.stopMonitoringForRegion(n));
            },
            requestStateForRegion: function(n) {
                return r.when(e.cordova.plugins.locationManager.requestStateForRegion(n));
            },
            startRangingBeaconsInRegion: function(n) {
                return r.when(e.cordova.plugins.locationManager.startRangingBeaconsInRegion(n));
            },
            stopRangingBeaconsInRegion: function(n) {
                return r.when(e.cordova.plugins.locationManager.stopRangingBeaconsInRegion(n));
            },
            getAuthorizationStatus: function() {
                return r.when(e.cordova.plugins.locationManager.getAuthorizationStatus());
            },
            requestWhenInUseAuthorization: function() {
                return r.when(e.cordova.plugins.locationManager.requestWhenInUseAuthorization());
            },
            requestAlwaysAuthorization: function() {
                return r.when(e.cordova.plugins.locationManager.requestAlwaysAuthorization());
            },
            getMonitoredRegions: function() {
                return r.when(e.cordova.plugins.locationManager.getMonitoredRegions());
            },
            getRangedRegions: function() {
                return r.when(e.cordova.plugins.locationManager.getRangedRegions());
            },
            isRangingAvailable: function() {
                return r.when(e.cordova.plugins.locationManager.isRangingAvailable());
            },
            isMonitoringAvailableForClass: function(n) {
                return r.when(e.cordova.plugins.locationManager.isMonitoringAvailableForClass(n));
            },
            startAdvertising: function(n, t) {
                return r.when(e.cordova.plugins.locationManager.startAdvertising(n, t));
            },
            stopAdvertising: function() {
                return r.when(e.cordova.plugins.locationManager.stopAdvertising());
            },
            isAdvertisingAvailable: function() {
                return r.when(e.cordova.plugins.locationManager.isAdvertisingAvailable());
            },
            isAdvertising: function() {
                return r.when(e.cordova.plugins.locationManager.isAdvertising());
            },
            disableDebugLogs: function() {
                return r.when(e.cordova.plugins.locationManager.disableDebugLogs());
            },
            enableDebugNotifications: function() {
                return r.when(e.cordova.plugins.locationManager.enableDebugNotifications());
            },
            disableDebugNotifications: function() {
                return r.when(e.cordova.plugins.locationManager.disableDebugNotifications());
            },
            enableDebugLogs: function() {
                return r.when(e.cordova.plugins.locationManager.enableDebugLogs());
            },
            appendToDeviceLog: function(n) {
                return r.when(e.cordova.plugins.locationManager.appendToDeviceLog(n));
            }
        };
    } ]), angular.module("ngCordova.plugins.ble", []).factory("$cordovaBLE", [ "$q", "$timeout", function(e, n) {
        return {
            scan: function(t, r) {
                var o = e.defer();
                return ble.startScan(t, function(e) {
                    o.notify(e);
                }, function(e) {
                    o.reject(e);
                }), n(function() {
                    ble.stopScan(function() {
                        o.resolve();
                    }, function(e) {
                        o.reject(e);
                    });
                }, 1e3 * r), o.promise;
            },
            connect: function(n) {
                var t = e.defer();
                return ble.connect(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            disconnect: function(n) {
                var t = e.defer();
                return ble.disconnect(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            read: function(n, t, r) {
                var o = e.defer();
                return ble.read(n, t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            write: function(n, t, r, o) {
                var i = e.defer();
                return ble.write(n, t, r, o, function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.reject(e);
                }), i.promise;
            },
            writeCommand: function(n, t, r, o) {
                var i = e.defer();
                return ble.writeCommand(n, t, r, o, function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.reject(e);
                }), i.promise;
            },
            startNotification: function(n, t, r) {
                var o = e.defer();
                return ble.startNotification(n, t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            stopNotification: function(n, t, r) {
                var o = e.defer();
                return ble.stopNotification(n, t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            isConnected: function(n) {
                var t = e.defer();
                return ble.isConnected(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            isEnabled: function() {
                var n = e.defer();
                return ble.isEnabled(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.bluetoothSerial", []).factory("$cordovaBluetoothSerial", [ "$q", "$window", function(e, n) {
        return {
            connect: function(t) {
                var r = e.defer(), o = e.defer(), i = !1;
                return n.bluetoothSerial.connect(t, function() {
                    i = !0, r.resolve(o);
                }, function(e) {
                    i === !1 && o.reject(e), r.reject(e);
                }), r.promise;
            },
            connectInsecure: function(t) {
                var r = e.defer();
                return n.bluetoothSerial.connectInsecure(t, function() {
                    r.resolve();
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            disconnect: function() {
                var t = e.defer();
                return n.bluetoothSerial.disconnect(function() {
                    t.resolve();
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            list: function() {
                var t = e.defer();
                return n.bluetoothSerial.list(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            discoverUnpaired: function() {
                var t = e.defer();
                return n.bluetoothSerial.discoverUnpaired(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            setDeviceDiscoveredListener: function() {
                var t = e.defer();
                return n.bluetoothSerial.setDeviceDiscoveredListener(function(e) {
                    t.notify(e);
                }), t.promise;
            },
            clearDeviceDiscoveredListener: function() {
                n.bluetoothSerial.clearDeviceDiscoveredListener();
            },
            showBluetoothSettings: function() {
                var t = e.defer();
                return n.bluetoothSerial.showBluetoothSettings(function() {
                    t.resolve();
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            isEnabled: function() {
                var t = e.defer();
                return n.bluetoothSerial.isEnabled(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            enable: function() {
                var t = e.defer();
                return n.bluetoothSerial.enable(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            isConnected: function() {
                var t = e.defer();
                return n.bluetoothSerial.isConnected(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            available: function() {
                var t = e.defer();
                return n.bluetoothSerial.available(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            read: function() {
                var t = e.defer();
                return n.bluetoothSerial.read(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            readUntil: function(t) {
                var r = e.defer();
                return n.bluetoothSerial.readUntil(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            write: function(t) {
                var r = e.defer();
                return n.bluetoothSerial.write(t, function() {
                    r.resolve();
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            subscribe: function(t) {
                var r = e.defer();
                return n.bluetoothSerial.subscribe(t, function(e) {
                    r.notify(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            subscribeRawData: function() {
                var t = e.defer();
                return n.bluetoothSerial.subscribeRawData(function(e) {
                    t.notify(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            unsubscribe: function() {
                var t = e.defer();
                return n.bluetoothSerial.unsubscribe(function() {
                    t.resolve();
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            unsubscribeRawData: function() {
                var t = e.defer();
                return n.bluetoothSerial.unsubscribeRawData(function() {
                    t.resolve();
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            clear: function() {
                var t = e.defer();
                return n.bluetoothSerial.clear(function() {
                    t.resolve();
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            readRSSI: function() {
                var t = e.defer();
                return n.bluetoothSerial.readRSSI(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.brightness", []).factory("$cordovaBrightness", [ "$q", "$window", function(e, n) {
        return {
            get: function() {
                var t = e.defer();
                return n.cordova ? n.cordova.plugins.brightness.getBrightness(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }) : t.reject("Not supported without cordova.js"), t.promise;
            },
            set: function(t) {
                var r = e.defer();
                return n.cordova ? n.cordova.plugins.brightness.setBrightness(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }) : r.reject("Not supported without cordova.js"), r.promise;
            },
            setKeepScreenOn: function(t) {
                var r = e.defer();
                return n.cordova ? n.cordova.plugins.brightness.setKeepScreenOn(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }) : r.reject("Not supported without cordova.js"), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.calendar", []).factory("$cordovaCalendar", [ "$q", "$window", function(e, n) {
        return {
            createCalendar: function(t) {
                var r = e.defer(), o = n.plugins.calendar.getCreateCalendarOptions();
                return "string" == typeof t ? o.calendarName = t : o = angular.extend(o, t), n.plugins.calendar.createCalendar(o, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            deleteCalendar: function(t) {
                var r = e.defer();
                return n.plugins.calendar.deleteCalendar(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            createEvent: function(t) {
                var r = e.defer(), o = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.createEvent(o.title, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            createEventWithOptions: function(t) {
                var r = e.defer(), o = [], i = window.plugins.calendar.getCalendarOptions(), a = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null
                };
                o = Object.keys(a);
                for (var c in t) -1 === o.indexOf(c) ? i[c] = t[c] : a[c] = t[c];
                return n.plugins.calendar.createEventWithOptions(a.title, a.location, a.notes, new Date(a.startDate), new Date(a.endDate), i, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            createEventInteractively: function(t) {
                var r = e.defer(), o = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.createEventInteractively(o.title, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            createEventInNamedCalendar: function(t) {
                var r = e.defer(), o = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null,
                    calendarName: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.createEventInNamedCalendar(o.title, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), o.calendarName, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            findEvent: function(t) {
                var r = e.defer(), o = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.findEvent(o.title, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            listEventsInRange: function(t, r) {
                var o = e.defer();
                return n.plugins.calendar.listEventsInRange(t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            listCalendars: function() {
                var t = e.defer();
                return n.plugins.calendar.listCalendars(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            findAllEventsInNamedCalendar: function(t) {
                var r = e.defer();
                return n.plugins.calendar.findAllEventsInNamedCalendar(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            modifyEvent: function(t) {
                var r = e.defer(), o = {
                    title: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null,
                    newTitle: null,
                    newLocation: null,
                    newNotes: null,
                    newStartDate: null,
                    newEndDate: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.modifyEvent(o.title, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), o.newTitle, o.newLocation, o.newNotes, new Date(o.newStartDate), new Date(o.newEndDate), function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            deleteEvent: function(t) {
                var r = e.defer(), o = {
                    newTitle: null,
                    location: null,
                    notes: null,
                    startDate: null,
                    endDate: null
                };
                return o = angular.extend(o, t), n.plugins.calendar.deleteEvent(o.newTitle, o.location, o.notes, new Date(o.startDate), new Date(o.endDate), function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.camera", []).factory("$cordovaCamera", [ "$q", function(e) {
        return {
            getPicture: function(n) {
                var t = e.defer();
                return navigator.camera ? (navigator.camera.getPicture(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise) : (t.resolve(null), t.promise);
            },
            cleanup: function() {
                var n = e.defer();
                return navigator.camera.cleanup(function() {
                    n.resolve();
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.capture", []).factory("$cordovaCapture", [ "$q", function(e) {
        return {
            captureAudio: function(n) {
                var t = e.defer();
                return navigator.device.capture ? (navigator.device.capture.captureAudio(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise) : (t.resolve(null), t.promise);
            },
            captureImage: function(n) {
                var t = e.defer();
                return navigator.device.capture ? (navigator.device.capture.captureImage(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise) : (t.resolve(null), t.promise);
            },
            captureVideo: function(n) {
                var t = e.defer();
                return navigator.device.capture ? (navigator.device.capture.captureVideo(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise) : (t.resolve(null), t.promise);
            }
        };
    } ]), angular.module("ngCordova.plugins.cardIO", []).provider("$cordovaNgCardIO", [ function() {
        var e = [ "card_type", "redacted_card_number", "card_number", "expiry_month", "expiry_year", "short_expiry_year", "cvv", "zip" ], n = {
            expiry: !0,
            cvv: !0,
            zip: !1,
            suppressManual: !1,
            suppressConfirm: !1,
            hideLogo: !0
        };
        this.setCardIOResponseFields = function(n) {
            n && angular.isArray(n) && (e = n);
        }, this.setScanerConfig = function(e) {
            e && angular.isObject(e) && (n.expiry = e.expiry || !0, n.cvv = e.cvv || !0, n.zip = e.zip || !1, 
            n.suppressManual = e.suppressManual || !1, n.suppressConfirm = e.suppressConfirm || !1, 
            n.hideLogo = e.hideLogo || !0);
        }, this.$get = [ "$q", function(t) {
            return {
                scanCard: function() {
                    var r = t.defer();
                    return CardIO.scan(n, function(n) {
                        if (null === n) r.reject(null); else {
                            for (var t = {}, o = 0, i = e.length; i > o; o++) {
                                var a = e[o];
                                "short_expiry_year" === a ? t[a] = String(n.expiry_year).substr(2, 2) || "" : t[a] = n[a] || "";
                            }
                            r.resolve(t);
                        }
                    }, function() {
                        r.reject(null);
                    }), r.promise;
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.clipboard", []).factory("$cordovaClipboard", [ "$q", "$window", function(e, n) {
        return {
            copy: function(t) {
                var r = e.defer();
                return n.cordova.plugins.clipboard.copy(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            paste: function() {
                var t = e.defer();
                return n.cordova.plugins.clipboard.paste(function(e) {
                    t.resolve(e);
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.contacts", []).factory("$cordovaContacts", [ "$q", function(e) {
        return {
            save: function(n) {
                var t = e.defer(), r = navigator.contacts.create(n);
                return r.save(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            remove: function(n) {
                var t = e.defer(), r = navigator.contacts.create(n);
                return r.remove(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            clone: function(e) {
                var n = navigator.contacts.create(e);
                return n.clone(e);
            },
            find: function(n) {
                var t = e.defer(), r = n.fields || [ "id", "displayName" ];
                return delete n.fields, 0 === Object.keys(n).length ? navigator.contacts.find(r, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }) : navigator.contacts.find(r, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            pickContact: function() {
                var n = e.defer();
                return navigator.contacts.pickContact(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.datePicker", []).factory("$cordovaDatePicker", [ "$window", "$q", function(e, n) {
        return {
            show: function(t) {
                var r = n.defer();
                return t = t || {
                    date: new Date(),
                    mode: "date"
                }, e.datePicker.show(t, function(e) {
                    r.resolve(e);
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.device", []).factory("$cordovaDevice", [ function() {
        return {
            getDevice: function() {
                return device;
            },
            getCordova: function() {
                return device.cordova;
            },
            getModel: function() {
                return device.model;
            },
            getName: function() {
                return device.name;
            },
            getPlatform: function() {
                return device.platform;
            },
            getUUID: function() {
                return device.uuid;
            },
            getVersion: function() {
                return device.version;
            },
            getManufacturer: function() {
                return device.manufacturer;
            }
        };
    } ]), angular.module("ngCordova.plugins.deviceMotion", []).factory("$cordovaDeviceMotion", [ "$q", function(e) {
        return {
            getCurrentAcceleration: function() {
                var n = e.defer();
                return (angular.isUndefined(navigator.accelerometer) || !angular.isFunction(navigator.accelerometer.getCurrentAcceleration)) && n.reject("Device do not support watchAcceleration"), 
                navigator.accelerometer.getCurrentAcceleration(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            },
            watchAcceleration: function(n) {
                var t = e.defer();
                (angular.isUndefined(navigator.accelerometer) || !angular.isFunction(navigator.accelerometer.watchAcceleration)) && t.reject("Device do not support watchAcceleration");
                var r = navigator.accelerometer.watchAcceleration(function(e) {
                    t.notify(e);
                }, function(e) {
                    t.reject(e);
                }, n);
                return t.promise.cancel = function() {
                    navigator.accelerometer.clearWatch(r);
                }, t.promise.clearWatch = function(e) {
                    navigator.accelerometer.clearWatch(e || r);
                }, t.promise.watchID = r, t.promise;
            },
            clearWatch: function(e) {
                return navigator.accelerometer.clearWatch(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.deviceOrientation", []).factory("$cordovaDeviceOrientation", [ "$q", function(e) {
        var n = {
            frequency: 3e3
        };
        return {
            getCurrentHeading: function() {
                var n = e.defer();
                return navigator.compass ? (navigator.compass.getCurrentHeading(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise) : (n.reject("No compass on Device"), n.promise);
            },
            watchHeading: function(t) {
                var r = e.defer();
                if (!navigator.compass) return r.reject("No compass on Device"), r.promise;
                var o = angular.extend(n, t), i = navigator.compass.watchHeading(function(e) {
                    r.notify(e);
                }, function(e) {
                    r.reject(e);
                }, o);
                return r.promise.cancel = function() {
                    navigator.compass.clearWatch(i);
                }, r.promise.clearWatch = function(e) {
                    navigator.compass.clearWatch(e || i);
                }, r.promise.watchID = i, r.promise;
            },
            clearWatch: function(e) {
                return navigator.compass.clearWatch(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.dialogs", []).factory("$cordovaDialogs", [ "$q", "$window", function(e, n) {
        return {
            alert: function(t, r, o) {
                var i = e.defer();
                return n.navigator.notification ? navigator.notification.alert(t, function() {
                    i.resolve();
                }, r, o) : (n.alert(t), i.resolve()), i.promise;
            },
            confirm: function(t, r, o) {
                var i = e.defer();
                return n.navigator.notification ? navigator.notification.confirm(t, function(e) {
                    i.resolve(e);
                }, r, o) : n.confirm(t) ? i.resolve(1) : i.resolve(2), i.promise;
            },
            prompt: function(t, r, o, i) {
                var a = e.defer();
                if (n.navigator.notification) navigator.notification.prompt(t, function(e) {
                    a.resolve(e);
                }, r, o, i); else {
                    var c = n.prompt(t, i);
                    null !== c ? a.resolve({
                        input1: c,
                        buttonIndex: 1
                    }) : a.resolve({
                        input1: c,
                        buttonIndex: 2
                    });
                }
                return a.promise;
            },
            beep: function(e) {
                return navigator.notification.beep(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.emailComposer", []).factory("$cordovaEmailComposer", [ "$q", function(e) {
        return {
            isAvailable: function() {
                var n = e.defer();
                return cordova.plugins.email.isAvailable(function(e) {
                    e ? n.resolve() : n.reject();
                }), n.promise;
            },
            open: function(n) {
                var t = e.defer();
                return cordova.plugins.email.open(n, function() {
                    t.reject();
                }), t.promise;
            },
            addAlias: function(e, n) {
                cordova.plugins.email.addAlias(e, n);
            }
        };
    } ]), angular.module("ngCordova.plugins.facebook", []).provider("$cordovaFacebook", [ function() {
        this.browserInit = function(e, n) {
            this.appID = e, this.appVersion = n || "v2.0", facebookConnectPlugin.browserInit(this.appID, this.appVersion);
        }, this.$get = [ "$q", function(e) {
            return {
                login: function(n) {
                    var t = e.defer();
                    return facebookConnectPlugin.login(n, function(e) {
                        t.resolve(e);
                    }, function(e) {
                        t.reject(e);
                    }), t.promise;
                },
                showDialog: function(n) {
                    var t = e.defer();
                    return facebookConnectPlugin.showDialog(n, function(e) {
                        t.resolve(e);
                    }, function(e) {
                        t.reject(e);
                    }), t.promise;
                },
                api: function(n, t) {
                    var r = e.defer();
                    return facebookConnectPlugin.api(n, t, function(e) {
                        r.resolve(e);
                    }, function(e) {
                        r.reject(e);
                    }), r.promise;
                },
                getAccessToken: function() {
                    var n = e.defer();
                    return facebookConnectPlugin.getAccessToken(function(e) {
                        n.resolve(e);
                    }, function(e) {
                        n.reject(e);
                    }), n.promise;
                },
                getLoginStatus: function() {
                    var n = e.defer();
                    return facebookConnectPlugin.getLoginStatus(function(e) {
                        n.resolve(e);
                    }, function(e) {
                        n.reject(e);
                    }), n.promise;
                },
                logout: function() {
                    var n = e.defer();
                    return facebookConnectPlugin.logout(function(e) {
                        n.resolve(e);
                    }, function(e) {
                        n.reject(e);
                    }), n.promise;
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.facebookAds", []).factory("$cordovaFacebookAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.FacebookAds.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.FacebookAds.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.FacebookAds.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.FacebookAds.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.FacebookAds.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.FacebookAds.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.FacebookAds.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.FacebookAds.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.file", []).constant("$cordovaFileError", {
        1: "NOT_FOUND_ERR",
        2: "SECURITY_ERR",
        3: "ABORT_ERR",
        4: "NOT_READABLE_ERR",
        5: "ENCODING_ERR",
        6: "NO_MODIFICATION_ALLOWED_ERR",
        7: "INVALID_STATE_ERR",
        8: "SYNTAX_ERR",
        9: "INVALID_MODIFICATION_ERR",
        10: "QUOTA_EXCEEDED_ERR",
        11: "TYPE_MISMATCH_ERR",
        12: "PATH_EXISTS_ERR"
    }).provider("$cordovaFile", [ function() {
        this.$get = [ "$q", "$window", "$cordovaFileError", function(e, n, t) {
            return {
                getFreeDiskSpace: function() {
                    var n = e.defer();
                    return cordova.exec(function(e) {
                        n.resolve(e);
                    }, function(e) {
                        n.reject(e);
                    }, "File", "getFreeDiskSpace", []), n.promise;
                },
                checkDir: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("directory cannot start with /");
                    try {
                        var a = r + o;
                        n.resolveLocalFileSystemURL(a, function(e) {
                            e.isDirectory === !0 ? i.resolve(e) : i.reject({
                                code: 13,
                                message: "input is not a directory"
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (c) {
                        c.message = t[c.code], i.reject(c);
                    }
                    return i.promise;
                },
                checkFile: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("directory cannot start with /");
                    try {
                        var a = r + o;
                        n.resolveLocalFileSystemURL(a, function(e) {
                            e.isFile === !0 ? i.resolve(e) : i.reject({
                                code: 13,
                                message: "input is not a file"
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (c) {
                        c.message = t[c.code], i.reject(c);
                    }
                    return i.promise;
                },
                createDir: function(r, o, i) {
                    var a = e.defer();
                    /^\//.test(o) && a.reject("directory cannot start with /"), i = i ? !1 : !0;
                    var c = {
                        create: !0,
                        exclusive: i
                    };
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getDirectory(o, c, function(e) {
                                a.resolve(e);
                            }, function(e) {
                                e.message = t[e.code], a.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], a.reject(e);
                        });
                    } catch (s) {
                        s.message = t[s.code], a.reject(s);
                    }
                    return a.promise;
                },
                createFile: function(r, o, i) {
                    var a = e.defer();
                    /^\//.test(o) && a.reject("file-name cannot start with /"), i = i ? !1 : !0;
                    var c = {
                        create: !0,
                        exclusive: i
                    };
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, c, function(e) {
                                a.resolve(e);
                            }, function(e) {
                                e.message = t[e.code], a.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], a.reject(e);
                        });
                    } catch (s) {
                        s.message = t[s.code], a.reject(s);
                    }
                    return a.promise;
                },
                removeDir: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getDirectory(o, {
                                create: !1
                            }, function(e) {
                                e.remove(function() {
                                    i.resolve({
                                        success: !0,
                                        fileRemoved: e
                                    });
                                }, function(e) {
                                    e.message = t[e.code], i.reject(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                removeFile: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.remove(function() {
                                    i.resolve({
                                        success: !0,
                                        fileRemoved: e
                                    });
                                }, function(e) {
                                    e.message = t[e.code], i.reject(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                removeRecursively: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getDirectory(o, {
                                create: !1
                            }, function(e) {
                                e.removeRecursively(function() {
                                    i.resolve({
                                        success: !0,
                                        fileRemoved: e
                                    });
                                }, function(e) {
                                    e.message = t[e.code], i.reject(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                writeFile: function(r, o, i, a) {
                    var c = e.defer();
                    /^\//.test(o) && c.reject("file-name cannot start with /"), a = a ? !1 : !0;
                    var s = {
                        create: !0,
                        exclusive: a
                    };
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, s, function(e) {
                                e.createWriter(function(e) {
                                    s.append === !0 && e.seek(e.length), s.truncate && e.truncate(s.truncate), e.onwriteend = function(e) {
                                        this.error ? c.reject(this.error) : c.resolve(e);
                                    }, e.write(i), c.promise.abort = function() {
                                        e.abort();
                                    };
                                });
                            }, function(e) {
                                e.message = t[e.code], c.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], c.reject(e);
                        });
                    } catch (u) {
                        u.message = t[u.code], c.reject(u);
                    }
                    return c.promise;
                },
                writeExistingFile: function(r, o, i) {
                    var a = e.defer();
                    /^\//.test(o) && a.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.createWriter(function(e) {
                                    e.seek(e.length), e.onwriteend = function(e) {
                                        this.error ? a.reject(this.error) : a.resolve(e);
                                    }, e.write(i), a.promise.abort = function() {
                                        e.abort();
                                    };
                                });
                            }, function(e) {
                                e.message = t[e.code], a.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], a.reject(e);
                        });
                    } catch (c) {
                        c.message = t[c.code], a.reject(c);
                    }
                    return a.promise;
                },
                readAsText: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.file(function(e) {
                                    var n = new FileReader();
                                    n.onloadend = function(e) {
                                        void 0 !== e.target.result || null !== e.target.result ? i.resolve(e.target.result) : void 0 !== e.target.error || null !== e.target.error ? i.reject(e.target.error) : i.reject({
                                            code: null,
                                            message: "READER_ONLOADEND_ERR"
                                        });
                                    }, n.readAsText(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                readAsDataURL: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.file(function(e) {
                                    var n = new FileReader();
                                    n.onloadend = function(e) {
                                        void 0 !== e.target.result || null !== e.target.result ? i.resolve(e.target.result) : void 0 !== e.target.error || null !== e.target.error ? i.reject(e.target.error) : i.reject({
                                            code: null,
                                            message: "READER_ONLOADEND_ERR"
                                        });
                                    }, n.readAsDataURL(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                readAsBinaryString: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.file(function(e) {
                                    var n = new FileReader();
                                    n.onloadend = function(e) {
                                        void 0 !== e.target.result || null !== e.target.result ? i.resolve(e.target.result) : void 0 !== e.target.error || null !== e.target.error ? i.reject(e.target.error) : i.reject({
                                            code: null,
                                            message: "READER_ONLOADEND_ERR"
                                        });
                                    }, n.readAsBinaryString(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                readAsArrayBuffer: function(r, o) {
                    var i = e.defer();
                    /^\//.test(o) && i.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1
                            }, function(e) {
                                e.file(function(e) {
                                    var n = new FileReader();
                                    n.onloadend = function(e) {
                                        void 0 !== e.target.result || null !== e.target.result ? i.resolve(e.target.result) : void 0 !== e.target.error || null !== e.target.error ? i.reject(e.target.error) : i.reject({
                                            code: null,
                                            message: "READER_ONLOADEND_ERR"
                                        });
                                    }, n.readAsArrayBuffer(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], i.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], i.reject(e);
                        });
                    } catch (a) {
                        a.message = t[a.code], i.reject(a);
                    }
                    return i.promise;
                },
                moveFile: function(t, r, o, i) {
                    var a = e.defer();
                    i = i || r, (/^\//.test(r) || /^\//.test(i)) && a.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(t, function(e) {
                            e.getFile(r, {
                                create: !1
                            }, function(e) {
                                n.resolveLocalFileSystemURL(o, function(n) {
                                    e.moveTo(n, i, function(e) {
                                        a.resolve(e);
                                    }, function(e) {
                                        a.reject(e);
                                    });
                                }, function(e) {
                                    a.reject(e);
                                });
                            }, function(e) {
                                a.reject(e);
                            });
                        }, function(e) {
                            a.reject(e);
                        });
                    } catch (c) {
                        a.reject(c);
                    }
                    return a.promise;
                },
                moveDir: function(t, r, o, i) {
                    var a = e.defer();
                    i = i || r, (/^\//.test(r) || /^\//.test(i)) && a.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(t, function(e) {
                            e.getDirectory(r, {
                                create: !1
                            }, function(e) {
                                n.resolveLocalFileSystemURL(o, function(n) {
                                    e.moveTo(n, i, function(e) {
                                        a.resolve(e);
                                    }, function(e) {
                                        a.reject(e);
                                    });
                                }, function(e) {
                                    a.reject(e);
                                });
                            }, function(e) {
                                a.reject(e);
                            });
                        }, function(e) {
                            a.reject(e);
                        });
                    } catch (c) {
                        a.reject(c);
                    }
                    return a.promise;
                },
                copyDir: function(r, o, i, a) {
                    var c = e.defer();
                    a = a || o, (/^\//.test(o) || /^\//.test(a)) && c.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getDirectory(o, {
                                create: !1,
                                exclusive: !1
                            }, function(e) {
                                n.resolveLocalFileSystemURL(i, function(n) {
                                    e.copyTo(n, a, function(e) {
                                        c.resolve(e);
                                    }, function(e) {
                                        e.message = t[e.code], c.reject(e);
                                    });
                                }, function(e) {
                                    e.message = t[e.code], c.reject(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], c.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], c.reject(e);
                        });
                    } catch (s) {
                        s.message = t[s.code], c.reject(s);
                    }
                    return c.promise;
                },
                copyFile: function(r, o, i, a) {
                    var c = e.defer();
                    a = a || o, /^\//.test(o) && c.reject("file-name cannot start with /");
                    try {
                        n.resolveLocalFileSystemURL(r, function(e) {
                            e.getFile(o, {
                                create: !1,
                                exclusive: !1
                            }, function(e) {
                                n.resolveLocalFileSystemURL(i, function(n) {
                                    e.copyTo(n, a, function(e) {
                                        c.resolve(e);
                                    }, function(e) {
                                        e.message = t[e.code], c.reject(e);
                                    });
                                }, function(e) {
                                    e.message = t[e.code], c.reject(e);
                                });
                            }, function(e) {
                                e.message = t[e.code], c.reject(e);
                            });
                        }, function(e) {
                            e.message = t[e.code], c.reject(e);
                        });
                    } catch (s) {
                        s.message = t[s.code], c.reject(s);
                    }
                    return c.promise;
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.fileOpener2", []).factory("$cordovaFileOpener2", [ "$q", function(e) {
        return {
            open: function(n, t) {
                var r = e.defer();
                return cordova.plugins.fileOpener2.open(n, t, {
                    error: function(e) {
                        r.reject(e);
                    },
                    success: function() {
                        r.resolve();
                    }
                }), r.promise;
            },
            uninstall: function(n) {
                var t = e.defer();
                return cordova.plugins.fileOpener2.uninstall(n, {
                    error: function(e) {
                        t.reject(e);
                    },
                    success: function() {
                        t.resolve();
                    }
                }), t.promise;
            },
            appIsInstalled: function(n) {
                var t = e.defer();
                return cordova.plugins.fileOpener2.appIsInstalled(n, {
                    success: function(e) {
                        t.resolve(e);
                    }
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.fileTransfer", []).factory("$cordovaFileTransfer", [ "$q", "$timeout", function(e, n) {
        return {
            download: function(t, r, o, i) {
                var a = e.defer(), c = new FileTransfer(), s = o && o.encodeURI === !1 ? t : encodeURI(t);
                return o && void 0 !== o.timeout && null !== o.timeout && (n(function() {
                    c.abort();
                }, o.timeout), o.timeout = null), c.onprogress = function(e) {
                    a.notify(e);
                }, a.promise.abort = function() {
                    c.abort();
                }, c.download(s, r, a.resolve, a.reject, i, o), a.promise;
            },
            upload: function(t, r, o, i) {
                var a = e.defer(), c = new FileTransfer(), s = o && o.encodeURI === !1 ? t : encodeURI(t);
                return o && void 0 !== o.timeout && null !== o.timeout && (n(function() {
                    c.abort();
                }, o.timeout), o.timeout = null), c.onprogress = function(e) {
                    a.notify(e);
                }, a.promise.abort = function() {
                    c.abort();
                }, c.upload(r, s, a.resolve, a.reject, o, i), a.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.flashlight", []).factory("$cordovaFlashlight", [ "$q", "$window", function(e, n) {
        return {
            available: function() {
                var t = e.defer();
                return n.plugins.flashlight.available(function(e) {
                    t.resolve(e);
                }), t.promise;
            },
            switchOn: function() {
                var t = e.defer();
                return n.plugins.flashlight.switchOn(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            switchOff: function() {
                var t = e.defer();
                return n.plugins.flashlight.switchOff(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            toggle: function() {
                var t = e.defer();
                return n.plugins.flashlight.toggle(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.flurryAds", []).factory("$cordovaFlurryAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.FlurryAds.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.FlurryAds.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.FlurryAds.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.FlurryAds.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.FlurryAds.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.FlurryAds.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.FlurryAds.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.FlurryAds.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.ga", []).factory("$cordovaGA", [ "$q", "$window", function(e, n) {
        return {
            init: function(t, r) {
                var o = e.defer();
                return r = r >= 0 ? r : 10, n.plugins.gaPlugin.init(function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }, t, r), o.promise;
            },
            trackEvent: function(t, r, o, i, a, c) {
                var s = e.defer();
                return n.plugins.gaPlugin.trackEvent(function(e) {
                    s.resolve(e);
                }, function(e) {
                    s.reject(e);
                }, o, i, a, c), s.promise;
            },
            trackPage: function(t, r, o) {
                var i = e.defer();
                return n.plugins.gaPlugin.trackPage(function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.reject(e);
                }, o), i.promise;
            },
            setVariable: function(t, r, o, i) {
                var a = e.defer();
                return n.plugins.gaPlugin.setVariable(function(e) {
                    a.resolve(e);
                }, function(e) {
                    a.reject(e);
                }, o, i), a.promise;
            },
            exit: function() {
                var t = e.defer();
                return n.plugins.gaPlugin.exit(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.geolocation", []).factory("$cordovaGeolocation", [ "$q", function(e) {
        return {
            getCurrentPosition: function(n) {
                var t = e.defer();
                return navigator.geolocation.getCurrentPosition(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            watchPosition: function(n) {
                var t = e.defer(), r = navigator.geolocation.watchPosition(function(e) {
                    t.notify(e);
                }, function(e) {
                    t.reject(e);
                }, n);
                return t.promise.cancel = function() {
                    navigator.geolocation.clearWatch(r);
                }, t.promise.clearWatch = function(e) {
                    navigator.geolocation.clearWatch(e || r);
                }, t.promise.watchID = r, t.promise;
            },
            clearWatch: function(e) {
                return navigator.geolocation.clearWatch(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.globalization", []).factory("$cordovaGlobalization", [ "$q", function(e) {
        return {
            getPreferredLanguage: function() {
                var n = e.defer();
                return navigator.globalization.getPreferredLanguage(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            },
            getLocaleName: function() {
                var n = e.defer();
                return navigator.globalization.getLocaleName(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            },
            getFirstDayOfWeek: function() {
                var n = e.defer();
                return navigator.globalization.getFirstDayOfWeek(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            },
            dateToString: function(n, t) {
                var r = e.defer();
                return navigator.globalization.dateToString(n, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            stringToDate: function(n, t) {
                var r = e.defer();
                return navigator.globalization.stringToDate(n, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            getDatePattern: function(n) {
                var t = e.defer();
                return navigator.globalization.getDatePattern(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            getDateNames: function(n) {
                var t = e.defer();
                return navigator.globalization.getDateNames(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            isDayLightSavingsTime: function(n) {
                var t = e.defer();
                return navigator.globalization.isDayLightSavingsTime(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            numberToString: function(n, t) {
                var r = e.defer();
                return navigator.globalization.numberToString(n, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            stringToNumber: function(n, t) {
                var r = e.defer();
                return navigator.globalization.stringToNumber(n, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            getNumberPattern: function(n) {
                var t = e.defer();
                return navigator.globalization.getNumberPattern(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            },
            getCurrencyPattern: function(n) {
                var t = e.defer();
                return navigator.globalization.getCurrencyPattern(n, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.googleAds", []).factory("$cordovaGoogleAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.AdMob.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.AdMob.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.AdMob.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.AdMob.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.AdMob.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.AdMob.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.AdMob.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.AdMob.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.googleAnalytics", []).factory("$cordovaGoogleAnalytics", [ "$q", "$window", function(e, n) {
        return {
            startTrackerWithId: function(t) {
                var r = e.defer();
                return n.analytics.startTrackerWithId(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            setUserId: function(t) {
                var r = e.defer();
                return n.analytics.setUserId(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            debugMode: function() {
                var t = e.defer();
                return n.analytics.debugMode(function(e) {
                    t.resolve(e);
                }, function() {
                    t.reject();
                }), t.promise;
            },
            trackView: function(t) {
                var r = e.defer();
                return n.analytics.trackView(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            addCustomDimension: function(t, r) {
                var o = e.defer();
                return n.analytics.addCustomDimension(t, r, function() {
                    o.resolve();
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            trackEvent: function(t, r, o, i) {
                var a = e.defer();
                return n.analytics.trackEvent(t, r, o, i, function(e) {
                    a.resolve(e);
                }, function(e) {
                    a.reject(e);
                }), a.promise;
            },
            trackException: function(t, r) {
                var o = e.defer();
                return n.analytics.trackException(t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            trackTiming: function(t, r, o, i) {
                var a = e.defer();
                return n.analytics.trackTiming(t, r, o, i, function(e) {
                    a.resolve(e);
                }, function(e) {
                    a.reject(e);
                }), a.promise;
            },
            addTransaction: function(t, r, o, i, a, c) {
                var s = e.defer();
                return n.analytics.addTransaction(t, r, o, i, a, c, function(e) {
                    s.resolve(e);
                }, function(e) {
                    s.reject(e);
                }), s.promise;
            },
            addTransactionItem: function(t, r, o, i, a, c, s) {
                var u = e.defer();
                return n.analytics.addTransactionItem(t, r, o, i, a, c, s, function(e) {
                    u.resolve(e);
                }, function(e) {
                    u.reject(e);
                }), u.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.googleMap", []).factory("$cordovaGoogleMap", [ "$q", "$window", function(e, n) {
        var t = null;
        return {
            getMap: function(r) {
                var o = e.defer();
                if (n.plugin.google.maps) {
                    var i = document.getElementById("map_canvas");
                    t = n.plugin.google.maps.Map.getMap(r), t.setDiv(i), o.resolve(t);
                } else o.reject(null);
                return o.promise;
            },
            isMapLoaded: function() {
                return !!t;
            },
            addMarker: function(n) {
                var r = e.defer();
                return t.addMarker(n, function(e) {
                    r.resolve(e);
                }), r.promise;
            },
            getMapTypeIds: function() {
                return n.plugin.google.maps.mapTypeId;
            },
            setVisible: function(n) {
                var r = e.defer();
                return t.setVisible(n), r.promise;
            },
            cleanup: function() {
                t = null;
            }
        };
    } ]), angular.module("ngCordova.plugins.googlePlayGame", []).factory("$cordovaGooglePlayGame", [ "$q", function(e) {
        return {
            auth: function() {
                var n = e.defer();
                return googleplaygame.auth(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            },
            signout: function() {
                var n = e.defer();
                return googleplaygame.signout(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            },
            isSignedIn: function() {
                var n = e.defer();
                return googleplaygame.isSignedIn(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            },
            showPlayer: function() {
                var n = e.defer();
                return googleplaygame.showPlayer(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            },
            submitScore: function(n) {
                var t = e.defer();
                return googleplaygame.submitScore(n, function(e) {
                    return t.resolve(e);
                }, function(e) {
                    return t.reject(e);
                }), t.promise;
            },
            showAllLeaderboards: function() {
                var n = e.defer();
                return googleplaygame.showAllLeaderboards(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            },
            showLeaderboard: function(n) {
                var t = e.defer();
                return googleplaygame.showLeaderboard(n, function(e) {
                    return t.resolve(e);
                }, function(e) {
                    return t.reject(e);
                }), t.promise;
            },
            unlockAchievement: function(n) {
                var t = e.defer();
                return googleplaygame.unlockAchievement(n, function(e) {
                    return t.resolve(e);
                }, function(e) {
                    return t.reject(e);
                }), t.promise;
            },
            incrementAchievement: function(n) {
                var t = e.defer();
                return googleplaygame.incrementAchievement(n, function(e) {
                    return t.resolve(e);
                }, function(e) {
                    return t.reject(e);
                }), t.promise;
            },
            showAchievements: function() {
                var n = e.defer();
                return googleplaygame.showAchievements(function(e) {
                    return n.resolve(e);
                }, function(e) {
                    return n.reject(e);
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.googlePlus", []).factory("$cordovaGooglePlus", [ "$q", "$window", function(e, n) {
        return {
            login: function(t) {
                var r = e.defer();
                return void 0 === t && (t = {}), n.plugins.googleplus.login({
                    iOSApiKey: t
                }, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            silentLogin: function(t) {
                var r = e.defer();
                return void 0 === t && (t = {}), n.plugins.googleplus.trySilentLogin({
                    iOSApiKey: t
                }, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            logout: function() {
                var t = e.defer();
                n.plugins.googleplus.logout(function(e) {
                    t.resolve(e);
                });
            },
            disconnect: function() {
                var t = e.defer();
                n.plugins.googleplus.disconnect(function(e) {
                    t.resolve(e);
                });
            },
            isAvailable: function() {
                var t = e.defer();
                return n.plugins.googleplus.isAvailable(function(e) {
                    e ? t.resolve(e) : t.reject(e);
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.healthKit", []).factory("$cordovaHealthKit", [ "$q", "$window", function(e, n) {
        return {
            isAvailable: function() {
                var t = e.defer();
                return n.plugins.healthkit.available(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }), t.promise;
            },
            checkAuthStatus: function(t) {
                var r = e.defer();
                return t = t || "HKQuantityTypeIdentifierHeight", n.plugins.healthkit.checkAuthStatus({
                    type: t
                }, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            requestAuthorization: function(t, r) {
                var o = e.defer();
                return t = t || [ "HKCharacteristicTypeIdentifierDateOfBirth", "HKQuantityTypeIdentifierActiveEnergyBurned", "HKQuantityTypeIdentifierHeight" ], 
                r = r || [ "HKQuantityTypeIdentifierActiveEnergyBurned", "HKQuantityTypeIdentifierHeight", "HKQuantityTypeIdentifierDistanceCycling" ], 
                n.plugins.healthkit.requestAuthorization({
                    readTypes: t,
                    writeTypes: r
                }, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            readDateOfBirth: function() {
                var t = e.defer();
                return n.plugins.healthkit.readDateOfBirth(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.resolve(e);
                }), t.promise;
            },
            readGender: function() {
                var t = e.defer();
                return n.plugins.healthkit.readGender(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.resolve(e);
                }), t.promise;
            },
            saveWeight: function(t, r, o) {
                var i = e.defer();
                return n.plugins.healthkit.saveWeight({
                    unit: r || "lb",
                    amount: t,
                    date: o || new Date()
                }, function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.resolve(e);
                }), i.promise;
            },
            readWeight: function(t) {
                var r = e.defer();
                return n.plugins.healthkit.readWeight({
                    unit: t || "lb"
                }, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.resolve(e);
                }), r.promise;
            },
            saveHeight: function(t, r, o) {
                var i = e.defer();
                return n.plugins.healthkit.saveHeight({
                    unit: r || "in",
                    amount: t,
                    date: o || new Date()
                }, function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.resolve(e);
                }), i.promise;
            },
            readHeight: function(t) {
                var r = e.defer();
                return n.plugins.healthkit.readHeight({
                    unit: t || "in"
                }, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.resolve(e);
                }), r.promise;
            },
            findWorkouts: function() {
                var t = e.defer();
                return n.plugins.healthkit.findWorkouts({}, function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.resolve(e);
                }), t.promise;
            },
            saveWorkout: function(t) {
                var r = e.defer();
                return n.plugins.healthkit.saveWorkout(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.resolve(e);
                }), r.promise;
            },
            querySampleType: function(t) {
                var r = e.defer();
                return n.plugins.healthkit.querySampleType(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.resolve(e);
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.httpd", []).factory("$cordovaHttpd", [ "$q", function(e) {
        return {
            startServer: function(n) {
                var t = e.defer();
                return cordova.plugins.CorHttpd.startServer(n, function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            stopServer: function() {
                var n = e.defer();
                return cordova.plugins.CorHttpd.stopServer(function() {
                    n.resolve();
                }, function() {
                    n.reject();
                }), n.promise;
            },
            getURL: function() {
                var n = e.defer();
                return cordova.plugins.CorHttpd.getURL(function(e) {
                    n.resolve(e);
                }, function() {
                    n.reject();
                }), n.promise;
            },
            getLocalPath: function() {
                var n = e.defer();
                return cordova.plugins.CorHttpd.getLocalPath(function(e) {
                    n.resolve(e);
                }, function() {
                    n.reject();
                }), n.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.iAd", []).factory("$cordovaiAd", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.iAd.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.iAd.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.iAd.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.iAd.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.iAd.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.iAd.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.iAd.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.iAd.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.imagePicker", []).factory("$cordovaImagePicker", [ "$q", "$window", function(e, n) {
        return {
            getPictures: function(t) {
                var r = e.defer();
                return n.imagePicker.getPictures(function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.inAppBrowser", []).provider("$cordovaInAppBrowser", [ function() {
        var e, n = this.defaultOptions = {};
        this.setDefaultOptions = function(e) {
            n = angular.extend(n, e);
        }, this.$get = [ "$rootScope", "$q", "$window", "$timeout", function(t, r, o, i) {
            return {
                open: function(a, c, s) {
                    var u = r.defer();
                    if (s && !angular.isObject(s)) return u.reject("options must be an object"), u.promise;
                    var l = angular.extend({}, n, s), d = [];
                    angular.forEach(l, function(e, n) {
                        d.push(n + "=" + e);
                    });
                    var f = d.join();
                    return e = o.open(a, c, f), e.addEventListener("loadstart", function(e) {
                        i(function() {
                            t.$broadcast("$cordovaInAppBrowser:loadstart", e);
                        });
                    }, !1), e.addEventListener("loadstop", function(e) {
                        u.resolve(e), i(function() {
                            t.$broadcast("$cordovaInAppBrowser:loadstop", e);
                        });
                    }, !1), e.addEventListener("loaderror", function(e) {
                        u.reject(e), i(function() {
                            t.$broadcast("$cordovaInAppBrowser:loaderror", e);
                        });
                    }, !1), e.addEventListener("exit", function(e) {
                        i(function() {
                            t.$broadcast("$cordovaInAppBrowser:exit", e);
                        });
                    }, !1), u.promise;
                },
                close: function() {
                    e.close(), e = null;
                },
                show: function() {
                    e.show();
                },
                executeScript: function(n) {
                    var t = r.defer();
                    return e.executeScript(n, function(e) {
                        t.resolve(e);
                    }), t.promise;
                },
                insertCSS: function(n) {
                    var t = r.defer();
                    return e.insertCSS(n, function(e) {
                        t.resolve(e);
                    }), t.promise;
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.insomnia", []).factory("$cordovaInsomnia", [ "$window", function(e) {
        return {
            keepAwake: function() {
                return e.plugins.insomnia.keepAwake();
            },
            allowSleepAgain: function() {
                return e.plugins.insomnia.allowSleepAgain();
            }
        };
    } ]), angular.module("ngCordova.plugins.instagram", []).factory("$cordovaInstagram", [ "$q", function(e) {
        return {
            share: function(n) {
                var t = e.defer();
                return window.Instagram ? (Instagram.share(n.image, n.caption, function(e) {
                    e ? t.reject(e) : t.resolve(!0);
                }), t.promise) : (console.error("Tried to call Instagram.share but the Instagram plugin isn't installed!"), 
                t.resolve(null), t.promise);
            },
            isInstalled: function() {
                var n = e.defer();
                return window.Instagram ? (Instagram.isInstalled(function(e, t) {
                    e ? n.reject(e) : n.resolve(t);
                }), n.promise) : (console.error("Tried to call Instagram.isInstalled but the Instagram plugin isn't installed!"), 
                n.resolve(null), n.promise);
            }
        };
    } ]), angular.module("ngCordova.plugins.keyboard", []).factory("$cordovaKeyboard", [ "$rootScope", function(e) {
        var n = function() {
            e.$evalAsync(function() {
                e.$broadcast("$cordovaKeyboard:show");
            });
        }, t = function() {
            e.$evalAsync(function() {
                e.$broadcast("$cordovaKeyboard:hide");
            });
        };
        return document.addEventListener("deviceready", function() {
            cordova.plugins.Keyboard && (window.addEventListener("native.keyboardshow", n, !1), 
            window.addEventListener("native.keyboardhide", t, !1));
        }), {
            hideAccessoryBar: function(e) {
                return cordova.plugins.Keyboard.hideKeyboardAccessoryBar(e);
            },
            close: function() {
                return cordova.plugins.Keyboard.close();
            },
            show: function() {
                return cordova.plugins.Keyboard.show();
            },
            disableScroll: function(e) {
                return cordova.plugins.Keyboard.disableScroll(e);
            },
            isVisible: function() {
                return cordova.plugins.Keyboard.isVisible;
            },
            clearShowWatch: function() {
                document.removeEventListener("native.keyboardshow", n), e.$$listeners["$cordovaKeyboard:show"] = [];
            },
            clearHideWatch: function() {
                document.removeEventListener("native.keyboardhide", t), e.$$listeners["$cordovaKeyboard:hide"] = [];
            }
        };
    } ]), angular.module("ngCordova.plugins.keychain", []).factory("$cordovaKeychain", [ "$q", function(e) {
        return {
            getForKey: function(n, t) {
                var r = e.defer(), o = new Keychain();
                return o.getForKey(r.resolve, r.reject, n, t), r.promise;
            },
            setForKey: function(n, t, r) {
                var o = e.defer(), i = new Keychain();
                return i.setForKey(o.resolve, o.reject, n, t, r), o.promise;
            },
            removeForKey: function(n, t) {
                var r = e.defer(), o = new Keychain();
                return o.removeForKey(r.resolve, r.reject, n, t), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.launchNavigator", []).factory("$cordovaLaunchNavigator", [ "$q", function(e) {
        return {
            navigate: function(n, t, r) {
                var o = e.defer();
                return launchnavigator.navigate(n, t, function() {
                    o.resolve();
                }, function(e) {
                    o.reject(e);
                }, r), o.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.localNotification", []).factory("$cordovaLocalNotification", [ "$q", "$window", "$rootScope", "$timeout", function(e, n, t, r) {
        return document.addEventListener("deviceready", function() {
            n.cordova && n.cordova.plugins && n.cordova.plugins.notification && n.cordova.plugins.notification.local && (n.cordova.plugins.notification.local.on("schedule", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:schedule", e, n);
                });
            }), n.cordova.plugins.notification.local.on("trigger", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:trigger", e, n);
                });
            }), n.cordova.plugins.notification.local.on("update", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:update", e, n);
                });
            }), n.cordova.plugins.notification.local.on("clear", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:clear", e, n);
                });
            }), n.cordova.plugins.notification.local.on("clearall", function(e) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:clearall", e);
                });
            }), n.cordova.plugins.notification.local.on("cancel", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:cancel", e, n);
                });
            }), n.cordova.plugins.notification.local.on("cancelall", function(e) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:cancelall", e);
                });
            }), n.cordova.plugins.notification.local.on("click", function(e, n) {
                r(function() {
                    t.$broadcast("$cordovaLocalNotification:click", e, n);
                });
            }));
        }, !1), {
            schedule: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.schedule(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            add: function(t, r) {
                console.warn('Deprecated: use "schedule" instead.');
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.schedule(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            update: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.update(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            clear: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.clear(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            clearAll: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.clearAll(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            cancel: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.cancel(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            cancelAll: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.cancelAll(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            isPresent: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.isPresent(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            isScheduled: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.isScheduled(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            isTriggered: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.isTriggered(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            hasPermission: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.hasPermission(function(e) {
                    e ? r.resolve(e) : r.reject(e);
                }, t), r.promise;
            },
            registerPermission: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.registerPermission(function(e) {
                    e ? r.resolve(e) : r.reject(e);
                }, t), r.promise;
            },
            promptForPermission: function(t) {
                console.warn('Deprecated: use "registerPermission" instead.');
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.registerPermission(function(e) {
                    e ? r.resolve(e) : r.reject(e);
                }, t), r.promise;
            },
            getAllIds: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getAllIds(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getIds: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getIds(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getScheduledIds: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getScheduledIds(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getTriggeredIds: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getTriggeredIds(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            get: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.get(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            getAll: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getAll(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getScheduled: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.getScheduled(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            getAllScheduled: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getAllScheduled(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getTriggered: function(t, r) {
                var o = e.defer();
                return r = r || null, n.cordova.plugins.notification.local.getTriggered(t, function(e) {
                    o.resolve(e);
                }, r), o.promise;
            },
            getAllTriggered: function(t) {
                var r = e.defer();
                return t = t || null, n.cordova.plugins.notification.local.getAllTriggered(function(e) {
                    r.resolve(e);
                }, t), r.promise;
            },
            getDefaults: function() {
                return n.cordova.plugins.notification.local.getDefaults();
            },
            setDefaults: function(e) {
                n.cordova.plugins.notification.local.setDefaults(e);
            }
        };
    } ]), angular.module("ngCordova.plugins.mMediaAds", []).factory("$cordovaMMediaAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.mMedia.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.mMedia.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.mMedia.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.mMedia.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.mMedia.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.mMedia.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.mMedia.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.mMedia.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.media", []).service("NewMedia", [ "$q", "$interval", function(e, n) {
        function t(e) {
            angular.isDefined(u) || (u = n(function() {
                0 > f && (f = e.getDuration(), a && f > 0 && a.notify({
                    duration: f
                })), e.getCurrentPosition(function(e) {
                    e > -1 && (d = e);
                }, function(e) {
                    console.log("Error getting pos=" + e);
                }), a && a.notify({
                    position: d
                });
            }, 1e3));
        }
        function r() {
            angular.isDefined(u) && (n.cancel(u), u = void 0);
        }
        function o() {
            d = -1, f = -1;
        }
        function i(e) {
            this.media = new Media(e, function(e) {
                r(), o(), a.resolve(e);
            }, function(e) {
                r(), o(), a.reject(e);
            }, function(e) {
                l = e, a.notify({
                    status: l
                });
            });
        }
        var a, c, s, u, l = null, d = -1, f = -1;
        return i.prototype.play = function(n) {
            return a = e.defer(), "object" != typeof n && (n = {}), this.media.play(n), t(this.media), 
            a.promise;
        }, i.prototype.pause = function() {
            r(), this.media.pause();
        }, i.prototype.stop = function() {
            this.media.stop();
        }, i.prototype.release = function() {
            this.media.release(), this.media = void 0;
        }, i.prototype.seekTo = function(e) {
            this.media.seekTo(e);
        }, i.prototype.setVolume = function(e) {
            this.media.setVolume(e);
        }, i.prototype.startRecord = function() {
            this.media.startRecord();
        }, i.prototype.stopRecord = function() {
            this.media.stopRecord();
        }, i.prototype.currentTime = function() {
            return c = e.defer(), this.media.getCurrentPosition(function(e) {
                c.resolve(e);
            }), c.promise;
        }, i.prototype.getDuration = function() {
            return s = e.defer(), this.media.getDuration(function(e) {
                s.resolve(e);
            }), s.promise;
        }, i;
    } ]).factory("$cordovaMedia", [ "NewMedia", function(e) {
        return {
            newMedia: function(n) {
                return new e(n);
            }
        };
    } ]), angular.module("ngCordova.plugins.mobfoxAds", []).factory("$cordovaMobFoxAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.MobFox.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.MobFox.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.MobFox.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.MobFox.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.MobFox.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.MobFox.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.MobFox.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.MobFox.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins", [ "ngCordova.plugins.actionSheet", "ngCordova.plugins.adMob", "ngCordova.plugins.appAvailability", "ngCordova.plugins.appRate", "ngCordova.plugins.appVersion", "ngCordova.plugins.backgroundGeolocation", "ngCordova.plugins.badge", "ngCordova.plugins.barcodeScanner", "ngCordova.plugins.batteryStatus", "ngCordova.plugins.beacon", "ngCordova.plugins.ble", "ngCordova.plugins.bluetoothSerial", "ngCordova.plugins.brightness", "ngCordova.plugins.calendar", "ngCordova.plugins.camera", "ngCordova.plugins.capture", "ngCordova.plugins.clipboard", "ngCordova.plugins.contacts", "ngCordova.plugins.datePicker", "ngCordova.plugins.device", "ngCordova.plugins.deviceMotion", "ngCordova.plugins.deviceOrientation", "ngCordova.plugins.dialogs", "ngCordova.plugins.emailComposer", "ngCordova.plugins.facebook", "ngCordova.plugins.facebookAds", "ngCordova.plugins.file", "ngCordova.plugins.fileTransfer", "ngCordova.plugins.fileOpener2", "ngCordova.plugins.flashlight", "ngCordova.plugins.flurryAds", "ngCordova.plugins.ga", "ngCordova.plugins.geolocation", "ngCordova.plugins.globalization", "ngCordova.plugins.googleAds", "ngCordova.plugins.googleAnalytics", "ngCordova.plugins.googleMap", "ngCordova.plugins.googlePlayGame", "ngCordova.plugins.googlePlus", "ngCordova.plugins.healthKit", "ngCordova.plugins.httpd", "ngCordova.plugins.iAd", "ngCordova.plugins.imagePicker", "ngCordova.plugins.inAppBrowser", "ngCordova.plugins.instagram", "ngCordova.plugins.keyboard", "ngCordova.plugins.keychain", "ngCordova.plugins.launchNavigator", "ngCordova.plugins.localNotification", "ngCordova.plugins.media", "ngCordova.plugins.mMediaAds", "ngCordova.plugins.mobfoxAds", "ngCordova.plugins.mopubAds", "ngCordova.plugins.nativeAudio", "ngCordova.plugins.network", "ngCordovaOauth", "ngCordova.plugins.pinDialog", "ngCordova.plugins.preferences", "ngCordova.plugins.printer", "ngCordova.plugins.progressIndicator", "ngCordova.plugins.push", "ngCordova.plugins.push_v5", "ngCordova.plugins.sms", "ngCordova.plugins.socialSharing", "ngCordova.plugins.spinnerDialog", "ngCordova.plugins.splashscreen", "ngCordova.plugins.sqlite", "ngCordova.plugins.statusbar", "ngCordova.plugins.toast", "ngCordova.plugins.touchid", "ngCordova.plugins.vibration", "ngCordova.plugins.videoCapturePlus", "ngCordova.plugins.zip", "ngCordova.plugins.insomnia" ]), 
    angular.module("ngCordova.plugins.mopubAds", []).factory("$cordovaMoPubAds", [ "$q", "$window", function(e, n) {
        return {
            setOptions: function(t) {
                var r = e.defer();
                return n.MoPub.setOptions(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            createBanner: function(t) {
                var r = e.defer();
                return n.MoPub.createBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            removeBanner: function() {
                var t = e.defer();
                return n.MoPub.removeBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            showBanner: function(t) {
                var r = e.defer();
                return n.MoPub.showBanner(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showBannerAtXY: function(t, r) {
                var o = e.defer();
                return n.MoPub.showBannerAtXY(t, r, function() {
                    o.resolve();
                }, function() {
                    o.reject();
                }), o.promise;
            },
            hideBanner: function() {
                var t = e.defer();
                return n.MoPub.hideBanner(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            },
            prepareInterstitial: function(t) {
                var r = e.defer();
                return n.MoPub.prepareInterstitial(t, function() {
                    r.resolve();
                }, function() {
                    r.reject();
                }), r.promise;
            },
            showInterstitial: function() {
                var t = e.defer();
                return n.MoPub.showInterstitial(function() {
                    t.resolve();
                }, function() {
                    t.reject();
                }), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.nativeAudio", []).factory("$cordovaNativeAudio", [ "$q", "$window", function(e, n) {
        return {
            preloadSimple: function(t, r) {
                var o = e.defer();
                return n.plugins.NativeAudio.preloadSimple(t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            },
            preloadComplex: function(t, r, o, i) {
                var a = e.defer();
                return n.plugins.NativeAudio.preloadComplex(t, r, o, i, function(e) {
                    a.resolve(e);
                }, function(e) {
                    a.reject(e);
                }), a.promise;
            },
            play: function(t, r) {
                var o = e.defer();
                return n.plugins.NativeAudio.play(t, r, function(e) {
                    o.reject(e);
                }, function(e) {
                    o.resolve(e);
                }), o.promise;
            },
            stop: function(t) {
                var r = e.defer();
                return n.plugins.NativeAudio.stop(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            loop: function(t) {
                var r = e.defer();
                return n.plugins.NativeAudio.loop(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            unload: function(t) {
                var r = e.defer();
                return n.plugins.NativeAudio.unload(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            setVolumeForComplexAsset: function(t, r) {
                var o = e.defer();
                return n.plugins.NativeAudio.setVolumeForComplexAsset(t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.network", []).factory("$cordovaNetwork", [ "$rootScope", "$timeout", function(e, n) {
        var t = function() {
            var t = navigator.connection.type;
            n(function() {
                e.$broadcast("$cordovaNetwork:offline", t);
            });
        }, r = function() {
            var t = navigator.connection.type;
            n(function() {
                e.$broadcast("$cordovaNetwork:online", t);
            });
        };
        return document.addEventListener("deviceready", function() {
            navigator.connection && (document.addEventListener("offline", t, !1), document.addEventListener("online", r, !1));
        }), {
            getNetwork: function() {
                return navigator.connection.type;
            },
            isOnline: function() {
                var e = navigator.connection.type;
                return e !== Connection.UNKNOWN && e !== Connection.NONE;
            },
            isOffline: function() {
                var e = navigator.connection.type;
                return e === Connection.UNKNOWN || e === Connection.NONE;
            },
            clearOfflineWatch: function() {
                document.removeEventListener("offline", t), e.$$listeners["$cordovaNetwork:offline"] = [];
            },
            clearOnlineWatch: function() {
                document.removeEventListener("online", r), e.$$listeners["$cordovaNetwork:online"] = [];
            }
        };
    } ]).run([ "$injector", function(e) {
        e.get("$cordovaNetwork");
    } ]), angular.module("ngCordova.plugins.pinDialog", []).factory("$cordovaPinDialog", [ "$q", "$window", function(e, n) {
        return {
            prompt: function(t, r, o) {
                var i = e.defer();
                return n.plugins.pinDialog.prompt(t, function(e) {
                    i.resolve(e);
                }, r, o), i.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.preferences", []).factory("$cordovaPreferences", [ "$window", "$q", function(e, n) {
        return {
            pluginNotEnabledMessage: "Plugin not enabled",
            decoratePromise: function(e) {
                e.success = function(n) {
                    return e.then(n), e;
                }, e.error = function(n) {
                    return e.then(null, n), e;
                };
            },
            store: function(t, r, o) {
                function i(e) {
                    c.resolve(e);
                }
                function a(e) {
                    c.reject(new Error(e));
                }
                var c = n.defer(), s = c.promise;
                if (e.plugins) {
                    var u;
                    u = 3 === arguments.length ? e.plugins.appPreferences.store(o, t, r) : e.plugins.appPreferences.store(t, r), 
                    u.then(i, a);
                } else c.reject(new Error(this.pluginNotEnabledMessage));
                return this.decoratePromise(s), s;
            },
            fetch: function(t, r) {
                function o(e) {
                    a.resolve(e);
                }
                function i(e) {
                    a.reject(new Error(e));
                }
                var a = n.defer(), c = a.promise;
                if (e.plugins) {
                    var s;
                    s = 2 === arguments.length ? e.plugins.appPreferences.fetch(r, t) : e.plugins.appPreferences.fetch(t), 
                    s.then(o, i);
                } else a.reject(new Error(this.pluginNotEnabledMessage));
                return this.decoratePromise(c), c;
            },
            remove: function(t, r) {
                function o(e) {
                    a.resolve(e);
                }
                function i(e) {
                    a.reject(new Error(e));
                }
                var a = n.defer(), c = a.promise;
                if (e.plugins) {
                    var s;
                    s = 2 === arguments.length ? e.plugins.appPreferences.remove(r, t) : e.plugins.appPreferences.remove(t), 
                    s.then(o, i);
                } else a.reject(new Error(this.pluginNotEnabledMessage));
                return this.decoratePromise(c), c;
            },
            show: function() {
                function t(e) {
                    o.resolve(e);
                }
                function r(e) {
                    o.reject(new Error(e));
                }
                var o = n.defer(), i = o.promise;
                return e.plugins ? e.plugins.appPreferences.show().then(t, r) : o.reject(new Error(this.pluginNotEnabledMessage)), 
                this.decoratePromise(i), i;
            }
        };
    } ]), angular.module("ngCordova.plugins.printer", []).factory("$cordovaPrinter", [ "$q", "$window", function(e, n) {
        return {
            isAvailable: function() {
                var t = e.defer();
                return n.plugin.printer.isAvailable(function(e) {
                    t.resolve(e);
                }), t.promise;
            },
            print: function(t, r) {
                var o = e.defer();
                return n.plugin.printer.print(t, r, function() {
                    o.resolve();
                }), o.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.progressIndicator", []).factory("$cordovaProgress", [ function() {
        return {
            show: function(e) {
                var n = e || "Please wait...";
                return ProgressIndicator.show(n);
            },
            showSimple: function(e) {
                var n = e || !1;
                return ProgressIndicator.showSimple(n);
            },
            showSimpleWithLabel: function(e, n) {
                var t = e || !1, r = n || "Loading...";
                return ProgressIndicator.showSimpleWithLabel(t, r);
            },
            showSimpleWithLabelDetail: function(e, n, t) {
                var r = e || !1, o = n || "Loading...", i = t || "Please wait";
                return ProgressIndicator.showSimpleWithLabelDetail(r, o, i);
            },
            showDeterminate: function(e, n) {
                var t = e || !1, r = n || 5e4;
                return ProgressIndicator.showDeterminate(t, r);
            },
            showDeterminateWithLabel: function(e, n, t) {
                var r = e || !1, o = n || 5e4, i = t || "Loading...";
                return ProgressIndicator.showDeterminateWithLabel(r, o, i);
            },
            showAnnular: function(e, n) {
                var t = e || !1, r = n || 5e4;
                return ProgressIndicator.showAnnular(t, r);
            },
            showAnnularWithLabel: function(e, n, t) {
                var r = e || !1, o = n || 5e4, i = t || "Loading...";
                return ProgressIndicator.showAnnularWithLabel(r, o, i);
            },
            showBar: function(e, n) {
                var t = e || !1, r = n || 5e4;
                return ProgressIndicator.showBar(t, r);
            },
            showBarWithLabel: function(e, n, t) {
                var r = e || !1, o = n || 5e4, i = t || "Loading...";
                return ProgressIndicator.showBarWithLabel(r, o, i);
            },
            showSuccess: function(e, n) {
                var t = e || !1, r = n || "Success";
                return ProgressIndicator.showSuccess(t, r);
            },
            showText: function(e, n, t) {
                var r = e || !1, o = n || "Warning", i = t || "center";
                return ProgressIndicator.showText(r, o, i);
            },
            hide: function() {
                return ProgressIndicator.hide();
            }
        };
    } ]), angular.module("ngCordova.plugins.push", []).factory("$cordovaPush", [ "$q", "$window", "$rootScope", "$timeout", function(e, n, t, r) {
        return {
            onNotification: function(e) {
                r(function() {
                    t.$broadcast("$cordovaPush:notificationReceived", e);
                });
            },
            register: function(t) {
                var r, o = e.defer();
                return void 0 !== t && void 0 === t.ecb && (r = null === document.querySelector("[ng-app]") ? "document.body" : "document.querySelector('[ng-app]')", 
                t.ecb = "angular.element(" + r + ").injector().get('$cordovaPush').onNotification"), 
                n.plugins.pushNotification.register(function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }, t), o.promise;
            },
            unregister: function(t) {
                var r = e.defer();
                return n.plugins.pushNotification.unregister(function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            setBadgeNumber: function(t) {
                var r = e.defer();
                return n.plugins.pushNotification.setApplicationIconBadgeNumber(function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.push_v5", []).factory("$cordovaPushV5", [ "$q", "$rootScope", "$timeout", function(e, n, t) {
        var r;
        return {
            initialize: function(n) {
                var t = e.defer();
                return r = PushNotification.init(n), t.resolve(r), t.promise;
            },
            onNotification: function() {
                t(function() {
                    r.on("notification", function(e) {
                        n.$emit("$cordovaPushV5:notificationReceived", e);
                    });
                });
            },
            onError: function() {
                t(function() {
                    r.on("error", function(e) {
                        n.$emit("$cordovaPushV5:errorOccurred", e);
                    });
                });
            },
            register: function() {
                var n = e.defer();
                return void 0 === r ? n.reject(new Error("init must be called before any other operation")) : r.on("registration", function(e) {
                    n.resolve(e.registrationId);
                }), n.promise;
            },
            unregister: function() {
                var n = e.defer();
                return void 0 === r ? n.reject(new Error("init must be called before any other operation")) : r.unregister(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }), n.promise;
            },
            setBadgeNumber: function(n) {
                var t = e.defer();
                return void 0 === r ? t.reject(new Error("init must be called before any other operation")) : r.setApplicationIconBadgeNumber(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.sms", []).factory("$cordovaSms", [ "$q", function(e) {
        return {
            send: function(n, t, r) {
                var o = e.defer();
                return sms.send(n, t, r, function(e) {
                    o.resolve(e);
                }, function(e) {
                    o.reject(e);
                }), o.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.socialSharing", []).factory("$cordovaSocialSharing", [ "$q", "$window", function(e, n) {
        return {
            share: function(t, r, o, i) {
                var a = e.defer();
                return r = r || null, o = o || null, i = i || null, n.plugins.socialsharing.share(t, r, o, i, function() {
                    a.resolve(!0);
                }, function() {
                    a.reject(!1);
                }), a.promise;
            },
            shareViaTwitter: function(t, r, o) {
                var i = e.defer();
                return r = r || null, o = o || null, n.plugins.socialsharing.shareViaTwitter(t, r, o, function() {
                    i.resolve(!0);
                }, function() {
                    i.reject(!1);
                }), i.promise;
            },
            shareViaWhatsApp: function(t, r, o) {
                var i = e.defer();
                return r = r || null, o = o || null, n.plugins.socialsharing.shareViaWhatsApp(t, r, o, function() {
                    i.resolve(!0);
                }, function() {
                    i.reject(!1);
                }), i.promise;
            },
            shareViaFacebook: function(t, r, o) {
                var i = e.defer();
                return t = t || null, r = r || null, o = o || null, n.plugins.socialsharing.shareViaFacebook(t, r, o, function() {
                    i.resolve(!0);
                }, function() {
                    i.reject(!1);
                }), i.promise;
            },
            shareViaFacebookWithPasteMessageHint: function(t, r, o, i) {
                var a = e.defer();
                return r = r || null, o = o || null, n.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(t, r, o, i, function() {
                    a.resolve(!0);
                }, function() {
                    a.reject(!1);
                }), a.promise;
            },
            shareViaSMS: function(t, r) {
                var o = e.defer();
                return n.plugins.socialsharing.shareViaSMS(t, r, function() {
                    o.resolve(!0);
                }, function() {
                    o.reject(!1);
                }), o.promise;
            },
            shareViaEmail: function(t, r, o, i, a, c) {
                var s = e.defer();
                return o = o || null, i = i || null, a = a || null, c = c || null, n.plugins.socialsharing.shareViaEmail(t, r, o, i, a, c, function() {
                    s.resolve(!0);
                }, function() {
                    s.reject(!1);
                }), s.promise;
            },
            shareVia: function(t, r, o, i, a) {
                var c = e.defer();
                return r = r || null, o = o || null, i = i || null, a = a || null, n.plugins.socialsharing.shareVia(t, r, o, i, a, function() {
                    c.resolve(!0);
                }, function() {
                    c.reject(!1);
                }), c.promise;
            },
            canShareViaEmail: function() {
                var t = e.defer();
                return n.plugins.socialsharing.canShareViaEmail(function() {
                    t.resolve(!0);
                }, function() {
                    t.reject(!1);
                }), t.promise;
            },
            canShareVia: function(t, r, o, i, a) {
                var c = e.defer();
                return n.plugins.socialsharing.canShareVia(t, r, o, i, a, function(e) {
                    c.resolve(e);
                }, function(e) {
                    c.reject(e);
                }), c.promise;
            },
            available: function() {
                var n = e.defer();
                window.plugins.socialsharing.available(function(e) {
                    e ? n.resolve() : n.reject();
                });
            }
        };
    } ]), angular.module("ngCordova.plugins.spinnerDialog", []).factory("$cordovaSpinnerDialog", [ "$window", function(e) {
        return {
            show: function(n, t, r) {
                return r = r || !1, e.plugins.spinnerDialog.show(n, t, r);
            },
            hide: function() {
                return e.plugins.spinnerDialog.hide();
            }
        };
    } ]), angular.module("ngCordova.plugins.splashscreen", []).factory("$cordovaSplashscreen", [ function() {
        return {
            hide: function() {
                return navigator.splashscreen.hide();
            },
            show: function() {
                return navigator.splashscreen.show();
            }
        };
    } ]), angular.module("ngCordova.plugins.sqlite", []).factory("$cordovaSQLite", [ "$q", "$window", function(e, n) {
        return {
            openDB: function(e, t) {
                return angular.isObject(e) && !angular.isString(e) ? ("undefined" != typeof t && (e.bgType = t), 
                n.sqlitePlugin.openDatabase(e)) : n.sqlitePlugin.openDatabase({
                    name: e,
                    bgType: t
                });
            },
            execute: function(n, t, r) {
                var o = e.defer();
                return n.transaction(function(e) {
                    e.executeSql(t, r, function(e, n) {
                        o.resolve(n);
                    }, function(e, n) {
                        o.reject(n);
                    });
                }), o.promise;
            },
            insertCollection: function(n, t, r) {
                var o = e.defer(), i = r.slice(0);
                return n.transaction(function(e) {
                    !function n() {
                        var r = i.splice(0, 1)[0];
                        try {
                            e.executeSql(t, r, function(e, t) {
                                0 === i.length ? o.resolve(t) : n();
                            }, function(e, n) {
                                o.reject(n);
                            });
                        } catch (a) {
                            o.reject(a);
                        }
                    }();
                }), o.promise;
            },
            nestedExecute: function(n, t, r, o, i) {
                var a = e.defer();
                return n.transaction(function(e) {
                    e.executeSql(t, o, function(e, n) {
                        a.resolve(n), e.executeSql(r, i, function(e, n) {
                            a.resolve(n);
                        });
                    });
                }, function(e, n) {
                    a.reject(n);
                }), a.promise;
            },
            deleteDB: function(t) {
                var r = e.defer();
                return n.sqlitePlugin.deleteDatabase(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.statusbar", []).factory("$cordovaStatusbar", [ function() {
        return {
            overlaysWebView: function(e) {
                return StatusBar.overlaysWebView(!!e);
            },
            STYLES: {
                DEFAULT: 0,
                LIGHT_CONTENT: 1,
                BLACK_TRANSLUCENT: 2,
                BLACK_OPAQUE: 3
            },
            style: function(e) {
                switch (e) {
                  case 0:
                    return StatusBar.styleDefault();

                  case 1:
                    return StatusBar.styleLightContent();

                  case 2:
                    return StatusBar.styleBlackTranslucent();

                  case 3:
                    return StatusBar.styleBlackOpaque();

                  default:
                    return StatusBar.styleDefault();
                }
            },
            styleColor: function(e) {
                return StatusBar.backgroundColorByName(e);
            },
            styleHex: function(e) {
                return StatusBar.backgroundColorByHexString(e);
            },
            hide: function() {
                return StatusBar.hide();
            },
            show: function() {
                return StatusBar.show();
            },
            isVisible: function() {
                return StatusBar.isVisible;
            }
        };
    } ]), angular.module("ngCordova.plugins.toast", []).factory("$cordovaToast", [ "$q", "$window", function(e, n) {
        return {
            showShortTop: function(t) {
                var r = e.defer();
                return n.plugins.toast.showShortTop(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            showShortCenter: function(t) {
                var r = e.defer();
                return n.plugins.toast.showShortCenter(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            showShortBottom: function(t) {
                var r = e.defer();
                return n.plugins.toast.showShortBottom(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            showLongTop: function(t) {
                var r = e.defer();
                return n.plugins.toast.showLongTop(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            showLongCenter: function(t) {
                var r = e.defer();
                return n.plugins.toast.showLongCenter(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            showLongBottom: function(t) {
                var r = e.defer();
                return n.plugins.toast.showLongBottom(t, function(e) {
                    r.resolve(e);
                }, function(e) {
                    r.reject(e);
                }), r.promise;
            },
            show: function(t, r, o) {
                var i = e.defer();
                return n.plugins.toast.show(t, r, o, function(e) {
                    i.resolve(e);
                }, function(e) {
                    i.reject(e);
                }), i.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.touchid", []).factory("$cordovaTouchID", [ "$q", function(e) {
        return {
            checkSupport: function() {
                var n = e.defer();
                return window.cordova ? touchid.checkSupport(function(e) {
                    n.resolve(e);
                }, function(e) {
                    n.reject(e);
                }) : n.reject("Not supported without cordova.js"), n.promise;
            },
            authenticate: function(n) {
                var t = e.defer();
                return window.cordova ? touchid.authenticate(function(e) {
                    t.resolve(e);
                }, function(e) {
                    t.reject(e);
                }, n) : t.reject("Not supported without cordova.js"), t.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.upsPush", []).factory("$cordovaUpsPush", [ "$q", "$window", "$rootScope", "$timeout", function(e, n, t, r) {
        return {
            register: function(o) {
                var i = e.defer();
                return n.push.register(function(e) {
                    r(function() {
                        t.$broadcast("$cordovaUpsPush:notificationReceived", e);
                    });
                }, function() {
                    i.resolve();
                }, function(e) {
                    i.reject(e);
                }, o), i.promise;
            },
            unregister: function(t) {
                var r = e.defer();
                return n.push.unregister(function() {
                    r.resolve();
                }, function(e) {
                    r.reject(e);
                }, t), r.promise;
            },
            setBadgeNumber: function(t) {
                var r = e.defer();
                return n.push.setApplicationIconBadgeNumber(function() {
                    r.resolve();
                }, t), r.promise;
            }
        };
    } ]), angular.module("ngCordova.plugins.vibration", []).factory("$cordovaVibration", [ function() {
        return {
            vibrate: function(e) {
                return navigator.notification.vibrate(e);
            },
            vibrateWithPattern: function(e, n) {
                return navigator.notification.vibrateWithPattern(e, n);
            },
            cancelVibration: function() {
                return navigator.notification.cancelVibration();
            }
        };
    } ]), angular.module("ngCordova.plugins.videoCapturePlus", []).provider("$cordovaVideoCapturePlus", [ function() {
        var e = {};
        this.setLimit = function(n) {
            e.limit = n;
        }, this.setMaxDuration = function(n) {
            e.duration = n;
        }, this.setHighQuality = function(n) {
            e.highquality = n;
        }, this.useFrontCamera = function(n) {
            e.frontcamera = n;
        }, this.setPortraitOverlay = function(n) {
            e.portraitOverlay = n;
        }, this.setLandscapeOverlay = function(n) {
            e.landscapeOverlay = n;
        }, this.setOverlayText = function(n) {
            e.overlayText = n;
        }, this.$get = [ "$q", "$window", function(n, t) {
            return {
                captureVideo: function(r) {
                    var o = n.defer();
                    return t.plugins.videocaptureplus ? (t.plugins.videocaptureplus.captureVideo(o.resolve, o.reject, angular.extend({}, e, r)), 
                    o.promise) : (o.resolve(null), o.promise);
                }
            };
        } ];
    } ]), angular.module("ngCordova.plugins.zip", []).factory("$cordovaZip", [ "$q", "$window", function(e, n) {
        return {
            unzip: function(t, r) {
                var o = e.defer();
                return n.zip.unzip(t, r, function(e) {
                    0 === e ? o.resolve() : o.reject();
                }, function(e) {
                    o.notify(e);
                }), o.promise;
            }
        };
    } ]), angular.module("oauth.providers", [ "oauth.utils" ]).factory("$cordovaOauth", [ "$q", "$http", "$cordovaOauthUtility", function(e, n, t) {
        return {
            azureAD: function(r, o, i) {
                var a = e.defer();
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) {
                        var s = window.open("https://login.microsoftonline.com/" + o + "/oauth2/authorize?response_type=code&client_id=" + r + "&redirect_uri=http://localhost/callback", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf("http://localhost/callback")) {
                                var t = e.url.split("code=")[1];
                                console.log(t), n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                                n({
                                    method: "post",
                                    url: "https://login.microsoftonline.com/" + o + "/oauth2/token",
                                    data: "client_id=" + r + "&code=" + t + "&redirect_uri=http://localhost/callback&grant_type=authorization_code&resource=" + i
                                }).success(function(e) {
                                    a.resolve(e);
                                }).error(function(e, n) {
                                    a.reject("Problem authenticating");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        s.close();
                                    }, 10);
                                });
                            }
                        }), s.addEventListener("exit", function(e) {
                            a.reject("The sign in flow was canceled");
                        });
                    } else a.reject("Could not find InAppBrowser plugin");
                } else a.reject("Cannot authenticate via a web browser");
                return a.promise;
            },
            adfs: function(r, o, i) {
                var a = e.defer();
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) {
                        var s = window.open(o + "/adfs/oauth2/authorize?response_type=code&client_id=" + r + "&redirect_uri=http://localhost/callback&resource=" + i, "_blank", "location=no");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf("http://localhost/callback")) {
                                var t = e.url.split("code=")[1];
                                n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                    method: "post",
                                    url: o + "/adfs/oauth2/token",
                                    data: "client_id=" + r + "&code=" + t + "&redirect_uri=http://localhost/callback&grant_type=authorization_code"
                                }).success(function(e) {
                                    a.resolve(e);
                                }).error(function(e, n) {
                                    a.reject("Problem authenticating");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        s.close();
                                    }, 10);
                                });
                            }
                        }), s.addEventListener("exit", function(e) {
                            a.reject("The sign in flow was canceled");
                        });
                    } else a.reject("Could not find InAppBrowser plugin");
                } else a.reject("Cannot authenticate via a web browser");
                return a.promise;
            },
            dropbox: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://www.dropbox.com/1/oauth2/authorize?client_id=" + n + "&redirect_uri=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve({
                                    access_token: r.access_token,
                                    token_type: r.token_type,
                                    uid: r.uid
                                }) : o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            digitalOcean: function(r, o, i) {
                var a = e.defer();
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) {
                        var s = "http://localhost/callback";
                        void 0 !== i && i.hasOwnProperty("redirect_uri") && (s = i.redirect_uri);
                        var u = window.open("https://cloud.digitalocean.com/v1/oauth/authorize?client_id=" + r + "&redirect_uri=" + s + "&response_type=code&scope=read%20write", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        u.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(s)) {
                                var t = e.url.split("code=")[1];
                                n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                    method: "post",
                                    url: "https://cloud.digitalocean.com/v1/oauth/token",
                                    data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + s + "&grant_type=authorization_code&code=" + t
                                }).success(function(e) {
                                    a.resolve(e);
                                }).error(function(e, n) {
                                    a.reject("Problem authenticating");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        u.close();
                                    }, 10);
                                });
                            }
                        }), u.addEventListener("exit", function(e) {
                            a.reject("The sign in flow was canceled");
                        });
                    } else a.reject("Could not find InAppBrowser plugin");
                } else a.reject("Cannot authenticate via a web browser");
                return a.promise;
            },
            google: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://accounts.google.com/o/oauth2/auth?client_id=" + n + "&redirect_uri=" + c + "&scope=" + r.join(" ") + "&approval_prompt=force&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                s.removeEventListener("exit", function(e) {}), s.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    token_type: r.token_type,
                                    expires_in: r.expires_in
                                }) : i.reject("Problem authenticating");
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            github: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://github.com/login/oauth/authorize?client_id=" + r + "&redirect_uri=" + u + "&scope=" + i.join(","), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(u) && (requestToken = e.url.split("code=")[1], n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                            n.defaults.headers.post.accept = "application/json", n({
                                method: "post",
                                url: "https://github.com/login/oauth/access_token",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + u + "&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            facebook: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = "https://www.facebook.com/v2.0/dialog/oauth?client_id=" + n + "&redirect_uri=" + c + "&response_type=token&scope=" + r.join(",");
                        void 0 !== o && o.hasOwnProperty("auth_type") && (s += "&auth_type=" + o.auth_type);
                        var u = window.open(s, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        u.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                u.removeEventListener("exit", function(e) {}), u.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in
                                }) : 0 !== e.url.indexOf("error_code=100") ? i.reject("Facebook returned error_code=100: Invalid permissions") : i.reject("Problem authenticating");
                            }
                        }), u.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            linkedin: function(r, o, i, a, c) {
                var s = e.defer();
                if (window.cordova) {
                    var u = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(u) === !0) {
                        var l = "http://localhost/callback";
                        void 0 !== c && c.hasOwnProperty("redirect_uri") && (l = c.redirect_uri);
                        var d = window.open("https://www.linkedin.com/uas/oauth2/authorization?client_id=" + r + "&redirect_uri=" + l + "&scope=" + i.join(" ") + "&response_type=code&state=" + a, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        d.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(l) && (requestToken = e.url.split("code=")[1].split("&")[0], 
                            n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                method: "post",
                                url: "https://www.linkedin.com/uas/oauth2/accessToken",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + l + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                s.resolve(e);
                            }).error(function(e, n) {
                                s.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    d.close();
                                }, 10);
                            }));
                        }), d.addEventListener("exit", function(e) {
                            s.reject("The sign in flow was canceled");
                        });
                    } else s.reject("Could not find InAppBrowser plugin");
                } else s.reject("Cannot authenticate via a web browser");
                return s.promise;
            },
            instagram: function(n, r, o) {
                var i = e.defer(), a = {
                    code: "?",
                    token: "#"
                };
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) {
                        var s = "http://localhost/callback", u = "token";
                        void 0 !== o && (o.hasOwnProperty("redirect_uri") && (s = o.redirect_uri), o.hasOwnProperty("response_type") && (u = o.response_type));
                        var l = "";
                        r && r.length > 0 && (l = "&scope" + r.join("+"));
                        var d = window.open("https://api.instagram.com/oauth/authorize/?client_id=" + n + "&redirect_uri=" + s + l + "&response_type=" + u, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        d.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(s)) {
                                d.removeEventListener("exit", function(e) {}), d.close();
                                var n = e.url.split(a[u])[1], r = t.parseResponseParameters(n);
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token
                                }) : void 0 !== r.code && null !== r.code ? i.resolve({
                                    code: r.code
                                }) : i.reject("Problem authenticating");
                            }
                        }), d.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            box: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://app.box.com/api/oauth2/authorize/?client_id=" + r + "&redirect_uri=" + u + "&state=" + i + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(u) && (requestToken = e.url.split("code=")[1], n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                            n({
                                method: "post",
                                url: "https://app.box.com/api/oauth2/token",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + u + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            reddit: function(r, o, i, a, c) {
                var s = e.defer();
                if (window.cordova) {
                    var u = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(u) === !0) {
                        var l = "http://localhost/callback";
                        void 0 !== c && c.hasOwnProperty("redirect_uri") && (l = c.redirect_uri);
                        var d = window.open("https://ssl.reddit.com/api/v1/authorize" + (a ? ".compact" : "") + "?client_id=" + r + "&redirect_uri=" + l + "&duration=permanent&state=ngcordovaoauth&scope=" + i.join(",") + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        d.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(l) && (requestToken = e.url.split("code=")[1], n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                            n.defaults.headers.post.Authorization = "Basic " + btoa(r + ":" + o), n({
                                method: "post",
                                url: "https://ssl.reddit.com/api/v1/access_token",
                                data: "redirect_uri=" + l + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                s.resolve(e);
                            }).error(function(e, n) {
                                s.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    d.close();
                                }, 10);
                            }));
                        }), d.addEventListener("exit", function(e) {
                            s.reject("The sign in flow was canceled");
                        });
                    } else s.reject("Could not find InAppBrowser plugin");
                } else s.reject("Cannot authenticate via a web browser");
                return s.promise;
            },
            slack: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://slack.com/oauth/authorize?client_id=" + r + "&redirect_uri=" + u + "&state=ngcordovaoauth&scope=" + i.join(","), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(u) && (requestToken = e.url.split("code=")[1], console.log("Request token is " + requestToken), 
                            n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                method: "post",
                                url: "https://slack.com/api/oauth.access",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + u + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            twitter: function(r, o, i) {
                var a = e.defer();
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) {
                        var s = "http://localhost/callback";
                        if (void 0 !== i && i.hasOwnProperty("redirect_uri") && (s = i.redirect_uri), "undefined" != typeof jsSHA) {
                            var u = {
                                oauth_consumer_key: r,
                                oauth_nonce: t.createNonce(10),
                                oauth_signature_method: "HMAC-SHA1",
                                oauth_timestamp: Math.round(new Date().getTime() / 1e3),
                                oauth_version: "1.0"
                            }, l = t.createSignature("POST", "https://api.twitter.com/oauth/request_token", u, {
                                oauth_callback: s
                            }, o);
                            n({
                                method: "post",
                                url: "https://api.twitter.com/oauth/request_token",
                                headers: {
                                    Authorization: l.authorization_header,
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                data: "oauth_callback=" + encodeURIComponent(s)
                            }).success(function(e) {
                                for (var r = e.split("&"), i = {}, c = 0; c < r.length; c++) i[r[c].split("=")[0]] = r[c].split("=")[1];
                                i.hasOwnProperty("oauth_token") === !1 && a.reject("Oauth request token was not received");
                                var l = window.open("https://api.twitter.com/oauth/authenticate?oauth_token=" + i.oauth_token, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                                l.addEventListener("loadstart", function(e) {
                                    if (0 === e.url.indexOf(s)) {
                                        for (var r = e.url.split("?")[1], i = r.split("&"), c = {}, d = 0; d < i.length; d++) c[i[d].split("=")[0]] = i[d].split("=")[1];
                                        c.hasOwnProperty("oauth_verifier") === !1 && a.reject("Browser authentication failed to complete.  No oauth_verifier was returned"), 
                                        delete u.oauth_signature, u.oauth_token = c.oauth_token;
                                        var f = t.createSignature("POST", "https://api.twitter.com/oauth/access_token", u, {
                                            oauth_verifier: c.oauth_verifier
                                        }, o);
                                        n({
                                            method: "post",
                                            url: "https://api.twitter.com/oauth/access_token",
                                            headers: {
                                                Authorization: f.authorization_header
                                            },
                                            params: {
                                                oauth_verifier: c.oauth_verifier
                                            }
                                        }).success(function(e) {
                                            for (var n = e.split("&"), t = {}, r = 0; r < n.length; r++) t[n[r].split("=")[0]] = n[r].split("=")[1];
                                            t.hasOwnProperty("oauth_token_secret") === !1 && a.reject("Oauth access token was not received"), 
                                            a.resolve(t);
                                        }).error(function(e) {
                                            a.reject(e);
                                        })["finally"](function() {
                                            setTimeout(function() {
                                                l.close();
                                            }, 10);
                                        });
                                    }
                                }), l.addEventListener("exit", function(e) {
                                    a.reject("The sign in flow was canceled");
                                });
                            }).error(function(e) {
                                a.reject(e);
                            });
                        } else a.reject("Missing jsSHA JavaScript library");
                    } else a.reject("Could not find InAppBrowser plugin");
                } else a.reject("Cannot authenticate via a web browser");
                return a.promise;
            },
            meetup: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://secure.meetup.com/oauth2/authorize/?client_id=" + n + "&redirect_uri=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = {}, i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve(r) : o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            salesforce: function(n, r) {
                var o = "http://localhost/callback", i = function(e, n, t) {
                    return e + "services/oauth2/authorize?display=touch&response_type=token&client_id=" + escape(n) + "&redirect_uri=" + escape(t);
                }, a = function(e, n) {
                    return e.substr(0, n.length) === n;
                }, c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = window.open(i(n, r, o), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        u.addEventListener("loadstart", function(e) {
                            if (a(e.url, o)) {
                                var n = {}, t = e.url.split("#")[1];
                                if (t) {
                                    var r = t.split("&");
                                    for (var i in r) {
                                        var s = r[i].split("=");
                                        n[s[0]] = unescape(s[1]);
                                    }
                                }
                                "undefined" == typeof n || "undefined" == typeof n.access_token ? c.reject("Problem authenticating") : c.resolve(n), 
                                setTimeout(function() {
                                    u.close();
                                }, 10);
                            }
                        }), u.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            strava: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://www.strava.com/oauth/authorize?client_id=" + r + "&redirect_uri=" + u + "&scope=" + i.join(",") + "&response_type=code&approval_prompt=force", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(u) && (requestToken = e.url.split("code=")[1], n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                            n({
                                method: "post",
                                url: "https://www.strava.com/oauth/token",
                                data: "client_id=" + r + "&client_secret=" + o + "&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            withings: function(r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) if ("undefined" != typeof jsSHA) {
                        var c = t.generateOauthParametersInstance(r);
                        c.oauth_callback = "http://localhost/callback";
                        var s = "https://oauth.withings.com/account/request_token", u = t.createSignature("GET", s, {}, c, o);
                        c.oauth_signature = u.signature;
                        var l = t.generateUrlParameters(c);
                        n({
                            method: "get",
                            url: s + "?" + l
                        }).success(function(e) {
                            var a = t.parseResponseParameters(e);
                            a.hasOwnProperty("oauth_token") === !1 && i.reject("Oauth request token was not received");
                            var c = t.generateOauthParametersInstance(r);
                            c.oauth_token = a.oauth_token;
                            var s = a.oauth_token_secret, u = "https://oauth.withings.com/account/authorize", l = t.createSignature("GET", u, {}, c, o);
                            c.oauth_signature = l.signature;
                            var d = t.generateUrlParameters(c), f = window.open(u + "?" + d, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                            f.addEventListener("loadstart", function(e) {
                                if (0 === e.url.indexOf("http://localhost/callback")) {
                                    var c = e.url.split("?")[1];
                                    a = t.parseResponseParameters(c), a.hasOwnProperty("oauth_verifier") === !1 && i.reject("Browser authentication failed to complete.  No oauth_verifier was returned");
                                    var u = t.generateOauthParametersInstance(r);
                                    u.oauth_token = a.oauth_token;
                                    var l = "https://oauth.withings.com/account/access_token", d = t.createSignature("GET", l, {}, u, o, s);
                                    u.oauth_signature = d.signature;
                                    var p = t.generateUrlParameters(u);
                                    n({
                                        method: "get",
                                        url: l + "?" + p
                                    }).success(function(e) {
                                        var n = t.parseResponseParameters(e);
                                        n.hasOwnProperty("oauth_token_secret") === !1 && i.reject("Oauth access token was not received"), 
                                        i.resolve(n);
                                    }).error(function(e) {
                                        i.reject(e);
                                    })["finally"](function() {
                                        setTimeout(function() {
                                            f.close();
                                        }, 10);
                                    });
                                }
                            }), f.addEventListener("exit", function(e) {
                                i.reject("The sign in flow was canceled");
                            });
                        }).error(function(e) {
                            i.reject(e);
                        });
                    } else i.reject("Missing jsSHA JavaScript library"); else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            foursquare: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://foursquare.com/oauth2/authenticate?client_id=" + n + "&redirect_uri=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                if (void 0 !== r.access_token && null !== r.access_token) {
                                    var s = {
                                        access_token: r.access_token,
                                        expires_in: r.expires_in
                                    };
                                    o.resolve(s);
                                } else o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            magento: function(r, o, i) {
                var a = e.defer();
                if (window.cordova) {
                    var c = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(c) === !0) if ("undefined" != typeof jsSHA) {
                        var s = {
                            oauth_callback: "http://localhost/callback",
                            oauth_consumer_key: o,
                            oauth_nonce: t.createNonce(5),
                            oauth_signature_method: "HMAC-SHA1",
                            oauth_timestamp: Math.round(new Date().getTime() / 1e3),
                            oauth_version: "1.0"
                        }, u = t.createSignature("POST", r + "/oauth/initiate", s, {
                            oauth_callback: "http://localhost/callback"
                        }, i);
                        n.defaults.headers.post.Authorization = u.authorization_header, n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                        n({
                            method: "post",
                            url: r + "/oauth/initiate",
                            data: "oauth_callback=http://localhost/callback"
                        }).success(function(e) {
                            for (var o = e.split("&"), c = {}, u = 0; u < o.length; u++) c[o[u].split("=")[0]] = o[u].split("=")[1];
                            c.hasOwnProperty("oauth_token") === !1 && a.reject("Oauth request token was not received");
                            var l = c.oauth_token_secret, d = window.open(r + "/oauth/authorize?oauth_token=" + c.oauth_token, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                            d.addEventListener("loadstart", function(e) {
                                if (0 === e.url.indexOf("http://localhost/callback")) {
                                    for (var o = e.url.split("?")[1], c = o.split("&"), u = {}, f = 0; f < c.length; f++) u[c[f].split("=")[0]] = c[f].split("=")[1];
                                    u.hasOwnProperty("oauth_verifier") === !1 && a.reject("Browser authentication failed to complete.  No oauth_verifier was returned"), 
                                    delete s.oauth_signature, delete s.oauth_callback, s.oauth_token = u.oauth_token, 
                                    s.oauth_nonce = t.createNonce(5), s.oauth_verifier = u.oauth_verifier;
                                    var p = t.createSignature("POST", r + "/oauth/token", s, {}, i, l);
                                    n.defaults.headers.post.Authorization = p.authorization_header, n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                                    n({
                                        method: "post",
                                        url: r + "/oauth/token"
                                    }).success(function(e) {
                                        for (var n = e.split("&"), t = {}, r = 0; r < n.length; r++) t[n[r].split("=")[0]] = n[r].split("=")[1];
                                        t.hasOwnProperty("oauth_token_secret") === !1 && a.reject("Oauth access token was not received"), 
                                        a.resolve(t);
                                    }).error(function(e) {
                                        a.reject(e);
                                    })["finally"](function() {
                                        setTimeout(function() {
                                            d.close();
                                        }, 10);
                                    });
                                }
                            }), d.addEventListener("exit", function(e) {
                                a.reject("The sign in flow was canceled");
                            });
                        }).error(function(e) {
                            a.reject(e);
                        });
                    } else a.reject("Missing jsSHA JavaScript library"); else a.reject("Could not find InAppBrowser plugin");
                } else a.reject("Cannot authenticate via a web browser");
                return a.promise;
            },
            vkontakte: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = window.open("https://oauth.vk.com/authorize?client_id=" + n + "&redirect_uri=http://oauth.vk.com/blank.html&response_type=token&scope=" + r.join(",") + "&display=touch&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        a.addEventListener("loadstart", function(e) {
                            var n = e.url.split("#");
                            if ("https://oauth.vk.com/blank.html" == n[0] || "http://oauth.vk.com/blank.html" == n[0]) {
                                a.removeEventListener("exit", function(e) {}), a.close();
                                for (var t = e.url.split("#")[1], r = t.split("&"), i = [], c = 0; c < r.length; c++) i[r[c].split("=")[0]] = r[c].split("=")[1];
                                if (void 0 !== i.access_token && null !== i.access_token) {
                                    var s = {
                                        access_token: i.access_token,
                                        expires_in: i.expires_in
                                    };
                                    void 0 !== i.email && null !== i.email && (s.email = i.email), o.resolve(s);
                                } else o.reject("Problem authenticating");
                            }
                        }), a.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            odnoklassniki: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = window.open("http://www.odnoklassniki.ru/oauth/authorize?client_id=" + n + "&scope=" + r.join(",") + "&response_type=token&redirect_uri=http://localhost/callback&layout=m", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        a.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf("http://localhost/callback")) {
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve({
                                    access_token: r.access_token,
                                    session_secret_key: r.session_secret_key
                                }) : o.reject("Problem authenticating"), setTimeout(function() {
                                    a.close();
                                }, 10);
                            }
                        }), a.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            imgur: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://api.imgur.com/oauth2/authorize?client_id=" + n + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in,
                                    account_username: r.account_username
                                }) : o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            spotify: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://accounts.spotify.com/authorize?client_id=" + n + "&redirect_uri=" + c + "&response_type=token&scope=" + r.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                s.removeEventListener("exit", function(e) {}), s.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in,
                                    account_username: r.account_username
                                }) : i.reject("Problem authenticating");
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            uber: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://login.uber.com/oauth/authorize?client_id=" + n + "&redirect_uri=" + c + "&response_type=token&scope=" + r.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                s.removeEventListener("exit", function(e) {}), s.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    token_type: r.token_type,
                                    expires_in: r.expires_in,
                                    scope: r.scope
                                }) : i.reject("Problem authenticating");
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            windowsLive: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "https://login.live.com/oauth20_desktop.srf";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://login.live.com/oauth20_authorize.srf?client_id=" + n + "&scope=" + r.join(",") + "&response_type=token&display=touch&redirect_uri=" + c, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                s.removeEventListener("exit", function(e) {}), s.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in
                                }) : i.reject("Problem authenticating");
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            yammer: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://www.yammer.com/dialog/oauth?client_id=" + n + "&redirect_uri=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve({
                                    access_token: r.access_token
                                }) : o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            venmo: function(n, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(a) === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://api.venmo.com/v1/oauth/authorize?client_id=" + n + "&redirect_uri=" + c + "&response_type=token&scope=" + r.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                s.removeEventListener("exit", function(e) {}), s.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], o = 0; o < t.length; o++) r[t[o].split("=")[0]] = t[o].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? i.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in
                                }) : i.reject("Problem authenticating");
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            stripe: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://connect.stripe.com/oauth/authorize?client_id=" + r + "&redirect_uri=" + u + "&scope=" + i + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf("http://localhost/callback") && (requestToken = e.url.split("code=")[1], 
                            n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                method: "post",
                                url: "https://connect.stripe.com/oauth/token",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + u + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            rally: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://rally1.rallydev.com/login/oauth2/auth?client_id=" + r + "&redirect_uri=" + u + "&scope=" + i + "&response_type=code", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf("http://localhost/callback") && (requestToken = e.url.split("code=")[1], 
                            n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                method: "post",
                                url: "https://rally1.rallydev.com/login/oauth2/auth",
                                data: "client_id=" + r + "&client_secret=" + o + "&redirect_uri=" + u + "&grant_type=authorization_code&code=" + requestToken
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    l.close();
                                }, 10);
                            }));
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            familySearch: function(t, r, o) {
                var i = e.defer();
                if (window.cordova) {
                    var a = cordova.require("cordova/plugin_list").metadata;
                    if (a.hasOwnProperty("cordova-plugin-inappbrowser") === !0) {
                        var c = "http://localhost/callback";
                        void 0 !== o && o.hasOwnProperty("redirect_uri") && (c = o.redirect_uri);
                        var s = window.open("https://ident.familysearch.org/cis-web/oauth2/v3/authorization?client_id=" + t + "&redirect_uri=" + c + "&response_type=code&state=" + r, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        s.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(c)) {
                                var r = e.url.split("code=")[1];
                                n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                    method: "post",
                                    url: "https://ident.familysearch.org/cis-web/oauth2/v3/token",
                                    data: "client_id=" + t + "&redirect_uri=" + c + "&grant_type=authorization_code&code=" + r
                                }).success(function(e) {
                                    i.resolve(e);
                                }).error(function(e, n) {
                                    i.reject("Problem authenticating");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        s.close();
                                    }, 10);
                                });
                            }
                        }), s.addEventListener("exit", function(e) {
                            i.reject("The sign in flow was canceled");
                        });
                    } else i.reject("Could not find InAppBrowser plugin");
                } else i.reject("Cannot authenticate via a web browser");
                return i.promise;
            },
            envato: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_uri") && (a = r.redirect_uri);
                        var c = window.open("https://api.envato.com/authorization?client_id=" + n + "&redirect_uri=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                void 0 !== r.access_token && null !== r.access_token ? o.resolve({
                                    access_token: r.access_token,
                                    expires_in: r.expires_in
                                }) : o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            weibo: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = "https://open.weibo.cn/oauth2/authorize?display=mobile&client_id=" + r + "&redirect_uri=" + u + "&scope=" + i.join(",");
                        void 0 !== a && (a.hasOwnProperty("language") && (l += "&language=" + a.language), 
                        a.hasOwnProperty("forcelogin") && (l += "&forcelogin=" + a.forcelogin));
                        var d = window.open(l, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        d.addEventListener("loadstart", function(e) {
                            0 === e.url.indexOf(u) && (requestToken = e.url.split("code=")[1], n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", 
                            n({
                                method: "post",
                                url: "https://api.weibo.com/oauth2/access_token",
                                data: "client_id=" + r + "&client_secret=" + o + "&grant_type=authorization_code&code=" + requestToken + "&redirect_uri=" + u
                            }).success(function(e) {
                                c.resolve(e);
                            }).error(function(e, n) {
                                c.reject("Problem authenticating");
                            })["finally"](function() {
                                setTimeout(function() {
                                    d.close();
                                }, 10);
                            }));
                        }), d.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            jawbone: function(r, o, i, a) {
                var c = e.defer();
                if (window.cordova) {
                    var s = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(s) === !0) {
                        var u = "http://localhost/callback";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (u = a.redirect_uri);
                        var l = window.open("https://jawbone.com/auth/oauth2/auth?client_id=" + r + "&redirect_uri=" + u + "&response_type=code&scope=" + i.join(" "), "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        l.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(u)) {
                                var t = e.url.split("code=")[1];
                                n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                    method: "post",
                                    url: "https://jawbone.com/auth/oauth2/token",
                                    data: "client_id=" + r + "&client_secret=" + o + "&grant_type=authorization_code&code=" + t
                                }).success(function(e) {
                                    c.resolve(e);
                                }).error(function(e, n) {
                                    c.reject("Problem authenticating");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        l.close();
                                    }, 10);
                                });
                            }
                        }), l.addEventListener("exit", function(e) {
                            c.reject("The sign in flow was canceled");
                        });
                    } else c.reject("Could not find InAppBrowser plugin");
                } else c.reject("Cannot authenticate via a web browser");
                return c.promise;
            },
            untappd: function(n, r) {
                var o = e.defer();
                if (window.cordova) {
                    var i = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(i) === !0) {
                        var a = "http://localhost/callback";
                        void 0 !== r && r.hasOwnProperty("redirect_url") && (a = r.redirect_url);
                        var c = window.open("https://untappd.com/oauth/authenticate/?client_id=" + n + "&redirect_url=" + a + "&response_type=token", "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        c.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(a)) {
                                c.removeEventListener("exit", function(e) {}), c.close();
                                for (var n = e.url.split("#")[1], t = n.split("&"), r = [], i = 0; i < t.length; i++) r[t[i].split("=")[0]] = t[i].split("=")[1];
                                if (void 0 !== r.access_token && null !== r.access_token) {
                                    var s = {
                                        access_token: r.access_token
                                    };
                                    o.resolve(s);
                                } else o.reject("Problem authenticating");
                            }
                        }), c.addEventListener("exit", function(e) {
                            o.reject("The sign in flow was canceled");
                        });
                    } else o.reject("Could not find InAppBrowser plugin");
                } else o.reject("Cannot authenticate via a web browser");
                return o.promise;
            },
            dribble: function(r, o, i, a, c) {
                var s = e.defer();
                if (window.cordova) {
                    var u = cordova.require("cordova/plugin_list").metadata;
                    if (t.isInAppBrowserInstalled(u) === !0) {
                        var l = "http://localhost/callback", d = "https://dribbble.com/oauth/authorize", f = "https://dribbble.com/oauth/token";
                        void 0 !== a && a.hasOwnProperty("redirect_uri") && (l = a.redirect_uri), void 0 === c && (c = t.createNonce(5));
                        var p = i.join(",").replace(/,/g, "+"), v = window.open(d + "?client_id=" + r + "&redirect_uri=" + l + "&scope=" + p + "&state=" + c, "_blank", "location=no,clearsessioncache=yes,clearcache=yes");
                        v.addEventListener("loadstart", function(e) {
                            if (0 === e.url.indexOf(l)) {
                                var t = e.url.split("code=")[1], i = t.split("&")[0];
                                n.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded", n({
                                    method: "post",
                                    url: f,
                                    data: "client_id=" + r + "&redirect_uri=" + l + "&client_secret=" + o + "&code=" + i
                                }).success(function(e) {
                                    s.resolve(e);
                                }).error(function(e, n) {
                                    s.reject("Problem authenticating ");
                                })["finally"](function() {
                                    setTimeout(function() {
                                        v.close();
                                    }, 10);
                                });
                            }
                        }), v.addEventListener("exit", function(e) {
                            s.reject("The sign in flow was canceled");
                        });
                    } else s.reject("Could not find InAppBrowser plugin");
                } else s.reject("Cannot authenticate via a web browser");
                return s.promise;
            }
        };
    } ]), angular.module("ngCordovaOauth", [ "oauth.providers", "oauth.utils" ]), angular.module("oauth.utils", []).factory("$cordovaOauthUtility", [ "$q", function(e) {
        return {
            isInAppBrowserInstalled: function(e) {
                var n = [ "cordova-plugin-inappbrowser", "org.apache.cordova.inappbrowser" ];
                return n.some(function(n) {
                    return e.hasOwnProperty(n);
                });
            },
            createSignature: function(e, n, t, r, o, i) {
                if ("undefined" != typeof jsSHA) {
                    for (var a = angular.copy(t), c = Object.keys(r), s = 0; s < c.length; s++) a[c[s]] = encodeURIComponent(r[c[s]]);
                    var u = e + "&" + encodeURIComponent(n) + "&", l = Object.keys(a).sort();
                    for (s = 0; s < l.length; s++) u += s == l.length - 1 ? encodeURIComponent(l[s] + "=" + a[l[s]]) : encodeURIComponent(l[s] + "=" + a[l[s]] + "&");
                    var d = new jsSHA(u, "TEXT"), f = "";
                    i && (f = encodeURIComponent(i)), t.oauth_signature = encodeURIComponent(d.getHMAC(encodeURIComponent(o) + "&" + f, "TEXT", "SHA-1", "B64"));
                    var p = Object.keys(t), v = "OAuth ";
                    for (s = 0; s < p.length; s++) v += s == p.length - 1 ? p[s] + '="' + t[p[s]] + '"' : p[s] + '="' + t[p[s]] + '",';
                    return {
                        signature_base_string: u,
                        authorization_header: v,
                        signature: t.oauth_signature
                    };
                }
                return "Missing jsSHA JavaScript library";
            },
            createNonce: function(e) {
                for (var n = "", t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", r = 0; e > r; r++) n += t.charAt(Math.floor(Math.random() * t.length));
                return n;
            },
            generateUrlParameters: function(e) {
                var n = Object.keys(e);
                n.sort();
                for (var t = "", r = "", o = 0; o < n.length; o++) t += r + n[o] + "=" + e[n[o]], 
                r = "&";
                return t;
            },
            parseResponseParameters: function(e) {
                if (e.split) {
                    for (var n = e.split("&"), t = {}, r = 0; r < n.length; r++) t[n[r].split("=")[0]] = n[r].split("=")[1];
                    return t;
                }
                return {};
            },
            generateOauthParametersInstance: function(e) {
                var n = new jsSHA(Math.round(new Date().getTime() / 1e3), "TEXT"), t = {
                    oauth_consumer_key: e,
                    oauth_nonce: n.getHash("SHA-1", "HEX"),
                    oauth_signature_method: "HMAC-SHA1",
                    oauth_timestamp: Math.round(new Date().getTime() / 1e3),
                    oauth_version: "1.0"
                };
                return t;
            }
        };
    } ]);
}();

!function(a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define([ "angular" ], b) : "object" == typeof exports ? module.exports = b(require("angular")) : b(a.angular);
}(this, function(a) {
    "use strict";
    function b(b) {
        return function() {
            var c = "ngStorage-";
            this.setKeyPrefix = function(a) {
                if ("string" != typeof a) throw new TypeError("[ngStorage] - " + b + "Provider.setKeyPrefix() expects a String.");
                c = a;
            };
            var d = a.toJson, e = a.fromJson;
            this.setSerializer = function(a) {
                if ("function" != typeof a) throw new TypeError("[ngStorage] - " + b + "Provider.setSerializer expects a function.");
                d = a;
            }, this.setDeserializer = function(a) {
                if ("function" != typeof a) throw new TypeError("[ngStorage] - " + b + "Provider.setDeserializer expects a function.");
                e = a;
            }, this.get = function(a) {
                return e(window[b].getItem(c + a));
            }, this.set = function(a, e) {
                return window[b].setItem(c + a, d(e));
            }, this.$get = [ "$rootScope", "$window", "$log", "$timeout", function(f, g, h, i) {
                function j(a) {
                    var b;
                    try {
                        b = g[a];
                    } catch (c) {
                        b = !1;
                    }
                    if (b && "localStorage" === a) {
                        var d = "__" + Math.round(1e7 * Math.random());
                        try {
                            localStorage.setItem(d, d), localStorage.removeItem(d);
                        } catch (c) {
                            b = !1;
                        }
                    }
                    return b;
                }
                var k, l, m = c.length, n = j(b) || (h.warn("This browser does not support Web Storage!"), 
                {
                    setItem: a.noop,
                    getItem: a.noop
                }), o = {
                    $default: function(b) {
                        for (var c in b) a.isDefined(o[c]) || (o[c] = b[c]);
                        return o.$sync(), o;
                    },
                    $reset: function(a) {
                        for (var b in o) "$" === b[0] || delete o[b] && n.removeItem(c + b);
                        return o.$default(a);
                    },
                    $sync: function() {
                        for (var a, b = 0, d = n.length; d > b; b++) (a = n.key(b)) && c === a.slice(0, m) && (o[a.slice(m)] = e(n.getItem(a)));
                    },
                    $apply: function() {
                        var b;
                        if (l = null, !a.equals(o, k)) {
                            b = a.copy(k), a.forEach(o, function(e, f) {
                                a.isDefined(e) && "$" !== f[0] && (n.setItem(c + f, d(e)), delete b[f]);
                            });
                            for (var e in b) n.removeItem(c + e);
                            k = a.copy(o);
                        }
                    }
                };
                return o.$sync(), k = a.copy(o), f.$watch(function() {
                    l || (l = i(o.$apply, 100, !1));
                }), g.addEventListener && g.addEventListener("storage", function(b) {
                    c === b.key.slice(0, m) && (b.newValue ? o[b.key.slice(m)] = e(b.newValue) : delete o[b.key.slice(m)], 
                    k = a.copy(o), f.$apply());
                }), g.addEventListener && g.addEventListener("beforeunload", function() {
                    o.$apply();
                }), o;
            } ];
        };
    }
    return a = a && a.module ? a : window.angular, a.module("ngStorage", []).provider("$localStorage", b("localStorage")).provider("$sessionStorage", b("sessionStorage"));
});

angular.module("ui.bootstrap", [ "ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.dateparser", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdown", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead" ]), 
angular.module("ui.bootstrap.tpls", [ "template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/day.html", "template/datepicker/month.html", "template/datepicker/popup.html", "template/datepicker/year.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/progressbar/progressbar.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html" ]), 
angular.module("ui.bootstrap.transition", []).factory("$transition", [ "$q", "$timeout", "$rootScope", function(a, b, c) {
    function d(a) {
        for (var b in a) if (void 0 !== f.style[b]) return a[b];
    }
    var e = function(d, f, g) {
        g = g || {};
        var h = a.defer(), i = e[g.animation ? "animationEndEventName" : "transitionEndEventName"], j = function() {
            c.$apply(function() {
                d.unbind(i, j), h.resolve(d);
            });
        };
        return i && d.bind(i, j), b(function() {
            angular.isString(f) ? d.addClass(f) : angular.isFunction(f) ? f(d) : angular.isObject(f) && d.css(f), 
            i || h.resolve(d);
        }), h.promise.cancel = function() {
            i && d.unbind(i, j), h.reject("Transition cancelled");
        }, h.promise;
    }, f = document.createElement("trans"), g = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        transition: "transitionend"
    }, h = {
        WebkitTransition: "webkitAnimationEnd",
        MozTransition: "animationend",
        OTransition: "oAnimationEnd",
        transition: "animationend"
    };
    return e.transitionEndEventName = d(g), e.animationEndEventName = d(h), e;
} ]), angular.module("ui.bootstrap.collapse", [ "ui.bootstrap.transition" ]).directive("collapse", [ "$transition", function(a) {
    return {
        link: function(b, c, d) {
            function e(b) {
                function d() {
                    j === e && (j = void 0);
                }
                var e = a(c, b);
                return j && j.cancel(), j = e, e.then(d, d), e;
            }
            function f() {
                k ? (k = !1, g()) : (c.removeClass("collapse").addClass("collapsing"), e({
                    height: c[0].scrollHeight + "px"
                }).then(g));
            }
            function g() {
                c.removeClass("collapsing"), c.addClass("collapse in"), c.css({
                    height: "auto"
                });
            }
            function h() {
                if (k) k = !1, i(), c.css({
                    height: 0
                }); else {
                    c.css({
                        height: c[0].scrollHeight + "px"
                    });
                    {
                        c[0].offsetWidth;
                    }
                    c.removeClass("collapse in").addClass("collapsing"), e({
                        height: 0
                    }).then(i);
                }
            }
            function i() {
                c.removeClass("collapsing"), c.addClass("collapse");
            }
            var j, k = !0;
            b.$watch(d.collapse, function(a) {
                a ? h() : f();
            });
        }
    };
} ]), angular.module("ui.bootstrap.accordion", [ "ui.bootstrap.collapse" ]).constant("accordionConfig", {
    closeOthers: !0
}).controller("AccordionController", [ "$scope", "$attrs", "accordionConfig", function(a, b, c) {
    this.groups = [], this.closeOthers = function(d) {
        var e = angular.isDefined(b.closeOthers) ? a.$eval(b.closeOthers) : c.closeOthers;
        e && angular.forEach(this.groups, function(a) {
            a !== d && (a.isOpen = !1);
        });
    }, this.addGroup = function(a) {
        var b = this;
        this.groups.push(a), a.$on("$destroy", function() {
            b.removeGroup(a);
        });
    }, this.removeGroup = function(a) {
        var b = this.groups.indexOf(a);
        -1 !== b && this.groups.splice(b, 1);
    };
} ]).directive("accordion", function() {
    return {
        restrict: "EA",
        controller: "AccordionController",
        transclude: !0,
        replace: !1,
        templateUrl: "template/accordion/accordion.html"
    };
}).directive("accordionGroup", function() {
    return {
        require: "^accordion",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/accordion/accordion-group.html",
        scope: {
            heading: "@",
            isOpen: "=?",
            isDisabled: "=?"
        },
        controller: function() {
            this.setHeading = function(a) {
                this.heading = a;
            };
        },
        link: function(a, b, c, d) {
            d.addGroup(a), a.$watch("isOpen", function(b) {
                b && d.closeOthers(a);
            }), a.toggleOpen = function() {
                a.isDisabled || (a.isOpen = !a.isOpen);
            };
        }
    };
}).directive("accordionHeading", function() {
    return {
        restrict: "EA",
        transclude: !0,
        template: "",
        replace: !0,
        require: "^accordionGroup",
        link: function(a, b, c, d, e) {
            d.setHeading(e(a, function() {}));
        }
    };
}).directive("accordionTransclude", function() {
    return {
        require: "^accordionGroup",
        link: function(a, b, c, d) {
            a.$watch(function() {
                return d[c.accordionTransclude];
            }, function(a) {
                a && (b.html(""), b.append(a));
            });
        }
    };
}), angular.module("ui.bootstrap.alert", []).controller("AlertController", [ "$scope", "$attrs", function(a, b) {
    a.closeable = "close" in b, this.close = a.close;
} ]).directive("alert", function() {
    return {
        restrict: "EA",
        controller: "AlertController",
        templateUrl: "template/alert/alert.html",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@",
            close: "&"
        }
    };
}).directive("dismissOnTimeout", [ "$timeout", function(a) {
    return {
        require: "alert",
        link: function(b, c, d, e) {
            a(function() {
                e.close();
            }, parseInt(d.dismissOnTimeout, 10));
        }
    };
} ]), angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
    return function(a, b, c) {
        b.addClass("ng-binding").data("$binding", c.bindHtmlUnsafe), a.$watch(c.bindHtmlUnsafe, function(a) {
            b.html(a || "");
        });
    };
}), angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
    activeClass: "active",
    toggleEvent: "click"
}).controller("ButtonsController", [ "buttonConfig", function(a) {
    this.activeClass = a.activeClass || "active", this.toggleEvent = a.toggleEvent || "click";
} ]).directive("btnRadio", function() {
    return {
        require: [ "btnRadio", "ngModel" ],
        controller: "ButtonsController",
        link: function(a, b, c, d) {
            var e = d[0], f = d[1];
            f.$render = function() {
                b.toggleClass(e.activeClass, angular.equals(f.$modelValue, a.$eval(c.btnRadio)));
            }, b.bind(e.toggleEvent, function() {
                var d = b.hasClass(e.activeClass);
                (!d || angular.isDefined(c.uncheckable)) && a.$apply(function() {
                    f.$setViewValue(d ? null : a.$eval(c.btnRadio)), f.$render();
                });
            });
        }
    };
}).directive("btnCheckbox", function() {
    return {
        require: [ "btnCheckbox", "ngModel" ],
        controller: "ButtonsController",
        link: function(a, b, c, d) {
            function e() {
                return g(c.btnCheckboxTrue, !0);
            }
            function f() {
                return g(c.btnCheckboxFalse, !1);
            }
            function g(b, c) {
                var d = a.$eval(b);
                return angular.isDefined(d) ? d : c;
            }
            var h = d[0], i = d[1];
            i.$render = function() {
                b.toggleClass(h.activeClass, angular.equals(i.$modelValue, e()));
            }, b.bind(h.toggleEvent, function() {
                a.$apply(function() {
                    i.$setViewValue(b.hasClass(h.activeClass) ? f() : e()), i.$render();
                });
            });
        }
    };
}), angular.module("ui.bootstrap.carousel", [ "ui.bootstrap.transition" ]).controller("CarouselController", [ "$scope", "$timeout", "$interval", "$transition", function(a, b, c, d) {
    function e() {
        f();
        var b = +a.interval;
        !isNaN(b) && b > 0 && (h = c(g, b));
    }
    function f() {
        h && (c.cancel(h), h = null);
    }
    function g() {
        var b = +a.interval;
        i && !isNaN(b) && b > 0 ? a.next() : a.pause();
    }
    var h, i, j = this, k = j.slides = a.slides = [], l = -1;
    j.currentSlide = null;
    var m = !1;
    j.select = a.select = function(c, f) {
        function g() {
            if (!m) {
                if (j.currentSlide && angular.isString(f) && !a.noTransition && c.$element) {
                    c.$element.addClass(f);
                    {
                        c.$element[0].offsetWidth;
                    }
                    angular.forEach(k, function(a) {
                        angular.extend(a, {
                            direction: "",
                            entering: !1,
                            leaving: !1,
                            active: !1
                        });
                    }), angular.extend(c, {
                        direction: f,
                        active: !0,
                        entering: !0
                    }), angular.extend(j.currentSlide || {}, {
                        direction: f,
                        leaving: !0
                    }), a.$currentTransition = d(c.$element, {}), function(b, c) {
                        a.$currentTransition.then(function() {
                            h(b, c);
                        }, function() {
                            h(b, c);
                        });
                    }(c, j.currentSlide);
                } else h(c, j.currentSlide);
                j.currentSlide = c, l = i, e();
            }
        }
        function h(b, c) {
            angular.extend(b, {
                direction: "",
                active: !0,
                leaving: !1,
                entering: !1
            }), angular.extend(c || {}, {
                direction: "",
                active: !1,
                leaving: !1,
                entering: !1
            }), a.$currentTransition = null;
        }
        var i = k.indexOf(c);
        void 0 === f && (f = i > l ? "next" : "prev"), c && c !== j.currentSlide && (a.$currentTransition ? (a.$currentTransition.cancel(), 
        b(g)) : g());
    }, a.$on("$destroy", function() {
        m = !0;
    }), j.indexOfSlide = function(a) {
        return k.indexOf(a);
    }, a.next = function() {
        var b = (l + 1) % k.length;
        return a.$currentTransition ? void 0 : j.select(k[b], "next");
    }, a.prev = function() {
        var b = 0 > l - 1 ? k.length - 1 : l - 1;
        return a.$currentTransition ? void 0 : j.select(k[b], "prev");
    }, a.isActive = function(a) {
        return j.currentSlide === a;
    }, a.$watch("interval", e), a.$on("$destroy", f), a.play = function() {
        i || (i = !0, e());
    }, a.pause = function() {
        a.noPause || (i = !1, f());
    }, j.addSlide = function(b, c) {
        b.$element = c, k.push(b), 1 === k.length || b.active ? (j.select(k[k.length - 1]), 
        1 == k.length && a.play()) : b.active = !1;
    }, j.removeSlide = function(a) {
        var b = k.indexOf(a);
        k.splice(b, 1), k.length > 0 && a.active ? j.select(b >= k.length ? k[b - 1] : k[b]) : l > b && l--;
    };
} ]).directive("carousel", [ function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        controller: "CarouselController",
        require: "carousel",
        templateUrl: "template/carousel/carousel.html",
        scope: {
            interval: "=",
            noTransition: "=",
            noPause: "="
        }
    };
} ]).directive("slide", function() {
    return {
        require: "^carousel",
        restrict: "EA",
        transclude: !0,
        replace: !0,
        templateUrl: "template/carousel/slide.html",
        scope: {
            active: "=?"
        },
        link: function(a, b, c, d) {
            d.addSlide(a, b), a.$on("$destroy", function() {
                d.removeSlide(a);
            }), a.$watch("active", function(b) {
                b && d.select(a);
            });
        }
    };
}), angular.module("ui.bootstrap.dateparser", []).service("dateParser", [ "$locale", "orderByFilter", function(a, b) {
    function c(a) {
        var c = [], d = a.split("");
        return angular.forEach(e, function(b, e) {
            var f = a.indexOf(e);
            if (f > -1) {
                a = a.split(""), d[f] = "(" + b.regex + ")", a[f] = "$";
                for (var g = f + 1, h = f + e.length; h > g; g++) d[g] = "", a[g] = "$";
                a = a.join(""), c.push({
                    index: f,
                    apply: b.apply
                });
            }
        }), {
            regex: new RegExp("^" + d.join("") + "$"),
            map: b(c, "index")
        };
    }
    function d(a, b, c) {
        return 1 === b && c > 28 ? 29 === c && (a % 4 === 0 && a % 100 !== 0 || a % 400 === 0) : 3 === b || 5 === b || 8 === b || 10 === b ? 31 > c : !0;
    }
    this.parsers = {};
    var e = {
        yyyy: {
            regex: "\\d{4}",
            apply: function(a) {
                this.year = +a;
            }
        },
        yy: {
            regex: "\\d{2}",
            apply: function(a) {
                this.year = +a + 2e3;
            }
        },
        y: {
            regex: "\\d{1,4}",
            apply: function(a) {
                this.year = +a;
            }
        },
        MMMM: {
            regex: a.DATETIME_FORMATS.MONTH.join("|"),
            apply: function(b) {
                this.month = a.DATETIME_FORMATS.MONTH.indexOf(b);
            }
        },
        MMM: {
            regex: a.DATETIME_FORMATS.SHORTMONTH.join("|"),
            apply: function(b) {
                this.month = a.DATETIME_FORMATS.SHORTMONTH.indexOf(b);
            }
        },
        MM: {
            regex: "0[1-9]|1[0-2]",
            apply: function(a) {
                this.month = a - 1;
            }
        },
        M: {
            regex: "[1-9]|1[0-2]",
            apply: function(a) {
                this.month = a - 1;
            }
        },
        dd: {
            regex: "[0-2][0-9]{1}|3[0-1]{1}",
            apply: function(a) {
                this.date = +a;
            }
        },
        d: {
            regex: "[1-2]?[0-9]{1}|3[0-1]{1}",
            apply: function(a) {
                this.date = +a;
            }
        },
        EEEE: {
            regex: a.DATETIME_FORMATS.DAY.join("|")
        },
        EEE: {
            regex: a.DATETIME_FORMATS.SHORTDAY.join("|")
        }
    };
    this.parse = function(b, e) {
        if (!angular.isString(b) || !e) return b;
        e = a.DATETIME_FORMATS[e] || e, this.parsers[e] || (this.parsers[e] = c(e));
        var f = this.parsers[e], g = f.regex, h = f.map, i = b.match(g);
        if (i && i.length) {
            for (var j, k = {
                year: 1900,
                month: 0,
                date: 1,
                hours: 0
            }, l = 1, m = i.length; m > l; l++) {
                var n = h[l - 1];
                n.apply && n.apply.call(k, i[l]);
            }
            return d(k.year, k.month, k.date) && (j = new Date(k.year, k.month, k.date, k.hours)), 
            j;
        }
    };
} ]), angular.module("ui.bootstrap.position", []).factory("$position", [ "$document", "$window", function(a, b) {
    function c(a, c) {
        return a.currentStyle ? a.currentStyle[c] : b.getComputedStyle ? b.getComputedStyle(a)[c] : a.style[c];
    }
    function d(a) {
        return "static" === (c(a, "position") || "static");
    }
    var e = function(b) {
        for (var c = a[0], e = b.offsetParent || c; e && e !== c && d(e); ) e = e.offsetParent;
        return e || c;
    };
    return {
        position: function(b) {
            var c = this.offset(b), d = {
                top: 0,
                left: 0
            }, f = e(b[0]);
            f != a[0] && (d = this.offset(angular.element(f)), d.top += f.clientTop - f.scrollTop, 
            d.left += f.clientLeft - f.scrollLeft);
            var g = b[0].getBoundingClientRect();
            return {
                width: g.width || b.prop("offsetWidth"),
                height: g.height || b.prop("offsetHeight"),
                top: c.top - d.top,
                left: c.left - d.left
            };
        },
        offset: function(c) {
            var d = c[0].getBoundingClientRect();
            return {
                width: d.width || c.prop("offsetWidth"),
                height: d.height || c.prop("offsetHeight"),
                top: d.top + (b.pageYOffset || a[0].documentElement.scrollTop),
                left: d.left + (b.pageXOffset || a[0].documentElement.scrollLeft)
            };
        },
        positionElements: function(a, b, c, d) {
            var e, f, g, h, i = c.split("-"), j = i[0], k = i[1] || "center";
            e = d ? this.offset(a) : this.position(a), f = b.prop("offsetWidth"), g = b.prop("offsetHeight");
            var l = {
                center: function() {
                    return e.left + e.width / 2 - f / 2;
                },
                left: function() {
                    return e.left;
                },
                right: function() {
                    return e.left + e.width;
                }
            }, m = {
                center: function() {
                    return e.top + e.height / 2 - g / 2;
                },
                top: function() {
                    return e.top;
                },
                bottom: function() {
                    return e.top + e.height;
                }
            };
            switch (j) {
              case "right":
                h = {
                    top: m[k](),
                    left: l[j]()
                };
                break;

              case "left":
                h = {
                    top: m[k](),
                    left: e.left - f
                };
                break;

              case "bottom":
                h = {
                    top: m[j](),
                    left: l[k]()
                };
                break;

              default:
                h = {
                    top: e.top - g,
                    left: l[k]()
                };
            }
            return h;
        }
    };
} ]), angular.module("ui.bootstrap.datepicker", [ "ui.bootstrap.dateparser", "ui.bootstrap.position" ]).constant("datepickerConfig", {
    formatDay: "dd",
    formatMonth: "MMMM",
    formatYear: "yyyy",
    formatDayHeader: "EEE",
    formatDayTitle: "MMMM yyyy",
    formatMonthTitle: "yyyy",
    datepickerMode: "day",
    minMode: "day",
    maxMode: "year",
    showWeeks: !0,
    startingDay: 0,
    yearRange: 20,
    minDate: null,
    maxDate: null
}).controller("DatepickerController", [ "$scope", "$attrs", "$parse", "$interpolate", "$timeout", "$log", "dateFilter", "datepickerConfig", function(a, b, c, d, e, f, g, h) {
    var i = this, j = {
        $setViewValue: angular.noop
    };
    this.modes = [ "day", "month", "year" ], angular.forEach([ "formatDay", "formatMonth", "formatYear", "formatDayHeader", "formatDayTitle", "formatMonthTitle", "minMode", "maxMode", "showWeeks", "startingDay", "yearRange" ], function(c, e) {
        i[c] = angular.isDefined(b[c]) ? 8 > e ? d(b[c])(a.$parent) : a.$parent.$eval(b[c]) : h[c];
    }), angular.forEach([ "minDate", "maxDate" ], function(d) {
        b[d] ? a.$parent.$watch(c(b[d]), function(a) {
            i[d] = a ? new Date(a) : null, i.refreshView();
        }) : i[d] = h[d] ? new Date(h[d]) : null;
    }), a.datepickerMode = a.datepickerMode || h.datepickerMode, a.uniqueId = "datepicker-" + a.$id + "-" + Math.floor(1e4 * Math.random()), 
    this.activeDate = angular.isDefined(b.initDate) ? a.$parent.$eval(b.initDate) : new Date(), 
    a.isActive = function(b) {
        return 0 === i.compare(b.date, i.activeDate) ? (a.activeDateId = b.uid, !0) : !1;
    }, this.init = function(a) {
        j = a, j.$render = function() {
            i.render();
        };
    }, this.render = function() {
        if (j.$modelValue) {
            var a = new Date(j.$modelValue), b = !isNaN(a);
            b ? this.activeDate = a : f.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'), 
            j.$setValidity("date", b);
        }
        this.refreshView();
    }, this.refreshView = function() {
        if (this.element) {
            this._refreshView();
            var a = j.$modelValue ? new Date(j.$modelValue) : null;
            j.$setValidity("date-disabled", !a || this.element && !this.isDisabled(a));
        }
    }, this.createDateObject = function(a, b) {
        var c = j.$modelValue ? new Date(j.$modelValue) : null;
        return {
            date: a,
            label: g(a, b),
            selected: c && 0 === this.compare(a, c),
            disabled: this.isDisabled(a),
            current: 0 === this.compare(a, new Date())
        };
    }, this.isDisabled = function(c) {
        return this.minDate && this.compare(c, this.minDate) < 0 || this.maxDate && this.compare(c, this.maxDate) > 0 || b.dateDisabled && a.dateDisabled({
            date: c,
            mode: a.datepickerMode
        });
    }, this.split = function(a, b) {
        for (var c = []; a.length > 0; ) c.push(a.splice(0, b));
        return c;
    }, a.select = function(b) {
        if (a.datepickerMode === i.minMode) {
            var c = j.$modelValue ? new Date(j.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
            c.setFullYear(b.getFullYear(), b.getMonth(), b.getDate()), j.$setViewValue(c), j.$render();
        } else i.activeDate = b, a.datepickerMode = i.modes[i.modes.indexOf(a.datepickerMode) - 1];
    }, a.move = function(a) {
        var b = i.activeDate.getFullYear() + a * (i.step.years || 0), c = i.activeDate.getMonth() + a * (i.step.months || 0);
        i.activeDate.setFullYear(b, c, 1), i.refreshView();
    }, a.toggleMode = function(b) {
        b = b || 1, a.datepickerMode === i.maxMode && 1 === b || a.datepickerMode === i.minMode && -1 === b || (a.datepickerMode = i.modes[i.modes.indexOf(a.datepickerMode) + b]);
    }, a.keys = {
        13: "enter",
        32: "space",
        33: "pageup",
        34: "pagedown",
        35: "end",
        36: "home",
        37: "left",
        38: "up",
        39: "right",
        40: "down"
    };
    var k = function() {
        e(function() {
            i.element[0].focus();
        }, 0, !1);
    };
    a.$on("datepicker.focus", k), a.keydown = function(b) {
        var c = a.keys[b.which];
        if (c && !b.shiftKey && !b.altKey) if (b.preventDefault(), b.stopPropagation(), 
        "enter" === c || "space" === c) {
            if (i.isDisabled(i.activeDate)) return;
            a.select(i.activeDate), k();
        } else !b.ctrlKey || "up" !== c && "down" !== c ? (i.handleKeyDown(c, b), i.refreshView()) : (a.toggleMode("up" === c ? 1 : -1), 
        k());
    };
} ]).directive("datepicker", function() {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/datepicker.html",
        scope: {
            datepickerMode: "=?",
            dateDisabled: "&"
        },
        require: [ "datepicker", "?^ngModel" ],
        controller: "DatepickerController",
        link: function(a, b, c, d) {
            var e = d[0], f = d[1];
            f && e.init(f);
        }
    };
}).directive("daypicker", [ "dateFilter", function(a) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/day.html",
        require: "^datepicker",
        link: function(b, c, d, e) {
            function f(a, b) {
                return 1 !== b || a % 4 !== 0 || a % 100 === 0 && a % 400 !== 0 ? i[b] : 29;
            }
            function g(a, b) {
                var c = new Array(b), d = new Date(a), e = 0;
                for (d.setHours(12); b > e; ) c[e++] = new Date(d), d.setDate(d.getDate() + 1);
                return c;
            }
            function h(a) {
                var b = new Date(a);
                b.setDate(b.getDate() + 4 - (b.getDay() || 7));
                var c = b.getTime();
                return b.setMonth(0), b.setDate(1), Math.floor(Math.round((c - b) / 864e5) / 7) + 1;
            }
            b.showWeeks = e.showWeeks, e.step = {
                months: 1
            }, e.element = c;
            var i = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
            e._refreshView = function() {
                var c = e.activeDate.getFullYear(), d = e.activeDate.getMonth(), f = new Date(c, d, 1), i = e.startingDay - f.getDay(), j = i > 0 ? 7 - i : -i, k = new Date(f);
                j > 0 && k.setDate(-j + 1);
                for (var l = g(k, 42), m = 0; 42 > m; m++) l[m] = angular.extend(e.createDateObject(l[m], e.formatDay), {
                    secondary: l[m].getMonth() !== d,
                    uid: b.uniqueId + "-" + m
                });
                b.labels = new Array(7);
                for (var n = 0; 7 > n; n++) b.labels[n] = {
                    abbr: a(l[n].date, e.formatDayHeader),
                    full: a(l[n].date, "EEEE")
                };
                if (b.title = a(e.activeDate, e.formatDayTitle), b.rows = e.split(l, 7), b.showWeeks) {
                    b.weekNumbers = [];
                    for (var o = h(b.rows[0][0].date), p = b.rows.length; b.weekNumbers.push(o++) < p; ) ;
                }
            }, e.compare = function(a, b) {
                return new Date(a.getFullYear(), a.getMonth(), a.getDate()) - new Date(b.getFullYear(), b.getMonth(), b.getDate());
            }, e.handleKeyDown = function(a) {
                var b = e.activeDate.getDate();
                if ("left" === a) b -= 1; else if ("up" === a) b -= 7; else if ("right" === a) b += 1; else if ("down" === a) b += 7; else if ("pageup" === a || "pagedown" === a) {
                    var c = e.activeDate.getMonth() + ("pageup" === a ? -1 : 1);
                    e.activeDate.setMonth(c, 1), b = Math.min(f(e.activeDate.getFullYear(), e.activeDate.getMonth()), b);
                } else "home" === a ? b = 1 : "end" === a && (b = f(e.activeDate.getFullYear(), e.activeDate.getMonth()));
                e.activeDate.setDate(b);
            }, e.refreshView();
        }
    };
} ]).directive("monthpicker", [ "dateFilter", function(a) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/month.html",
        require: "^datepicker",
        link: function(b, c, d, e) {
            e.step = {
                years: 1
            }, e.element = c, e._refreshView = function() {
                for (var c = new Array(12), d = e.activeDate.getFullYear(), f = 0; 12 > f; f++) c[f] = angular.extend(e.createDateObject(new Date(d, f, 1), e.formatMonth), {
                    uid: b.uniqueId + "-" + f
                });
                b.title = a(e.activeDate, e.formatMonthTitle), b.rows = e.split(c, 3);
            }, e.compare = function(a, b) {
                return new Date(a.getFullYear(), a.getMonth()) - new Date(b.getFullYear(), b.getMonth());
            }, e.handleKeyDown = function(a) {
                var b = e.activeDate.getMonth();
                if ("left" === a) b -= 1; else if ("up" === a) b -= 3; else if ("right" === a) b += 1; else if ("down" === a) b += 3; else if ("pageup" === a || "pagedown" === a) {
                    var c = e.activeDate.getFullYear() + ("pageup" === a ? -1 : 1);
                    e.activeDate.setFullYear(c);
                } else "home" === a ? b = 0 : "end" === a && (b = 11);
                e.activeDate.setMonth(b);
            }, e.refreshView();
        }
    };
} ]).directive("yearpicker", [ "dateFilter", function() {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/datepicker/year.html",
        require: "^datepicker",
        link: function(a, b, c, d) {
            function e(a) {
                return parseInt((a - 1) / f, 10) * f + 1;
            }
            var f = d.yearRange;
            d.step = {
                years: f
            }, d.element = b, d._refreshView = function() {
                for (var b = new Array(f), c = 0, g = e(d.activeDate.getFullYear()); f > c; c++) b[c] = angular.extend(d.createDateObject(new Date(g + c, 0, 1), d.formatYear), {
                    uid: a.uniqueId + "-" + c
                });
                a.title = [ b[0].label, b[f - 1].label ].join(" - "), a.rows = d.split(b, 5);
            }, d.compare = function(a, b) {
                return a.getFullYear() - b.getFullYear();
            }, d.handleKeyDown = function(a) {
                var b = d.activeDate.getFullYear();
                "left" === a ? b -= 1 : "up" === a ? b -= 5 : "right" === a ? b += 1 : "down" === a ? b += 5 : "pageup" === a || "pagedown" === a ? b += ("pageup" === a ? -1 : 1) * d.step.years : "home" === a ? b = e(d.activeDate.getFullYear()) : "end" === a && (b = e(d.activeDate.getFullYear()) + f - 1), 
                d.activeDate.setFullYear(b);
            }, d.refreshView();
        }
    };
} ]).constant("datepickerPopupConfig", {
    datepickerPopup: "yyyy-MM-dd",
    currentText: "Today",
    clearText: "Clear",
    closeText: "Done",
    closeOnDateSelection: !0,
    appendToBody: !1,
    showButtonBar: !0
}).directive("datepickerPopup", [ "$compile", "$parse", "$document", "$position", "dateFilter", "dateParser", "datepickerPopupConfig", function(a, b, c, d, e, f, g) {
    return {
        restrict: "EA",
        require: "ngModel",
        scope: {
            isOpen: "=?",
            currentText: "@",
            clearText: "@",
            closeText: "@",
            dateDisabled: "&"
        },
        link: function(h, i, j, k) {
            function l(a) {
                return a.replace(/([A-Z])/g, function(a) {
                    return "-" + a.toLowerCase();
                });
            }
            function m(a) {
                if (a) {
                    if (angular.isDate(a) && !isNaN(a)) return k.$setValidity("date", !0), a;
                    if (angular.isString(a)) {
                        var b = f.parse(a, n) || new Date(a);
                        return isNaN(b) ? void k.$setValidity("date", !1) : (k.$setValidity("date", !0), 
                        b);
                    }
                    return void k.$setValidity("date", !1);
                }
                return k.$setValidity("date", !0), null;
            }
            var n, o = angular.isDefined(j.closeOnDateSelection) ? h.$parent.$eval(j.closeOnDateSelection) : g.closeOnDateSelection, p = angular.isDefined(j.datepickerAppendToBody) ? h.$parent.$eval(j.datepickerAppendToBody) : g.appendToBody;
            h.showButtonBar = angular.isDefined(j.showButtonBar) ? h.$parent.$eval(j.showButtonBar) : g.showButtonBar, 
            h.getText = function(a) {
                return h[a + "Text"] || g[a + "Text"];
            }, j.$observe("datepickerPopup", function(a) {
                n = a || g.datepickerPopup, k.$render();
            });
            var q = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
            q.attr({
                "ng-model": "date",
                "ng-change": "dateSelection()"
            });
            var r = angular.element(q.children()[0]);
            j.datepickerOptions && angular.forEach(h.$parent.$eval(j.datepickerOptions), function(a, b) {
                r.attr(l(b), a);
            }), h.watchData = {}, angular.forEach([ "minDate", "maxDate", "datepickerMode" ], function(a) {
                if (j[a]) {
                    var c = b(j[a]);
                    if (h.$parent.$watch(c, function(b) {
                        h.watchData[a] = b;
                    }), r.attr(l(a), "watchData." + a), "datepickerMode" === a) {
                        var d = c.assign;
                        h.$watch("watchData." + a, function(a, b) {
                            a !== b && d(h.$parent, a);
                        });
                    }
                }
            }), j.dateDisabled && r.attr("date-disabled", "dateDisabled({ date: date, mode: mode })"), 
            k.$parsers.unshift(m), h.dateSelection = function(a) {
                angular.isDefined(a) && (h.date = a), k.$setViewValue(h.date), k.$render(), o && (h.isOpen = !1, 
                i[0].focus());
            }, i.bind("input change keyup", function() {
                h.$apply(function() {
                    h.date = k.$modelValue;
                });
            }), k.$render = function() {
                var a = k.$viewValue ? e(k.$viewValue, n) : "";
                i.val(a), h.date = m(k.$modelValue);
            };
            var s = function(a) {
                h.isOpen && a.target !== i[0] && h.$apply(function() {
                    h.isOpen = !1;
                });
            }, t = function(a) {
                h.keydown(a);
            };
            i.bind("keydown", t), h.keydown = function(a) {
                27 === a.which ? (a.preventDefault(), a.stopPropagation(), h.close()) : 40 !== a.which || h.isOpen || (h.isOpen = !0);
            }, h.$watch("isOpen", function(a) {
                a ? (h.$broadcast("datepicker.focus"), h.position = p ? d.offset(i) : d.position(i), 
                h.position.top = h.position.top + i.prop("offsetHeight"), c.bind("click", s)) : c.unbind("click", s);
            }), h.select = function(a) {
                if ("today" === a) {
                    var b = new Date();
                    angular.isDate(k.$modelValue) ? (a = new Date(k.$modelValue), a.setFullYear(b.getFullYear(), b.getMonth(), b.getDate())) : a = new Date(b.setHours(0, 0, 0, 0));
                }
                h.dateSelection(a);
            }, h.close = function() {
                h.isOpen = !1, i[0].focus();
            };
            var u = a(q)(h);
            q.remove(), p ? c.find("body").append(u) : i.after(u), h.$on("$destroy", function() {
                u.remove(), i.unbind("keydown", t), c.unbind("click", s);
            });
        }
    };
} ]).directive("datepickerPopupWrap", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        templateUrl: "template/datepicker/popup.html",
        link: function(a, b) {
            b.bind("click", function(a) {
                a.preventDefault(), a.stopPropagation();
            });
        }
    };
}), angular.module("ui.bootstrap.dropdown", []).constant("dropdownConfig", {
    openClass: "open"
}).service("dropdownService", [ "$document", function(a) {
    var b = null;
    this.open = function(e) {
        b || (a.bind("click", c), a.bind("keydown", d)), b && b !== e && (b.isOpen = !1), 
        b = e;
    }, this.close = function(e) {
        b === e && (b = null, a.unbind("click", c), a.unbind("keydown", d));
    };
    var c = function(a) {
        if (b) {
            var c = b.getToggleElement();
            a && c && c[0].contains(a.target) || b.$apply(function() {
                b.isOpen = !1;
            });
        }
    }, d = function(a) {
        27 === a.which && (b.focusToggleElement(), c());
    };
} ]).controller("DropdownController", [ "$scope", "$attrs", "$parse", "dropdownConfig", "dropdownService", "$animate", function(a, b, c, d, e, f) {
    var g, h = this, i = a.$new(), j = d.openClass, k = angular.noop, l = b.onToggle ? c(b.onToggle) : angular.noop;
    this.init = function(d) {
        h.$element = d, b.isOpen && (g = c(b.isOpen), k = g.assign, a.$watch(g, function(a) {
            i.isOpen = !!a;
        }));
    }, this.toggle = function(a) {
        return i.isOpen = arguments.length ? !!a : !i.isOpen;
    }, this.isOpen = function() {
        return i.isOpen;
    }, i.getToggleElement = function() {
        return h.toggleElement;
    }, i.focusToggleElement = function() {
        h.toggleElement && h.toggleElement[0].focus();
    }, i.$watch("isOpen", function(b, c) {
        f[b ? "addClass" : "removeClass"](h.$element, j), b ? (i.focusToggleElement(), e.open(i)) : e.close(i), 
        k(a, b), angular.isDefined(b) && b !== c && l(a, {
            open: !!b
        });
    }), a.$on("$locationChangeSuccess", function() {
        i.isOpen = !1;
    }), a.$on("$destroy", function() {
        i.$destroy();
    });
} ]).directive("dropdown", function() {
    return {
        controller: "DropdownController",
        link: function(a, b, c, d) {
            d.init(b);
        }
    };
}).directive("dropdownToggle", function() {
    return {
        require: "?^dropdown",
        link: function(a, b, c, d) {
            if (d) {
                d.toggleElement = b;
                var e = function(e) {
                    e.preventDefault(), b.hasClass("disabled") || c.disabled || a.$apply(function() {
                        d.toggle();
                    });
                };
                b.bind("click", e), b.attr({
                    "aria-haspopup": !0,
                    "aria-expanded": !1
                }), a.$watch(d.isOpen, function(a) {
                    b.attr("aria-expanded", !!a);
                }), a.$on("$destroy", function() {
                    b.unbind("click", e);
                });
            }
        }
    };
}), angular.module("ui.bootstrap.modal", [ "ui.bootstrap.transition" ]).factory("$$stackedMap", function() {
    return {
        createNew: function() {
            var a = [];
            return {
                add: function(b, c) {
                    a.push({
                        key: b,
                        value: c
                    });
                },
                get: function(b) {
                    for (var c = 0; c < a.length; c++) if (b == a[c].key) return a[c];
                },
                keys: function() {
                    for (var b = [], c = 0; c < a.length; c++) b.push(a[c].key);
                    return b;
                },
                top: function() {
                    return a[a.length - 1];
                },
                remove: function(b) {
                    for (var c = -1, d = 0; d < a.length; d++) if (b == a[d].key) {
                        c = d;
                        break;
                    }
                    return a.splice(c, 1)[0];
                },
                removeTop: function() {
                    return a.splice(a.length - 1, 1)[0];
                },
                length: function() {
                    return a.length;
                }
            };
        }
    };
}).directive("modalBackdrop", [ "$timeout", function(a) {
    return {
        restrict: "EA",
        replace: !0,
        templateUrl: "template/modal/backdrop.html",
        link: function(b, c, d) {
            b.backdropClass = d.backdropClass || "", b.animate = !1, a(function() {
                b.animate = !0;
            });
        }
    };
} ]).directive("modalWindow", [ "$modalStack", "$timeout", function(a, b) {
    return {
        restrict: "EA",
        scope: {
            index: "@",
            animate: "="
        },
        replace: !0,
        transclude: !0,
        templateUrl: function(a, b) {
            return b.templateUrl || "template/modal/window.html";
        },
        link: function(c, d, e) {
            d.addClass(e.windowClass || ""), c.size = e.size, b(function() {
                c.animate = !0, d[0].querySelectorAll("[autofocus]").length || d[0].focus();
            }), c.close = function(b) {
                var c = a.getTop();
                c && c.value.backdrop && "static" != c.value.backdrop && b.target === b.currentTarget && (b.preventDefault(), 
                b.stopPropagation(), a.dismiss(c.key, "backdrop click"));
            };
        }
    };
} ]).directive("modalTransclude", function() {
    return {
        link: function(a, b, c, d, e) {
            e(a.$parent, function(a) {
                b.empty(), b.append(a);
            });
        }
    };
}).factory("$modalStack", [ "$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap", function(a, b, c, d, e, f) {
    function g() {
        for (var a = -1, b = n.keys(), c = 0; c < b.length; c++) n.get(b[c]).value.backdrop && (a = c);
        return a;
    }
    function h(a) {
        var b = c.find("body").eq(0), d = n.get(a).value;
        n.remove(a), j(d.modalDomEl, d.modalScope, 300, function() {
            d.modalScope.$destroy(), b.toggleClass(m, n.length() > 0), i();
        });
    }
    function i() {
        if (k && -1 == g()) {
            var a = l;
            j(k, l, 150, function() {
                a.$destroy(), a = null;
            }), k = void 0, l = void 0;
        }
    }
    function j(c, d, e, f) {
        function g() {
            g.done || (g.done = !0, c.remove(), f && f());
        }
        d.animate = !1;
        var h = a.transitionEndEventName;
        if (h) {
            var i = b(g, e);
            c.bind(h, function() {
                b.cancel(i), g(), d.$apply();
            });
        } else b(g);
    }
    var k, l, m = "modal-open", n = f.createNew(), o = {};
    return e.$watch(g, function(a) {
        l && (l.index = a);
    }), c.bind("keydown", function(a) {
        var b;
        27 === a.which && (b = n.top(), b && b.value.keyboard && (a.preventDefault(), e.$apply(function() {
            o.dismiss(b.key, "escape key press");
        })));
    }), o.open = function(a, b) {
        n.add(a, {
            deferred: b.deferred,
            modalScope: b.scope,
            backdrop: b.backdrop,
            keyboard: b.keyboard
        });
        var f = c.find("body").eq(0), h = g();
        if (h >= 0 && !k) {
            l = e.$new(!0), l.index = h;
            var i = angular.element("<div modal-backdrop></div>");
            i.attr("backdrop-class", b.backdropClass), k = d(i)(l), f.append(k);
        }
        var j = angular.element("<div modal-window></div>");
        j.attr({
            "template-url": b.windowTemplateUrl,
            "window-class": b.windowClass,
            size: b.size,
            index: n.length() - 1,
            animate: "animate"
        }).html(b.content);
        var o = d(j)(b.scope);
        n.top().value.modalDomEl = o, f.append(o), f.addClass(m);
    }, o.close = function(a, b) {
        var c = n.get(a);
        c && (c.value.deferred.resolve(b), h(a));
    }, o.dismiss = function(a, b) {
        var c = n.get(a);
        c && (c.value.deferred.reject(b), h(a));
    }, o.dismissAll = function(a) {
        for (var b = this.getTop(); b; ) this.dismiss(b.key, a), b = this.getTop();
    }, o.getTop = function() {
        return n.top();
    }, o;
} ]).provider("$modal", function() {
    var a = {
        options: {
            backdrop: !0,
            keyboard: !0
        },
        $get: [ "$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack", function(b, c, d, e, f, g, h) {
            function i(a) {
                return a.template ? d.when(a.template) : e.get(angular.isFunction(a.templateUrl) ? a.templateUrl() : a.templateUrl, {
                    cache: f
                }).then(function(a) {
                    return a.data;
                });
            }
            function j(a) {
                var c = [];
                return angular.forEach(a, function(a) {
                    (angular.isFunction(a) || angular.isArray(a)) && c.push(d.when(b.invoke(a)));
                }), c;
            }
            var k = {};
            return k.open = function(b) {
                var e = d.defer(), f = d.defer(), k = {
                    result: e.promise,
                    opened: f.promise,
                    close: function(a) {
                        h.close(k, a);
                    },
                    dismiss: function(a) {
                        h.dismiss(k, a);
                    }
                };
                if (b = angular.extend({}, a.options, b), b.resolve = b.resolve || {}, !b.template && !b.templateUrl) throw new Error("One of template or templateUrl options is required.");
                var l = d.all([ i(b) ].concat(j(b.resolve)));
                return l.then(function(a) {
                    var d = (b.scope || c).$new();
                    d.$close = k.close, d.$dismiss = k.dismiss;
                    var f, i = {}, j = 1;
                    b.controller && (i.$scope = d, i.$modalInstance = k, angular.forEach(b.resolve, function(b, c) {
                        i[c] = a[j++];
                    }), f = g(b.controller, i), b.controllerAs && (d[b.controllerAs] = f)), h.open(k, {
                        scope: d,
                        deferred: e,
                        content: a[0],
                        backdrop: b.backdrop,
                        keyboard: b.keyboard,
                        backdropClass: b.backdropClass,
                        windowClass: b.windowClass,
                        windowTemplateUrl: b.windowTemplateUrl,
                        size: b.size
                    });
                }, function(a) {
                    e.reject(a);
                }), l.then(function() {
                    f.resolve(!0);
                }, function() {
                    f.reject(!1);
                }), k;
            }, k;
        } ]
    };
    return a;
}), angular.module("ui.bootstrap.pagination", []).controller("PaginationController", [ "$scope", "$attrs", "$parse", function(a, b, c) {
    var d = this, e = {
        $setViewValue: angular.noop
    }, f = b.numPages ? c(b.numPages).assign : angular.noop;
    this.init = function(f, g) {
        e = f, this.config = g, e.$render = function() {
            d.render();
        }, b.itemsPerPage ? a.$parent.$watch(c(b.itemsPerPage), function(b) {
            d.itemsPerPage = parseInt(b, 10), a.totalPages = d.calculateTotalPages();
        }) : this.itemsPerPage = g.itemsPerPage;
    }, this.calculateTotalPages = function() {
        var b = this.itemsPerPage < 1 ? 1 : Math.ceil(a.totalItems / this.itemsPerPage);
        return Math.max(b || 0, 1);
    }, this.render = function() {
        a.page = parseInt(e.$viewValue, 10) || 1;
    }, a.selectPage = function(b) {
        a.page !== b && b > 0 && b <= a.totalPages && (e.$setViewValue(b), e.$render());
    }, a.getText = function(b) {
        return a[b + "Text"] || d.config[b + "Text"];
    }, a.noPrevious = function() {
        return 1 === a.page;
    }, a.noNext = function() {
        return a.page === a.totalPages;
    }, a.$watch("totalItems", function() {
        a.totalPages = d.calculateTotalPages();
    }), a.$watch("totalPages", function(b) {
        f(a.$parent, b), a.page > b ? a.selectPage(b) : e.$render();
    });
} ]).constant("paginationConfig", {
    itemsPerPage: 10,
    boundaryLinks: !1,
    directionLinks: !0,
    firstText: "First",
    previousText: "Previous",
    nextText: "Next",
    lastText: "Last",
    rotate: !0
}).directive("pagination", [ "$parse", "paginationConfig", function(a, b) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            firstText: "@",
            previousText: "@",
            nextText: "@",
            lastText: "@"
        },
        require: [ "pagination", "?ngModel" ],
        controller: "PaginationController",
        templateUrl: "template/pagination/pagination.html",
        replace: !0,
        link: function(c, d, e, f) {
            function g(a, b, c) {
                return {
                    number: a,
                    text: b,
                    active: c
                };
            }
            function h(a, b) {
                var c = [], d = 1, e = b, f = angular.isDefined(k) && b > k;
                f && (l ? (d = Math.max(a - Math.floor(k / 2), 1), e = d + k - 1, e > b && (e = b, 
                d = e - k + 1)) : (d = (Math.ceil(a / k) - 1) * k + 1, e = Math.min(d + k - 1, b)));
                for (var h = d; e >= h; h++) {
                    var i = g(h, h, h === a);
                    c.push(i);
                }
                if (f && !l) {
                    if (d > 1) {
                        var j = g(d - 1, "...", !1);
                        c.unshift(j);
                    }
                    if (b > e) {
                        var m = g(e + 1, "...", !1);
                        c.push(m);
                    }
                }
                return c;
            }
            var i = f[0], j = f[1];
            if (j) {
                var k = angular.isDefined(e.maxSize) ? c.$parent.$eval(e.maxSize) : b.maxSize, l = angular.isDefined(e.rotate) ? c.$parent.$eval(e.rotate) : b.rotate;
                c.boundaryLinks = angular.isDefined(e.boundaryLinks) ? c.$parent.$eval(e.boundaryLinks) : b.boundaryLinks, 
                c.directionLinks = angular.isDefined(e.directionLinks) ? c.$parent.$eval(e.directionLinks) : b.directionLinks, 
                i.init(j, b), e.maxSize && c.$parent.$watch(a(e.maxSize), function(a) {
                    k = parseInt(a, 10), i.render();
                });
                var m = i.render;
                i.render = function() {
                    m(), c.page > 0 && c.page <= c.totalPages && (c.pages = h(c.page, c.totalPages));
                };
            }
        }
    };
} ]).constant("pagerConfig", {
    itemsPerPage: 10,
    previousText: "Â« Previous",
    nextText: "Next Â»",
    align: !0
}).directive("pager", [ "pagerConfig", function(a) {
    return {
        restrict: "EA",
        scope: {
            totalItems: "=",
            previousText: "@",
            nextText: "@"
        },
        require: [ "pager", "?ngModel" ],
        controller: "PaginationController",
        templateUrl: "template/pagination/pager.html",
        replace: !0,
        link: function(b, c, d, e) {
            var f = e[0], g = e[1];
            g && (b.align = angular.isDefined(d.align) ? b.$parent.$eval(d.align) : a.align, 
            f.init(g, a));
        }
    };
} ]), angular.module("ui.bootstrap.tooltip", [ "ui.bootstrap.position", "ui.bootstrap.bindHtml" ]).provider("$tooltip", function() {
    function a(a) {
        var b = /[A-Z]/g, c = "-";
        return a.replace(b, function(a, b) {
            return (b ? c : "") + a.toLowerCase();
        });
    }
    var b = {
        placement: "top",
        animation: !0,
        popupDelay: 0
    }, c = {
        mouseenter: "mouseleave",
        click: "click",
        focus: "blur"
    }, d = {};
    this.options = function(a) {
        angular.extend(d, a);
    }, this.setTriggers = function(a) {
        angular.extend(c, a);
    }, this.$get = [ "$window", "$compile", "$timeout", "$document", "$position", "$interpolate", function(e, f, g, h, i, j) {
        return function(e, k, l) {
            function m(a) {
                var b = a || n.trigger || l, d = c[b] || b;
                return {
                    show: b,
                    hide: d
                };
            }
            var n = angular.extend({}, b, d), o = a(e), p = j.startSymbol(), q = j.endSymbol(), r = "<div " + o + '-popup title="' + p + "title" + q + '" content="' + p + "content" + q + '" placement="' + p + "placement" + q + '" animation="animation" is-open="isOpen"></div>';
            return {
                restrict: "EA",
                compile: function() {
                    var a = f(r);
                    return function(b, c, d) {
                        function f() {
                            D.isOpen ? l() : j();
                        }
                        function j() {
                            (!C || b.$eval(d[k + "Enable"])) && (s(), D.popupDelay ? z || (z = g(o, D.popupDelay, !1), 
                            z.then(function(a) {
                                a();
                            })) : o()());
                        }
                        function l() {
                            b.$apply(function() {
                                p();
                            });
                        }
                        function o() {
                            return z = null, y && (g.cancel(y), y = null), D.content ? (q(), w.css({
                                top: 0,
                                left: 0,
                                display: "block"
                            }), D.$digest(), E(), D.isOpen = !0, D.$digest(), E) : angular.noop;
                        }
                        function p() {
                            D.isOpen = !1, g.cancel(z), z = null, D.animation ? y || (y = g(r, 500)) : r();
                        }
                        function q() {
                            w && r(), x = D.$new(), w = a(x, function(a) {
                                A ? h.find("body").append(a) : c.after(a);
                            });
                        }
                        function r() {
                            y = null, w && (w.remove(), w = null), x && (x.$destroy(), x = null);
                        }
                        function s() {
                            t(), u();
                        }
                        function t() {
                            var a = d[k + "Placement"];
                            D.placement = angular.isDefined(a) ? a : n.placement;
                        }
                        function u() {
                            var a = d[k + "PopupDelay"], b = parseInt(a, 10);
                            D.popupDelay = isNaN(b) ? n.popupDelay : b;
                        }
                        function v() {
                            var a = d[k + "Trigger"];
                            F(), B = m(a), B.show === B.hide ? c.bind(B.show, f) : (c.bind(B.show, j), c.bind(B.hide, l));
                        }
                        var w, x, y, z, A = angular.isDefined(n.appendToBody) ? n.appendToBody : !1, B = m(void 0), C = angular.isDefined(d[k + "Enable"]), D = b.$new(!0), E = function() {
                            var a = i.positionElements(c, w, D.placement, A);
                            a.top += "px", a.left += "px", w.css(a);
                        };
                        D.isOpen = !1, d.$observe(e, function(a) {
                            D.content = a, !a && D.isOpen && p();
                        }), d.$observe(k + "Title", function(a) {
                            D.title = a;
                        });
                        var F = function() {
                            c.unbind(B.show, j), c.unbind(B.hide, l);
                        };
                        v();
                        var G = b.$eval(d[k + "Animation"]);
                        D.animation = angular.isDefined(G) ? !!G : n.animation;
                        var H = b.$eval(d[k + "AppendToBody"]);
                        A = angular.isDefined(H) ? H : A, A && b.$on("$locationChangeSuccess", function() {
                            D.isOpen && p();
                        }), b.$on("$destroy", function() {
                            g.cancel(y), g.cancel(z), F(), r(), D = null;
                        });
                    };
                }
            };
        };
    } ];
}).directive("tooltipPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-popup.html"
    };
}).directive("tooltip", [ "$tooltip", function(a) {
    return a("tooltip", "tooltip", "mouseenter");
} ]).directive("tooltipHtmlUnsafePopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
    };
}).directive("tooltipHtmlUnsafe", [ "$tooltip", function(a) {
    return a("tooltipHtmlUnsafe", "tooltip", "mouseenter");
} ]), angular.module("ui.bootstrap.popover", [ "ui.bootstrap.tooltip" ]).directive("popoverPopup", function() {
    return {
        restrict: "EA",
        replace: !0,
        scope: {
            title: "@",
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/popover/popover.html"
    };
}).directive("popover", [ "$tooltip", function(a) {
    return a("popover", "popover", "click");
} ]), angular.module("ui.bootstrap.progressbar", []).constant("progressConfig", {
    animate: !0,
    max: 100
}).controller("ProgressController", [ "$scope", "$attrs", "progressConfig", function(a, b, c) {
    var d = this, e = angular.isDefined(b.animate) ? a.$parent.$eval(b.animate) : c.animate;
    this.bars = [], a.max = angular.isDefined(b.max) ? a.$parent.$eval(b.max) : c.max, 
    this.addBar = function(b, c) {
        e || c.css({
            transition: "none"
        }), this.bars.push(b), b.$watch("value", function(c) {
            b.percent = +(100 * c / a.max).toFixed(2);
        }), b.$on("$destroy", function() {
            c = null, d.removeBar(b);
        });
    }, this.removeBar = function(a) {
        this.bars.splice(this.bars.indexOf(a), 1);
    };
} ]).directive("progress", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        require: "progress",
        scope: {},
        templateUrl: "template/progressbar/progress.html"
    };
}).directive("bar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        require: "^progress",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/bar.html",
        link: function(a, b, c, d) {
            d.addBar(a, b);
        }
    };
}).directive("progressbar", function() {
    return {
        restrict: "EA",
        replace: !0,
        transclude: !0,
        controller: "ProgressController",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/progressbar.html",
        link: function(a, b, c, d) {
            d.addBar(a, angular.element(b.children()[0]));
        }
    };
}), angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
    max: 5,
    stateOn: null,
    stateOff: null
}).controller("RatingController", [ "$scope", "$attrs", "ratingConfig", function(a, b, c) {
    var d = {
        $setViewValue: angular.noop
    };
    this.init = function(e) {
        d = e, d.$render = this.render, this.stateOn = angular.isDefined(b.stateOn) ? a.$parent.$eval(b.stateOn) : c.stateOn, 
        this.stateOff = angular.isDefined(b.stateOff) ? a.$parent.$eval(b.stateOff) : c.stateOff;
        var f = angular.isDefined(b.ratingStates) ? a.$parent.$eval(b.ratingStates) : new Array(angular.isDefined(b.max) ? a.$parent.$eval(b.max) : c.max);
        a.range = this.buildTemplateObjects(f);
    }, this.buildTemplateObjects = function(a) {
        for (var b = 0, c = a.length; c > b; b++) a[b] = angular.extend({
            index: b
        }, {
            stateOn: this.stateOn,
            stateOff: this.stateOff
        }, a[b]);
        return a;
    }, a.rate = function(b) {
        !a.readonly && b >= 0 && b <= a.range.length && (d.$setViewValue(b), d.$render());
    }, a.enter = function(b) {
        a.readonly || (a.value = b), a.onHover({
            value: b
        });
    }, a.reset = function() {
        a.value = d.$viewValue, a.onLeave();
    }, a.onKeydown = function(b) {
        /(37|38|39|40)/.test(b.which) && (b.preventDefault(), b.stopPropagation(), a.rate(a.value + (38 === b.which || 39 === b.which ? 1 : -1)));
    }, this.render = function() {
        a.value = d.$viewValue;
    };
} ]).directive("rating", function() {
    return {
        restrict: "EA",
        require: [ "rating", "ngModel" ],
        scope: {
            readonly: "=?",
            onHover: "&",
            onLeave: "&"
        },
        controller: "RatingController",
        templateUrl: "template/rating/rating.html",
        replace: !0,
        link: function(a, b, c, d) {
            var e = d[0], f = d[1];
            f && e.init(f);
        }
    };
}), angular.module("ui.bootstrap.tabs", []).controller("TabsetController", [ "$scope", function(a) {
    var b = this, c = b.tabs = a.tabs = [];
    b.select = function(a) {
        angular.forEach(c, function(b) {
            b.active && b !== a && (b.active = !1, b.onDeselect());
        }), a.active = !0, a.onSelect();
    }, b.addTab = function(a) {
        c.push(a), 1 === c.length ? a.active = !0 : a.active && b.select(a);
    }, b.removeTab = function(a) {
        var e = c.indexOf(a);
        if (a.active && c.length > 1 && !d) {
            var f = e == c.length - 1 ? e - 1 : e + 1;
            b.select(c[f]);
        }
        c.splice(e, 1);
    };
    var d;
    a.$on("$destroy", function() {
        d = !0;
    });
} ]).directive("tabset", function() {
    return {
        restrict: "EA",
        transclude: !0,
        replace: !0,
        scope: {
            type: "@"
        },
        controller: "TabsetController",
        templateUrl: "template/tabs/tabset.html",
        link: function(a, b, c) {
            a.vertical = angular.isDefined(c.vertical) ? a.$parent.$eval(c.vertical) : !1, a.justified = angular.isDefined(c.justified) ? a.$parent.$eval(c.justified) : !1;
        }
    };
}).directive("tab", [ "$parse", function(a) {
    return {
        require: "^tabset",
        restrict: "EA",
        replace: !0,
        templateUrl: "template/tabs/tab.html",
        transclude: !0,
        scope: {
            active: "=?",
            heading: "@",
            onSelect: "&select",
            onDeselect: "&deselect"
        },
        controller: function() {},
        compile: function(b, c, d) {
            return function(b, c, e, f) {
                b.$watch("active", function(a) {
                    a && f.select(b);
                }), b.disabled = !1, e.disabled && b.$parent.$watch(a(e.disabled), function(a) {
                    b.disabled = !!a;
                }), b.select = function() {
                    b.disabled || (b.active = !0);
                }, f.addTab(b), b.$on("$destroy", function() {
                    f.removeTab(b);
                }), b.$transcludeFn = d;
            };
        }
    };
} ]).directive("tabHeadingTransclude", [ function() {
    return {
        restrict: "A",
        require: "^tab",
        link: function(a, b) {
            a.$watch("headingElement", function(a) {
                a && (b.html(""), b.append(a));
            });
        }
    };
} ]).directive("tabContentTransclude", function() {
    function a(a) {
        return a.tagName && (a.hasAttribute("tab-heading") || a.hasAttribute("data-tab-heading") || "tab-heading" === a.tagName.toLowerCase() || "data-tab-heading" === a.tagName.toLowerCase());
    }
    return {
        restrict: "A",
        require: "^tabset",
        link: function(b, c, d) {
            var e = b.$eval(d.tabContentTransclude);
            e.$transcludeFn(e.$parent, function(b) {
                angular.forEach(b, function(b) {
                    a(b) ? e.headingElement = b : c.append(b);
                });
            });
        }
    };
}), angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
    hourStep: 1,
    minuteStep: 1,
    showMeridian: !0,
    meridians: null,
    readonlyInput: !1,
    mousewheel: !0
}).controller("TimepickerController", [ "$scope", "$attrs", "$parse", "$log", "$locale", "timepickerConfig", function(a, b, c, d, e, f) {
    function g() {
        var b = parseInt(a.hours, 10), c = a.showMeridian ? b > 0 && 13 > b : b >= 0 && 24 > b;
        return c ? (a.showMeridian && (12 === b && (b = 0), a.meridian === p[1] && (b += 12)), 
        b) : void 0;
    }
    function h() {
        var b = parseInt(a.minutes, 10);
        return b >= 0 && 60 > b ? b : void 0;
    }
    function i(a) {
        return angular.isDefined(a) && a.toString().length < 2 ? "0" + a : a;
    }
    function j(a) {
        k(), o.$setViewValue(new Date(n)), l(a);
    }
    function k() {
        o.$setValidity("time", !0), a.invalidHours = !1, a.invalidMinutes = !1;
    }
    function l(b) {
        var c = n.getHours(), d = n.getMinutes();
        a.showMeridian && (c = 0 === c || 12 === c ? 12 : c % 12), a.hours = "h" === b ? c : i(c), 
        a.minutes = "m" === b ? d : i(d), a.meridian = n.getHours() < 12 ? p[0] : p[1];
    }
    function m(a) {
        var b = new Date(n.getTime() + 6e4 * a);
        n.setHours(b.getHours(), b.getMinutes()), j();
    }
    var n = new Date(), o = {
        $setViewValue: angular.noop
    }, p = angular.isDefined(b.meridians) ? a.$parent.$eval(b.meridians) : f.meridians || e.DATETIME_FORMATS.AMPMS;
    this.init = function(c, d) {
        o = c, o.$render = this.render;
        var e = d.eq(0), g = d.eq(1), h = angular.isDefined(b.mousewheel) ? a.$parent.$eval(b.mousewheel) : f.mousewheel;
        h && this.setupMousewheelEvents(e, g), a.readonlyInput = angular.isDefined(b.readonlyInput) ? a.$parent.$eval(b.readonlyInput) : f.readonlyInput, 
        this.setupInputEvents(e, g);
    };
    var q = f.hourStep;
    b.hourStep && a.$parent.$watch(c(b.hourStep), function(a) {
        q = parseInt(a, 10);
    });
    var r = f.minuteStep;
    b.minuteStep && a.$parent.$watch(c(b.minuteStep), function(a) {
        r = parseInt(a, 10);
    }), a.showMeridian = f.showMeridian, b.showMeridian && a.$parent.$watch(c(b.showMeridian), function(b) {
        if (a.showMeridian = !!b, o.$error.time) {
            var c = g(), d = h();
            angular.isDefined(c) && angular.isDefined(d) && (n.setHours(c), j());
        } else l();
    }), this.setupMousewheelEvents = function(b, c) {
        var d = function(a) {
            a.originalEvent && (a = a.originalEvent);
            var b = a.wheelDelta ? a.wheelDelta : -a.deltaY;
            return a.detail || b > 0;
        };
        b.bind("mousewheel wheel", function(b) {
            a.$apply(d(b) ? a.incrementHours() : a.decrementHours()), b.preventDefault();
        }), c.bind("mousewheel wheel", function(b) {
            a.$apply(d(b) ? a.incrementMinutes() : a.decrementMinutes()), b.preventDefault();
        });
    }, this.setupInputEvents = function(b, c) {
        if (a.readonlyInput) return a.updateHours = angular.noop, void (a.updateMinutes = angular.noop);
        var d = function(b, c) {
            o.$setViewValue(null), o.$setValidity("time", !1), angular.isDefined(b) && (a.invalidHours = b), 
            angular.isDefined(c) && (a.invalidMinutes = c);
        };
        a.updateHours = function() {
            var a = g();
            angular.isDefined(a) ? (n.setHours(a), j("h")) : d(!0);
        }, b.bind("blur", function() {
            !a.invalidHours && a.hours < 10 && a.$apply(function() {
                a.hours = i(a.hours);
            });
        }), a.updateMinutes = function() {
            var a = h();
            angular.isDefined(a) ? (n.setMinutes(a), j("m")) : d(void 0, !0);
        }, c.bind("blur", function() {
            !a.invalidMinutes && a.minutes < 10 && a.$apply(function() {
                a.minutes = i(a.minutes);
            });
        });
    }, this.render = function() {
        var a = o.$modelValue ? new Date(o.$modelValue) : null;
        isNaN(a) ? (o.$setValidity("time", !1), d.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : (a && (n = a), 
        k(), l());
    }, a.incrementHours = function() {
        m(60 * q);
    }, a.decrementHours = function() {
        m(60 * -q);
    }, a.incrementMinutes = function() {
        m(r);
    }, a.decrementMinutes = function() {
        m(-r);
    }, a.toggleMeridian = function() {
        m(720 * (n.getHours() < 12 ? 1 : -1));
    };
} ]).directive("timepicker", function() {
    return {
        restrict: "EA",
        require: [ "timepicker", "?^ngModel" ],
        controller: "TimepickerController",
        replace: !0,
        scope: {},
        templateUrl: "template/timepicker/timepicker.html",
        link: function(a, b, c, d) {
            var e = d[0], f = d[1];
            f && e.init(f, b.find("input"));
        }
    };
}), angular.module("ui.bootstrap.typeahead", [ "ui.bootstrap.position", "ui.bootstrap.bindHtml" ]).factory("typeaheadParser", [ "$parse", function(a) {
    var b = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;
    return {
        parse: function(c) {
            var d = c.match(b);
            if (!d) throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "' + c + '".');
            return {
                itemName: d[3],
                source: a(d[4]),
                viewMapper: a(d[2] || d[1]),
                modelMapper: a(d[1])
            };
        }
    };
} ]).directive("typeahead", [ "$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser", function(a, b, c, d, e, f, g) {
    var h = [ 9, 13, 27, 38, 40 ];
    return {
        require: "ngModel",
        link: function(i, j, k, l) {
            var m, n = i.$eval(k.typeaheadMinLength) || 1, o = i.$eval(k.typeaheadWaitMs) || 0, p = i.$eval(k.typeaheadEditable) !== !1, q = b(k.typeaheadLoading).assign || angular.noop, r = b(k.typeaheadOnSelect), s = k.typeaheadInputFormatter ? b(k.typeaheadInputFormatter) : void 0, t = k.typeaheadAppendToBody ? i.$eval(k.typeaheadAppendToBody) : !1, u = i.$eval(k.typeaheadFocusFirst) !== !1, v = b(k.ngModel).assign, w = g.parse(k.typeahead), x = i.$new();
            i.$on("$destroy", function() {
                x.$destroy();
            });
            var y = "typeahead-" + x.$id + "-" + Math.floor(1e4 * Math.random());
            j.attr({
                "aria-autocomplete": "list",
                "aria-expanded": !1,
                "aria-owns": y
            });
            var z = angular.element("<div typeahead-popup></div>");
            z.attr({
                id: y,
                matches: "matches",
                active: "activeIdx",
                select: "select(activeIdx)",
                query: "query",
                position: "position"
            }), angular.isDefined(k.typeaheadTemplateUrl) && z.attr("template-url", k.typeaheadTemplateUrl);
            var A = function() {
                x.matches = [], x.activeIdx = -1, j.attr("aria-expanded", !1);
            }, B = function(a) {
                return y + "-option-" + a;
            };
            x.$watch("activeIdx", function(a) {
                0 > a ? j.removeAttr("aria-activedescendant") : j.attr("aria-activedescendant", B(a));
            });
            var C = function(a) {
                var b = {
                    $viewValue: a
                };
                q(i, !0), c.when(w.source(i, b)).then(function(c) {
                    var d = a === l.$viewValue;
                    if (d && m) if (c.length > 0) {
                        x.activeIdx = u ? 0 : -1, x.matches.length = 0;
                        for (var e = 0; e < c.length; e++) b[w.itemName] = c[e], x.matches.push({
                            id: B(e),
                            label: w.viewMapper(x, b),
                            model: c[e]
                        });
                        x.query = a, x.position = t ? f.offset(j) : f.position(j), x.position.top = x.position.top + j.prop("offsetHeight"), 
                        j.attr("aria-expanded", !0);
                    } else A();
                    d && q(i, !1);
                }, function() {
                    A(), q(i, !1);
                });
            };
            A(), x.query = void 0;
            var D, E = function(a) {
                D = d(function() {
                    C(a);
                }, o);
            }, F = function() {
                D && d.cancel(D);
            };
            l.$parsers.unshift(function(a) {
                return m = !0, a && a.length >= n ? o > 0 ? (F(), E(a)) : C(a) : (q(i, !1), F(), 
                A()), p ? a : a ? void l.$setValidity("editable", !1) : (l.$setValidity("editable", !0), 
                a);
            }), l.$formatters.push(function(a) {
                var b, c, d = {};
                return s ? (d.$model = a, s(i, d)) : (d[w.itemName] = a, b = w.viewMapper(i, d), 
                d[w.itemName] = void 0, c = w.viewMapper(i, d), b !== c ? b : a);
            }), x.select = function(a) {
                var b, c, e = {};
                e[w.itemName] = c = x.matches[a].model, b = w.modelMapper(i, e), v(i, b), l.$setValidity("editable", !0), 
                r(i, {
                    $item: c,
                    $model: b,
                    $label: w.viewMapper(i, e)
                }), A(), d(function() {
                    j[0].focus();
                }, 0, !1);
            }, j.bind("keydown", function(a) {
                0 !== x.matches.length && -1 !== h.indexOf(a.which) && (-1 != x.activeIdx || 13 !== a.which && 9 !== a.which) && (a.preventDefault(), 
                40 === a.which ? (x.activeIdx = (x.activeIdx + 1) % x.matches.length, x.$digest()) : 38 === a.which ? (x.activeIdx = (x.activeIdx > 0 ? x.activeIdx : x.matches.length) - 1, 
                x.$digest()) : 13 === a.which || 9 === a.which ? x.$apply(function() {
                    x.select(x.activeIdx);
                }) : 27 === a.which && (a.stopPropagation(), A(), x.$digest()));
            }), j.bind("blur", function() {
                m = !1;
            });
            var G = function(a) {
                j[0] !== a.target && (A(), x.$digest());
            };
            e.bind("click", G), i.$on("$destroy", function() {
                e.unbind("click", G), t && H.remove();
            });
            var H = a(z)(x);
            t ? e.find("body").append(H) : j.after(H);
        }
    };
} ]).directive("typeaheadPopup", function() {
    return {
        restrict: "EA",
        scope: {
            matches: "=",
            query: "=",
            active: "=",
            position: "=",
            select: "&"
        },
        replace: !0,
        templateUrl: "template/typeahead/typeahead-popup.html",
        link: function(a, b, c) {
            a.templateUrl = c.templateUrl, a.isOpen = function() {
                return a.matches.length > 0;
            }, a.isActive = function(b) {
                return a.active == b;
            }, a.selectActive = function(b) {
                a.active = b;
            }, a.selectMatch = function(b) {
                a.select({
                    activeIdx: b
                });
            };
        }
    };
}).directive("typeaheadMatch", [ "$http", "$templateCache", "$compile", "$parse", function(a, b, c, d) {
    return {
        restrict: "EA",
        scope: {
            index: "=",
            match: "=",
            query: "="
        },
        link: function(e, f, g) {
            var h = d(g.templateUrl)(e.$parent) || "template/typeahead/typeahead-match.html";
            a.get(h, {
                cache: b
            }).success(function(a) {
                f.replaceWith(c(a.trim())(e));
            });
        }
    };
} ]).filter("typeaheadHighlight", function() {
    function a(a) {
        return a.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    }
    return function(b, c) {
        return c ? ("" + b).replace(new RegExp(a(c), "gi"), "<strong>$&</strong>") : b;
    };
}), angular.module("template/accordion/accordion-group.html", []).run([ "$templateCache", function(a) {
    a.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a href class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>\n');
} ]), angular.module("template/accordion/accordion.html", []).run([ "$templateCache", function(a) {
    a.put("template/accordion/accordion.html", '<div class="panel-group" ng-transclude></div>');
} ]), angular.module("template/alert/alert.html", []).run([ "$templateCache", function(a) {
    a.put("template/alert/alert.html", '<div class="alert" ng-class="[\'alert-\' + (type || \'warning\'), closeable ? \'alert-dismissable\' : null]" role="alert">\n    <button ng-show="closeable" type="button" class="close" ng-click="close()">\n        <span aria-hidden="true">&times;</span>\n        <span class="sr-only">Close</span>\n    </button>\n    <div ng-transclude></div>\n</div>\n');
} ]), angular.module("template/carousel/carousel.html", []).run([ "$templateCache", function(a) {
    a.put("template/carousel/carousel.html", '<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel" ng-swipe-right="prev()" ng-swipe-left="next()">\n    <ol class="carousel-indicators" ng-show="slides.length > 1">\n        <li ng-repeat="slide in slides track by $index" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-left"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-right"></span></a>\n</div>\n');
} ]), angular.module("template/carousel/slide.html", []).run([ "$templateCache", function(a) {
    a.put("template/carousel/slide.html", "<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item text-center\" ng-transclude></div>\n");
} ]), angular.module("template/datepicker/datepicker.html", []).run([ "$templateCache", function(a) {
    a.put("template/datepicker/datepicker.html", '<div ng-switch="datepickerMode" role="application" ng-keydown="keydown($event)">\n  <daypicker ng-switch-when="day" tabindex="0"></daypicker>\n  <monthpicker ng-switch-when="month" tabindex="0"></monthpicker>\n  <yearpicker ng-switch-when="year" tabindex="0"></yearpicker>\n</div>');
} ]), angular.module("template/datepicker/day.html", []).run([ "$templateCache", function(a) {
    a.put("template/datepicker/day.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n');
} ]), angular.module("template/datepicker/month.html", []).run([ "$templateCache", function(a) {
    a.put("template/datepicker/month.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n');
} ]), angular.module("template/datepicker/popup.html", []).run([ "$templateCache", function(a) {
    a.put("template/datepicker/popup.html", '<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" ng-keydown="keydown($event)">\n	<li ng-transclude></li>\n	<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group pull-left">\n			<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n	</li>\n</ul>\n');
} ]), angular.module("template/datepicker/year.html", []).run([ "$templateCache", function(a) {
    a.put("template/datepicker/year.html", '<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n');
} ]), angular.module("template/modal/backdrop.html", []).run([ "$templateCache", function(a) {
    a.put("template/modal/backdrop.html", '<div class="modal-backdrop fade {{ backdropClass }}"\n     ng-class="{in: animate}"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n');
} ]), angular.module("template/modal/window.html", []).run([ "$templateCache", function(a) {
    a.put("template/modal/window.html", '<div tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog" ng-class="{\'modal-sm\': size == \'sm\', \'modal-lg\': size == \'lg\'}"><div class="modal-content" modal-transclude></div></div>\n</div>');
} ]), angular.module("template/pagination/pager.html", []).run([ "$templateCache", function(a) {
    a.put("template/pagination/pager.html", '<ul class="pager">\n  <li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n</ul>');
} ]), angular.module("template/pagination/pagination.html", []).run([ "$templateCache", function(a) {
    a.put("template/pagination/pagination.html", '<ul class="pagination">\n  <li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1)">{{getText(\'first\')}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n  <li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages)">{{getText(\'last\')}}</a></li>\n</ul>');
} ]), angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run([ "$templateCache", function(a) {
    a.put("template/tooltip/tooltip-html-unsafe-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n');
} ]), angular.module("template/tooltip/tooltip-popup.html", []).run([ "$templateCache", function(a) {
    a.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n');
} ]), angular.module("template/popover/popover.html", []).run([ "$templateCache", function(a) {
    a.put("template/popover/popover.html", '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n');
} ]), angular.module("template/progressbar/bar.html", []).run([ "$templateCache", function(a) {
    a.put("template/progressbar/bar.html", '<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>');
} ]), angular.module("template/progressbar/progress.html", []).run([ "$templateCache", function(a) {
    a.put("template/progressbar/progress.html", '<div class="progress" ng-transclude></div>');
} ]), angular.module("template/progressbar/progressbar.html", []).run([ "$templateCache", function(a) {
    a.put("template/progressbar/progressbar.html", '<div class="progress">\n  <div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n</div>');
} ]), angular.module("template/rating/rating.html", []).run([ "$templateCache", function(a) {
    a.put("template/rating/rating.html", '<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="{{range.length}}" aria-valuenow="{{value}}">\n    <i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < value && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')">\n        <span class="sr-only">({{ $index < value ? \'*\' : \' \' }})</span>\n    </i>\n</span>');
} ]), angular.module("template/tabs/tab.html", []).run([ "$templateCache", function(a) {
    a.put("template/tabs/tab.html", '<li ng-class="{active: active, disabled: disabled}">\n  <a href ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n');
} ]), angular.module("template/tabs/tabset.html", []).run([ "$templateCache", function(a) {
    a.put("template/tabs/tabset.html", '<div>\n  <ul class="nav nav-{{type || \'tabs\'}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n');
} ]), angular.module("template/timepicker/timepicker.html", []).run([ "$templateCache", function(a) {
    a.put("template/timepicker/timepicker.html", '<table>\n	<tbody>\n		<tr class="text-center">\n			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n		<tr>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n				<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td>:</td>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n				<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n		</tr>\n		<tr class="text-center">\n			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n	</tbody>\n</table>\n');
} ]), angular.module("template/typeahead/typeahead-match.html", []).run([ "$templateCache", function(a) {
    a.put("template/typeahead/typeahead-match.html", '<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>');
} ]), angular.module("template/typeahead/typeahead-popup.html", []).run([ "$templateCache", function(a) {
    a.put("template/typeahead/typeahead-popup.html", '<ul class="dropdown-menu" ng-show="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">\n    <li ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n');
} ]);

var app = angular.module("App", [ "ngRoute", "ngCordova", "ngResource", "ngStorage", "ui.bootstrap", "ngAnimate", "angucomplete" ]);

var appResolves = angular.module("app.controllers", []);

app.config([ "$routeProvider", function($routeProvider) {
    $routeProvider.when("/", {
        controller: "CommonCtrl",
        templateUrl: "partials/common.html",
        resolve: {
            CurrentLocation: appResolves.CommonResolve.getCurrentPosition
        }
    });
    $routeProvider.otherwise({
        redirectTo: "/"
    });
} ]);

app.config([ "$provide", "$httpProvider", function($provide, $httpProvider) {
    $provide.factory("httpInterceptor", [ "$q", function($q) {
        return {
            response: function(response) {
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                return $q.reject(rejection);
            },
            request: function(request) {
                return request || $q.when(request);
            }
        };
    } ]);
    $httpProvider.interceptors.push("httpInterceptor");
} ]);

app.run(function($rootScope) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {});
});

var URI_PATH = {
    getWeather: "/getweather",
    API_HOST: "https://fathomless-anchorage-91805.herokuapp.com/api"
};

var COUNTRIES = [ {
    name: "Afghanistan",
    code: "AF"
}, {
    name: "Albania",
    code: "AL"
}, {
    name: "Algeria",
    code: "DZ"
}, {
    name: "American Samoa",
    code: "AS"
}, {
    name: "AndorrA",
    code: "AD"
}, {
    name: "Angola",
    code: "AO"
}, {
    name: "Anguilla",
    code: "AI"
}, {
    name: "Antigua and Barbuda",
    code: "AG"
}, {
    name: "Argentina",
    code: "AR"
}, {
    name: "Armenia",
    code: "AM"
}, {
    name: "Aruba",
    code: "AW"
}, {
    name: "Australia",
    code: "AU"
}, {
    name: "Austria",
    code: "AT"
}, {
    name: "Azerbaijan",
    code: "AZ"
}, {
    name: "Bahamas",
    code: "BS"
}, {
    name: "Bahrain",
    code: "BH"
}, {
    name: "Bangladesh",
    code: "BD"
}, {
    name: "Barbados",
    code: "BB"
}, {
    name: "Belarus",
    code: "BY"
}, {
    name: "Belgium",
    code: "BE"
}, {
    name: "Belize",
    code: "BZ"
}, {
    name: "Benin",
    code: "BJ"
}, {
    name: "Bermuda",
    code: "BM"
}, {
    name: "Bhutan",
    code: "BT"
}, {
    name: "Bolivia",
    code: "BO"
}, {
    name: "Bosnia and Herzegovina",
    code: "BA"
}, {
    name: "Botswana",
    code: "BW"
}, {
    name: "Bouvet Island",
    code: "BV"
}, {
    name: "Brazil",
    code: "BR"
}, {
    name: "British Indian Ocean Territory",
    code: "IO"
}, {
    name: "Brunei Darussalam",
    code: "BN"
}, {
    name: "Bulgaria",
    code: "BG"
}, {
    name: "Burkina Faso",
    code: "BF"
}, {
    name: "Burundi",
    code: "BI"
}, {
    name: "Cambodia",
    code: "KH"
}, {
    name: "Cameroon",
    code: "CM"
}, {
    name: "Canada",
    code: "CA"
}, {
    name: "Cape Verde",
    code: "CV"
}, {
    name: "Cayman Islands",
    code: "KY"
}, {
    name: "Central African Republic",
    code: "CF"
}, {
    name: "Chad",
    code: "TD"
}, {
    name: "Chile",
    code: "CL"
}, {
    name: "China",
    code: "CN"
}, {
    name: "Christmas Island",
    code: "CX"
}, {
    name: "Cocos (Keeling) Islands",
    code: "CC"
}, {
    name: "Colombia",
    code: "CO"
}, {
    name: "Comoros",
    code: "KM"
}, {
    name: "Congo",
    code: "CG"
}, {
    name: "Congo, The Democratic Republic of the",
    code: "CD"
}, {
    name: "Cook Islands",
    code: "CK"
}, {
    name: "Costa Rica",
    code: "CR"
}, {
    name: 'Cote D"Ivoire',
    code: "CI"
}, {
    name: "Croatia",
    code: "HR"
}, {
    name: "Cuba",
    code: "CU"
}, {
    name: "Cyprus",
    code: "CY"
}, {
    name: "Czech Republic",
    code: "CZ"
}, {
    name: "Denmark",
    code: "DK"
}, {
    name: "Djibouti",
    code: "DJ"
}, {
    name: "Dominica",
    code: "DM"
}, {
    name: "Dominican Republic",
    code: "DO"
}, {
    name: "Ecuador",
    code: "EC"
}, {
    name: "Egypt",
    code: "EG"
}, {
    name: "El Salvador",
    code: "SV"
}, {
    name: "Equatorial Guinea",
    code: "GQ"
}, {
    name: "Eritrea",
    code: "ER"
}, {
    name: "Estonia",
    code: "EE"
}, {
    name: "Ethiopia",
    code: "ET"
}, {
    name: "Falkland Islands (Malvinas)",
    code: "FK"
}, {
    name: "Faroe Islands",
    code: "FO"
}, {
    name: "Fiji",
    code: "FJ"
}, {
    name: "Finland",
    code: "FI"
}, {
    name: "France",
    code: "FR"
}, {
    name: "French Guiana",
    code: "GF"
}, {
    name: "French Polynesia",
    code: "PF"
}, {
    name: "French Southern Territories",
    code: "TF"
}, {
    name: "Gabon",
    code: "GA"
}, {
    name: "Gambia",
    code: "GM"
}, {
    name: "Georgia",
    code: "GE"
}, {
    name: "Germany",
    code: "DE"
}, {
    name: "Ghana",
    code: "GH"
}, {
    name: "Gibraltar",
    code: "GI"
}, {
    name: "Greece",
    code: "GR"
}, {
    name: "Greenland",
    code: "GL"
}, {
    name: "Grenada",
    code: "GD"
}, {
    name: "Guadeloupe",
    code: "GP"
}, {
    name: "Guam",
    code: "GU"
}, {
    name: "Guatemala",
    code: "GT"
}, {
    name: "Guernsey",
    code: "GG"
}, {
    name: "Guinea",
    code: "GN"
}, {
    name: "Guinea-Bissau",
    code: "GW"
}, {
    name: "Guyana",
    code: "GY"
}, {
    name: "Haiti",
    code: "HT"
}, {
    name: "Heard Island and Mcdonald Islands",
    code: "HM"
}, {
    name: "Holy See (Vatican City State)",
    code: "VA"
}, {
    name: "Honduras",
    code: "HN"
}, {
    name: "Hong Kong",
    code: "HK"
}, {
    name: "Hungary",
    code: "HU"
}, {
    name: "Iceland",
    code: "IS"
}, {
    name: "India",
    code: "IN"
}, {
    name: "Indonesia",
    code: "ID"
}, {
    name: "Iran, Islamic Republic Of",
    code: "IR"
}, {
    name: "Iraq",
    code: "IQ"
}, {
    name: "Ireland",
    code: "IE"
}, {
    name: "Isle of Man",
    code: "IM"
}, {
    name: "Israel",
    code: "IL"
}, {
    name: "Italy",
    code: "IT"
}, {
    name: "Jamaica",
    code: "JM"
}, {
    name: "Japan",
    code: "JP"
}, {
    name: "Jersey",
    code: "JE"
}, {
    name: "Jordan",
    code: "JO"
}, {
    name: "Kazakhstan",
    code: "KZ"
}, {
    name: "Kenya",
    code: "KE"
}, {
    name: "Kiribati",
    code: "KI"
}, {
    name: 'Korea, Democratic People"S Republic of',
    code: "KP"
}, {
    name: "Korea, Republic of",
    code: "KR"
}, {
    name: "Kuwait",
    code: "KW"
}, {
    name: "Kyrgyzstan",
    code: "KG"
}, {
    name: 'Lao People"S Democratic Republic',
    code: "LA"
}, {
    name: "Latvia",
    code: "LV"
}, {
    name: "Lebanon",
    code: "LB"
}, {
    name: "Lesotho",
    code: "LS"
}, {
    name: "Liberia",
    code: "LR"
}, {
    name: "Libyan Arab Jamahiriya",
    code: "LY"
}, {
    name: "Liechtenstein",
    code: "LI"
}, {
    name: "Lithuania",
    code: "LT"
}, {
    name: "Luxembourg",
    code: "LU"
}, {
    name: "Macao",
    code: "MO"
}, {
    name: "Macedonia, The Former Yugoslav Republic of",
    code: "MK"
}, {
    name: "Madagascar",
    code: "MG"
}, {
    name: "Malawi",
    code: "MW"
}, {
    name: "Malaysia",
    code: "MY"
}, {
    name: "Maldives",
    code: "MV"
}, {
    name: "Mali",
    code: "ML"
}, {
    name: "Malta",
    code: "MT"
}, {
    name: "Marshall Islands",
    code: "MH"
}, {
    name: "Martinique",
    code: "MQ"
}, {
    name: "Mauritania",
    code: "MR"
}, {
    name: "Mauritius",
    code: "MU"
}, {
    name: "Mayotte",
    code: "YT"
}, {
    name: "Mexico",
    code: "MX"
}, {
    name: "Micronesia, Federated States of",
    code: "FM"
}, {
    name: "Moldova, Republic of",
    code: "MD"
}, {
    name: "Monaco",
    code: "MC"
}, {
    name: "Mongolia",
    code: "MN"
}, {
    name: "Montserrat",
    code: "MS"
}, {
    name: "Morocco",
    code: "MA"
}, {
    name: "Mozambique",
    code: "MZ"
}, {
    name: "Myanmar",
    code: "MM"
}, {
    name: "Namibia",
    code: "NA"
}, {
    name: "Nauru",
    code: "NR"
}, {
    name: "Nepal",
    code: "NP"
}, {
    name: "Netherlands",
    code: "NL"
}, {
    name: "Netherlands Antilles",
    code: "AN"
}, {
    name: "New Caledonia",
    code: "NC"
}, {
    name: "New Zealand",
    code: "NZ"
}, {
    name: "Nicaragua",
    code: "NI"
}, {
    name: "Niger",
    code: "NE"
}, {
    name: "Nigeria",
    code: "NG"
}, {
    name: "Niue",
    code: "NU"
}, {
    name: "Norfolk Island",
    code: "NF"
}, {
    name: "Northern Mariana Islands",
    code: "MP"
}, {
    name: "Norway",
    code: "NO"
}, {
    name: "Oman",
    code: "OM"
}, {
    name: "Pakistan",
    code: "PK"
}, {
    name: "Palau",
    code: "PW"
}, {
    name: "Palestinian Territory, Occupied",
    code: "PS"
}, {
    name: "Panama",
    code: "PA"
}, {
    name: "Papua New Guinea",
    code: "PG"
}, {
    name: "Paraguay",
    code: "PY"
}, {
    name: "Peru",
    code: "PE"
}, {
    name: "Philippines",
    code: "PH"
}, {
    name: "Pitcairn",
    code: "PN"
}, {
    name: "Poland",
    code: "PL"
}, {
    name: "Portugal",
    code: "PT"
}, {
    name: "Puerto Rico",
    code: "PR"
}, {
    name: "Qatar",
    code: "QA"
}, {
    name: "Reunion",
    code: "RE"
}, {
    name: "Romania",
    code: "RO"
}, {
    name: "Russian Federation",
    code: "RU"
}, {
    name: "RWANDA",
    code: "RW"
}, {
    name: "Saint Helena",
    code: "SH"
}, {
    name: "Saint Kitts and Nevis",
    code: "KN"
}, {
    name: "Saint Lucia",
    code: "LC"
}, {
    name: "Saint Pierre and Miquelon",
    code: "PM"
}, {
    name: "Saint Vincent and the Grenadines",
    code: "VC"
}, {
    name: "Samoa",
    code: "WS"
}, {
    name: "San Marino",
    code: "SM"
}, {
    name: "Sao Tome and Principe",
    code: "ST"
}, {
    name: "Saudi Arabia",
    code: "SA"
}, {
    name: "Senegal",
    code: "SN"
}, {
    name: "Serbia and Montenegro",
    code: "CS"
}, {
    name: "Seychelles",
    code: "SC"
}, {
    name: "Sierra Leone",
    code: "SL"
}, {
    name: "Singapore",
    code: "SG"
}, {
    name: "Slovakia",
    code: "SK"
}, {
    name: "Slovenia",
    code: "SI"
}, {
    name: "Solomon Islands",
    code: "SB"
}, {
    name: "Somalia",
    code: "SO"
}, {
    name: "South Africa",
    code: "ZA"
}, {
    name: "South Georgia and the South Sandwich Islands",
    code: "GS"
}, {
    name: "Spain",
    code: "ES"
}, {
    name: "Sri Lanka",
    code: "LK"
}, {
    name: "Sudan",
    code: "SD"
}, {
    name: "Suri",
    code: "SR"
}, {
    name: "Svalbard and Jan Mayen",
    code: "SJ"
}, {
    name: "Swaziland",
    code: "SZ"
}, {
    name: "Sweden",
    code: "SE"
}, {
    name: "Switzerland",
    code: "CH"
}, {
    name: "Syrian Arab Republic",
    code: "SY"
}, {
    name: "Taiwan, Province of China",
    code: "TW"
}, {
    name: "Tajikistan",
    code: "TJ"
}, {
    name: "Tanzania, United Republic of",
    code: "TZ"
}, {
    name: "Thailand",
    code: "TH"
}, {
    name: "Timor-Leste",
    code: "TL"
}, {
    name: "Togo",
    code: "TG"
}, {
    name: "Tokelau",
    code: "TK"
}, {
    name: "Tonga",
    code: "TO"
}, {
    name: "Trinidad and Tobago",
    code: "TT"
}, {
    name: "Tunisia",
    code: "TN"
}, {
    name: "Turkey",
    code: "TR"
}, {
    name: "Turkmenistan",
    code: "TM"
}, {
    name: "Turks and Caicos Islands",
    code: "TC"
}, {
    name: "Tuvalu",
    code: "TV"
}, {
    name: "Uganda",
    code: "UG"
}, {
    name: "Ukraine",
    code: "UA"
}, {
    name: "United Arab Emirates",
    code: "AE"
}, {
    name: "United Kingdom",
    code: "GB"
}, {
    name: "United States",
    code: "US"
}, {
    name: "United States Minor Outlying Islands",
    code: "UM"
}, {
    name: "Uruguay",
    code: "UY"
}, {
    name: "Uzbekistan",
    code: "UZ"
}, {
    name: "Vanuatu",
    code: "VU"
}, {
    name: "Venezuela",
    code: "VE"
}, {
    name: "Viet Nam",
    code: "VN"
}, {
    name: "Virgin Islands, British",
    code: "VG"
}, {
    name: "Virgin Islands, U.S.",
    code: "VI"
}, {
    name: "Wallis and Futuna",
    code: "WF"
}, {
    name: "Western Sahara",
    code: "EH"
}, {
    name: "Yemen",
    code: "YE"
}, {
    name: "Zambia",
    code: "ZM"
}, {
    name: "Zimbabwe",
    code: "ZW"
} ];

angular.module("app.constants", []).constant("COUNTRIES", COUNTRIES);

var phone = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener("deviceready", this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        phone.receivedEvent("deviceready");
        phone.checkConnection();
    },
    receivedEvent: function(id) {
        var body = document.getElementsByTagName("body")[0];
        body.style.minWidth = window.innerWidth;
        body.style.minHeight = window.innerHeight;
        document.addEventListener("backbutton", function(e) {
            e.preventDefault();
        }, false);
    },
    checkConnection: function() {
        var networkState = navigator.connection.type;
        if (networkState == Connection.NONE) {
            window.location.href = "error.html";
        }
    }
};

app.controller("CommonCtrl", [ "$scope", "$location", "$rootScope", "$localStorage", "$http", "CurrentLocation", "CommonService", function($scope, $location, $rootScope, $localStorage, $http, CurrentLocation, CommonService) {
    $scope.getWeather = function(pos) {
        if (pos != null) {
            CommonService.getWeatherNow(pos.latitude, pos.longitude).then(function(response) {
                $scope.weather = response;
            }, function(error) {
                console.log("Something went wrong.");
            });
        }
    };
    $scope.codeAddress = function() {
        geocoder = new google.maps.Geocoder();
        $scope.CurrentLocation = document.getElementById("my-address_value").value;
        geocoder.geocode({
            address: $scope.CurrentLocation
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $scope.CurrentLocation = {
                    coords: {}
                };
                $scope.CurrentLocation.coords.latitude = results[0].geometry.location.lat();
                $scope.CurrentLocation.coords.longitude = results[0].geometry.location.lng();
                $scope.getWeather($scope.CurrentLocation.coords);
            } else {
                $scope.CurrentLocation = {};
            }
        });
    };
    $scope.CurrentLocation = CurrentLocation;
    if (angular.isDefined($scope.CurrentLocation.coords)) {
        $scope.getWeather($scope.CurrentLocation.coords);
    }
} ]);

app.controller("MainCtrl", [ "$scope", "$location", "$rootScope", "$localStorage", "$cordovaOauth", "$http", function($scope, $location, $rootScope, $localStorage, $cordovaOauth, $http) {
    $rootScope.countries = COUNTRIES;
} ]);

app.directive("weatherIcon", function() {
    return {
        restrict: "E",
        replace: true,
        scope: {
            cloudiness: "@"
        },
        controller: function($scope) {
            $scope.imgurl = function() {
                var baseUrl = "https://ssl.gstatic.com/onebox/weather/128/";
                if ($scope.cloudiness < 20) {
                    return baseUrl + "sunny.png";
                } else if ($scope.cloudiness < 90) {
                    return baseUrl + "partly_cloudy.png";
                } else {
                    return baseUrl + "cloudy.png";
                }
            };
        },
        template: '<div style="float:left"><img ng-src="{{ imgurl() }}"></div>'
    };
});

app.filter("temp", function($filter) {
    return function(input, precision) {
        if (!precision) {
            precision = 1;
        }
        var numberFilter = $filter("number");
        return numberFilter(input, precision) + "°C";
    };
});

appResolves.CommonResolve = {
    getCurrentPosition: function($q, CommonService) {
        var deferred = $q.defer();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                deferred.resolve(position);
            }, function(err) {
                deferred.resolve("Rejected");
            });
        } else {
            deferred.resolve("Rejected");
        }
        return deferred.promise;
    }
};

angular.module("App").factory("CommonService", [ "$location", "$resource", "$q", function($location, $resource, $q) {
    var getWeatherNow = function(lat, lon) {
        var deferred = $q.defer();
        var error = {};
        var path = URI_PATH.API_HOST + URI_PATH.getWeather + "?lat=" + lat + "&lon=" + lon;
        $resource(path).get(function(data, responseHeaders) {
            var weather = {
                temp: {}
            };
            if (data && data.main) {
                weather.temp.current = data.main.temp;
                weather.temp.min = data.main.temp_min;
                weather.temp.max = data.main.temp_max;
                weather.city = data.name;
                weather.country = data.sys.country;
                weather.clouds = data.clouds;
            }
            deferred.resolve(weather);
        }, function(httpResponse) {
            var data = httpResponse.data, status = httpResponse.status, headers = httpResponse.headers, config = httpResponse.config;
            error.message = data ? data.errorDescription : data;
            error.errorResp = data;
            deferred.reject(error);
        });
        return deferred.promise;
    };
    return {
        getWeatherNow: getWeatherNow
    };
} ]);