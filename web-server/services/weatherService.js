exports = module.exports = {};
var q = require('q');
var serverLogger = require('../error/logging').logger.weatherServer;
var http = require('http');
var config = require('../config/environment/common').config();

exports.getWeatherByLatAndLon = function(latitude, longitude) {
  serverLogger.error("Get Weather by latitude and longitude service: method called");
	var deferred = q.defer();

  var options = {
    host: 'api.openweathermap.org',
    port: 80,
    path: '/data/2.5/weather?appid=44db6a862fba0b067b1930da0d769e98&units=metric'
  };
  options.path += '&lat=' + latitude + '&lon=' + longitude;

  console.log(options.path);
  http.get(options, function(resp){
    var body = '';
    resp.on('data', function(chunk){
      //do something with chunk
      body += chunk;
    });

    resp.on('end', function() {
      // Data reception is done
      options = null;
      console.log(body);
      var parsed = JSON.parse(body);
      serverLogger.debug("Get Weather by latitude and longitude service: request returned");
      deferred.resolve(parsed);
    });
  }).on("error", function(e){
      serverLogger.error("Get Weather by latitude and longitude service: error", JSON.stringify(e));
      deferred.reject({message: "Server Error"});
  });

	return deferred.promise;
};

