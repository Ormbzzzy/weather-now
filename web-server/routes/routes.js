var config = require('../config/environment/common').config();
var weatherController = require('../controllers/weatherController');


//Routes
module.exports = function(router) {
  router.get('/', function(req, res){
    res.json({ "message": 'hooray! welcome to our api!' });
  });

  router.get('/getWeather', weatherController.getWeather);
};
