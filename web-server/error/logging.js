var winston = require('winston');
// Define levels to be like log4j in java
var customLevels = {
	levels : {
		debug : 0,
		info : 1,
		warn : 2,
		error : 3
	},
	colors : {
		debug : 'blue',
		info : 'green',
		warn : 'yellow',
		error : 'red'
	}
};

winston.addColors(customLevels.colors);
winston.loggers.add('serverLogger', {
	transports : [new (winston.transports.Console)()]
	// console : {
	// 	level : 'info',
	// 	colorize : 'true',
	// 	label : 'WeatherNodeServer',
	// 	timestamp : true
	// }
});
var serverLogger = winston.loggers.get('serverLogger');
serverLogger.setLevels(customLevels.levels);

serverLogger.info('Weather Node Server logger set up!');

exports.logger = {
	weatherServer : serverLogger
};
