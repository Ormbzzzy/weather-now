var config = module.exports;
var path       = require('path');
config.port = process.env.PORT || 3000;

config.path = path.normalize(path.join(__dirname, '..'));

config.weatherApiOptions = {
  host: 'api.openweathermap.org',
  port: 80,
  path: '/data/2.5/weather?appid=44db6a862fba0b067b1930da0d769e98'
};
