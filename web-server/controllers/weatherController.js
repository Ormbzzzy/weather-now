exports = module.exports = {};
var q = require('q');
var serverLogger = require('../error/logging').logger.weatherServer;
var config = require('../config/environment/common').config();
var weatherService = require('../services/weatherService.js');


exports.getWeather = function(req, res, next) {
 console.log(req.query);
  if (req.query && req.query.lat && req.query.lon) {
    weatherService.getWeatherByLatAndLon(req.query.lat, req.query.lon).then(function(resp){
      return res.send(resp);
    }, function(err){
      return res.status(500).send(err);
    });
  }
  else {
    serverLogger.error("Get Weather: error", "Query params not present");
    res.status(400).send({message: "Bad Request"});
  }
};

