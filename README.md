# Weather Now App #
================

Development Dependencies

* Install Node https://nodejs.org/en/
* Install PhoneGap http://phonegap.com/install/
* Install node development dependencies: npm install *sudo or running as administrator may be required
* Install Ruby http://rubyinstaller.org/ . This is for Windows users. Ruby comes with Mac OS X


Build Tools
-----------

To setup **Build Tools**:
* Execute `gem update --system && gem install compass`
* Open a shell in this directory and execute `npm install` to install dependencies.
* Thats All!

Disclaimer: Will take a while to install them all, but its worth it, makes life easier and faster !

###It will create a heavy `node_modules` folder, this folder should be in .gitignore file

###Run the project

* run the command `grunt build` to package the application into a phonegap application
* run the command `phonegap serve` to start the phonegap server
* `node scripts/server.js` to run the application in a browser

###Run the project on a device

* `phonegap build android` to build the android app
* `phonegap run android` to run the android app

###Build Commands:

* `grunt` run all unit tests
* `grunt build` compile scss,javascript and html partials to the www folder. To test simply run emulator OR start the node server in www/ and view in browser.
* `grunt test` watch all javascript files for changes then run all unit tests. THIS IS NOT UP RIGHT NOW.

###Rebuilding package and deployment steps

* `git push heroku master` to deploy to heroku