/*
 * Taken from http://jsfiddle.net/derkoe/yc4yG/presentation/
 */

app.filter('temp', function($filter) {
    return function(input, precision) {
        if (!precision) {
            precision = 1;
        }
        var numberFilter = $filter('number');
        return numberFilter(input, precision) + '\u00B0C';
    };
});