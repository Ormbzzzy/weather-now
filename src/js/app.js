var app = angular.module('App',['ngRoute','ngCordova','ngResource', 'ngStorage', 'ui.bootstrap', 'ngAnimate', 'angucomplete']);

var appResolves = angular.module('app.controllers', []);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
      controller : 'CommonCtrl',
      templateUrl :  'partials/common.html',
      resolve: {
        CurrentLocation: appResolves.CommonResolve.getCurrentPosition
      }
    });

    $routeProvider.
      otherwise({
        redirectTo: '/'
      });
  }]);


/* httpInterceptor is used to examine each http request
 * and determine if a request to the backend has failed
 * by checking relevant the status code where necessary
 */
app.config(['$provide','$httpProvider', function($provide, $httpProvider) {

  $provide.factory('httpInterceptor', ['$q', function($q){
    return {
      'response': function(response) {
        return response || $q.when(response);
      },
      'responseError': function(rejection) {
        return $q.reject(rejection);
      },
      'request': function(request) {
        return request || $q.when(request);
      }
    };
  }]);

  $httpProvider.interceptors.push('httpInterceptor');
}]);

app.run(function($rootScope) {

  // listen to $routeChangeStart and execute this whenevever it is invoked
  $rootScope.$on("$routeChangeStart", function (event, next, current) {
      
  });
});