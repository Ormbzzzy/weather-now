appResolves.CommonResolve = {
    getCurrentPosition: function($q, CommonService) {

        var deferred = $q.defer();
        
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                deferred.resolve(position);
            }, function(err){
                deferred.resolve("Rejected");
            });
        }
        else {
            deferred.resolve("Rejected");
        }
       
        return deferred.promise;

    }

};