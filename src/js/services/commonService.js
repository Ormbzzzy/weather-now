angular.module('App').factory('CommonService',['$location','$resource', '$q', function($location, $resource, $q){
  	var getWeatherNow = function(lat, lon) {
  		var deferred = $q.defer();
		var error = {};


		var path = URI_PATH.API_HOST + URI_PATH.getWeather + "?lat=" + lat + "&lon=" + lon;

		$resource(path).get(function(data, responseHeaders) {

			var weather = {
				temp: {}
			};

			if(data && data.main){
				weather.temp.current = data.main.temp;
                weather.temp.min = data.main.temp_min;
                weather.temp.max = data.main.temp_max;
                weather.city = data.name;
                weather.country = data.sys.country;
                weather.clouds = data.clouds;
			}

			deferred.resolve(weather);
		}, function(httpResponse) {
			var data = httpResponse.data, status = httpResponse.status, headers = httpResponse.headers, config = httpResponse.config;

			error.message = ((data) ? data.errorDescription : data);
			error.errorResp = data;
			deferred.reject(error);
		});

		return deferred.promise;
  	};

	return {
		getWeatherNow: getWeatherNow
    };

}]);