/*
 * @description Parent Controller of the App in which all views will inherit from
 */

app.controller('MainCtrl',['$scope', '$location', '$rootScope', '$localStorage', '$cordovaOauth', '$http', 
	function($scope, $location, $rootScope, $localStorage, $cordovaOauth, $http){

	$rootScope.countries = COUNTRIES;
}]);
