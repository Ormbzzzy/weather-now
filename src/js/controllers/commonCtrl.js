/*
 * @description Controller for the default view
 */

app.controller('CommonCtrl',['$scope', '$location', '$rootScope', '$localStorage', '$http', 'CurrentLocation',
	'CommonService', function($scope, $location, $rootScope, $localStorage, $http, CurrentLocation, CommonService){

	// retrieve the weather for the position passed in or for the current position
	$scope.getWeather = function(pos) {
		if(pos != null){
			CommonService.getWeatherNow(pos.latitude, pos.longitude).then(function(response)
		    {
		        $scope.weather = response;
		    }, function(error)
		    {
		        console.log('Something went wrong.');
		    });
		}
	}


	// gets the coords of the entered value of the input box
	// taken from http://stackoverflow.com/questions/3490622/get-latitude-and-longitude-based-on-location-name-with-google-autocomplete-api
	$scope.codeAddress = function() {
	    geocoder = new google.maps.Geocoder();
	    $scope.CurrentLocation = document.getElementById("my-address_value").value;
	    geocoder.geocode( { 'address': $scope.CurrentLocation}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {

		      $scope.CurrentLocation = {
		      	coords: {}
		      };
		      $scope.CurrentLocation.coords.latitude = results[0].geometry.location.lat();
		      $scope.CurrentLocation.coords.longitude = results[0].geometry.location.lng();
		      $scope.getWeather($scope.CurrentLocation.coords);
	      } 

	      else {
	        $scope.CurrentLocation = {
		      };
	      }
	    });
	}

	$scope.CurrentLocation = CurrentLocation;

	if(angular.isDefined($scope.CurrentLocation.coords)){
		// get the weather for the current coords
		$scope.getWeather($scope.CurrentLocation.coords);
	}
	
}]);
